//
//  MainScreenViewController.swift
//  JEDE_r
//
//  Created by Exarcplus iOS 2 on 02/05/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import SDWebImage
import CoreLocation
import AFNetworking
import LGSideMenuController
import Crashlytics
import Firebase
//import  PINRemoteImage

class MainScreenViewController: UIViewController,CLLocationManagerDelegate,GMSMapViewDelegate, ABCGooglePlacesSearchViewControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate,UIAlertViewDelegate {
    var bookingid : String!
    var myclass : MyClass!
    
    var dummystr : String!
    var SideMenuView:SidemenuViewController!
     var SideMenu:LGSideMenuController!
    @IBOutlet weak var viewmap: GMSMapView!
    var firstload = Bool()
    var firstTimeLoads = Bool()
    var locmanager:CLLocationManager!
    var currentlocation = CLLocation()
    var movelocation = CLLocation()
    var searchlocation = CLLocation()
    var currentadddress:GMSAddress!
    @IBOutlet weak var mycenterimg: UIImageView!
    @IBOutlet weak var mytopimg: UIImageView!
    
    @IBOutlet weak var Listview: UIView!
    @IBOutlet weak var Menulist: UICollectionView!
    @IBOutlet weak var Menulistscroll: UIScrollView!
    var Mselectedindex : Int!
    var menuListarr: NSMutableArray!
    @IBOutlet weak var table_All: UITableView!
    @IBOutlet weak var table_Offerers: UITableView!
    @IBOutlet weak var table_Public: UITableView!
    var array_All: NSMutableArray!
    var array_Offerers: NSMutableArray!
    var array_Public: NSMutableArray!
    @IBOutlet weak var empty_All: UILabel!
    @IBOutlet weak var empty_Offerers: UILabel!
    @IBOutlet weak var empty_Public: UILabel!
    var Markerarray: NSMutableArray!
    var selectedIndex:NSInteger?
    
    @IBOutlet weak var bottomlistmapimg : UIImageView!
    @IBOutlet weak var bottomlocsortimg : UIImageView!
    @IBOutlet weak var filterbgbutton : UIButton?
    @IBOutlet weak var searchview : UIView!
    @IBOutlet weak var searchthislocview : UIView!
    var beforemove = Bool()
    @IBOutlet weak var locationtext : UITextField!
    var mark : NSArray!
    
    var getdic : NSMutableDictionary!

    var passtype : String!
    var passloc : String!
    var passrating : String!
    var passdistance : String!
    var passprice : String!
    var passcategory : String!
    var passfacility : String!
    var passfacilitycat : String!
    var passaccessibility : String!
    var sarchstr : String!
    var passUserId: String!

    //sort
    var pickerarr = NSMutableArray()
    var sortPopup : SortPopup!
    var sortrowindex : Int!
    var passsort : String!
    
    var fromfilter : NSMutableDictionary!
    var once: Int = 0
    var searchstr = ""
    
    var searchaddress = ""
    
    var mapInfoWindow:CustomInfoWindow!
    
    var isListView = Bool()
    let serverResponseForAlreadyBooked = "already Booked"
    let facilityCategoryIdMale = "1"
    let facilityCategoryIdFeMale = "2"
    let facilityCategoryIdUnisex = "3"
    let alreadyBookedAlertIdentifier = "alreadyBookedAlertIdentifier"
    let freeProviderToilet = "1"
    

    var userId = ""
    var userName = ""
    var userlat = ""
    var userlong = ""
    var userAdd = ""
    var toiletCount = ""
    var userTime = ""
    var userDevice = ""
    var Userdic = NSMutableDictionary()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        myclass = MyClass()
        getdic = NSMutableDictionary()
        menuListarr = NSMutableArray()
        array_All = NSMutableArray()
        array_Offerers = NSMutableArray()
        array_Public  = NSMutableArray()
        Markerarray = NSMutableArray()
        Listview.isHidden = true;
        isListView = false
        viewmap.clear()
       
        passUserId = self.getdic.value(forKey: "user_id") as? String
        if !isListView
        {
            let view = UIView(frame:  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 20))
            view.backgroundColor = UIColor.init(red: 27.0/255.0, green: 34.0/255.0, blue: 40.0/255.0, alpha: 1.0)
            self.view.addSubview(view)
            self.navigationController?.navigationBar.isHidden = true
        }
        else
        {
            self.navigationItem.title = "list_view".localized
            self.navigationController?.navigationBar.isHidden = false
            let view = UIView(frame:  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 20))
            view.backgroundColor = UIColor.init(red: 27.0/255.0, green: 34.0/255.0, blue: 40.0/255.0, alpha: 1.0)
            self.navigationController?.view.addSubview(view)
        }
        
        sortrowindex = 0
        
        if UserDefaults.standard.object(forKey: "Logindetail") == nil
        {
            passUserId = ""
        }
        else
        {
            let placesData = UserDefaults.standard.object(forKey: "Logindetail") as? NSDictionary
            self.getdic.removeAllObjects()
            if placesData != nil{
                self.getdic.addEntries(from: (placesData as? [String : Any])!)
            }
            passUserId = self.getdic.value(forKey: "user_id") as? String
        }
        
        self.parseFilterDataForDisplay()
        
        Mselectedindex = 0;
        self.navigationController?.navigationBar.isHidden = true
        let view = UIView(frame:  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 20))
        view.backgroundColor = UIColor.init(red: 27.0/255.0, green: 34.0/255.0, blue: 40.0/255.0, alpha: 1.0)
        self.view.addSubview(view)
        
        self.navigationItem.title = "Map View"
        
        bottomlistmapimg.image = UIImage.init(named: "List-1.png")
        bottomlocsortimg.image = UIImage.init(named: "CU.png")
        
        
        searchview.layer.masksToBounds =  false
        searchview.layer.shadowColor = UIColor.black.cgColor;
        searchview.layer.shadowOffset = CGSize(width: 1.5, height: 1.5)
        searchview.layer.shadowOpacity = 0.5
        searchview.isHidden = true
        
        let dispatchTime1: DispatchTime = DispatchTime.now() + Double(Int64(0.8 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime1, execute: {
            
            UIView.animate(withDuration: 0.5, delay: 0.5, options: [.repeat, .curveEaseOut, .autoreverse], animations: {
                
                let transition = CATransition()
                transition.duration = 0.5
                transition.type = kCATransitionPush
                transition.subtype = kCATransitionFromBottom
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                self.searchview.layer.add(transition, forKey: nil)
                self.searchview.isHidden = false
            }, completion: nil)
            
        })
        
        locmanager = CLLocationManager()

        locmanager.delegate = self
        locmanager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locmanager.activityType = .automotiveNavigation
        locmanager.requestAlwaysAuthorization()
        viewmap.settings.myLocationButton=false
        viewmap.isMyLocationEnabled = true
        viewmap.settings.compassButton=true
        viewmap.delegate = self;
        locmanager.distanceFilter = 10.0  // Movement threshold for new events
        locmanager.startUpdatingLocation()
         print("home view controller came")
        
        
        //Default sorting order is order by Closese
        passsort = self.getSortingOrderNameForIndex(index: 0)
    }
    
    
    func getSortingOrderNameForIndex(index:Int) -> String{
        
        var sortOrderName = ""
        if index == 0 {
            sortOrderName = "Closest"
        } else if index == 1 {
            sortOrderName = "Best_rating"
        } else if index == 2 {
            sortOrderName = "Lowest_price"
        }  else if index == 3 {
            sortOrderName = "Highest_price"
        }
        return sortOrderName
    }

    func parseFilterDataForDisplay() {
        if UserDefaults.standard.object(forKey: "Filterdetail") as? NSMutableDictionary == nil
        {
            passrating = ""
            passdistance = ""
            passprice = ""
            passfacilitycat = ""
            passfacility = ""
            passaccessibility = ""
            passcategory = ""
            self.filterbgbutton?.badgeValue = String(format:"")
        }
        else
        {
            let placesData = UserDefaults.standard.object(forKey: "Filterdetail") as? NSMutableDictionary
            self.getdic.removeAllObjects()
            if placesData != nil{
                self.getdic.addEntries(from: (placesData as? [String : Any])!)
            }
            print(self.getdic)
            passrating =  getdic.value(forKey: "rating") as? String
            passdistance = getdic.value(forKey: "distance") as? String
            passprice = getdic.value(forKey: "price") as? String
            let facar = getdic.value(forKey: "facility") as? NSArray
            passfacility = facar?.componentsJoined(by: ",")
            let accar = getdic.value(forKey: "accessibility") as? NSArray
            let faccatar = getdic.value(forKey: "facility_category") as? NSArray
            passfacilitycat = faccatar?.componentsJoined(by: ",")
            
            
            // If the facility Male or Female selected then the filter result should show
            // the results for unisex facilities also
            if passfacilitycat.contains(facilityCategoryIdFeMale) || passfacilitycat.contains(facilityCategoryIdMale) {
                if !passfacilitycat.contains(facilityCategoryIdUnisex) {
                    passfacilitycat.append(",")
                    passfacilitycat.append(facilityCategoryIdUnisex)
                }
            }
            
            passaccessibility = accar?.componentsJoined(by: ",")
            let catar = getdic.value(forKey: "category") as? NSArray
            passcategory = catar?.componentsJoined(by: ",")
            
            var a : Int
            a = 0
            if !passrating.isEmpty
            {
                print("FILTERLOG: passrating \(a)")
                a = a+1
                
            }
            if passdistance != "0.0" && passdistance != "10.0"
            {
                
                a = a+1
                print("FILTERLOG: passdistance \(a)")
            }
            if !passprice.isEmpty
            {
                a = a+1
                print("FILTERLOG: price \(a)")
            }
            if !passfacility.isEmpty
            {
                a = a+1
            }
            if !passaccessibility.isEmpty
            {
                print("FILTERLOG: passaccessibility \(a)")
                a = a+1
            }
            if !passcategory.isEmpty
            {
                a = a+1
                print("FILTERLOG: passcategory \(a)")
            }
            let badgeValue = String(format: "%d", a)
            if a == 0 {
                self.filterbgbutton?.badgeValue = ""
                self.filterbgbutton?.badgeBGColor = UIColor.clear
            } else {
                self.filterbgbutton?.badgeValue = badgeValue
                self.filterbgbutton?.badgeBGColor = UIColor.orange
            }
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        locationtext.placeholder = "Search_your_location".localized
        self.empty_All.text = "activity_list_no_loo_available".localized
        pickerarr.removeAllObjects()
        pickerarr.add("sorting_loo_closest".localized);
        pickerarr.add("sorting_loo_best_rating".localized);
        pickerarr.add("sorting_loo_lowest_price".localized);
        pickerarr.add("sorting_loo_highest_price".localized);
   
//        //comment below code == starts ===== //
        if(UserDefaultManager.sharedManager.getLanguageName() == "English"){
            menuListarr.removeAllObjects()
            //locationtext.placeholder = " your location"

            let distdic = NSMutableDictionary()
            distdic.setObject("ALL", forKey: "menuTitle" as NSCopying);
            menuListarr.add(distdic)
            //self.empty_All.text = "No Toilet Available"
            let speeddic = NSMutableDictionary()
            speeddic.setObject("PUBLIC", forKey: "menuTitle" as NSCopying);
            menuListarr.add(speeddic)

            let breakdic = NSMutableDictionary()
            breakdic.setObject("COMMERCIAL", forKey: "menuTitle" as NSCopying);
            menuListarr.add(breakdic)
        }
        else if(UserDefaultManager.sharedManager.getLanguageName() == "German"){
            locationtext.placeholder = "Suchen Sie Ihren Standort"
            menuListarr.removeAllObjects()
            let distdic = NSMutableDictionary()
            distdic.setObject("ALLE", forKey: "menuTitle" as NSCopying);
            menuListarr.add(distdic)

            let speeddic = NSMutableDictionary()
            speeddic.setObject("ÖFFENTLICHKEIT", forKey: "menuTitle" as NSCopying);
            menuListarr.add(speeddic)

            let breakdic = NSMutableDictionary()
            breakdic.setObject("KOMMERZIELL", forKey: "menuTitle" as NSCopying);
            menuListarr.add(breakdic)

        }
        else if(UserDefaultManager.sharedManager.getLanguageName() == "French"){
            menuListarr.removeAllObjects()
           // self.empty_All.text = "Pas de Loos Trouvé"
           // locationtext.placeholder = "Recherchez votre position"
            let distdic = NSMutableDictionary()
            distdic.setObject("TOUT", forKey: "menuTitle" as NSCopying);
            menuListarr.add(distdic)

            let speeddic = NSMutableDictionary()
            speeddic.setObject("PUBLIQUE", forKey: "menuTitle" as NSCopying);
            menuListarr.add(speeddic)

            let breakdic = NSMutableDictionary()
            breakdic.setObject("COMMERCIAL", forKey: "menuTitle" as NSCopying);
            menuListarr.add(breakdic)
        }
 
        //comment below code == starts ===== //
        
        Menulist.register(UINib.init(nibName:"MenuCollectionViewCell", bundle: nil),forCellWithReuseIdentifier:"MenuCollectionViewCell")
        Menulist.reloadData()
        
        table_All.register(UINib.init(nibName:"ListTableViewCell", bundle: nil),forCellReuseIdentifier: "table_All")
        table_All.estimatedRowHeight = 130;
        table_All.rowHeight = UITableViewAutomaticDimension
        
        table_Public.register(UINib.init(nibName:"ListTableViewCell", bundle: nil),forCellReuseIdentifier: "table_Public")
        table_Public.estimatedRowHeight = 130;
        table_Public.rowHeight = UITableViewAutomaticDimension
        
        table_Offerers.register(UINib.init(nibName:"ListTableViewCell", bundle: nil),forCellReuseIdentifier: "table_Offerers")
        table_Offerers.estimatedRowHeight = 130;
        table_Offerers.rowHeight = UITableViewAutomaticDimension
        empty_All.isHidden = true;
        empty_Offerers.isHidden = true;
        empty_Public.isHidden = true;
        beforemove = true;
        
        table_All.reloadData()
        
        table_Offerers.reloadData()
        table_Public.reloadData()
        
        searchthislocview.layer.masksToBounds =  false
        searchthislocview.layer.shadowColor = UIColor.black.cgColor;
        searchthislocview.layer.shadowOffset = CGSize(width: 1.5, height: 1.5)
        searchthislocview.layer.shadowOpacity = 0.5
        searchthislocview.isHidden = true
        
        getdic = NSMutableDictionary()
        if !isListView
        {
            let view = UIView(frame:  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 20))
            view.backgroundColor = UIColor.init(red: 27.0/255.0, green: 34.0/255.0, blue: 40.0/255.0, alpha: 1.0)
            self.view.addSubview(view)
            self.navigationController?.navigationBar.isHidden = true
        }
        else
        {
            self.navigationItem.title = "list_view".localized
            self.navigationController?.navigationBar.isHidden = false
            let view = UIView(frame:  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 20))
            view.backgroundColor = UIColor.init(red: 27.0/255.0, green: 34.0/255.0, blue: 40.0/255.0, alpha: 1.0)
            self.navigationController?.view.addSubview(view)
        }
        
        let checkstr = UserDefaults.standard.string(forKey: "screen")
        if checkstr == "comingfromfilter"
        {
            self.parseFilterDataForDisplay()
            self.runlink()
        }
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    @IBAction func OpenSideMenu(_ x:AnyObject)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.SideMenu.showLeftView(animated: true, completionHandler: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadsidemenu"), object: nil, userInfo: nil)
    }
    
    @IBAction func mylocationbutton(sender: AnyObject)
    {

        if !isListView 
        {
            UIView.animate(withDuration: 0.3, animations: {
                self.bottomlocsortimg.transform = CGAffineTransform.identity.scaledBy(x: 0.3, y: 0.3)
            }, completion: { (finish) in
                UIView.animate(withDuration: 0.3, animations: {
                    self.bottomlocsortimg.transform = CGAffineTransform.identity
                    let location = self.viewmap.myLocation as! CLLocation
                    self.searchlocation = CLLocation()
                    self.movelocation = location
                    self.viewmap.animate(toLocation: location.coordinate)
                    self.getAddressFromLatLon(location: location)
                    self.searchaddress = ""
                    self.beforemove = true
                    self.once = 0
                })
            })
            
        }
        else
        {
            sortPopup = Bundle.main.loadNibNamed("SortPopup", owner: self, options: nil)?[0] as! SortPopup
            sortPopup.frame = CGRect(x:0,y:0,width:UIScreen.main.bounds.size.width,height:UIScreen.main.bounds.size.height)
            sortPopup.picker.delegate = self
            sortPopup.picker.dataSource = self
            sortPopup.aniview.isHidden = true
            let dispatchTime1: DispatchTime = DispatchTime.now() + Double(Int64(0.4 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: dispatchTime1, execute: {
                
                UIView.animate(withDuration: 0.5, delay: 0.5, options: [.repeat, .curveEaseOut, .autoreverse], animations: {
                    let transition = CATransition()
                    transition.duration = 0.3
                    transition.type = kCATransitionPush
                    transition.subtype = kCATransitionFromTop
                    transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                    self.sortPopup.aniview.layer.add(transition, forKey: nil)
                    self.sortPopup.aniview.isHidden = false
                }, completion: nil)
                
            })
            
            sortPopup.donebutton.addTarget(self, action: #selector(self.doneButtonPressed), for: .touchUpInside)
            sortPopup.cancelbutton.addTarget(self, action: #selector(self.cancelbuttonPressed), for: .touchUpInside)
            self.navigationController?.view.addSubview(sortPopup)
        }
    }
    
    func doneButtonPressed()
    {
        print("donepressed")
        let dispatchTime1: DispatchTime = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime1, execute: {
            
            UIView.animate(withDuration: 0.5, delay: 0.5, options: [.repeat, .curveEaseOut, .autoreverse], animations: {
                let transition = CATransition()
                transition.duration = 0.5
                transition.type = kCATransitionPush
                transition.subtype = kCATransitionFromBottom
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                self.sortPopup.aniview.layer.add(transition, forKey: nil)
                self.sortPopup.aniview.isHidden = true
            }, completion: nil)
        })
        
        let dispatchTime2: DispatchTime = DispatchTime.now() + Double(Int64(0.6 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime2, execute: {
            self.sortPopup.isHidden = true
        })
        
        passsort = self.getSortingOrderNameForIndex(index: sortrowindex)
        print(passsort)
        sortPopup.removeFromSuperview()
        
        self.runlink()
    }
    
    func cancelbuttonPressed()
    {
        
        print("cancelpressed")
        let dispatchTime1: DispatchTime = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime1, execute: {
            
            UIView.animate(withDuration: 0.5, delay: 0.5, options: [.repeat, .curveEaseOut, .autoreverse], animations: {
                let transition = CATransition()
                transition.duration = 0.5
                transition.type = kCATransitionPush
                transition.subtype = kCATransitionFromBottom
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                self.sortPopup.aniview.layer.add(transition, forKey: nil)
                self.sortPopup.aniview.isHidden = true
            }, completion: nil)
            
        })
        
        let dispatchTime2: DispatchTime = DispatchTime.now() + Double(Int64(0.6 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime2, execute: {
            self.sortPopup.isHidden = true
        })
        sortPopup.removeFromSuperview()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickerarr.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.pickerarr[row] as? String
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        sortrowindex = row
        let dat = self.pickerarr.object(at: row) as! String;
        print(dat)
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error:" + error.localizedDescription)
        if !CLLocationManager .locationServicesEnabled()
        {
            let alert: UIAlertView = UIAlertView(title: "location_service_disabled_title".localized, message: "location_service_disabled_message".localized, delegate: self, cancelButtonTitle: "settings_title".localized)
            alert.restorationIdentifier = "userLocation"
            alert.tag = 100
            alert.show()
        }
        else
        {
            if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.denied
            {
                let alert: UIAlertView = UIAlertView(title: "location_service_disabled_title".localized, message: "location_service_disabled_message".localized, delegate: self, cancelButtonTitle: "settings_title".localized)
                alert.restorationIdentifier = "userLocation"
                alert.tag = 200
                alert.show()
            }
            else
            {
                manager.startUpdatingLocation()
            }
        }
    }
    
    
    /// Method to display the booking list page
    func showBookingListPage() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.navigationController?.isNavigationBarHidden = false
        let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
        currentview.isNavigationBarHidden = false
        appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
        let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "BookingListViewController") as! BookingListViewController
        currentview.pushViewController(mainview, animated: false)
    }
    
    func alertView(_ View: UIAlertView, clickedButtonAt buttonIndex: Int)
    {
        print("LOG:ALERT CLICKED")
        if buttonIndex == 0 && View.restorationIdentifier == alreadyBookedAlertIdentifier {
            print("LOG:ALREADY BOOKED ALERT")
            self.showBookingListPage()
            return
        }
        
        switch buttonIndex
        {
        case 1:
            NSLog("Retry");
            break;
        case 0:
            NSLog("Dismiss");
            if View.restorationIdentifier=="userLocation"
            {
                if buttonIndex == 0
                {
                    if View.tag == 100
                    {
                       UIApplication .shared .openURL(NSURL (string: UIApplicationOpenSettingsURLString)! as URL);
                        // UIApplication .shared .openURL(NSURL (string: "prefs:root=LOCATION_SERVICES")! as URL)
                    }
                    else if View.tag == 200
                    {
                        UIApplication .shared .openURL(NSURL (string: UIApplicationOpenSettingsURLString)! as URL);
                    }
                }
            }
            break;
        default:
            NSLog("Default");
            break;
            //Some code here..
        }
    }
    
    func getAdressName(coords: CLLocation) {
        
        CLGeocoder().reverseGeocodeLocation(coords) { (placemark, error) in
            if error != nil {
                print("Hay un error")
            } else {
                
                let place = placemark! as [CLPlacemark]
                if place.count > 0 {
                    let place = placemark![0]
                    var adressString : String = ""
                
                    
                    if place.name != nil {
                        adressString = adressString + place.name! + " - "
                    }
                    if place.subLocality != nil {
                        adressString = adressString + place.subLocality! + ", "
                    }
                   
                    if place.subAdministrativeArea != nil {
                        adressString = adressString + place.subAdministrativeArea! + " - "
                    }
                
                    if place.country != nil {
                        adressString = adressString + place.country!
                    }
                   self.Userdic.setValue(adressString, forKey: "useraddress")
                    self.userAdd = adressString
                }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        viewmap.settings.compassButton = true
        print("location address, lat and long")
        print("didUpdateToLocation: %@", locations.last)
        let geoCoder = CLGeocoder()
        let Uname = self.Userdic.value(forKey: "user_name") as? String
        let UId = self.Userdic.value(forKey: "user_id") as? String
        let time  = DateFormatter.localizedString(from: NSDate() as Date, dateStyle: .medium, timeStyle: .short)
       print(firstload)
  
        if  firstload == false  {
            firstload = true
            firstTimeLoads = true
            currentlocation = locations.last!
            let cityCoords = CLLocation(latitude: currentlocation.coordinate.latitude, longitude: currentlocation.coordinate.longitude)
            let addresss = getAdressName(coords: cityCoords)
            self.userlat =  "\(currentlocation.coordinate.latitude)"
            self.userlong = "\(currentlocation.coordinate.longitude)"
            self.userDevice = "iOS"
            self.userName = Uname ?? "NA"
            self.userId = UId ?? "NA"
            self.userTime = time
           
        
            let camera = GMSCameraPosition.init(target: currentlocation.coordinate, zoom: 11, bearing: 0, viewingAngle: 0)
            passloc = String(format: "%f,%f", currentlocation.coordinate.latitude,currentlocation.coordinate.longitude)
            if let locationData = passloc {
                self.movelocation = currentlocation
                UserDefaults.standard.set(locationData, forKey: "storemyloc")
                UserDefaults.standard.synchronize()
                viewmap.animate(to: camera)
                self.getAddressFromLatLon(location: currentlocation)
                print("my address")
                print(currentlocation)
                
                print("LOG: LOCATION 1 \(searchlocation)")
                if CLLocationCoordinate2DIsValid(self.searchlocation.coordinate) {
                    self.movelocation = self.searchlocation
                } else {
                    self.runlink()
                }
            }
        }
    }
    
    func getAddressFromLatLon(location:CLLocation)
    {
        print("LOG: address \(location)")
        let geoCoder = GMSGeocoder()
            geoCoder.reverseGeocodeCoordinate(location.coordinate, completionHandler: {(respones,err) in
                print("current Add")
                let gmaddr = respones?.firstResult()
//                let Add = gmaddr?.lines?.first
//                self.userAdd = Add ?? "NA"
                if err == nil
                {
                    if respones != nil
                    {
                        let gmaddr = respones?.firstResult()
                        print(gmaddr!)
                        print(gmaddr?.addressLine1())
                        self.currentadddress = gmaddr
                        var addrarray : NSMutableArray! = NSMutableArray()
                        addrarray.add(gmaddr?.locality as Any)
                        addrarray.add(gmaddr?.administrativeArea as Any)
                        addrarray.add(gmaddr?.country as Any)
                        let formaddr = addrarray.componentsJoined(by: ", ");
                        print("below r formadeer")
                        print(gmaddr)
                        NSLog("%@",formaddr)
                        addrarray = nil
                        if self.searchaddress.isEmpty {
                            self.locationtext.text = formaddr
                            if self.isListView {
                                DispatchQueue.main.async {
                                    self.navigationItem.title = "list_view".localized
                                }
                            }
                        } else {
                            self.locationtext.text = self.searchaddress
                            if self.isListView {
                                DispatchQueue.main.async {
                                  self.navigationItem.title = self.searchaddress
                                }
                            }
                        }
                        
                        self.movelocation = location
                        self.searchlocation = location
                        print("movelocation \(self.movelocation)")
                        self.dummystr = UserDefaults.standard.string(forKey: "runlink")
                        if self.dummystr == nil || self.dummystr == "" || self.dummystr == "needrunlink"
                        {
                            print(self.dummystr)
                            let timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.runlink), userInfo: nil, repeats: false)
                            let valueToSave = "noneedrunlink"
                            UserDefaults.standard.set(valueToSave, forKey: "runlink")

                        }
                        
                        else
                        {
                            print(self.dummystr)
                            
                            if (self.once == 0) {
                                /* TODO: move below code to a static variable initializer (dispatch_once is deprecated) */
                                // Code to run once
                                let timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.runlink), userInfo: nil, repeats: false)
                                let valueToSave = "noneedrunlink"
                                UserDefaults.standard.set(valueToSave, forKey: "runlink")
                            }
                            self.once = 1
                        }
                    }
                    else
                    {
                        self.locationtext.text = "Address_not_found".localized
                    }
                }
                else
                {
                    //self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("JEDE_r"), withAlert:self.myclass.StringfromKey("errorinretrive"), withIdentifier:"error")
                }
            })
    
    }
    
    func runlink()
    {
        print("RUN LINK")
        if AFNetworkReachabilityManager.shared().isReachable
        {
            print("%@",kBaseURL)
           passloc = String(format: "%f,%f", movelocation.coordinate.latitude,movelocation.coordinate.longitude)
             //passloc = String(format: "%f,%f", 47.5433307951724,7.57127906858171)
            print(passloc)
            UserDefaults.standard.set(passloc, forKey: "storemyloc")
            UserDefaults.standard.synchronize()
            print("passType\(passtype)")
            
            if (passtype == nil || passtype == "" || passtype == "ALL") {
                passtype = "\(kToiletCategoryTypePublic),\(kToiletCategoryOfferers)"
            } 
            
            print(passfacilitycat)
            print(passfacility)
            print(passaccessibility)
            print(passprice)
            print(passrating)
            print(passdistance)
            print(passsort)

             let   urlstring = String(format:"%@/home.php?current_location=%@&facility_category=%@&facilities=%@&accessbility=%@&price=%@&rating=%@&distance=%@&type=%@&category=%@&sort=%@",kBaseURL ?? "",passloc ?? "",passfacilitycat ?? "",passfacility ?? "",passaccessibility ?? "",passprice ?? "",passrating ?? "",passdistance ?? "",passtype ?? "",passcategory ?? "",passsort ?? "")
            
            print(self.searchstr)
            if self.searchstr == "yes"
            {
                var markeree = GMSMarker()
                let position = CLLocationCoordinate2D(latitude: self.searchlocation.coordinate.latitude, longitude: self.searchlocation.coordinate.longitude)
                print(position)
                markeree = GMSMarker(position: position)
                markeree.icon = UIImage(named: "checkpin")
                markeree.map = self.viewmap
            }
            else if self.searchstr == "no"
            {
                //marker.icon = UIImage.init(named: "checkpin")
            }
            
            
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: true, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                if Stats == true
                {
                    DispatchQueue.main.async {
                        let checkstr = UserDefaults.standard.string(forKey: "screen")
                        if checkstr == "comingfromfilter"
                        {
                            self.array_All.removeAllObjects()
                            self.array_Offerers.removeAllObjects()
                            self.array_Public.removeAllObjects()
                            self.viewmap.clear()
                        }
                        NSLog("sathishObject=%@", jsonObject);
                        if jsonObject.count != 0
                        {
                            let copyarr = jsonObject;
                            if copyarr.count != 0
                            {
                                self.array_All.removeAllObjects()
                                self.Markerarray.removeAllObjects()
                                self.array_All = copyarr.mutableCopy() as! NSMutableArray;
                                if self.array_All.count == 0
                                {
                                    //self.toiletCount = "0"
                                    self.empty_All.isHidden = false;
                                }
                                else
                                {
                                    //self.toiletCount = ("\(self.array_All.count)")
                                    self.empty_All.isHidden = true;
                                    self.viewmap.clear()
                                    print("LOG: ARRAY COUNT3 \(self.array_All.count)")
                     
                                    for c in 0 ..< self.array_All.count
                                    {
                                        let dat = self.array_All.object(at: c) as! NSDictionary;
                                        let location = dat.value(forKey: "loo_location") as? String
                                        let categoryType = dat.value(forKey: "category_name") as! String
                                        print("LOG: categoryType \(categoryType)")
                                        print("LOG: location \(location)")
                                        if let locationData = location {
                                            let locar:NSArray = locationData.components(separatedBy: ",") as NSArray
                                            print("LOG: locar \(locar) - \(locar.count)")
                                            if locar.count == 2 {
                                                let lat = Double(locar.object(at: 0) as! String)
                                                let lon = Double(locar.object(at: 1) as! String)
                                                print("LOG: lat \(lat) - \(lon)")
                                                if let latitude = lat {
                                                    if let longitude = lon {
                                                        let categoryType2 = dat.value(forKey: "category_name") as! String
                                                        print("LOG: categoryType2 \(categoryType2)")
                                                        let imageName = self.getDefaultImageForMarker(categoryName: categoryType2)
                                                        let loosloc = CLLocation.init(latitude: latitude, longitude: longitude)
                                                        let position = CLLocationCoordinate2DMake(loosloc.coordinate.latitude, loosloc.coordinate.longitude)
                                                        let marker = GMSMarker(position: position)
                                                        marker.icon = UIImage.init(named: imageName)
                                                        marker.map  = self.viewmap
                                                        self.Markerarray.add(marker);
                                                        print("LOG: marker \(marker)")
                                                    }
                                                }
                                            }
                                        }
                                        
                                        
                                        //draw circle
                                        if let locationDetails = location {
                                            if categoryType == "" {
                                                
                                            } else {
                                                let locar:NSArray = locationDetails.components(separatedBy: ",") as NSArray
                                                if locar.count == 2 {
                                                    let lat = Double(locar.object(at: 0) as! String)
                                                    let lon = Double(locar.object(at: 1) as! String)
                                                    if let latitude = lat {
                                                        if let longitude = lon {
                                                            let loosloc = CLLocation.init(latitude: latitude, longitude: longitude)
                                                            let geoFenceCircle = GMSCircle()
                                                            geoFenceCircle.radius = 100
                                                            geoFenceCircle.position = CLLocationCoordinate2DMake(loosloc.coordinate.latitude, loosloc.coordinate.longitude)
                                                            
                                                            if categoryType == "Gas Station" {
                                                                geoFenceCircle.fillColor = UIColor.init(red: 129/255.0, green: 130/255.0, blue: 133/255.0, alpha: 1.0)
                                                            } else if categoryType == "Hotel" {
                                                                geoFenceCircle.fillColor = UIColor.init(red: 40/255.0, green: 56/255.0, blue: 145/255.0, alpha: 0.4)
                                                            } else if categoryType == "Restaurant" {
                                                                geoFenceCircle.fillColor = UIColor.init(red: 0.788, green: 0.090, blue: 0.141, alpha: 0.15)
                                                            } else if categoryType == "Shopping Mall" {
                                                                geoFenceCircle.fillColor = UIColor.init(red: 247/255.0, green: 148/255.0, blue: 30/255.0, alpha: 0.15)
                                                            } else if categoryType == "Bar" {
                                                                geoFenceCircle.fillColor = UIColor.init(red: 146/255.0, green: 39/255.0, blue: 143/255.0, alpha: 0.15)
                                                            }
                                                            geoFenceCircle.strokeWidth = 0
                                                            geoFenceCircle.map = self.viewmap
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    //self.focusallmarkers()
                                }
                                
                                //print("toilet count here at first time search")
                                //print(self.toiletCount)
                                //print(self.array_All.count)
                               //self.toiletCount = "\(self.array_All.count)"
                               //self.firstTimeApi(ToiletCount: self.toiletCount)
                                self.table_All.reloadData();
                                self.array_Offerers.removeAllObjects()
                                for c in 0 ..< self.array_All.count
                                {
                                    let dat = self.array_All.object(at: c) as! NSDictionary;
                                    if dat.value(forKey: "type") != nil
                                    {
                                        let price = dat.value(forKey: "type") as! String;
                                        if price == "Offerers"
                                        {
                                            if self.passtype == "offerers"
                                            {
                                                self.array_All.removeAllObjects()
                                                //                                        self.array_Offerers.add(copyarr.object(at: c) as! NSDictionary)
                                                self.array_All.add(copyarr.object(at: c) as! NSDictionary)
                                            }
                                        }
                                    }
                                }
                                if self.array_All.count == 0
                                {
                                    self.empty_All.isHidden = false;
                                }
                                else
                                {
                                    self.empty_All.isHidden = true;
                                }
                                self.table_All.reloadData();
                                
                                self.array_Public.removeAllObjects()
                                for c in 0 ..< copyarr.count
                                {
                                    let dat = copyarr.object(at: c) as! NSDictionary;
                                    if dat.value(forKey: "type") != nil
                                    {
                                        let price = dat.value(forKey: "type") as! String;
                                        if price == "Public"
                                        {
                                            if self.passtype == "public"
                                            {
                                                self.array_All.removeAllObjects()
                                                self.array_All.add(copyarr.object(at: c) as! NSDictionary)
                                            }
                                        }
                                    }
                                    else
                                    {
                                        self.array_All.removeAllObjects()
                                        self.array_All.add(copyarr.object(at: c) as! NSDictionary)
                                    }
                                }
                                if self.array_All.count == 0
                                {
                                    self.empty_All.isHidden = false;
                                }
                                else
                                {
                                    self.empty_All.isHidden = true;
                                }
                                self.table_All.reloadData();
                                self.searchthislocview.isHidden = true
                            }
                            else
                            {
                                self.viewmap.clear()
                                self.array_All.removeAllObjects()
                                self.array_Offerers.removeAllObjects()
                                self.array_Public.removeAllObjects()
                                self.Markerarray.removeAllObjects()
                                self.empty_All.isHidden = false;
                                self.table_All.reloadData();
                                self.empty_Offerers.isHidden = false;
                                self.table_Offerers.reloadData();
                                self.empty_Public.isHidden = false;
                                self.table_Public.reloadData();
                                
                                self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("No Loos's Found"), withIdentifier:"")
                            }
                            self.focusMapToShowAllMarkers()
                            
                        } // end of jsonObject.count
                        else
                        {
                            NSLog("jsonObject=error");
                            self.viewmap.clear()
                            self.array_All.removeAllObjects()
                            self.array_Offerers.removeAllObjects()
                            self.array_Public.removeAllObjects()
                            self.Markerarray.removeAllObjects()
                            self.empty_All.isHidden = false;
                            self.table_All.reloadData();
                            self.empty_Offerers.isHidden = false;
                            self.table_Offerers.reloadData();
                            self.empty_Public.isHidden = false;
                            self.table_Public.reloadData();
                            self.firstTimeApi(ToiletCount: "0")
                        }
                    } //Dispatch queue
                }// Stats == true
                else
                {
                    print("no errorr 1")
                    NSLog("jsonObject=error");
                    self.viewmap.clear()
                    self.array_All.removeAllObjects()
                    self.array_Offerers.removeAllObjects()
                    self.array_Public.removeAllObjects()
                    self.Markerarray.removeAllObjects()
                    self.empty_All.isHidden = false;
                    self.table_All.reloadData();
                    self.empty_Offerers.isHidden = false;
                    self.table_Offerers.reloadData();
                    self.empty_Public.isHidden = false;
                    self.table_Public.reloadData();
                  
                    
                }
                
                // no toilet count here
                
                 print("no toilet count here at first time")
                 //self.toiletCount = "\(self.array_All.count)"
                //self.firstTimeApi(ToiletCount: self.toiletCount)
                
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
        print("done screen")

    }
    func firstTimeApi(ToiletCount:String) {
        
        print(ToiletCount)
        print(self.userlong)
        print(self.userlat)
        if self.firstTimeLoads == true {
           firstTimeLoads = false
            self.userAdd = (self.Userdic.value(forKey: "useraddress") as? String) ?? "NA"
            print(self.userId,self.userName, self.userlat, self.userlong, self.userAdd, self.userTime, self.toiletCount, self.userDevice)
            
            let urlstring = String(format:"%@/add_userlocation.php?uname=%@&uid=%@&address=%@&lat=%@&lng=%@&count=%@&time=%@&device=%@",kBaseURL,self.userName,self.userId,self.userAdd,self.userlat,self.userlong,ToiletCount,self.userTime,self.userDevice);
            print(urlstring)
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: true, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                print(jsonObject)
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    let message = (jsonObject.object(at: 0) as AnyObject).value(forKey:"message") as! String
                    if message == "location recorded"
                    {
                        
                        NSLog("jsonObject=%@", jsonObject);
                        // success
                        
                    }else {
                        //some error
                    }
                }
                
            })
       }
    }
    
    func mapView(_ mapView: GMSMapView!, idleAt position: GMSCameraPosition!)
    {
        
        if  firstload == true
        {
            currentlocation = self.movelocation
            self.getAddressFromLatLon(location: currentlocation)
        }
        
    }
    
    @IBAction func searchinthislocbutton(_sender:UIButton)
    {
        self.runlink()
        self.searchthislocview.isHidden = false
    }
    
    @IBAction func searchbuttonclick(sender: UIButton)
    {
        searchstr = "yes"
        let searchViewController = ABCGooglePlacesSearchViewController()
        searchViewController.delegate = self
        let navigationController = UINavigationController(rootViewController: searchViewController)
        self.present(navigationController, animated: true, completion: { _ in })
        
    }
    
    public func searchViewController(_ controller: ABCGooglePlacesSearchViewController!, didReturn place: ABCGooglePlace!)
    {
        searchstr = "yes"
        print("SEARCH RESULT IS RECEIVED")
        print(place)
        print(place.location)
        print(place.formatted_address)
        print(place.name)
        self.searchaddress = place.formatted_address
        
       
        var marker = GMSMarker()
        viewmap.settings.compassButton = true
        let coordinatesString = "(\(place.location.coordinate.latitude),\(place.location.coordinate.longitude))"
        
        print("\(coordinatesString)")
        FIRAnalytics.logEvent(withName: "location_search", parameters: [
            "lat_long": coordinatesString,
            "item_name" : place.name,
            "address" : place.formatted_address,
            "login_device" : "iOS",
            "content_type" : "search_map"
            
            ])
        let camera = GMSCameraPosition.camera(withLatitude: place.location.coordinate.latitude, longitude: place.location.coordinate.longitude, zoom: 11)
        viewmap.camera = camera
        let position = CLLocationCoordinate2D(latitude: place.location.coordinate.latitude, longitude: place.location.coordinate.longitude)
        marker = GMSMarker(position: position)
        marker.icon = UIImage(named: "checkpin")
        marker.map = self.viewmap
        self.once = 0
        self.searchlocation = place.location
        self.movelocation = self.searchlocation
        print("LOG: LOCATION 2 \(searchlocation)")
        
       
    }
    
    override func viewWillLayoutSubviews()
    {
        
        Menulist.layer.cornerRadius = 4.0
        Menulist.layer.masksToBounds =  true
    }
    
    // Function to show the list view
    func showListView() {
      
        let userId = UserDefaults.standard.string(forKey: "user_id") ?? ""
        FIRAnalytics.logEvent(withName: "listview_click", parameters: [
            "button_name": "listView_button_click",
            "user_id" : userId,
            "login_device" : "iOS"
            ])
        
        //Update the image
        self.bottomlocsortimg.image = UIImage.init(named: "Sort.png")
        self.navigationItem.title = "list_view".localized
        
        UIView.animate(withDuration: 0.3, animations: {
            self.bottomlistmapimg.transform = CGAffineTransform.identity.scaledBy(x: 0.3, y: 0.3)
        }, completion: { (finish) in
            UIView.animate(withDuration: 0.3, animations: {
                self.bottomlistmapimg.transform = CGAffineTransform.identity
                self.bottomlistmapimg.image = UIImage.init(named:"Mapp.png")
            })
        })
        
        Listview.isHidden = false;
        Menulist.isHidden = false;
        searchview.isHidden = true;
        
        self.navigationController?.navigationBar.isHidden = false
        let view = UIView(frame:  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 20))
        view.backgroundColor = UIColor.init(red: 27.0/255.0, green: 34.0/255.0, blue: 40.0/255.0, alpha: 1.0)
        self.navigationController?.view.addSubview(view)
        
        print(Mselectedindex)
        
        if Mselectedindex == 0
        {
            passtype = ""
        }
        else if Mselectedindex == 1
        {
            passtype = "Public"
        }
        else
        {
            passtype = "Offerers"
        }
        self.viewmap.clear()
        self.Markerarray.removeAllObjects()
        self.array_All.removeAllObjects()
        self.array_Offerers.removeAllObjects()
        self.array_Public.removeAllObjects()
        self.runlink()
    }
    
    func showMapView() {
        
        let view = UIView(frame:  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 20))
        view.backgroundColor = UIColor.init(red: 27.0/255.0, green: 34.0/255.0, blue: 40.0/255.0, alpha: 1.0)
        self.view.addSubview(view)
        self.bottomlocsortimg.image = UIImage.init(named: "CU.png")
        UIView.animate(withDuration: 0.3, animations: {
            self.bottomlistmapimg.transform = CGAffineTransform.identity.scaledBy(x: 0.3, y: 0.3)
        }, completion: { (finish) in
            UIView.animate(withDuration: 0.3, animations: {
                self.bottomlistmapimg.transform = CGAffineTransform.identity
                self.bottomlistmapimg.image = UIImage.init(named:"List-1.png")
                self.once = 0
                
            })
        })
        
        Listview.isHidden = true;
        searchview.isHidden = true;
        Menulist.isHidden = false;
        
        let dispatchTime1: DispatchTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime1, execute: {
            
            UIView.animate(withDuration: 0.5, delay: 0.5, options: [.repeat, .curveEaseOut, .autoreverse], animations: {
                
                let transition = CATransition()
                transition.duration = 0.5
                transition.type = kCATransitionPush
                transition.subtype = kCATransitionFromBottom
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                self.searchview.layer.add(transition, forKey: nil)
                self.searchview.isHidden = false
            }, completion: nil)
            
        })
        
        self.navigationController?.navigationBar.isHidden = true
        if Mselectedindex == 0
        {
            passtype = ""
        }
        else if Mselectedindex == 1
        {
            passtype = "Public"
        }
        else
        {
            passtype = "Offerers"
        }
        print(UIScreen.main.bounds.size.width)
        print(Mselectedindex)
        print(CGFloat(Mselectedindex)*UIScreen.main.bounds.size.width)
        self.runlink()
    }
    
    
    @IBAction func ShowListMap(sender: AnyObject)
    {
        
        if isListView
        {
            self.showMapView()
        }
        else
        {
            self.showListView()
        }
        isListView = !isListView
        
    }
    
    // MARK:Collectionview
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        print("MENU LISIT \(menuListarr)")
        return menuListarr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        let numberOfCellInRow : Int = 3
        let padding : Int = 1
        let collectionCellWidth : CGFloat = (collectionView.frame.size.width/CGFloat(numberOfCellInRow)) - CGFloat(padding)
        return CGSize(width: collectionCellWidth ,height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell:MenuCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuCollectionViewCell", for:indexPath as IndexPath) as! MenuCollectionViewCell
        cell.setNeedsUpdateConstraints()
        cell.updateConstraintsIfNeeded()
        let dic = menuListarr.object(at: indexPath.item) as! NSDictionary;
        
        if indexPath.row == 0 {
            cell.name.text = "activity_list_all".localized
        } else if indexPath.row == 1 {
            cell.name.text = "activity_list_public".localized
        } else if indexPath.row == 2 {
            cell.name.text = "activity_maps_offerers_button".localized
        }
        
        if Mselectedindex == indexPath.item
        {
            cell.selectlabel.isHidden = false;
            cell.name.textColor = UIColor.white
        }
        else
        {
            cell.selectlabel.isHidden = true;
            cell.name.textColor = UIColor.black
        }
        
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
//        self.viewmap.clear()
        self.array_All.removeAllObjects()
        self.array_Offerers.removeAllObjects()
        self.array_Public.removeAllObjects()
        if Listview.isHidden == false
        {
            if Mselectedindex != indexPath.item
            {
                Mselectedindex = indexPath.item;
                collectionView .reloadData()
                if Mselectedindex == 0
                {
                    passtype = ""
                }
                else if Mselectedindex == 1
                {
                    passtype = "Public"
                }
                else
                {
                    passtype = "Offerers"
                }
                
                self.runlink()
            }
        }
        else
        {
            if Mselectedindex != indexPath.item
            {
                Mselectedindex = indexPath.item;
                collectionView .reloadData()
                if Mselectedindex == 0
                {
                    passtype = ""
                }
                else if Mselectedindex == 1
                {
                    passtype = "Public"
                }
                else
                {
                    passtype = "Offerers"
                }
                
                self.runlink()
            }
        }
    }
    
    // MARK: - Tabelview Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        

        if tableView == table_All
        {
            return array_All.count
        }
        else if tableView == table_Public
        {
            return array_Public.count
        }
        else
        {
            return array_Offerers.count
        }
    }
    
    func tableView(_  tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension;
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let simpleTableIdentifier  = NSString(format:"table_All")
        let cell:ListTableViewCell?=tableView.dequeueReusableCell(withIdentifier: simpleTableIdentifier as String) as? ListTableViewCell
        
        if tableView == table_All
        {
            let simpleTableIdentifier  = NSString(format:"table_All")
            let cell:ListTableViewCell?=tableView.dequeueReusableCell(withIdentifier: simpleTableIdentifier as String) as? ListTableViewCell
            cell?.selectionStyle = .none
            
            let dic = array_All.object(at: indexPath.row) as! NSDictionary;
            
            let imageurl = dic.value(forKey: "image") as? String;
            
//            if !imageurl!.isEmpty
//            {
                if imageurl?.characters.count == 0 || imageurl == "" || imageurl == "http://stitchplus.com/JEDE_rpanel/"
                {
                    if let categoryType = dic.value(forKey: "category_name") as? String {
                        let imageNamed = self.getDefaultImageForCategory(categoryName: categoryType)
                        cell?.List_image.image = UIImage.init(named: imageNamed)
                    } else {
                        cell?.List_image.image = UIImage.init(named: "default_image")
                    }
                }
                else
                {
                    let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                        print(self)
                        cell?.List_image.setNeedsDisplay()
                    }
                    cell?.List_image.sd_setImage(with: NSURL(string:imageurl ?? "default_image") as URL!, completed: block)
                    cell?.List_image.contentMode = UIViewContentMode.scaleToFill;
                }
 
            cell?.List_name.text = dic.value(forKey: "loo_name") as? String;
 
            let mytype = dic.value(forKey: "type") as? String;
            if (mytype != nil) {
                cell?.timelab.text = "Type: " + mytype!
            }
            
            if dic.value(forKey: "price") != nil
            {
                let price = dic.value(forKey: "price") as! String;
                if price != ""
                {
                    cell?.pricelab.text = String(format: "CHF %@",price)
                }
                else
                {
                    cell?.pricelab.text = "FREE"
                }
            }
            else
            {
                cell?.pricelab.text = "FREE"
            }
            
            if dic.value(forKey: "distance") != nil
            {
                let distance = dic.value(forKey: "distance") as! String;
                if distance != ""
                {
                    cell?.distancelab.text = "adapter_listview_distance".localized + ": " + distance + " KM"
                }
                else
                {
                    cell?.distancelab.text = "adapter_listview_distance".localized
                }
            }
            else
            {
                cell?.distancelab.text = "adapter_listview_distance".localized
            }

            if dic.value(forKey: "rating") != nil
            {
                let rating = dic.value(forKey: "rating") as! NSNumber;
                print(rating)
                
                if rating.doubleValue != 0
                {
                    cell?.ratingview.value = CGFloat(rating.doubleValue)
                }
                else
                {
                    cell?.ratingview.value = 0
                }
            }
            else
            {
                cell?.ratingview.value = 0
            }
             
            return cell!
        }
            
        else if tableView == table_Public
        {
            let simpleTableIdentifier  = NSString(format:"table_Public")
            let cell:ListTableViewCell?=tableView.dequeueReusableCell(withIdentifier: simpleTableIdentifier as String) as? ListTableViewCell
            cell?.selectionStyle = .none
            
            let dic = array_Public.object(at: indexPath.row) as! NSDictionary;
            
            let imageurl = dic.value(forKey: "image") as? String;
            //            if !imageurl!.isEmpty
            //            {
            if imageurl?.characters.count == 0 || imageurl == "" || imageurl == "http://stitchplus.com/JEDE_rpanel/"
            {
                if let categoryType = dic.value(forKey: "category_name") as? String {
                    let imageNamed = self.getDefaultImageForCategory(categoryName: categoryType)
                    cell?.List_image.image = UIImage.init(named: imageNamed)
                } else {
                    cell?.List_image.image = UIImage.init(named: "default_image")
                }
            }
            else
            {
                let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                    print(self)
                }
                cell?.List_image.sd_setImage(with: NSURL(string:imageurl ?? "default_image") as URL!, completed: block)
                cell?.List_image.contentMode = UIViewContentMode.scaleToFill;
            }
            //            }
            cell?.List_name.text = dic.value(forKey: "loo_name") as? String;
            
            if  passtype == "Offerers"
            {
                cell?.List_name.text = "OFFERERS"
            }
            else if passtype == "Public"
            {
                cell?.List_name.text  = "PUBLIC"
            }
            
            
            if dic.value(forKey: "price") != nil
            {
                let price = dic.value(forKey: "price") as! String;
                if price != ""
                {
                    //                    cell?.Pricebg.backgroundColor = myclass.colorWithHexString(hex: "#01a550",Alpha: 0.7)
                    cell?.pricelab.text = String(format: "CHF %@",price)
                }
                else
                {
                    //                    cell?.Pricebg.backgroundColor = myclass.colorWithHexString(hex: "#ec0d1a",Alpha: 0.7)
                    cell?.pricelab.text = "FREE"
                }
            }
            else
            {
                //                cell?.pricelab.backgroundColor = myclass.colorWithHexString(hex: "#01a550",Alpha: 0.7)
                cell?.pricelab.text = "FREE"
            }
            
            if dic.value(forKey: "distance") != nil
            {
                let distance = dic.value(forKey: "distance") as! String;
                if distance != ""
                {
                    //                    cell?.Pricebg.backgroundColor = myclass.colorWithHexString(hex: "#01a550",Alpha: 0.7)
                    cell?.distancelab.text = "adapter_listview_distance".localized + ": " + distance + " KM"
                        //String(format: "Distance: %@ KM",distance)
                }
                else
                {
                    //                    cell?.Pricebg.backgroundColor = myclass.colorWithHexString(hex: "#ec0d1a",Alpha: 0.7)
                    cell?.distancelab.text = "adapter_listview_distance".localized
                        //String(format: "Distance:")
                }
            }
            else
            {
                //                cell?.pricelab.backgroundColor = myclass.colorWithHexString(hex: "#01a550",Alpha: 0.7)
                cell?.distancelab.text = "adapter_listview_distance".localized
            }
            
            if dic.value(forKey: "rating") != nil
            {
                let rating = dic.value(forKey: "rating") as! NSNumber;
                print(rating)
                if rating.doubleValue != 0
                {
                    cell?.ratingview.value = CGFloat(rating.doubleValue)
                }
                else
                {
                    cell?.ratingview.value = 0
                }
            }
            else
            {
                cell?.ratingview.value = 0
            }
 
            
            if dic.value(forKey: "price") != nil
            {
                let price = dic.value(forKey: "price") as! String;
                if price != ""
                {
                    cell?.pricelab.text = String(format: "CHF %@",price)
                }
                else
                {
                    cell?.pricelab.text = "FREE"
                }
            }
            else
            {
                cell?.pricelab.text = "FREE"
            }

            if dic.value(forKey: "price") != nil {
                let price = dic.value(forKey: "price") as! String;
                if price != "" {
                    cell?.pricelab.text = String(format: "CHF %@",price)
                } else {
                    cell?.pricelab.text = "FREE"
                }
            } else {
                cell?.pricelab.text = "FREE"
            }
            if dic.value(forKey: "price") != nil
            {
                let price = dic.value(forKey: "price") as! String;
                if price != "" {
                    cell?.pricelab.text = String(format: "CHF %@",price)
                } else {
                    cell?.pricelab.text = "FREE"
                }
            } else {
                cell?.pricelab.text = "FREE"
            }
            
            return cell!
        }
        else
        {
            let simpleTableIdentifier  = NSString(format:"table_Offerers")
            let cell:ListTableViewCell?=tableView.dequeueReusableCell(withIdentifier: simpleTableIdentifier as String) as? ListTableViewCell
            cell?.selectionStyle = .none
            
            let dic = array_Offerers.object(at: indexPath.row) as! NSDictionary;
            
            let imageurl = dic.value(forKey: "image") as? String;
            
            if imageurl?.characters.count == 0 || imageurl == "" || imageurl == "http://stitchplus.com/JEDE_rpanel/"
            {
                if let categoryType = dic.value(forKey: "category_name") as? String {
                    let imageNamed = self.getDefaultImageForCategory(categoryName: categoryType)
                    cell?.List_image.image = UIImage.init(named: imageNamed)
                } else {
                    cell?.List_image.image = UIImage.init(named: "default_image")
                }
            }
            else
            {
                let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                    print(self)
                }
                
                cell?.List_image.sd_setImage(with: NSURL(string:imageurl ?? "default_image") as URL!, completed: block)
                cell?.List_image.contentMode = UIViewContentMode.scaleToFill;
            }
            //            }
            cell?.List_name.text = dic.value(forKey: "loo_name") as? String;
            
            if  passtype == "Offerers"
            {
                cell?.List_name.text = "OFFERERS"
            }
            else if passtype == "Public"
            {
                cell?.List_name.text  = "PUBLIC"
            }
            
            if dic.value(forKey: "price") != nil
            {
                let price = dic.value(forKey: "price") as! String;
                if price != ""
                {
                    //                    cell?.Pricebg.backgroundColor = myclass.colorWithHexString(hex: "#01a550",Alpha: 0.7)
                    cell?.pricelab.text = String(format: "CHF %@",price)
                }
                else
                {
                    //                    cell?.Pricebg.backgroundColor = myclass.colorWithHexString(hex: "#ec0d1a",Alpha: 0.7)
                    cell?.pricelab.text = "FREE"
                }
            }
            else
            {
                //                cell?.pricelab.backgroundColor = myclass.colorWithHexString(hex: "#01a550",Alpha: 0.7)
                cell?.pricelab.text = "FREE"
            }
            
            if dic.value(forKey: "distance") != nil
            {
                let distance = dic.value(forKey: "distance") as! String;
                if distance != ""
                {
                    //                    cell?.Pricebg.backgroundColor = myclass.colorWithHexString(hex: "#01a550",Alpha: 0.7)
                    cell?.distancelab.text = "adapter_listview_distance".localized + ": " + distance + " KM"
                }
                else
                {
                    //                    cell?.Pricebg.backgroundColor = myclass.colorWithHexString(hex: "#ec0d1a",Alpha: 0.7)
                    cell?.distancelab.text = "adapter_listview_distance".localized
                }
            }
            else
            {
                //                cell?.pricelab.backgroundColor = myclass.colorWithHexString(hex: "#01a550",Alpha: 0.7)
                cell?.distancelab.text = "adapter_listview_distance".localized
                    //String(format: "Distance:")
            }
            
            if dic.value(forKey: "rating") != nil
            {
                let rating = dic.value(forKey: "rating") as! NSNumber;
//                let myInteger = Int(rating)
//                let myNumber = NSNumber(value:myInteger!)
                
                if rating.doubleValue != 0
                {
                    //                    cell?.Pricebg.backgroundColor = myclass.colorWithHexString(hex: "#01a550",Alpha: 0.7)
//                    cell?.ratingview.stepInterval = 0.0
                    cell?.ratingview.value = CGFloat(rating.doubleValue)
                }
                else
                {
                    //                    cell?.Pricebg.backgroundColor = myclass.colorWithHexString(hex: "#ec0d1a",Alpha: 0.7)
//                    cell?.ratingview.stepInterval = 0.0
                    cell?.ratingview.value = 0
                }
            }
            else
            {
                //                cell?.pricelab.backgroundColor = myclass.colorWithHexString(hex: "#01a550",Alpha: 0.7)
//                cell?.ratingview.stepInterval = 0.0
                cell?.ratingview.value = 0
            }
 
            return cell!
        }
 
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
         let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "NewDetailsViewController") as! NewDetailsViewController
        var selectdic : NSMutableDictionary?
        if tableView == table_All
        {
           // let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "NewDetailsViewController") as! NewDetailsViewController
            secondViewController.selectdic = array_All.object(at: indexPath.row) as! NSMutableDictionary
            secondViewController.passcurrentloc = passloc
            selectdic = (array_All.object(at: indexPath.row) as! NSMutableDictionary)
            self.navigationController?.pushViewController(secondViewController, animated: true)
        }
        else if tableView == table_Public
        {
           // let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "NewDetailsViewController") as! NewDetailsViewController
            secondViewController.selectdic = array_Public.object(at: indexPath.row) as! NSMutableDictionary
            secondViewController.passcurrentloc = passloc
            selectdic = (array_Public.object(at: indexPath.row) as! NSMutableDictionary)
            self.navigationController?.pushViewController(secondViewController, animated: true)
        }
        else
        {
            //let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "NewDetailsViewController") as! NewDetailsViewController
            secondViewController.selectdic = array_Offerers.object(at: indexPath.row) as! NSMutableDictionary
            secondViewController.passcurrentloc = passloc
            selectdic = (array_Offerers.object(at: indexPath.row) as! NSMutableDictionary)
            self.navigationController?.pushViewController(secondViewController, animated: true)
        }

        let looId = selectdic?.value(forKey: "loo_id") as? String ?? "NA"
        let looCatName = selectdic?.value(forKey: "category_name") as? String ?? "NA"
        let looAdd = selectdic?.value(forKey: "address") as? String ?? "NA"
        let looImage = selectdic?.value(forKey: "image") as? String ?? "NA"
    
        FIRAnalytics.logEvent(withName: "listview_item_open", parameters: [
            "loo_id": looId,
            "loo_category" : looCatName,
            "loo_address" : looAdd,
            "login_device" : "iOS",
            "loo_image_url" : looImage
            ])
    }
    
    func mapView(_ mapView: GMSMapView!, markerInfoWindow marker: GMSMarker!) -> UIView!
    {
        print("markerInfoWindow called!!!")
        let firstview = UIView.init(frame: CGRect(x: 0, y: 0, width: 275, height: 195))
        self.mapInfoWindow = Bundle.main.loadNibNamed("CustomInfoWindow", owner: self, options: nil)?[0] as! CustomInfoWindow
        
        self.mapInfoWindow.ClickForMore.text = "click_here_for_more".localized
        marker.tracksInfoWindowChanges = true
       
        
        print("LOG: \(marker)")
        
        if let markerValue = marker {
            print("LOG: \(markerValue)")
            
            let index =  self.Markerarray.index(of: markerValue)
            print("LOG: \(index)")
            
            //let index = Markerarray.index(of: marker)
            if index < self.array_All.count {
                let dat = self.array_All.object(at: index) as! NSDictionary;
                print(dat)
                
                let looId = dat.object(forKey: "loo_id") ?? ""
                let looName = dat.object(forKey: "loo_name") ?? ""
                let contentType = dat.object(forKey: "image") ?? ""
                let address =  dat.object(forKey: "address") ?? ""
                let loocatId =  dat.object(forKey: "category_id") ?? ""
                let catName = dat.object(forKey: "category_name") ?? ""
                
               FIRAnalytics.logEvent(withName: "View_item", parameters: [
                    "loo_id": looId,
                    "loo_name": looName,
                    "content_type" : contentType,
                    "address" : address,
                    "login_device" : "iOS",
                    "loo_category_id" : loocatId,
                    "category_name" : catName
                    
                    ])
                let loonamestr = dat.value(forKey: "loo_name") as! String;
                if loonamestr != ""
                {
                    self.mapInfoWindow.namelabel.text = loonamestr
                }
                else
                {
                    self.mapInfoWindow.namelabel.text = ""
                }
                
                
                var typestr = dat.value(forKey: "category_name") as! String;
                let distance = dat.value(forKey: "distance") as! String;
                let pricestr = dat.value(forKey: "price") as! String;
                
                if typestr != ""
                {
                    self.mapInfoWindow.categorylab.text = typestr.localized + ", " + distance + "KM, " + String(format: "CHF %@", pricestr)
                }
                else
                {
                    self.mapInfoWindow.categorylab.text = "adapter_listview_public".localized + ", " + distance + "KM, " + String(format: "%@ CHF", pricestr)
                }
                
                if dat.value(forKey: "rating") != nil
                {
                    let rating = dat.value(forKey: "rating") as! NSNumber;
                    
                    if rating.doubleValue != 0
                    {
                        self.mapInfoWindow.ratingview.stepInterval = 0.0
                        self.mapInfoWindow.ratingview.value = CGFloat(rating.doubleValue)
                    }
                    else
                    {
                        self.mapInfoWindow.ratingview.value = 0
                    }
                }
                
                //
                let imageurl = dat.value(forKey: "image") as? String;
                
                
                if imageurl?.characters.count == 0 || imageurl == "" || imageurl == "http://stitchplus.com/JEDE_rpanel/"
                    
                {
                    if typestr == "Hotel"
                    {
                        self.mapInfoWindow.imageview.sd_setImage(with: imageurl as? URL,placeholderImage: "Hotel_Placeholder_Image" as? UIImage, options: .refreshCached, progress: nil) { (image, error, cache, urls) in
                            if (error != nil) {
                                self.mapInfoWindow.imageview.image = UIImage(named: "Hotel_Placeholder_Image")
                            }
                            else
                            {
                                self.mapInfoWindow.imageview.image = image
                            }
                        }
                        // self.mapInfoWindow.imageview.image = UIImage.init(named: "Hotel_Placeholder_Image")
                    } else if typestr == "Restaurant" {
                        self.mapInfoWindow.imageview.image = UIImage.init(named: "Restaurant_Placeholder_Image")
                    } else if typestr == "Bar" {
                        self.mapInfoWindow.imageview.image = UIImage.init(named: "Bar_Placeholder_Image")
                    } else if typestr == "Gas Station" {
                        self.mapInfoWindow.imageview.image = UIImage.init(named: "Tankstelle_Placeholder_Image")
                    } else if typestr == "Shopping Mall" {
                        self.mapInfoWindow.imageview.image = UIImage.init(named: "Laden_Kaufhaus_Placeholder_Image")
                    } else if typestr ==  "Public" {
                        self.mapInfoWindow.imageview.image = UIImage.init(named: "Public-Icon")
                    } else if typestr == "Temp" {
                        self.mapInfoWindow.imageview.image = UIImage.init(named: "Temp-Icon")
                    } else if typestr == "Shower" {
                        self.mapInfoWindow.imageview.image = UIImage.init(named: "Shower-Icon")
                    } else {
                        self.mapInfoWindow.imageview.image = UIImage.init(named: "default_image")
                    }
                }
                else
                {
                    let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                        print("Completion block here!!!")
                    }
                    if imageurl != nil{
                        //cell.insideimg.sd_setImage(with:URL(string: imageurl))
                        let imageCompleteUrl = URL(string: imageurl!) as? URL
                        
                        let placeHolder = UIImage.init(named:"default_image")
                        
                        print("imageCompleteUrl \(imageCompleteUrl)")
                        // self.mapInfoWindow.updateImage(imageUrl: imageurl!)
                        if let imageUrlWithCompletList = imageCompleteUrl {
                            self.mapInfoWindow.imageview.sd_setImage(with: imageUrlWithCompletList, placeholderImage: placeHolder, options: .refreshCached, progress: nil) { (image, error, cacheType, imageUrl) in
                                if image != nil {
                                    DispatchQueue.main.async {
                                        self.mapInfoWindow.imageview.image = image
                                        self.mapInfoWindow.imageview.contentMode = UIViewContentMode.scaleToFill;
                                        self.mapInfoWindow.imageData = image!
                                        self.mapInfoWindow.setNeedsDisplay()
                                        self.mapInfoWindow.setNeedsLayout()
                                    }
                                }
                                if error != nil {
                                    print("ERROREL FILE FOR IUMAGE \(error)")
                                }
                                if imageurl != nil {
                                    print(imageurl)
                                }
                            }
                        }
                    }
                }
                
                //facilities
                var facilitiesstr = dat.value(forKey: "facility_category") as? String
                
                print("facilitiesstr \(facilitiesstr)")
                
                self.mapInfoWindow.Malepic.image = UIImage.init(named: "unselectmale.png")
                self.mapInfoWindow.Femalepic.image = UIImage.init(named: "unselectfemale.png")
                self.mapInfoWindow.Unisexpic.image = UIImage.init(named: "unselectunisex.png")
                self.mapInfoWindow.wheelchairPic.image = UIImage.init(named: "unselectwheelchair.png")
                
                if(facilitiesstr?.contains("1"))!{
                    self.mapInfoWindow.Malepic.image = UIImage.init(named: "maleselect.png")
                }
                if(facilitiesstr?.contains("2"))!{
                    self.mapInfoWindow.Femalepic.image = UIImage.init(named: "Male_select.png")
                }
                if(facilitiesstr?.contains("3"))!{
                    self.mapInfoWindow.Unisexpic.image = UIImage.init(named: "Unisex_select.png")
                }
                if(facilitiesstr?.contains("4"))!{
                    self.mapInfoWindow.wheelchairPic.image = UIImage.init(named: "Wheel-Chair_select.png")
                }
                
                var accessbilitystr = dat.value(forKey: "accessbility") as? String
                print("ACCESSIBILITYT DATA \(accessbilitystr)")
                if(accessbilitystr != ""){
                    if(accessbilitystr == "1"){
                        self.mapInfoWindow.wheelchairPic.image = UIImage.init(named: "Wheel-Chair_select.png")
                        self.mapInfoWindow.carPic.image = UIImage.init(named: "carunselect.png")
                        
                    }
                    else if(accessbilitystr == "2"){
                        self.mapInfoWindow.wheelchairPic.image = UIImage.init(named: "unselectwheelchair.png")
                        self.mapInfoWindow.carPic.image = UIImage.init(named: "Car_select.png")
                    }
                    else if(accessbilitystr == "1,2"){
                        self.mapInfoWindow.wheelchairPic.image = UIImage.init(named: "Wheel-Chair_select.png")
                        self.mapInfoWindow.carPic.image = UIImage.init(named: "Car_select.png")
                        
                    }
                }
                else if(accessbilitystr == "") {
                    self.mapInfoWindow.wheelchairPic.image = UIImage.init(named: "unselectwheelchair.png")
                    self.mapInfoWindow.carPic.image = UIImage.init(named: "carunselect.png")
                    
                }
                
            }
        }
        
        self.mapInfoWindow.bgview.layer.shadowColor = UIColor.black.cgColor
        self.mapInfoWindow.bgview.layer.shadowOpacity = 0.4
        self.mapInfoWindow.bgview.layer.shadowOffset = CGSize.zero
        self.mapInfoWindow.bgview.layer.shadowRadius = 2
        self.mapInfoWindow.bgview.backgroundColor = UIColor.clear
    
     
        firstview.addSubview(self.mapInfoWindow)
       
        
        return firstview
    }
    
    
    func getDefaultImageForCategory(categoryName:String) -> String {
        var imageName = "public"
        
        if categoryName == "" {
            imageName = "public"
        } else if categoryName == "Gas Station" {
            imageName = "Gas"
        } else if categoryName == "Hotel" {
            imageName = "Hotels"
        } else if categoryName == "Restaurant" {
            imageName = "Restaurant"
        } else if categoryName == "Shopping Mall" {
            imageName = "shoping_marker"
        } else if categoryName == "Bar" {
            imageName = "bar_marker"
        } else if categoryName == "Public" {
            imageName = "Public-Icon"
        } else if categoryName == "Temp" {
            imageName = "Temp-Icon"
        } else if categoryName == "Shower" {
            imageName = "Shower-Icon"
        }
        
        return imageName
    }
    
    
    func getDefaultImageForMarker(categoryName:String) -> String {
        var imageName = "public"
        if categoryName == "Public" {
            imageName = "Public_low"
        } else if categoryName == "Temp" {
            imageName = "Temp_low"
        } else if categoryName == "Shower" {
            imageName = "Shower_low"
        } else {
            imageName = getDefaultImageForCategory(categoryName: categoryName)
        }
        return imageName
    }
    
    
    
    func focusMapToShowAllMarkers()
    {
        //CLLocationCoordinate2D firstLocation = ((GMSMarker *)markers.firstObject).position;
        DispatchQueue.main.async {
            var bounds = GMSCoordinateBounds()
            print("self.mapView.camera.zoom->\(bounds)")
            for c in 0 ..< self.Markerarray.count
            {
                let marker = self.Markerarray.object(at: c) as! GMSMarker
                bounds = bounds.includingCoordinate(marker.position)
            }
            self.viewmap.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 25.0))
            if self.Markerarray.count == 1
            {
                for c in 0 ..< self.Markerarray.count
                {
                    let marker = self.Markerarray.object(at: c) as! GMSMarker
                    let camera = GMSCameraPosition.camera(withLatitude: marker.position.latitude, longitude: marker.position.longitude, zoom: 16)
                    self.viewmap.camera = camera
                }
            }
        }
        firstTimeApi(ToiletCount: "\(self.array_All.count)")
    }
    
    func mapView(_ mapView: GMSMapView!, didTapInfoWindowOf marker: GMSMarker!)
    {
 
        let index = Markerarray.index(of: marker)
        print("LOG:didTapInfoWindowOf called")
        selectedIndex = index
    
        let selectdic = array_All.object(at: selectedIndex ?? 0) as! NSMutableDictionary
        let isFreeProvider = selectdic.value(forKey: "free_provider") as! String
        let type = selectdic.value(forKey: "type") as! String
//        if (self.isFreeBooking(categoryType: type, freeProvider: isFreeProvider)) {
//            self.quickToNav(sender: nil)
//            return
//        }
        
        /* On tap of the marker, the toilet details can be checked to see if it is a provider toilet then it will redirect to more details page without any pop up */
        if (!self.isPublicToilet(category: type)){
            self.moreInfo(sender: nil)
            return;
        }
        
        let windowMode = Bundle.main.loadNibNamed("Whatdoyouchoose", owner: self, options: nil)?[0] as! Whatdoyouchoose
        KGModal.sharedInstance().show(withContentView:windowMode, andAnimated: true)
        self.view.endEditing(true)
        
       windowMode.moreInfoButton.layer.cornerRadius = 20.0
       windowMode.moreInfoButton.clipsToBounds = true
       windowMode.quickToNav.layer.cornerRadius = 20.0
       windowMode.quickToNav.clipsToBounds = true
       windowMode.layer.cornerRadius = 20.0
       windowMode.clipsToBounds = true
       windowMode.Moreinfo.setTitle("More_Info".localized, for: .normal)
       windowMode.Quicknav.setTitle("Quick_Nav".localized, for: UIControlState.normal)
       windowMode.moreInfoButton.addTarget(self, action: #selector(MainScreenViewController.moreInfo(sender:)), for: UIControlEvents.touchUpInside)
       windowMode.quickToNav.addTarget(self, action: #selector(MainScreenViewController.quickToNav(sender:)), for: UIControlEvents.touchUpInside)
       
    }
    
    func moreInfo(sender:UIButton!){
        
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "NewDetailsViewController") as! NewDetailsViewController
        secondViewController.selectdic = array_All.object(at: selectedIndex ?? 0) as! NSMutableDictionary
        secondViewController.passcurrentloc = passloc
        KGModal.sharedInstance().hide(animated: true)
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
   
    func quickToNav(sender:UIButton!)
    {
        let selectdic = array_All.object(at: selectedIndex ?? 0) as! NSMutableDictionary
        let isFreeProvider = selectdic.value(forKey: "free_provider") as! String
        let category = selectdic.value(forKey: "type") as! String
        let toiletId = selectdic.value(forKey: "loo_id") as! String
        print(toiletId)
        
        KGModal.sharedInstance().hide(animated: true)
        
        if (self.isFreeBooking(categoryType: category, freeProvider: isFreeProvider)) {
            self.bookinglink()
        } else {
            if (self.isLoginRequired(categoryType: category,freeProvider: isFreeProvider)) {
                let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                self.navigationController?.pushViewController(mainview, animated: true)
            } else {
                self.bookinglink()
            }
        }
    }
    
    @IBAction func filtersbuttonclick(sender: AnyObject)
    {
       
        FIRAnalytics.logEvent(withName: "filter_click", parameters: [
            "button_name": "filter_button",
            "content_type" : "UIButton",
            "login_device" : "iOS"
            ])
        let next = self.storyboard?.instantiateViewController(withIdentifier: "NewFiltersViewController") as! NewFiltersViewController
       let nav = UINavigationController.init(rootViewController: next)
       self.present(nav, animated: true, completion: nil)
    }
    
    
    func load(URL: NSURL) {
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        let request = NSMutableURLRequest(url: URL as URL)
        request.httpMethod = "GET"
        let task = session.dataTask(with: request as URLRequest) {(data, response, error) -> Void in
            if (error == nil) {
                // Success
                let statusCode = (response as! HTTPURLResponse).statusCode
                print("Success: \(statusCode)")
                DispatchQueue.main.async {
                    self.mapInfoWindow.imageview.image = UIImage(data: data!)
                }
            }
            else {
                // Failure
                print("Failure: %@", error?.localizedDescription);
            }
        }
        task.resume()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func bookinglink()
    {
        let selectdic = array_All.object(at: selectedIndex ?? 0) as! NSMutableDictionary
        //var publicToilets = array_Public
        
        let passLooId = selectdic.value(forKey: "loo_id") as? String
        let passLocation = selectdic.value(forKey: "loo_location") as? String
        let toiletId = selectdic.value(forKey: "loo_id") as! String
        
        print(toiletId)
        
        if AFNetworkReachabilityManager.shared().isReachable
        {
            let userId = passUserId
            //If the user is not logged in then the directly take to navigation screen
            if UserDefaults.standard.object(forKey: "Logindetail") == nil {
                let next = self.storyboard?.instantiateViewController(withIdentifier: "MovetoNavigateViewController") as! MovetoNavigateViewController
                next.selectdic = self.array_All.object(at: self.selectedIndex ?? 0) as! NSMutableDictionary
                next.passLocationDetails = passloc
                next.selectdic.setValue(toiletId, forKey: "loo_ids")
                next.bookingidstr = "0"
                
                self.navigationController?.pushViewController(next, animated: true)
            } else {
                let urlstring = String(format:"%@/add_booking.php?user_id=%@&loo_id=%@&current_location=%@",kBaseURL,userId!,passLooId!,passLocation!)
                print(urlstring)
                self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: false, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                    
                    if Stats == true
                    {
                        NSLog("camerObject=%@", jsonObject);
                        if jsonObject.count != 0
                        {
                            var str = (jsonObject.object(at: 0) as AnyObject).value(forKey: "message") as? String
                            KGModal.sharedInstance().hide(animated: true)
                            
                            if str == "success"
                            {
                                let next = self.storyboard?.instantiateViewController(withIdentifier: "MovetoNavigateViewController") as! MovetoNavigateViewController
                                next.selectdic = self.array_All.object(at: self.selectedIndex ?? 0) as! NSMutableDictionary
                                next.passLocationDetails = self.passloc
                                next.bookingidstr = (jsonObject.object(at: 0) as AnyObject).value(forKey: "booking_id") as? String
                                self.navigationController?.pushViewController(next, animated: true)
                                
                            }
                            else
                            {
                                if str == self.serverResponseForAlreadyBooked {
                                    str = "Ongoing_booking_message".localized
                                    self.showAlertMessage(title: "WCi", message: str!, cancelButtonString: "Ok", identifier: self.alreadyBookedAlertIdentifier, isActionNeeded: true)
                                } else {
                                    self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:str!, withIdentifier:"")
                                }
                            }
                        }
                    }
                        
                    else
                    {
                        NSLog("jsonObject=error");
                        
                    }
                })
            }
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
            
        }
    }
    
    func showAlertMessage(title:String, message:String, cancelButtonString:String, identifier:String, isActionNeeded:Bool) {
        
        let alert = UIAlertView(title:title, message:message, delegate:self, cancelButtonTitle:cancelButtonString)
        alert.restorationIdentifier = identifier;
        alert.show()
    }
    
    
    func  isPublicToilet(category:String) -> Bool {
        return (category == "Public")
    }
    
    //Method to check if login is required
    func isLoginRequired(categoryType: String, freeProvider:String) -> Bool {
        if isPublicToilet(category: categoryType) || (freeProvider == "1")  {
            return false
        } else if (UserDefaults.standard.object(forKey: "Logindetail") != nil) {
            return false
        } else {
            return true
        }
    }
    
    //Method to check is free booking
    func isFreeBooking(categoryType: String, freeProvider:String) -> Bool {
        if isPublicToilet(category: categoryType) || (freeProvider == "1")  {
            return true
        } else {
            return false
        }
    }
}


func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
    UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
    image.draw(in: CGRect(origin: CGPoint.zero, size: CGSize(width: newSize.width, height: newSize.height)))
    let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    return newImage
}
public extension UIView {
    
    public var top: CGFloat {
        get { return self.frame.origin.y }
        set { self.frame.origin.y = newValue }
    }
    public var left: CGFloat {
        get { return self.frame.origin.x }
        set { self.frame.origin.x = newValue }
    }
    // swiftlint:disable:next identifier_name
    public var x: CGFloat {
        get { return self.frame.origin.x }
        set { self.frame.origin.x = newValue }
    }
    // swiftlint:disable:next identifier_name
    public var y: CGFloat {
        get { return self.frame.origin.y }
        set { self.frame.origin.y = newValue }
    }
    public var width: CGFloat {
        get { return self.frame.size.width }
        set { self.frame.size.width = newValue }
    }
    public var height: CGFloat {
        get { return self.frame.size.height }
        set { self.frame.size.height = newValue }
    }
    public var right: CGFloat {
        get { return self.frame.origin.x + self.width }
        set { self.frame.origin.x = newValue - self.width }
    }
    public var bottom: CGFloat {
        get { return self.frame.origin.y + self.height }
        set { self.frame.origin.y = newValue - self.height }
    }
    public var centerX: CGFloat {
        get { return self.center.x }
        set { self.center = CGPoint(x: newValue, y: self.centerY) }
    }
    public var centerY: CGFloat {
        get { return self.center.y }
        set { self.center = CGPoint(x: self.centerX, y: newValue) }
    }
    public var origin: CGPoint {
        set { self.frame.origin = newValue }
        get { return self.frame.origin }
    }
    public var size: CGSize {
        set { self.frame.size = newValue }
        get { return self.frame.size }
    }
}
