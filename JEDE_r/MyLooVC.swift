//
//  MyLooVC.swift
//  JEDE_r
//
//  Created by Exarcplus on 9/21/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import Foundation

class MyLooVC: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    
    var Userdic = NSMutableDictionary()
    var passuserid : String!
    
    struct Objects {
        var looId : String!
        var looName : String
        var looImageUrl:String!
        var looCategoryType:String!
    }
    
    var looId1:String = ""
    var looName1:String = ""
    var looImageUrl1:String = ""
    var looCategoryType1:String = ""
    
    @IBOutlet weak var looTable: UITableView!
    
    var objectArray = [Objects]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = false
        looTable.delegate = self
        looTable.dataSource = self
        
        if UserDefaults.standard.object(forKey: "Logindetail") == nil
        {
            passuserid = ""
        }
        else
        {
            let placesData = UserDefaults.standard.object(forKey: "Logindetail") as? NSDictionary
            self.Userdic.removeAllObjects()
            if placesData != nil{
                self.Userdic.addEntries(from: (placesData as? [String : Any])!)
            }
            print(self.Userdic)
            passuserid = self.Userdic.value(forKey: "user_id") as? String
            print(passuserid)
        }
        downloadLooList(id: passuserid)
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backtoSideMenu(_ sender: UIBarButtonItem) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.SideMenu.showLeftView(animated: true, completionHandler: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadsidemenu"), object: nil, userInfo: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objectArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier:"cell", for: indexPath) as? MyLooCell{
            
            //cell.memberImage.image = UIImage(named:"1862205")
            
            
            cell.looImage.setShowActivityIndicator(true)
            cell.looImage.setIndicatorStyle(.gray)
            let imageURL = objectArray[indexPath.row].looImageUrl
            
            if (imageURL?.isEmpty)! {
                let imageUrl = self.getDefaultImageForCategory(categoryName: objectArray[indexPath.row].looCategoryType)
                cell.looImage.image = UIImage(named: imageUrl)
            } else {
                cell.looImage.sd_setImage(with: URL(string: objectArray[indexPath.row].looImageUrl))
            }
            cell.looName.text = objectArray[indexPath.row].looName
            
            return cell
        }
        else {
            return UITableViewCell()
        }
    }
    
    func getDefaultImageForCategory(categoryName:String) -> String {
        var imageName = "public"
        
        if categoryName == "" {
            imageName = "public"
        } else if categoryName == "Gas Station" {
            imageName = "Gas"
        } else if categoryName == "Hotel" {
            imageName = "Hotels"
        } else if categoryName == "Restaurant" {
            imageName = "Restaurant"
        } else if categoryName == "Shopping Mall" {
            imageName = "shoping_marker"
        } else if categoryName == "Bar" {
            imageName = "bar_marker"
        } else if categoryName == "Public" {
            imageName = "Public-Icon"
        } else if categoryName == "Temp" {
            imageName = "Temp-Icon"
        } else if categoryName == "Shower" {
            imageName = "Shower-Icon"
        }
        
        return imageName
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(objectArray[indexPath.row])
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "AddLooViewController") as! AddLooViewController
//        let navi = UINavigationController.init(rootViewController: nextViewController)
        nextViewController.vendorlooId = objectArray[indexPath.row].looId
        nextViewController.isEditMode = true
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    func downloadLooList(id:String){
        
        let params:[String:String] = ["vendor_id":id]
        let Url = URL(string:kBaseURL + "/vendor_loo_list.php")!
        Alamofire.request(Url,method:.get,parameters:params,encoding: URLEncoding()).responseJSON{ response in
            let result = response.result
            print(Url)
            print(params)
            print(response)
            print(result)
            if let dict = result.value as? Dictionary<String,AnyObject>{
                if let values = dict["result"] as? [Dictionary<String,AnyObject>]
                {
                    self.objectArray = [Objects]()
                    for obj in values {
                        if let loo_id = obj["loo_id"] as? String {
                            self.looId1 = loo_id
                        }
                        if let loo_name = obj["loo_name"] as? String {
                            self.looName1 = loo_name
                        }
                        if let image = obj["image"] as? String {
                            self.looImageUrl1 = image
                        }
                        
                        if let categoryName = obj["category_name"] as? String {
                            self.looCategoryType1 = categoryName
                        }
                        self.objectArray.append(Objects(looId:self.looId1,looName:self.looName1,looImageUrl:self.looImageUrl1,looCategoryType: self.looCategoryType1))
                    }
                }
            }
            print(self.objectArray)
            self.looTable.reloadData()
        }
    }
}
