//
//  UserProfileViewController.swift
//  JEDE_r
//
//  Created by Exarcplus on 20/05/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import KKActionSheet
import AFNetworking
import SDWebImage
import MessageUI
var editButton = 0

class UserProfileViewController: UIViewController,UIActionSheetDelegate,PECropViewControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate, UICollectionViewDelegate,UICollectionViewDataSource, MFMailComposeViewControllerDelegate
{
    var myclass : MyClass!
    @IBOutlet weak var scrollview : UIScrollView!
    @IBOutlet weak var namelab : UILabel!
    @IBOutlet weak var mobilenolab : UILabel!
    @IBOutlet weak var emaillab : UILabel!
    
    @IBOutlet weak var email: UILabel!
    
    @IBOutlet weak var mobilenumber: UILabel!
    
    @IBOutlet weak var inviteButton: UIButton!
    var Userdic = NSMutableDictionary()
    var passimagename : String!
    var passuserid : String!
    var imageHud:MyHUDProgress!
    var checkstr : String!
    var recentlylisarr = NSMutableArray()
    var updatearr = NSMutableArray()
    
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var recentorloo: UILabel!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var changepasswordbutton : UIButton!
    @IBOutlet weak var collectionview : UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        //Add notification observer
        
        myclass = MyClass()
        self.navigationController?.navigationBar.isHidden = false
        checkstr = "openedit"
        
        self.scrollview.removeTwitterCover()
        self.passimagename = ""
        if UserDefaults.standard.object(forKey: "Logindetail") != nil
        {
           let placesData = UserDefaults.standard.object(forKey: "Logindetail") as? NSDictionary
            self.Userdic.removeAllObjects()
            if placesData != nil{
                self.Userdic.addEntries(from: (placesData as? [String : Any])!)
            }
            print(self.Userdic)
            passuserid = self.Userdic.value(forKey: "user_id") as? String
            print(passuserid)
            let type = self.Userdic.value(forKey: "login_using") as? String
            let vs = self.Userdic.value(forKey: "vendor_status") as? String
            
            print(type)
            if type == "WCi"
            {
                self.changepasswordbutton.isHidden = false
            }
            else
            {
                self.changepasswordbutton.isHidden = true
            }
            emaillab.text = self.Userdic.value(forKey: "email") as? String
            mobilenolab.text = self.Userdic.value(forKey: "mobile_num") as? String
            namelab.text = self.Userdic.value(forKey: "user_name") as? String
            //scrollview.addTwitterCover(with: UIImage(named: "Toilets-Defualt.png"))
            
            let imageurl = self.Userdic.value(forKey: "image") as? String
            print(imageurl)
            
            
            
            if vs == "1"
            {
                mylooslink()
                recentorloo.text = "My Loos"
            }
            else
            {
            self.recentlyviewlink()
            }
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
        if(UserDefaultManager.sharedManager.getLanguageName() == "English"){
            self.title = "PROFILE"
            self.editBtn.setTitle("Edit", for: .normal)
            self.email.text = "Email"
            self.mobilenumber.text = "Mobile Number"
            self.recentorloo.text = "Recently Views"
            self.changepasswordbutton.setTitle("Change Password", for: .normal)
            self.inviteButton.setTitle("profile_layout_invite".localized, for: .normal)
        }
        else if(UserDefaultManager.sharedManager.getLanguageName() == "German"){
            self.title = "PROFIL"
            self.editBtn.setTitle("Bearbeiten", for: .normal)
            self.email.text = "Email"
            self.mobilenumber.text = "Handynummer"
            self.recentorloo.text = "Kürzlich Aufrufe"
            self.changepasswordbutton.setTitle("Passwort ändern", for: .normal)
            self.inviteButton.setTitle("profile_layout_invite".localized, for: .normal)
        }
        else if(UserDefaultManager.sharedManager.getLanguageName() == "French"){
            self.title = "PROFIL"
            self.editBtn.setTitle("modifier", for: .normal)
            self.email.text = "Email"
            self.mobilenumber.text = "Numéro de portable"
            self.recentorloo.text = "Vues récentes"
            self.changepasswordbutton.setTitle("Changer le mot de passe", for: .normal)
            self.inviteButton.setTitle("profile_layout_invite".localized, for: .normal)
        }
        
        if UserDefaults.standard.object(forKey: "Logindetail") != nil
        {
            let placesData = UserDefaults.standard.object(forKey: "Logindetail") as? NSDictionary
            print("PLACES DATA")
            if placesData != nil{
                self.Userdic.removeAllObjects()
                self.Userdic.addEntries(from: (placesData as? [String : Any])!)
            }
            print(self.Userdic)
            passuserid = self.Userdic.value(forKey: "user_id") as? String
            print(passuserid)
            emaillab.text = self.Userdic.value(forKey: "email") as? String
            mobilenolab.text = self.Userdic.value(forKey: "mobile_num") as? String
            namelab.text = self.Userdic.value(forKey: "user_name") as? String
            let imageurl = self.Userdic.value(forKey: "image") as? String
            if imageurl == "" || imageurl?.characters.count == 0 || imageurl == nil {
                scrollview.addTwitterCover(with: UIImage(named: "NewLogo"))
            } else {
                let url = URL(string: imageurl!)
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                if data != nil {
                    scrollview.addTwitterCover(with: UIImage(data: data!))
                }
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    override func viewWillAppear(_ animated: Bool)
    {
        if UserDefaults.standard.object(forKey: "Logindetail") == nil
        {
            
        }
        else
        {
            let placesData = UserDefaults.standard.object(forKey: "Logindetail") as? NSData
            self.Userdic.removeAllObjects()
            self.Userdic.addEntries(from: NSKeyedUnarchiver.unarchiveObject(with: placesData! as Data) as! NSDictionary as [NSObject : AnyObject])
            print(self.Userdic)
            //            passuserid = self.Userdic.value(forKey: "user_id") as? String
            //            print(passuserid)
            emaillab.text = self.Userdic.value(forKey: "email") as? String
            mobilenolab.text = self.Userdic.value(forKey: "mobile_num") as? String
            namelab.text = self.Userdic.value(forKey: "user_name") as? String
            
     
            let imageurl = self.Userdic.value(forKey: "image") as? String
            print(imageurl!)
            if imageurl == "" || imageurl?.characters.count == 0 || imageurl == nil
            {
                scrollview.addTwitterCover(with: UIImage(named: "Toilets-Defualt.png"))
            }
            else
            {
                let url = URL(string: imageurl!)
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                scrollview.addTwitterCover(with: UIImage(data: data!))
            }
        }
    }
    */
    
    @IBAction func OpenSideMenu(_ x:AnyObject)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.SideMenu.showLeftView(animated: true, completionHandler: nil)
          NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadsidemenu"), object: nil, userInfo: nil)
    }
    
    @IBAction func Editbutton(_ x:AnyObject)
    {
        checkstr = "openedit"
        let actionsheet = KKActionSheet.init(title:"Edit Profile", delegate:self, cancelButtonTitle:"Cancel", destructiveButtonTitle:nil ,otherButtonTitles:"Edit Image","Edit Profile")
        actionsheet.show(in: self.view)
        self.view.endEditing(true)
    }
    
    
    // MARK: - Action Sheet
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        if checkstr == "openedit"
        {
            if buttonIndex == 1
            {
                checkstr = "opencamera"
                let actionsheet = KKActionSheet.init(title:"Change Picture", delegate:self, cancelButtonTitle:"Cancel", destructiveButtonTitle:nil ,otherButtonTitles:"take_photo".localized,"choose_from_gallery".localized)
                actionsheet.show(in: self.view)
                self.view.endEditing(true)
                
            }
            else if buttonIndex == 2
            {
                self.scrollview.removeTwitterCover()
                let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditViewController") as! EditViewController
                secondViewController.previousdic = self.Userdic
                self.navigationController?.pushViewController(secondViewController, animated: true)
            }
        }
        else
        {
            if buttonIndex == 1
            {
                let imagePickerController = UIImagePickerController()
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
                    imagePickerController.delegate = self
                    imagePickerController.sourceType = UIImagePickerControllerSourceType.camera;
                    imagePickerController.allowsEditing = false
                    UIApplication.shared.statusBarStyle = UIStatusBarStyle.default;
                    self.present(imagePickerController, animated: true, completion: nil)
                    UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent;
                }
                
            }
            else if buttonIndex == 2
            {
                let imagePickerController = UIImagePickerController()
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
                    imagePickerController.delegate = self
                    imagePickerController.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
                    imagePickerController.allowsEditing = false
                    UIApplication.shared.statusBarStyle = UIStatusBarStyle.default;
                    self.present(imagePickerController, animated: true, completion: nil)
                }
            }
        }
        
    }
    
    // MARK: - Image Picker Delegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        picker .dismiss(animated: true, completion: nil)
        let media =  info[UIImagePickerControllerOriginalImage] as? UIImage
        
        let controller = PECropViewController();
        controller.delegate = self;
        controller.image = media;
        controller.toolbarItems=nil;
        let ratio:CGFloat = 16.0 / 9.0;
        controller.cropAspectRatio = ratio;
        
        let navigationController = UINavigationController.init(rootViewController: controller);
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            navigationController.modalPresentationStyle = UIModalPresentationStyle.formSheet;
        }
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker .dismiss(animated: true, completion: nil)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent;
    }
    
    func cropViewController(_ controller: PECropViewController!, didFinishCroppingImage croppedImage: UIImage!, transform: CGAffineTransform, cropRect: CGRect)
    {
        let ims:UIImage=croppedImage
        controller.dismiss(animated: true, completion:nil)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent;
        self.UploadimageWithImage(image: ims, size:CGSize(width: 200, height: 200))
    }
    func cropViewControllerDidCancel(_ controller: PECropViewController!)
    {
        controller.dismiss(animated: true, completion:nil)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent;
    }
    
    func UploadimageWithImage(image:UIImage,size:CGSize)
    {
        UIGraphicsBeginImageContext(size)
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let newimage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        let date=NSDate() as NSDate;
        let form = DateFormatter() as DateFormatter
        form.dateFormat="yyyy-MM-dd"
        let form1 = DateFormatter() as DateFormatter
        form1.dateFormat="HH:MM:SS"
        let datesstr = form.string(from: date as Date);
        let timestr = form1.string(from: date as Date);
        let datearr = datesstr.components(separatedBy: "-") as NSArray
        let timearr = timestr.components(separatedBy: ":") as NSArray
        let imageData = UIImageJPEGRepresentation(newimage,0.7);
        let imagename = String(format:"Pro_%@_%@_%@_%@_%@_%@.png",datearr.object(at: 0) as! String,datearr.object(at: 1) as! String,datearr.object(at: 2) as! String,timearr.object(at: 0) as! String,timearr.object(at: 1) as! String,timearr.object(at: 2) as! String)
        if AFNetworkReachabilityManager.shared().isReachable
        {
            let  yval = UserDefaults()
            yval.setValue("20", forKey: "yval")
            yval.setValue("#0063AD", forKey:"loadcolor")
            yval.setValue("photo", forKey: "Type")
            
            imageHud = MyHUDProgress.init(view: self.view, yavl: 20);
            imageHud.hidehud(20.0)
            //            imageHud.frame = CGRectMake(0,0,UIScreen.main.bounds.size.width,UIScreen.mainScreen.bounds.size.height);
            imageHud.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            self.view.addSubview(imageHud)
            
            let request : NSMutableURLRequest = AFHTTPRequestSerializer().multipartFormRequest(withMethod: "POST", urlString:kBaseDomainUrl + "/upload_profile_image.php", parameters: nil, constructingBodyWith: { (formData:AFMultipartFormData) -> Void in
                
                formData.appendPart(withFileData: imageData!, name:"uploaded_file", fileName:imagename, mimeType: "image/png")
                
            }, error: nil)
            
            let conf : URLSessionConfiguration = URLSessionConfiguration.default
            
            let manager : AFURLSessionManager = AFURLSessionManager(sessionConfiguration: conf)
            
            manager.responseSerializer = AFHTTPResponseSerializer()
            let uploadTask:URLSessionUploadTask = manager.uploadTask(withStreamedRequest: request as URLRequest, progress:{(uploadProgress:Progress!) -> Void in
                
                DispatchQueue.main.async {
                    self.imageHud.progress.setProgress(Float(uploadProgress.fractionCompleted), animated: true)
                }
            })
            {(response, responseObject, error) -> Void in
                
                print(error)
                self.imageHud.removeFromSuperview();
                self.imageHud = nil;
                if((error == nil))
                {
                    print("%@ %@", response, responseObject);
                    self.scrollview.removeTwitterCover()
                    self.scrollview.addTwitterCover(with: image)
                    self.passimagename = String(format: kBaseDomainUrl + "/images/user/%@", imagename)
                    print(self.passimagename)
                    
                    let urlstring = String(format:"%@/profile_update.php?user_id=%@&user_name=%@&mobile_num=%@&address=&image=%@",kBaseURL,self.passuserid,self.namelab.text!,self.mobilenolab.text!,self.passimagename);
                    print(urlstring)
                    self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: true, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                        if Stats == true
                        {
                            NSLog("jsonObject=%@", jsonObject);
                            let message = (jsonObject.object(at: 0) as AnyObject).value(forKey:"message") as! String
                            if message == "success"
                            {
                                NSLog("jsonObject=%@", jsonObject);
                                print("BEFORE")
                                print(self.updatearr)
                                self.updatearr = jsonObject.mutableCopy() as! NSMutableArray
                                print("DATA from Server \(self.updatearr)")
                                UserDefaults.standard.set(self.updatearr.object(at: 0), forKey:"Logindetail")
                                UserDefaults.standard.synchronize()
                                
                                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DispatchTimeInterval.seconds(4), execute: {
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadLogin"), object: nil, userInfo: nil)
                                })
                            }
                            else
                            {
                                self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Please Check Your Username and Password"), withIdentifier:"")
                            }
                        }
                        else
                        {
                            
                        }
                    })
                    
                }
                else
                {
                    self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert: self.myclass.StringfromKey("Errorwhieleupload"), withIdentifier:"error")
                }
            }
            
            uploadTask.resume()
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
    
    @IBAction func changepasswordbutton(_sender: UIButton)
    {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
        secondViewController.previousdic = self.Userdic
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    
    func recentlyviewlink()
    {
        if AFNetworkReachabilityManager.shared().isReachable
        {
            if(passuserid == nil){
                passuserid = ""
            }
            let urlstring = String(format:"%@/recently_view_list.php?user_id=%@",kBaseURL,passuserid)
            print(urlstring)
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: false, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    if jsonObject.count != 0
                    {
                        self.recentlylisarr .removeAllObjects()
                        self.recentlylisarr.addObjects(from: jsonObject as [AnyObject])
                        print(self.recentlylisarr)
                        self.collectionview.reloadData()
                    }
                }
                else
                {
                    NSLog("jsonObject=error");
                }
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return self.recentlylisarr.count
    }
    
    //        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize
    //        {
    //            let numberOfCellInRow : Int = 4
    //            let padding : CGFloat = 20
    //            let collectionCellWidth : CGFloat = ((collectionView.frame.size.width - padding) / CGFloat(numberOfCellInRow))
    //            let collectionCellHeight : CGFloat = (collectionView.frame.size.width / CGFloat(numberOfCellInRow)) + 45
    //            return CGSize(width: collectionCellWidth , height: collectionCellHeight)
    //        }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecentlyViewCollectionViewCell", for: indexPath) as! RecentlyViewCollectionViewCell
        let dic = self.recentlylisarr.object(at: (indexPath as NSIndexPath).row) as! NSDictionary;
        print(dic)
        let imageurl = dic.value(forKey: "image") as? String;
        if imageurl != nil {
        if !imageurl!.isEmpty
        {
            if imageurl?.characters.count == 0 || imageurl == "" || imageurl == "http://stitchplus.com/BNI 3CTpanel/"
            {
                //cell.gallery_image.image = UIImage.init(named: "ios_default_image.png")
            }
            else
            {
                let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                    print(self)
                }
                let imageAbsoluteUrlList:[String] = (imageurl?.components(separatedBy: ","))!
                let imageAbsoluteUrl = imageAbsoluteUrlList.first
                cell.imgview.sd_setImage(with: URL(string:imageAbsoluteUrl!) as URL!, completed: block)
                cell.imgview.contentMode = UIViewContentMode.scaleToFill;
            }
        }
        }
    
        let str = dic.value(forKey: "loo_name") as? String
        if str == ""
        {
            cell.loonamelab.text = ""
        }
        else
        {
            cell.loonamelab.text = str
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        editButton = 1
        
        print(indexPath.row)
        
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "NewDetailsViewController") as! NewDetailsViewController
        secondViewController.selectdic = recentlylisarr.object(at: indexPath.row) as! NSMutableDictionary
        secondViewController.passcurrentloc = " "
        self.navigationController?.pushViewController(secondViewController, animated: true)
        
        
       
    }
    
    @IBAction func invitebutton(_ sender: UIButton)
    {
       //self.sendEmail()
        
        let string: String = kAppstoreLink
        //let URL: NSURL = ...
        
        let activityViewController = UIActivityViewController(activityItems: [string], applicationActivities: nil)
        navigationController?.present(activityViewController, animated: true) {
            // ...
        }
    }
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["support@mail.com"])
            mail.setSubject("Jede_r App")
            mail.setMessageBody("<p>Send us your issue!</p>", isHTML: true)
            present(mail, animated: true, completion: nil)
            
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func mylooslink()
    {
        if AFNetworkReachabilityManager.shared().isReachable
        {
            let urlstring = String(format:"%@/vendor_loo_list.php?vendor_id=%@",kBaseURL,passuserid)
            print(urlstring)
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: false, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    if jsonObject.count != 0
                    {
                        self.recentlylisarr .removeAllObjects()
                        self.recentlylisarr.addObjects(from: jsonObject as [AnyObject])
                        print(self.recentlylisarr)
                        self.collectionview.reloadData()
                    }
                }
                else
                {
                    NSLog("jsonObject=error");
                }
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
}
