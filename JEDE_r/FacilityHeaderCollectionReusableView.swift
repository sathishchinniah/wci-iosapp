//
//  FacilityHeaderCollectionReusableView.swift
//  JEDE_r
//
//  Created by Exarcplus on 21/06/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit

class FacilityHeaderCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var headernamelab : UILabel!
    @IBOutlet weak var headerimg : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
