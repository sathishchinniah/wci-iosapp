//
//  DateandTimeViewController.swift
//  JEDE_r
//
//  Created by Exarcplus on 06/06/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit

var timesave = [String]()
class DateandTimeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, IQActionSheetPickerViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, BSHourPickerDelegate, UITextViewDelegate {
    
    @IBOutlet weak var moncollection : UICollectionView!
    @IBOutlet weak var tuecollection : UICollectionView!
    @IBOutlet weak var SubmitLabel: UIBarButtonItem!
    @IBOutlet weak var wedcollection : UICollectionView!
    @IBOutlet weak var thucollection : UICollectionView!
    @IBOutlet weak var fricollection : UICollectionView!
    @IBOutlet weak var satcollection : UICollectionView!
    @IBOutlet weak var suncollection : UICollectionView!
    var lastdic = NSMutableDictionary()
    var availabilityArray: [[String: String]] = []
   
    var timearr = NSMutableArray()
    var dayarr = NSMutableArray()
    var selecteddayarr = NSMutableArray()
    var selectedtime:Int!
    var tableindex:Int!
    var str : String!
    var checkopenorclose : String!
    var openclosetimearr = NSMutableArray()
    var sponser : TimeAddingPopup!
    var timePicker = MIDatePicker.getFromNib()
    var dateFormatter = DateFormatter()
    var txtHours = BSHourPicker()
    
    let prefixSunday = "SUN"
    let prefixMonday = "MON"
    let prefixTuesday = "TUE"
    let prefixWednesday = "WED"
    let prefixThrusday = "THU"
    let prefixFriday = "FRI"
    let prefixSaturday = "SAT"
    let navigationTitleKey = "Opening_hours"
    let addTimingsLimit = 3
    
    var time = ["12:00 AM","01:00 AM","02:00 AM","03:00 AM","04:00 AM","05:00 AM","06:00 AM","07:00 AM","08:00 AM","09:00 AM","10:00 AM","11:00 AM","12:00 PM","01:00 PM","02:00 PM","03:00 PM","04:00 PM","05:00 PM","06:00 PM","07:00 PM","08:00 PM","09:00 PM","10:00 PM","11:00 PM"]
    
    var timeDisp = Array(repeating: Array(repeating: 0, count: 24), count: 7)
    
    @IBOutlet weak var collectionView: UICollectionView!
    let dateCellIdentifier = "DateCellIdentifier"
    let contentCellIdentifier = "ContentCellIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        for i in 0...23
//        {
//            for j in 0...6{
//                
//                timeDisp[i][j] = 0
//                
//            }
//        }
        
        //print(timeDisp)
        
       print(timeDisp)
        timearr.add("12:00 AM");
        timearr.add("01:00 AM");
        timearr.add("02:00 AM");
        timearr.add("03:00 AM");
        timearr.add("04:00 AM");
        timearr.add("05:00 AM");
        timearr.add("06:00 AM");
        timearr.add("07:00 AM");
        timearr.add("08:00 AM");
        timearr.add("09:00 AM");
        timearr.add("10:00 AM");
        timearr.add("11:00 AM");
        timearr.add("12:00 PM");
        timearr.add("01:00 PM");
        timearr.add("02:00 PM");
        timearr.add("03:00 PM");
        timearr.add("04:00 PM");
        timearr.add("05:00 PM");
        timearr.add("06:00 PM");
        timearr.add("07:00 PM");
        timearr.add("08:00 PM");
        timearr.add("09:00 PM");
        timearr.add("10:00 PM");
        timearr.add("11:00 PM");
        
        dayarr.add("");
        dayarr.add(prefixSunday.localized);
        dayarr.add(prefixMonday.localized);
        dayarr.add(prefixTuesday.localized);
        dayarr.add(prefixWednesday.localized);
        dayarr.add(prefixThrusday.localized);
        dayarr.add(prefixFriday.localized);
        dayarr.add(prefixSaturday.localized);
        

        str = "show"
 
        self.title = "Opening_hours".localized
        self.collectionView .register(UINib(nibName: "DateCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: dateCellIdentifier)
        self.collectionView .register(UINib(nibName: "ContentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: contentCellIdentifier)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.rightBarButtonItem = nil
        let rightBarButton = UIBarButtonItem(title: "add_data_time_submit".localized, style: .done, target: self, action: #selector(Submitbutton(_:)))
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK - UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 8
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(self.timearr)
        return self.timearr.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let dateCell : DateCollectionViewCell = collectionView .dequeueReusableCell(withReuseIdentifier: dateCellIdentifier, for: indexPath) as! DateCollectionViewCell
                dateCell.backgroundColor = UIColor.white
                dateCell.dateLabel.font = UIFont.systemFont(ofSize: 13)
                dateCell.dateLabel.textColor = UIColor.black
                dateCell.dateLabel.text = "Day".localized
                
                return dateCell
            }
            else
            {
                let contentCell : ContentCollectionViewCell = collectionView .dequeueReusableCell(withReuseIdentifier: contentCellIdentifier, for: indexPath) as! ContentCollectionViewCell
                contentCell.contentLabel.font = UIFont.systemFont(ofSize: 13)
                contentCell.contentLabel.textColor = UIColor.black
//                contentCell.contentLabel.text = "Section"
                contentCell.leftlab.isHidden = true
                contentCell.rightlab.isHidden = true
                contentCell.img.isHidden = true
                contentCell.contentLabel.isHidden = false
                contentCell.contentLabel.text = self.timearr.object(at: indexPath.row) as? String
                contentCell.centerlab.isHidden = true
                
                
                if indexPath.section % 2 != 0 {
                    contentCell.backgroundColor = UIColor(white: 242/255.0, alpha: 1.0)
                } else {
                    contentCell.backgroundColor = UIColor.white
                }
                
                return contentCell
            }
        } else {
            if indexPath.row == 0 {
                let dateCell : DateCollectionViewCell = collectionView .dequeueReusableCell(withReuseIdentifier: dateCellIdentifier, for: indexPath) as! DateCollectionViewCell
                dateCell.dateLabel.font = UIFont.systemFont(ofSize: 13)
                dateCell.dateLabel.textColor = UIColor.black
//                dateCell.dateLabel.text = String(indexPath.section)
                let LanguageDateLabel = self.dayarr.object(at: indexPath.section) as? String
               dateCell.dateLabel.text = LanguageDateLabel?.localized
               print(dateCell.dateLabel.text)
                
 
                
                if indexPath.section % 2 != 0 {
                    dateCell.backgroundColor = UIColor(white: 242/255.0, alpha: 1.0)
                } else {
                    dateCell.backgroundColor = UIColor.white
                }
                
                return dateCell
            } else {
                let contentCell : ContentCollectionViewCell = collectionView .dequeueReusableCell(withReuseIdentifier: contentCellIdentifier, for: indexPath) as! ContentCollectionViewCell
//                contentCell.contentLabel.font = UIFont.systemFont(ofSize: 13)
//                contentCell.contentLabel.textColor = UIColor.black
//                contentCell.contentLabel.text = "Content"
                
                contentCell.leftlab.isHidden = false
                contentCell.leftlab.backgroundColor = UIColor.lightGray
                contentCell.rightlab.isHidden = false
                contentCell.rightlab.backgroundColor = UIColor.lightGray
                contentCell.img.isHidden = false
                contentCell.img.image = UIImage.init(named: "Plusimg.png")
                contentCell.contentLabel.isHidden = true
                contentCell.centerlab.isHidden = false
 
                
                if indexPath.section % 2 != 0 {
                    contentCell.backgroundColor = UIColor(white: 242/255.0, alpha: 1.0)
                } else {
                    contentCell.backgroundColor = UIColor.white
                }
                print(indexPath.section,indexPath.row)
                print(indexPath.section,indexPath.row)
                
                print(timeDisp[indexPath.section-1][indexPath.row])
                print(timeDisp)
                
                if timeDisp[indexPath.section-1][indexPath.row] == 1
                {
                    contentCell.leftlab.isHidden = false
                    contentCell.leftlab.backgroundColor = UIColor.init(red: 0.000, green: 0.749, blue: 0.765, alpha: 1.0)
                    contentCell.rightlab.isHidden = false
                    contentCell.rightlab.backgroundColor = UIColor.init(red: 0.000, green: 0.749, blue: 0.765, alpha: 1.0)
                    contentCell.img.isHidden = true
                    contentCell.img.image = UIImage.init(named: "Circle.png")
                    contentCell.contentLabel.isHidden = true
                }
                
                
                
                
                return contentCell
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if indexPath.row == 0
        {

        }
        else
        {
            print(indexPath.row)
            print(self.timearr.object(at: indexPath.row) as! String)
            sponser = Bundle.main.loadNibNamed("TimeAddingPopup", owner: self, options: nil)?[0] as! TimeAddingPopup
            sponser.AddTimingsLabel.text = "loo_add_timings".localized
            sponser.layer.zPosition = 2
            KGModal.sharedInstance().show(withContentView: sponser, andAnimated: true)
            KGModal.sharedInstance().tapOutsideToDismiss = false
            
            self.view.endEditing(true)
            sponser.mybgview.layer.cornerRadius = 20.0
            sponser.mybgview.clipsToBounds = true
            
            sponser.addbutton.addTarget(self, action: #selector(DateandTimeViewController.addbottombutton(sender:)), for: UIControlEvents.touchUpInside)
            sponser.popupclose.addTarget(self, action: #selector(DateandTimeViewController.popupclosebutton(sender:)), for: UIControlEvents.touchUpInside)
            
            sponser.mondaybutton.addTarget(self, action: #selector(DateandTimeViewController.popupmondaybutton(sender:)), for: UIControlEvents.touchUpInside)
            sponser.tuesdaybutton.addTarget(self, action: #selector(DateandTimeViewController.popuptuesdaybutton(sender:)), for: UIControlEvents.touchUpInside)
            sponser.wednesdaybutton.addTarget(self, action: #selector(DateandTimeViewController.popupwednesdaybutton(sender:)), for: UIControlEvents.touchUpInside)
            sponser.thursdaybutton.addTarget(self, action: #selector(DateandTimeViewController.popupthursdaybutton(sender:)), for: UIControlEvents.touchUpInside)
            sponser.fridaybutton.addTarget(self, action: #selector(DateandTimeViewController.popupfridaybutton(sender:)), for: UIControlEvents.touchUpInside)
            sponser.saturdaybutton.addTarget(self, action: #selector(DateandTimeViewController.popupsaturdaybutton(sender:)), for: UIControlEvents.touchUpInside)
            sponser.sundaybutton.addTarget(self, action: #selector(DateandTimeViewController.popupsundaybutton(sender:)), for: UIControlEvents.touchUpInside)
            
            
            sponser.clearbutton.addTarget(self, action: #selector(DateandTimeViewController.popupclearbutton(sender:)), for: UIControlEvents.touchUpInside)
            sponser.savebutton.addTarget(self, action: #selector(DateandTimeViewController.popupsavebutton(sender:)), for: UIControlEvents.touchUpInside)
            
            //img
            sponser.mondayimg.image = UIImage.init(named: "munselect.png")
            sponser.mondaystr = ""
            
            sponser.tuesdayimg.image = UIImage.init(named: "tunselect.png")
            sponser.tuesdaystr = ""
            
            sponser.wednesdayimg.image = UIImage.init(named: "wunselect.png")
            sponser.wednesdaystr = ""
            
            sponser.thursdayimg.image = UIImage.init(named: "tunselect.png")
            sponser.thursdaystr = ""
            
            sponser.fridayimg.image = UIImage.init(named: "funselect.png")
            sponser.fridaystr = ""
            
            sponser.saturdayimg.image = UIImage.init(named: "sunselect.png")
            sponser.saturdaystr = ""
            
            sponser.sundayimg.image = UIImage.init(named: "sunselect.png")
            sponser.sundaystr = ""
            
            if indexPath.section == 2
            {
                sponser.mondayimg.image = UIImage.init(named: "mselect.png")
                sponser.mondaystr = "1"
                selecteddayarr.add("MON")
                
            }
            else if indexPath.section == 3
            {
                sponser.tuesdayimg.image = UIImage.init(named: "tselect.png")
                sponser.tuesdaystr = "1"
                selecteddayarr.add("TUE")
            }
            else if indexPath.section == 4
            {
                sponser.wednesdayimg.image = UIImage.init(named: "wselect.png")
                sponser.wednesdaystr = "1"
                selecteddayarr.add("WED")
            }
            else if indexPath.section == 5
            {
                sponser.thursdayimg.image = UIImage.init(named: "tselect.png")
                sponser.thursdaystr = "1"
                selecteddayarr.add("THU")
            }
            else if indexPath.section == 6
            {
                sponser.fridayimg.image = UIImage.init(named: "fselect.png")
                sponser.fridaystr = "1"
                selecteddayarr.add("FRI")
            }
            else if indexPath.section == 7
            {
                sponser.saturdayimg.image = UIImage.init(named: "sselect.png")
                sponser.saturdaystr = "1"
                selecteddayarr.add("SAT")
            }
//            else if indexPath.section == 8
//            {
//                sponser.sundayimg.image = UIImage.init(named: "sselect.png")
//                sponser.sundaystr = "1"
//            }
            else
            {
                sponser.sundayimg.image = UIImage.init(named: "sselect.png")
                sponser.sundaystr = "1"
                selecteddayarr.add("SUN")
            }
            
            sponser.tablelist.dataSource = self
            sponser.tablelist.delegate = self
            sponser.tablelist.register(UINib.init(nibName:"TimeAddingTableViewCell", bundle: nil),forCellReuseIdentifier: "TimeAddingTableViewCell")
            self.sponser.savebutton.setTitle("pop_date_time_save".localized, for: .normal)
            self.sponser.clearbutton.setTitle("pop_date_time_clear".localized, for: .normal)
            
            
            let newdic = NSMutableDictionary()
            newdic.setValue(self.timearr.object(at: indexPath.row) as! String, forKey: "open_time")
            newdic.setValue("____ : ____", forKey: "close_time")
            newdic.setValue("", forKey: "timelist")
            newdic.setValue("", forKey: "timestat")
            
            print(newdic)
            self.openclosetimearr.add(newdic)
            print(self.openclosetimearr)
            
            sponser.tablelist.dataSource = self
            sponser.tablelist.delegate = self
            
            sponser.tablelist.reloadData()
            let lastRow: Int = sponser.tablelist.numberOfRows(inSection: 0) - 1
            let indexPath = IndexPath(row: lastRow, section: 0);
            sponser.tablelist.scrollToRow(at: indexPath, at: .top, animated: true)
        }
        
    }
    
    func popupclosebutton(sender:UIButton!)
    {
        KGModal.sharedInstance().hide(animated: true)
        self.view.endEditing(true)
    }
    
    func popupmondaybutton(sender:UIButton!)
    {
        if sponser.mondayimg.image == UIImage.init(named: "munselect.png")
        {
            sponser.mondayimg.image = UIImage.init(named: "mselect.png")
            sponser.mondaystr = "1"
            selecteddayarr.add("MON")
        }
        else
        {
            sponser.mondayimg.image = UIImage.init(named: "munselect.png")
            sponser.mondaystr = ""
            selecteddayarr.remove("MON")
            
        }
    }
    
    
    func popuptuesdaybutton(sender:UIButton!)
    {
        if sponser.tuesdayimg.image == UIImage.init(named: "tunselect.png")
        {
            sponser.tuesdayimg.image = UIImage.init(named: "tselect.png")
            sponser.tuesdaystr = "1"
            selecteddayarr.add("TUE")
        }
        else
        {
            sponser.tuesdayimg.image = UIImage.init(named: "tunselect.png")
            sponser.tuesdaystr = ""
            selecteddayarr.remove("TUE")
        }
    }
    
    func popupwednesdaybutton(sender:UIButton!)
    {
        if sponser.wednesdayimg.image == UIImage.init(named: "wunselect.png")
        {
            sponser.wednesdayimg.image = UIImage.init(named: "wselect.png")
            sponser.wednesdaystr = "1"
            selecteddayarr.add("WED")
        }
        else
        {
            sponser.wednesdayimg.image = UIImage.init(named: "wunselect.png")
            sponser.wednesdaystr = ""
            selecteddayarr.remove("WED")
        }
    }
    
    func popupthursdaybutton(sender:UIButton!)
    {
        if sponser.thursdayimg.image == UIImage.init(named: "tunselect.png")
        {
            sponser.thursdayimg.image = UIImage.init(named: "tselect.png")
            sponser.thursdaystr = "1"
            selecteddayarr.add("THU")
        }
        else
        {
            sponser.thursdayimg.image = UIImage.init(named: "tunselect.png")
            sponser.thursdaystr = ""
            selecteddayarr.remove("THU")
        }
    }
    
    func popupfridaybutton(sender:UIButton!)
    {
        if sponser.fridayimg.image == UIImage.init(named: "funselect.png")
        {
            sponser.fridayimg.image = UIImage.init(named: "fselect.png")
            sponser.fridaystr = "1"
            selecteddayarr.add("FRI")
        }
        else
        {
            sponser.fridayimg.image = UIImage.init(named: "funselect.png")
            sponser.fridaystr = ""
            selecteddayarr.remove("FRI")
        }
    }
    
    func popupsaturdaybutton(sender:UIButton!)
    {
        if sponser.saturdayimg.image == UIImage.init(named: "sunselect.png")
        {
            sponser.saturdayimg.image = UIImage.init(named: "sselect.png")
            sponser.saturdaystr = "1"
            selecteddayarr.add("SAT")
        }
        else
        {
            sponser.saturdayimg.image = UIImage.init(named: "sunselect.png")
            sponser.saturdaystr = ""
            selecteddayarr.remove("SAT")
        }
    }
    
    func popupsundaybutton(sender:UIButton!)
    
    {
        if sponser.sundayimg.image == UIImage.init(named: "sunselect.png")
        {
            sponser.sundayimg.image = UIImage.init(named: "sselect.png")
            sponser.sundaystr = "1"
            selecteddayarr.add("SUN")
        }
        else
        {
            sponser.sundayimg.image = UIImage.init(named: "sunselect.png")
            sponser.sundaystr = ""
            selecteddayarr.remove("SUN")
        }
    }
    
    func popupsavebutton(sender:UIButton!)
    {
        //let lastdic = NSMutableArray()
        
        for dayPrefix in selecteddayarr {
            let selectedDayIndex = getSelectedDayIndex(day: (dayPrefix as? String)!)
            
            if (openclosetimearr.count > 0)
            {
                
                for i in 0...openclosetimearr.count-1{
                    
                    let openTime = (self.openclosetimearr.object(at: i) as AnyObject).value(forKey: "open_time") as? String
                    let closeTime = (self.openclosetimearr.object(at: i) as AnyObject).value(forKey: "close_time") as? String
                    
                    print(openTime,closeTime)
                    if (!(openTime?.contains("____"))! && !(closeTime?.contains("____"))!)
                    {
                        
                        let x = time.index(of: openTime!)
                        let y = time.index(of: closeTime!)
                        
                        print(x,y)
                        
                        if (x!>=y!)
                        {
                            self.view.makeToast("Invalid Time")
                        }
                        else{
                            for i in Int(x!)...Int(y!){
                                timeDisp[selectedDayIndex][i] = 1
                            }
                            
                        }
                    }
                }
            }
        }
        
        print(timeDisp)
        
        print(self.openclosetimearr)
        
        print(timeDisp.count)
        
        print(lastdic)
        
        
        lastdic.setObject(selecteddayarr, forKey:"seleted_days" as NSCopying);
        
        
        print(lastdic)
        
        print(selecteddayarr)
        
        print(lastdic)
        
        print(openclosetimearr)
        
        
        
        
        let anotherarr = NSMutableArray()
        if selecteddayarr.contains("SUN")
        {
            anotherarr.insert(self.openclosetimearr.mutableCopy(), at: 0)
        }
        else
        {
            anotherarr.insert(NSMutableArray(), at: 0)
        }
        
        
        if selecteddayarr.contains("MON")
        {
            anotherarr.insert(self.openclosetimearr.mutableCopy(), at: 1)
        }
        else
        {
            anotherarr.insert(NSMutableArray(), at: 1)
        }
        
        if selecteddayarr.contains("TUE")
        {
            anotherarr.insert(self.openclosetimearr.mutableCopy(), at: 2)
        }
        else
        {
            anotherarr.insert(NSMutableArray(), at:2)
        }
        
        if selecteddayarr.contains("WED")
        {
            anotherarr.insert(self.openclosetimearr.mutableCopy(), at:3)
        }
        else
        {
            anotherarr.insert(NSMutableArray(), at:3)
        }
        
        if selecteddayarr.contains("THU")
        {
            anotherarr.insert(self.openclosetimearr.mutableCopy(), at: 4)
        }
        else
        {
            anotherarr.insert(NSMutableArray(), at:4)
        }
        
        if selecteddayarr.contains("FRI")
        {
            anotherarr.insert(self.openclosetimearr.mutableCopy(), at:5)
        }
        else
        {
            anotherarr.insert(NSMutableArray(), at:5)
        }
        
        if selecteddayarr.contains("SAT")
        {
            anotherarr.insert(self.openclosetimearr.mutableCopy(), at:6)
        }
        else
        {
            anotherarr.insert(NSMutableArray(), at:6)
        }

        lastdic.setObject(anotherarr, forKey:"timings" as NSCopying);
        print(lastdic)
        
        
        
        
        
        
        
        
        
//        let data = NSKeyedArchiver.archivedData(withRootObject: anotherarr)
//        UserDefaults.standard.set(data, forKey: "timings")
//        print(data)
        let data = NSKeyedArchiver.archivedData(withRootObject: lastdic)
        UserDefaults.standard.set(data, forKey:"storedateandtime")
        UserDefaults.standard.synchronize()
        print(data)
        //self.availabilityArray.append(lastdic as! [String : String])
        self.collectionView.reloadData()
        
        selecteddayarr.removeAllObjects()
        self.openclosetimearr.removeAllObjects()
        KGModal.sharedInstance().hide(animated: true)

    }
    
    func popupclearbutton(sender:UIButton!)
    {
        self.openclosetimearr.removeAllObjects()
        let newdic = NSMutableDictionary()
        newdic.setValue("____ : ____", forKey: "open_time")
        newdic.setValue("____ : ____", forKey: "close_time")
        
        newdic.setValue("", forKey: "timelist")
        newdic.setValue("", forKey: "timestat")
        self.openclosetimearr.add(newdic)
        self.sponser.tablelist.reloadData()
    }
    
    
    func getSelectedDayIndex(day:String) -> Int {
        
        if  (day == "SUN")
        {
            return 0
        }
        else if (day == "MON")
        {
            return 1
        }
        else if (day == "TUE")
        {
            return 2
        }
        else if (day == "WED")
        {
            return 3
        }
        else if (day == "THU")
        {
            return 4
        }
        else if (day == "FRI")
        {
            return 5
        }
        else if (day == "SAT")
        {
            return 6
        }
        
        return 10
    }
    
    
    func actionSheetPickerViewDidCancel(_ pickerView: IQActionSheetPickerView!)
    {
        
    }
    
    func actionSheetPickerView(_ pickerView: IQActionSheetPickerView!, didSelectTitles titles: NSArray!)
    {
        if checkopenorclose == "open"
        {
            
            let formatter = DateFormatter()
            formatter.dateStyle = .none
            formatter.timeStyle = .short
            sponser.tablelist.register(UINib.init(nibName:"TimeAddingTableViewCell", bundle: nil),forCellReuseIdentifier: "TimeAddingTableViewCell")
            
            sponser.tablelist.dataSource = self
            sponser.tablelist.delegate = self
            
            let str = titles.componentsJoined(by: " ")
            print(str)
            let nsmutdic = NSMutableDictionary()
            let closetimestr = (self.openclosetimearr.object(at: tableindex) as AnyObject).value(forKey: "close_time") as? String
            print("open: open_time \(str)")
            print("open: closetimestr \(closetimestr)")
            nsmutdic.setValue(str, forKey: "open_time")
            nsmutdic.setValue(closetimestr, forKey: "close_time")
            nsmutdic.setValue("", forKey: "timelist")
            nsmutdic.setValue("", forKey: "timestat")
            print(nsmutdic)
            //        self.openclosetimearr.add(nsmutdic)
            self.openclosetimearr.replaceObject(at: tableindex, with: nsmutdic)
            print(self.openclosetimearr)
            let indexPath = IndexPath(row: tableindex, section: 0)
            sponser.tablelist.reloadRows(at: [indexPath], with: .none)
        }
        else
        {
            let formatter = DateFormatter()
            formatter.dateStyle = .none
            formatter.timeStyle = .short
            sponser.tablelist.register(UINib.init(nibName:"TimeAddingTableViewCell", bundle: nil),forCellReuseIdentifier: "TimeAddingTableViewCell")
            
            sponser.tablelist.dataSource = self
            sponser.tablelist.delegate = self
            let str = titles.componentsJoined(by: " ")
            print(str)
            let nsmutdic = NSMutableDictionary()
            let opentimestr = (self.openclosetimearr.object(at: tableindex) as AnyObject).value(forKey: "open_time") as? String
            print("close: open_time \(opentimestr)")
            print("clsoe: closetimestr \(str)")
            let timeformat = DateFormatter()
            timeformat.dateFormat = "hh:mm a"
            var starttime = timeformat.date(from:opentimestr!)
            let endtime = timeformat.date(from:str)
            
            let calendar = NSCalendar.current as NSCalendar
            
            // Replace the hour (time) of both dates with 00:00
            
            let flags = NSCalendar.Unit.hour
            let components = calendar.components(flags, from: starttime!, to: endtime!, options: [])
            
            print("COMPONENETS \(components.hour!)")
            
            var timelist = String()
            var timestat = String()
            
            if components.hour! >= -1 {
                for c in 0 ..< components.hour!+1
                {
                    if c == 0
                    {
                        timelist.append("\(timeformat.string(from: starttime!))")
                        timestat.append("1")
                    }
                    else
                    {
                        starttime = starttime?.addingTimeInterval(3600)
                        timelist.append(" , \(timeformat.string(from: starttime!))")
                        if c == components.hour!
                        {
                            timestat.append(",1")
                        }
                        else{
                            timestat.append(",0")
                        }
                    }
                }
            } else {
                timestat.append("")
                timelist.append("")
            }
            
            print(timelist)
            print(timestat)
            nsmutdic.setValue(opentimestr, forKey: "open_time")
            nsmutdic.setValue(str, forKey: "close_time")
            nsmutdic.setValue(timelist, forKey: "timelist")
            nsmutdic.setValue(timestat, forKey: "timestat")
            print(nsmutdic)
            
            //        self.openclosetimearr.add(nsmutdic)
            self.openclosetimearr.replaceObject(at: tableindex, with: nsmutdic)
            print(self.openclosetimearr)
            let indexPath = IndexPath(row: tableindex, section: 0)
            sponser.tablelist.reloadRows(at: [indexPath], with: .none)
            
        }
    }
    
    func openbutton(sender:UIButton!)
    {
        checkopenorclose = "open"
        tableindex = sender.tag-123
        print(tableindex)
        let picker = IQActionSheetPickerView(title: "Hour Picker", delegate: self)
        picker?.tag = 8
        picker?.titlesForComponents = [["12:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00"], ["AM", "PM"]]
        picker?.show()
        
    }
    
    
    func shouldShow(_ picker: BSHourPicker!) -> Bool
    {
        return true
    }
    
    func time(_ picker: BSHourPicker!, selctedItem item: String!)
    {
        print(item)
    }
 
    func closebutton(sender:UIButton!)
    {
        checkopenorclose = "close"
        tableindex = sender.tag-123
        print(tableindex)
        let picker = IQActionSheetPickerView(title: "Hour Picker", delegate: self)
        picker?.tag = 8
        picker?.titlesForComponents = [["12:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00"], ["AM", "PM"]]
        picker?.show()
    }
    
    func addbottombutton(sender:UIButton!)
    {
        let alertTitle = "Limit Exceeded".localized
        let alertMessage = "You cannot have more than the allowed limit".localized
        
        //If count is less than limit then add entries
        if self.openclosetimearr.count < addTimingsLimit {
            print(self.openclosetimearr)
            sponser.tablelist.register(UINib.init(nibName:"TimeAddingTableViewCell", bundle: nil),forCellReuseIdentifier: "TimeAddingTableViewCell")
            let newdic = NSMutableDictionary()
            
            newdic.setValue("____ : ____", forKey: "open_time")
            newdic.setValue("____ : ____", forKey: "close_time")
            
            newdic.setValue("", forKey: "timelist")
            newdic.setValue("", forKey: "timestat")
            
            print(newdic)
            self.openclosetimearr.add(newdic)
            print(self.openclosetimearr)
            
            sponser.tablelist.dataSource = self
            sponser.tablelist.delegate = self
            
            sponser.tablelist.reloadData()
            let lastRow: Int = sponser.tablelist.numberOfRows(inSection: 0) - 1
            let indexPath = IndexPath(row: lastRow, section: 0);
            sponser.tablelist.scrollToRow(at: indexPath, at: .top, animated: true)
        } else {
            //Display alert
            let alert = UIAlertController(title: alertTitle, message:alertMessage , preferredStyle: .alert)
            self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now() + 2
            DispatchQueue.main.asyncAfter(deadline: when){
                // your code with delay
                alert.dismiss(animated: true, completion: nil)
            }
        }
        
    }
    
    
    //tableview TimeAddingPopup
    // MARK: - Tabelview Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.openclosetimearr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let simpleTableIdentifier  = NSString(format:"TimeAddingTableViewCell")
        let cell:TimeAddingTableViewCell?=tableView.dequeueReusableCell(withIdentifier: simpleTableIdentifier as String) as? TimeAddingTableViewCell
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        cell?.showOpenLabel.text =  "pop_date_time_open".localized
        cell?.showCloseLabel.text = "pop_date_time_close".localized
        let openstr = (self.openclosetimearr.object(at: indexPath.row) as AnyObject).value(forKey: "open_time") as? String
        cell?.opentime.text = openstr
        cell?.closetime.text = (self.openclosetimearr.object(at: indexPath.row) as AnyObject).value(forKey: "close_time") as? String
        cell?.openbutton.tag = indexPath.row + 123
        cell?.openbutton.addTarget(self, action: #selector(DateandTimeViewController.openbutton(sender:)), for: UIControlEvents.touchUpInside)
        cell?.closebutton.tag = indexPath.row + 123
        cell?.closebutton.addTarget(self, action: #selector(DateandTimeViewController.closebutton(sender:)), for: UIControlEvents.touchUpInside)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if (editingStyle == UITableViewCellEditingStyle.delete)
        {
            // you might want to delete the item at the array first before calling this function
//            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        let deleteAction = UITableViewRowAction(style: .default, title: "Delete", handler: {action in
                        //handle delete
            
                        self.sponser.tablelist.register(UINib.init(nibName:"TimeAddingTableViewCell", bundle: nil),forCellReuseIdentifier: "TimeAddingTableViewCell")
                        self.sponser.tablelist.dataSource = self
                        self.sponser.tablelist.delegate = self
//                        self.openclosetimearr.remove(indexPath.row)
                        self.openclosetimearr.removeObject(at: indexPath.row)
                        self.sponser.tablelist.reloadData()
                        })
        return [deleteAction]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    @IBAction func Submitbutton(_ x:AnyObject)
    {
        print(timeDisp)
        timesave.removeAll()
        
        for i in 0...timeDisp.count - 1{
            print(i)
            var c = ""
            var s = ""
            
            if timeDisp[i].contains(1)
            {
                print(i)
                
                var j = 0
                
                while j <= 23 {
                    print(j)
                    var v = 0
                    var u = j
                    print(i,j)
                    //print(timeDisp[i][j])
                    //print(timeDisp)
                    while timeDisp[i][j+1] == 1 && j < 24
                    {
                        
                        v = j
                        
                        print(i,j)
                        
                        j=j+1
                        
                        print(i,j)
                        
                        
                        if j+1 >= 23{
                            
                            break
                        }
                        
                    }
                    
                    print(i,j)
                    print(timeDisp[i][j
                        ])
                    if timeDisp[i][j] == 1
                    {
                        print(u+1,v+1)
                        
                        var a:String
                        var b:String
                        
                        
                        if (u+1 > 23) {
                            
                            String(format: "%02d", (u+1)-23)
                            
                            a = "\(String(format: "%02d", (u+1)-23)):00"
                        }
                        else
                        {
                            a = "\(String(format: "%02d", (u+1))):00"
                        }
                        
                        
                        
                        if (v+1 > 23) {
                            
                            b = "\(String(format: "%02d", (v+1)-23)):00"
                        }
                        else
                        {
                            b = "\(String(format: "%02d", (v+1))):00"
                        }
                        
                        c = c + " | " + a + b
                        
                        
                        print(c)
                    }
                    j=j+1
                    
                    
                    if j >= 23{
                        
                        break
                    }
                    
                }
                
                
                if  i == 0 {
                    
                    s = "Sunday"
                }
                else if i == 1{
                    
                    s = "Monday"
                }
                else if i == 2{
                    
                    s = "Tuesday"
                }
                else if i == 3{
                    
                    s = "Wednesday"
                }
                else if i == 4{
                    
                    s = "Thursday"
                }
                else if i == 5{
                    
                    s = "Friday"
                }
                else if i == 6{
                    
                    s = "Saturday"
                }
                else if i == 7{
                    s = "sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday"
                }
                
                print(s)
                
                s = s+" -"+c
                
                timesave.append(s)
                print(timesave)
                
            }
        }
        print("FINAL RESULT")
        print(timesave)
        self.navigationController?.popViewController(animated: true)
        
    }
}
