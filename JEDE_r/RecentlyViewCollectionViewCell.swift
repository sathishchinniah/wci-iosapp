//
//  RecentlyViewCollectionViewCell.swift
//  JEDE_r
//
//  Created by Exarcplus on 31/05/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit

class RecentlyViewCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imgview: UIImageView!
    @IBOutlet var loonamelab : UILabel!
    @IBOutlet var gradientview : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        let gradient: CAGradientLayer = CAGradientLayer()
//        
//        gradient.colors = [UIColor.white.cgColor, UIColor.black.cgColor]
////        gradient.locations = [0.0 , 1.0]
////        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
////        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
//        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.gradientview.frame.size.width, height: self.gradientview.frame.size.height)
//        
//        self.gradientview.layer.insertSublayer(gradient, at: 0)
        
        let gradient = CAGradientLayer()
        gradient.frame = imgview.bounds
        
        let startColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.25)
        let endColor = UIColor.gray
        
        gradient.colors = [startColor.cgColor, endColor.cgColor]
        imgview.layer.insertSublayer(gradient, at: 0)
        
        
    }
    
}


