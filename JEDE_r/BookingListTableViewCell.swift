//
//  BookingListTableViewCell.swift
//  JEDE_r
//
//  Created by Exarcplus on 27/05/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit

class BookingListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var loonamelab : UILabel!
    @IBOutlet weak var lootimelab : UILabel!
    @IBOutlet weak var looaddresslab : UILabel!
    @IBOutlet weak var pricelab : UILabel!
    @IBOutlet weak var looimg : UIImageView!
    @IBOutlet weak var categoryimg : UIImageView!
    @IBOutlet weak var statuslab : UILabel!
    @IBOutlet var borderview: UIView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        borderview.layer.shadowColor = UIColor.black.cgColor
        borderview.layer.shadowOpacity = 0.2
        borderview.layer.shadowOffset = CGSize.zero
        borderview.layer.shadowRadius = 2
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
