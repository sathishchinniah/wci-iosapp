//
//  SettingsViewController.swift
//  JEDE_r
//
//  Created by Developer on 4/8/18.
//  Copyright © 2018 Exarcplus. All rights reserved.
//

import UIKit
import Firebase

class SettingsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var languageSwitchArray : [String] = []
    @IBOutlet weak var tablelist : UITableView!
    var selectedIndex : NSInteger!
    var selectedlanguageItem : String?
    @IBOutlet weak var openMenuButton: UIBarButtonItem!
    let barButtonItem = UIBarButtonItem?.self
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        tablelist.tableFooterView = UIView(frame: CGRect.zero)
        languageSwitchArray = ["English","German","French"]
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.title = "settings_title".localized
        self.navigationController?.navigationBar.isHidden = false
        //Show menu only if the
        if UserDefaultManager.sharedManager.userAgreedTerms() {
            let burButtonItem = UIBarButtonItem(image: UIImage(named: "Sidemenu"), style: .plain, target: self, action:#selector(OpenSideMenu(_:)))
            self.navigationItem.leftBarButtonItem  = burButtonItem
        }
        self.tablelist.reloadData()
    }
    
    
    @IBAction func OpenSideMenu(_ x:AnyObject)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.SideMenu.showLeftView(animated: true, completionHandler: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadsidemenu"), object: nil, userInfo: nil)
    }
    
    //tableview
    func tableView( _ tableView : UITableView,  titleForHeaderInSection section: Int)->String?
    {
        
        switch(section)
        {
        case 0:return "settings_select_language".localized
        default :return ""
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return languageSwitchArray.count
    }
    
    //selcting the languages in tableview
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier)!
        cell.textLabel?.text = languageSwitchArray[indexPath.row].localized
        cell.accessoryType = UITableViewCellAccessoryType.none
        
        let languageName = UserDefaultManager.sharedManager.getLanguageName()
        if languageName == languageSwitchArray[indexPath.row] {
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Language helper starts here
        //call this api to set selected language
        //Call this line of code for setting language
        if indexPath.row == 0 {
            Language.language = Language.english
        } else if indexPath.row == 1 {
            Language.language = Language.german
        } else if indexPath.row == 2 {
            Language.language = Language.french
        }
        
        let UserId =  UserDefaults.standard.string(forKey: "user_id") ?? "NA"
        FIRAnalytics.logEvent(withName: "Select_Language", parameters: [
            "user_id" : UserId,
            "selected_language" : Language.language,
            "login_device" : "iOS",
            "action_name" : "language_selection_click"
            ])
        UserDefaultManager.sharedManager.setLanguageName(languageName:languageSwitchArray[indexPath.row])
        
        //If it is initial load
        if !UserDefaultManager.sharedManager.userAgreedTerms() { //Initial load
            print("Initail load and loading TermsAndConditionsViewController")
            UserDefaultManager.sharedManager.setLanguageKeySet()
            tableView.cellForRow(at: indexPath as IndexPath)?.accessoryType = .checkmark
            selectedIndex = indexPath.row
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TermsAndConditionsViewController") as! TermsAndConditionsViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
    }
}
