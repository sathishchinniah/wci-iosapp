//
//  LoginViewController.swift
//  JEDE_r
//
//  Created by Vignesh Waran on 5/13/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FBSDKLoginKit
import LGSideMenuController
import Firebase

class LoginViewController: UIViewController, UITextFieldDelegate, GIDSignInDelegate, GIDSignInUIDelegate
{
    var iconClick : Bool!
    @IBOutlet weak var usernametext: LRTextField!
    @IBOutlet weak var SignUpTitle: UIButton!
    @IBOutlet weak var SignInTitle: UIButton!
    @IBOutlet weak var passwordtext: LRTextField!
    @IBOutlet weak var eyesimage: UIImageView!
    var myclass:MyClass!
    var loginarr = NSMutableArray()

    @IBOutlet weak var forgotPasswordButton: UIButton!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        myclass = MyClass()
        iconClick = true
        self.navigationController?.navigationBar.isHidden = true
        forgotPasswordButton.isHidden = true
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        usernametext.placeholder = "Email Id"
        usernametext.delegate = self
        usernametext.placeholderActiveColor = UIColor.white
        usernametext.placeholderInactiveColor = UIColor.init(colorLiteralRed: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0)
        
        
        passwordtext.placeholder = "Password"
        passwordtext.delegate = self
        passwordtext.placeholderActiveColor = UIColor.white
        passwordtext.placeholderInactiveColor = UIColor.init(colorLiteralRed: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0)
        passwordtext.clearsOnBeginEditing = false;
        
        eyesimage.image = UIImage.init(named: "closeeyes.png")
        
       
        self.usernametext.placeholder = "Email_id".localized
        self.passwordtext.placeholder =  "Password".localized
        
        self.usernametext.hintText = ""
        self.passwordtext.hintText = ""
        
        self.SignInTitle.setTitle("Sign_In".localized, for: .normal)
        self.SignUpTitle.setTitle("Sign_up".localized, for: .normal)
        
        SignInTitle.layer.cornerRadius = 5.0
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func iconAction(sender: AnyObject)
    {
        if (iconClick == true)
        {
            eyesimage.image = UIImage.init(named: "openeyes.png")
            passwordtext.isSecureTextEntry = false
            passwordtext.clearsOnBeginEditing = false;
            iconClick = false
        }
        else
        {
            eyesimage.image = UIImage.init(named: "closeeyes.png")
            passwordtext.isSecureTextEntry = true
            passwordtext.clearsOnBeginEditing = false;
            iconClick = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        if textField == usernametext
        {
            let nsString1 = usernametext.text as NSString?
            if  ((usernametext.text) != nil)
            {
                usernametext.text = nsString1?.replacingCharacters(in: range, with: string)
            }
            else
            {
                usernametext.text = string
            }
        }
        else
        {
            let nsString = passwordtext.text as NSString?
            if  ((passwordtext.text) != nil)
            {
                passwordtext.text = nsString?.replacingCharacters(in: range, with: string)
            }
            else
            {
                passwordtext.text = string
            }
        }
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.view.endEditing(true)
        return false
    }
    
    func validateEmail(_ candidate: String) -> Bool
    {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
    }
    
    @IBAction func loginbuttonclick(sender: UIButton)
    {
       if usernametext.text == "" && passwordtext.text == ""
        {
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.07
            animation.repeatCount = 4
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint(x: usernametext.center.x - 10,y :usernametext.center.y))
            animation.toValue = NSValue(cgPoint: CGPoint(x: usernametext.center.x + 10,y :usernametext.center.y))
            usernametext.layer.add(animation, forKey: "position")
            
            let animation1 = CABasicAnimation(keyPath: "position")
            animation1.duration = 0.07
            animation1.repeatCount = 4
            animation1.autoreverses = true
            animation1.fromValue = NSValue(cgPoint: CGPoint(x: passwordtext.center.x - 10,y :passwordtext.center.y))
            animation1.toValue = NSValue(cgPoint: CGPoint(x: passwordtext.center.x + 10,y :passwordtext.center.y))
            passwordtext.layer.add(animation1, forKey: "position")
        }
        else if usernametext.text == ""
        {
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.07
            animation.repeatCount = 4
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint(x: usernametext.center.x - 10,y :usernametext.center.y))
            animation.toValue = NSValue(cgPoint: CGPoint(x: usernametext.center.x + 10,y :usernametext.center.y))
            usernametext.layer.add(animation, forKey: "position")
        }
        else if passwordtext.text == ""
        {
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.07
            animation.repeatCount = 4
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint(x: passwordtext.center.x - 10,y :passwordtext.center.y))
            animation.toValue = NSValue(cgPoint: CGPoint(x: passwordtext.center.x + 10,y :passwordtext.center.y))
            passwordtext.layer.add(animation, forKey: "position")
        }
        else if usernametext.text != ""
        {
            if !validateEmail(usernametext.text!)
            {
                self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert: "Enter Valid Email Id", withIdentifier:"")
            }
            else
            {
                self.view.endEditing(true)
                let devicetoken : String!
                if UserDefaults.standard.string(forKey: "token") == nil
                {
                    devicetoken = ""
                }
                else
                {
                    devicetoken = UserDefaults.standard.string(forKey: "token") as String?
                }
                let urlstring = String(format:"%@/login.php?email=%@&password=%@&device_id=%@",kBaseURL,usernametext.text!,passwordtext.text!,devicetoken!);
                print(urlstring)
                self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: true, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                    if Stats == true
                    {
                        
                        NSLog("jsonObject=%@", jsonObject);
                        let message = (jsonObject.object(at: 0) as AnyObject).value(forKey:"message") as! String
                        let userId = (jsonObject.object(at: 0) as AnyObject).value(forKey:"user_id") as! String
                        let userName = (jsonObject.object(at: 0) as AnyObject).value(forKey:"user_name") as! String
                        
                       
                        UserDefaults.standard.set(userId, forKey: "user_id")
                        UserDefaults.standard.set(userName, forKey: "user_name")
                        
                               FIRAnalytics.logEvent(withName: "Login", parameters: [
                                "user_id": userId,
                                "user_name": userName,
                                   "login_device" : "iOS",
                                   "action_name" : "login_click"
    
                                   ])
                        if message == "success"
                        {
                            NSLog("jsonObject=%@", jsonObject);
                            self.loginarr = jsonObject.mutableCopy() as! NSMutableArray
                            let data = NSKeyedArchiver.archivedData(withRootObject: self.loginarr.object(at: 0) as! NSDictionary)
                            UserDefaults.standard.set(self.loginarr.object(at: 0), forKey:"Logindetail")
                            UserDefaults.standard.synchronize()
                            
//                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                            let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
//                            let nav = UINavigationController.init(rootViewController: mainview)
//                            appDelegate.SideMenu = LGSideMenuController.init(rootViewController: nav)
//                            appDelegate.SideMenu.setLeftViewEnabledWithWidth(280, presentationStyle:.slideAbove, alwaysVisibleOptions:[])
//                            appDelegate.SideMenuView = kmainStoryboard.instantiateViewController(withIdentifier: "SidemenuViewController") as! SidemenuViewController
//                            appDelegate.SideMenu.leftViewStatusBarVisibleOptions = .onAll
//                            appDelegate.SideMenu.leftViewStatusBarStyle = .lightContent
//                            var rect = appDelegate.SideMenuView.view.frame;
//                            rect.size.width = 280;
//                            appDelegate.SideMenuView.view.frame = rect
//                            appDelegate.SideMenu.leftView().addSubview(appDelegate.SideMenuView.view)
//                            print(appDelegate.SideMenu)
//                            nav.isNavigationBarHidden = false
//                            self.present(appDelegate.SideMenu, animated:true, completion: nil)
                            self.navigationController?.navigationBar.isHidden = false
                            UIApplication.shared.windows[0].rootViewController = UIStoryboard(
                                name: "Main",
                                bundle: nil
                                ).instantiateInitialViewController()
                            
                        }
                        else
                        {
                            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Please Check Your Username and Password"), withIdentifier:"")
                        }
                    }
                    else
                    {
                        
                    }
                })
            }
        }
      
    }
    
    
    @IBAction func btnFBLoginPressed(sender: AnyObject)
    {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager .logIn(withReadPermissions: ["public_profile","email","user_friends"], handler: { (result, error) -> Void in
            if error != nil
            {
                FBSDKLoginManager().logOut()
            }
            else if (result?.isCancelled)!
            {
                NSLog("Cancelled");
                FBSDKLoginManager().logOut()
            }
            else
            {
                NSLog("Logged in");
                print(result!)
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if(fbloginresult.grantedPermissions.contains("email"))
                {
                    self.getFBUserData()
                    //fbLoginManager.logOut()
                }
            }
        })
    }
    
    func getFBUserData()
    {
        if((FBSDKAccessToken.current()) != nil)
        {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email, gender"]).start(completionHandler: { (connection, resul, error) -> Void in
                if (error == nil)
                {
                    print(resul!)
                    let result = resul as! NSDictionary
                    let userEmail : NSString = result.value(forKey: "email") as! NSString
                    print("User Email is: \(userEmail)")
                    let devicetoken : String!
                    if UserDefaults.standard.string(forKey: "token") == nil
                    {
                        devicetoken = ""
                    }
                    else
                    {
                        devicetoken = UserDefaults.standard.string(forKey: "token") as String?
                    }
                    
                    let fid = result.value(forKey: "id") as! NSString;
                    let urlstring = String(format:"%@/socialmedia_id_check.php?socialmedia_id=%@&email=%@&login_using=Facebook&device_id=%@",kBaseURL,fid,userEmail,devicetoken!);
                    print(urlstring)
                    self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: true, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                        if Stats == true
                        {
                            NSLog("jsonObject=%@", jsonObject);
                            
                            let message = (jsonObject.object(at: 0) as AnyObject).value(forKey: "message") as! String
                            let copyarr = jsonObject;
                            if message == "success"
                            {
                                let userEmail : NSString = result.value(forKey: "email") as! NSString
                                let userName : NSString = result.value(forKey: "name") as! NSString
                                let Gender : NSString = result.value(forKey: "gender") as! NSString
                                let userfbid : NSString = result.value(forKey: "id") as! NSString
                                let picture_arr = result.value(forKey: "picture") as! NSDictionary;
                                let data_arr =  picture_arr.value(forKey: "data") as! NSDictionary;
                                
                                let nsmutdic = NSMutableDictionary()
                                nsmutdic.setObject("Facebook", forKey:"Rtype" as NSCopying);
                                nsmutdic.setObject(userName, forKey:"Name" as NSCopying);
                                nsmutdic.setObject(userEmail, forKey:"Email" as NSCopying);
                                nsmutdic.setObject(String(format:"%@",userfbid), forKey:"Sid" as NSCopying);
                                nsmutdic.setObject(Gender, forKey:"Gender" as NSCopying);
                                
                                if data_arr.value(forKey: "url") == nil
                                {
                                    let profile_pic = "";
                                    let dat = profile_pic.data(using: String.Encoding.utf8)
                                    let base64Encoded = dat?.base64EncodedData(options: [])
                                    print(base64Encoded!)
                                    nsmutdic.setObject(base64Encoded!, forKey:"Pimage" as NSCopying);
                                }
                                else
                                {
                                    let passingimg = data_arr.value(forKey: "url") as! NSString!
                                    let utf8str = passingimg?.data(using: String.Encoding.utf8.rawValue)
                                    
                                    let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
                                    print("Encoded : \(String(describing: base64Encoded))")
                                    nsmutdic.setObject(base64Encoded!, forKey:"Pimage" as NSCopying);
                                }
//                                let mainViewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
//                                self.navigationController?.pushViewController(mainViewcontroller, animated: true)
                                
                                let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
                                secondViewController.regdic = nsmutdic
                                self.navigationController?.pushViewController(secondViewController, animated: true)
                            }
                                
                            else if message == "blocked"
                            {
                                self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:"Your Account is blocked", withIdentifier:"")
                            }
                                
                            else if message == "already_register"
                            {
                                
                                let data = NSKeyedArchiver.archivedData(withRootObject: copyarr.object(at: 0) as! NSDictionary)
                                UserDefaults.standard.set(copyarr.object(at: 0), forKey:"Logindetail")
                                UserDefaults.standard.synchronize()
                                
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadlogin"), object: nil, userInfo: nil)
                                
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
                                let nav = UINavigationController.init(rootViewController: mainview)
                                appDelegate.SideMenu = LGSideMenuController.init(rootViewController: nav)
                                appDelegate.SideMenu.setLeftViewEnabledWithWidth(280, presentationStyle:.slideAbove, alwaysVisibleOptions:[])
                                appDelegate.SideMenuView = kmainStoryboard.instantiateViewController(withIdentifier: "SidemenuViewController") as! SidemenuViewController
                                appDelegate.SideMenu.leftViewStatusBarVisibleOptions = .onAll
                                appDelegate.SideMenu.leftViewStatusBarStyle = .lightContent
                                var rect = appDelegate.SideMenuView.view.frame;
                                rect.size.width = 280;
                                appDelegate.SideMenuView.view.frame = rect
                                appDelegate.SideMenu.leftView().addSubview(appDelegate.SideMenuView.view)
                                print(appDelegate.SideMenu)
                                self.present(appDelegate.SideMenu, animated:true, completion: nil)
                            }
                            else if message == "failed"
                            {
//                                let type = (jsonObject.object(at: 0) as AnyObject).value(forKey: "type") as! String
//                                self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("JEDE_r"), withAlert:String(format: "Your Email ID already Registered with %@",type), withIdentifier:"")
                                self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:message, withIdentifier:"")
                            }
                            else
                            {
                                self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("Wci"), withAlert:message, withIdentifier:"")
                            }
                        }
                        else
                        {
                            
                        }
                    })
                }
            })
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    @IBAction func googlePlusButtonTouchUpInside(sender: AnyObject)
    {
       

        let signIn = GIDSignIn.sharedInstance()
        //signIn?.clientID = KgoogTrackid
        signIn?.shouldFetchBasicProfile = true
        signIn?.delegate =  self
        signIn?.uiDelegate = self
        signIn?.signOut()
        signIn?.signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!)
    {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!)
    {
        if (error != nil)
        {
            // Do some error handling here.
            
        }
        else
        {
            //            let person = GPPSignIn.sharedInstance().googlePlusUser
            let person = GIDSignIn.sharedInstance().currentUser
            print(person!)
            print(person!.profile.name)
            
            if (person?.authentication == nil)
            {
                
            }
            else
            {
                
                let nsmutdic = NSMutableDictionary()
                nsmutdic.setObject("GooglePlus", forKey:"Rtype" as NSCopying);
                nsmutdic.setObject(person!.profile.name, forKey:"Name" as NSCopying);
                nsmutdic.setObject(GIDSignIn.sharedInstance().currentUser.profile.email, forKey:"Email" as NSCopying);
                nsmutdic.setObject(String(format:"%@",person!.userID), forKey:"Sid" as NSCopying);
                nsmutdic.setObject("Male", forKey: "Gender" as NSCopying);
                print(nsmutdic)
                
                let devicetoken : String!
                if UserDefaults.standard.string(forKey: "token") == nil
                {
                    devicetoken = ""
                }
                else
                {
                    devicetoken = UserDefaults.standard.string(forKey: "token") as String?
                }
                
                print(GIDSignIn.sharedInstance().currentUser.profile.email)
                let urlstring = String(format:"%@/socialmedia_id_check.php?socialmedia_id=%@&email=%@&login_using=GooglePlus&device_id=%@",kBaseURL,person!.userID,GIDSignIn.sharedInstance().currentUser.profile.email,devicetoken!);
//                let urlstring = String(format:"%@/social_media.php?social_id=g%@",kBaseURL,person!.userID)
                self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: true, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                    if Stats == true
                    {
                        NSLog("jsonObject=%@", jsonObject);
                        //                        let sts = (jsonObject.object(at: 0) as AnyObject).value(forKey: "user_status") as! String
                        //                        if sts == "Empty" || sts == "Active"
                        //                        {
                        let copyarr = jsonObject;
                        let message = (copyarr.object(at: 0) as AnyObject).value(forKey: "message") as! String;
                        if message == "success"
                        {
                            let nsmutdic = NSMutableDictionary()
                            nsmutdic.setObject("GooglePlus", forKey:"Rtype" as NSCopying);
                            nsmutdic.setObject(person!.profile.name, forKey:"Name" as NSCopying);
                            print(nsmutdic)
                            nsmutdic.setObject(GIDSignIn.sharedInstance().currentUser.profile.email, forKey:"Email" as NSCopying);
                            nsmutdic.setObject(String(format:"%@",person!.userID), forKey:"Sid" as NSCopying);
                            nsmutdic.setObject("Male", forKey: "Gender" as NSCopying);
                            let dimension = round(200 * UIScreen.main.scale)
                            nsmutdic.setObject(String(format:"%@",(person?.profile.imageURL(withDimension: UInt(dimension)).absoluteString)!), forKey: "Image" as NSCopying);
                            if person?.profile.imageURL(withDimension: UInt(dimension)).absoluteString == nil
                            {
                                let profile_pic = "";
//                                let dat = profile_pic.data(using: String.Encoding.utf8)
//                                //                                let  base64Encoded = dat?.base64EncodedStringWithOptions([]);
//                                let base64Encoded = dat?.base64EncodedString(options: []);
//                                print(base64Encoded)
                                let utf8str = profile_pic.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                                let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
                                print("Encoded : \(base64Encoded)")
                                nsmutdic.setObject(base64Encoded!, forKey:"Pimage" as NSCopying);
                            }
                            else
                            {
                                let profile_pic = person?.profile.imageURL(withDimension: UInt(dimension)).absoluteString
//                                let dat = profile_pic?.data(using: String.Encoding.utf8)
//                                //                                let  base64Encoded = dat?.base64EncodedStringWithOptions([]);
//                                let base64Encoded = dat?.base64EncodedString(options: []);
//                                print(base64Encoded)
                                let utf8str = profile_pic?.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                                let base64Encoded = utf8str?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
                                print("Encoded : \(base64Encoded)")
                                nsmutdic.setObject(base64Encoded!, forKey:"Pimage" as NSCopying);
                            }
                            let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
                            secondViewController.regdic = nsmutdic
                            self.navigationController?.pushViewController(secondViewController, animated: true)
                        }
                        else if message == "blocked"
                        {
                            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:"Your Account is blocked", withIdentifier:"")
                        }
                            
                        else if message == "already_register"
                        {
                            
                            let data = NSKeyedArchiver.archivedData(withRootObject: copyarr.object(at: 0) as! NSDictionary)
                            UserDefaults.standard.set(copyarr.object(at: 0), forKey:"Logindetail")
                            UserDefaults.standard.synchronize()
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadlogin"), object: nil, userInfo: nil)
                            
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
                            let nav = UINavigationController.init(rootViewController: mainview)
                            appDelegate.SideMenu = LGSideMenuController.init(rootViewController: nav)
                            appDelegate.SideMenu.setLeftViewEnabledWithWidth(280, presentationStyle:.slideAbove, alwaysVisibleOptions:[])
                            appDelegate.SideMenuView = kmainStoryboard.instantiateViewController(withIdentifier: "SidemenuViewController") as! SidemenuViewController
                            appDelegate.SideMenu.leftViewStatusBarVisibleOptions = .onAll
                            appDelegate.SideMenu.leftViewStatusBarStyle = .lightContent
                            var rect = appDelegate.SideMenuView.view.frame;
                            rect.size.width = 280;
                            appDelegate.SideMenuView.view.frame = rect
                            appDelegate.SideMenu.leftView().addSubview(appDelegate.SideMenuView.view)
                            print(appDelegate.SideMenu)
                            self.present(appDelegate.SideMenu, animated:true, completion: nil)
                        }
                        else if message == "failed"
                        {
                            let type = (jsonObject.object(at: 0) as AnyObject).value(forKey: "login_using") as! String
                            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:String(format: "Your Email ID already Registered with %@",type), withIdentifier:"")
                        }
                        else
                        {
                            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:message, withIdentifier:"")
                        }
                    }
                    else
                    {
                        NSLog("jsonObject=error");
                    }
                })
            }
            
            
        }
    }
    
    @IBAction func backbutton(sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signupbutton(sender: UIButton)
    {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
