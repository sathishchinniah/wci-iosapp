//
//  NewFiltersViewController.swift
//  JEDE_r
//
//  Created by Exarcplus on 19/06/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import AFNetworking
import SDWebImage
import Firebase

class NewFiltersViewController: UIViewController, RangeSeekSliderDelegate, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var myclass : MyClass!
    @IBOutlet weak var star1 : UIButton!
    @IBOutlet weak var star2 : UIButton!
    @IBOutlet weak var star3 : UIButton!
    @IBOutlet weak var star4 : UIButton!
    @IBOutlet weak var star5 : UIButton!
    @IBOutlet weak var rangesliderview : RangeSeekSlider!
    @IBOutlet weak var priceranceslider : RangeSeekSlider!

 
    @IBOutlet weak var nameScreen: UINavigationItem!
    @IBOutlet weak var closeButton: UIBarButtonItem!
    @IBOutlet var applyBtn: UIButton!
    @IBOutlet weak var resetBtn: UIButton!
    @IBOutlet weak var catogiry: UILabel!
    @IBOutlet weak var accessabulity: UILabel!
    @IBOutlet weak var facility: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var minimumRatingExpect: UILabel!
    
    var star1str : String!
    var star2str : String!
    var star3str : String!
    var star4str : String!
    var star5str : String!
    @IBOutlet weak var distancelab : UILabel!
    @IBOutlet weak var pricelab : UILabel!
    @IBOutlet weak var facilitycollection : UICollectionView!
    @IBOutlet weak var accessibilitycollection : UICollectionView!
    @IBOutlet weak var categorycollection : UICollectionView!
    @IBOutlet var collectionfacheight : NSLayoutConstraint!
    @IBOutlet var collectionaccheight : NSLayoutConstraint!
    @IBOutlet  var collectioncatheight : NSLayoutConstraint!
    var filterarr = NSMutableArray()
    var facilitiesarr = NSMutableArray()
    var insidefacilities = NSMutableArray()
    var accessibilityarr = NSMutableArray()
    var categoryarr = NSMutableArray()
    var facilityidstore = [""]
    var facility_cat_idstore = [""]
    var accessibilityidstore = [""]
    var categoryidstore = [""]
    var categorytype = [""]
    var dic = NSArray()
    
    var passrating : String!
    var passdistance : String?
    var passprice : String?
    var passfacility = NSArray()
    var passaccessibility = NSArray()
    var passcategory = NSArray()
    var mydic = NSMutableDictionary()
    var getdic = NSMutableDictionary()
    var facilityCategoryMapping = NSMutableDictionary()
    
    let defaultDistance:CGFloat = 10.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.nameScreen.title = "Filter".localized
        self.categorycollection!.reloadData()
        self.facilitycollection!.reloadData()
        self.accessibilitycollection!.reloadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.facilityCategoryMapping.removeAllObjects()
        self.initialLoadOnWillAppear()
    }
    
    func initialLoadOnWillAppear() {
        myclass = MyClass()
        
        let view = UIView(frame:  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 20))
        view.backgroundColor = UIColor.init(red: 27.0/255.0, green: 34.0/255.0, blue: 40.0/255.0, alpha: 1.0)
        self.navigationController?.view.addSubview(view)
        
        star1.setImage(UIImage.init(named: "starunselect1.png"), for: .normal)
        star2.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
        star3.setImage(UIImage.init(named: "starunselect3.png"), for: .normal)
        star4.setImage(UIImage.init(named: "starunselect4.png"), for: .normal)
        star5.setImage(UIImage.init(named: "starunselect5.png"), for: .normal)
        
        star1str = "0"
        star2str = "0"
        star3str = "0"
        star4str = "0"
        star5str = "0"
        
        passrating = ""
        passdistance = "10.0"
        passprice = "0"
        
        if let accessibilitycollectionView = accessibilitycollection {
            accessibilitycollection.reloadData()
        }
        if let facilitycollectionView = facilitycollection {
            facilitycollectionView.reloadData()
        }
        if let  categorycollectionView =  categorycollection{
            categorycollectionView.reloadData()
        }
        
        //Headers
        minimumRatingExpect.text = "filter_minimal_rating".localized
        distance.text = "adapter_listview_distance".localized
        price.text = "filter_pop_layout_price".localized
        facility.text = "filter_pop_layout_facility".localized
        accessabulity.text = "filter_pop_layout_accessibility_pop".localized
        catogiry.text = "filter_pop_layout_category".localized
        resetBtn.setTitle("filter_pop_layout_reset".localized, for: .normal)
        applyBtn.setTitle("filter_pop_layout_apply".localized, for: .normal)
        closeButton.title = "navigation_layout_close_button".localized
        self.loadlink()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func star1button(_sender:UIButton)
    {
        if star1str == "0"
        {
            star1.setImage(UIImage.init(named: "starselect1.png"), for: .normal)
            star2.setImage(UIImage.init(named: "starselect2.png"), for: .normal)
            star3.setImage(UIImage.init(named: "starselect3.png"), for: .normal)
            star4.setImage(UIImage.init(named: "starselect4.png"), for: .normal)
            star5.setImage(UIImage.init(named: "starselect5.png"), for: .normal)
            star1str = "1"
            passrating = "1"
        }
        else
        {
            star1.setImage(UIImage.init(named: "starunselect1.png"), for: .normal)
            star2.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
            star3.setImage(UIImage.init(named: "starunselect3.png"), for: .normal)
            star4.setImage(UIImage.init(named: "starunselect4.png"), for: .normal)
            star5.setImage(UIImage.init(named: "starunselect5.png"), for: .normal)
            star1str = "0"
            passrating = ""
        }
    }
    
    @IBAction func star2button(_sender:UIButton)
    {
        
        if star2str == "0"
        {
            star1.setImage(UIImage.init(named: "starunselect1.png"), for: .normal)
            star2.setImage(UIImage.init(named: "starselect2.png"), for: .normal)
            star3.setImage(UIImage.init(named: "starselect3.png"), for: .normal)
            star4.setImage(UIImage.init(named: "starselect4.png"), for: .normal)
            star5.setImage(UIImage.init(named: "starselect5.png"), for: .normal)
            star2str = "1"
            passrating = "2"
        }
        else
        {
            star1.setImage(UIImage.init(named: "starunselect1.png"), for: .normal)
            star2.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
            star3.setImage(UIImage.init(named: "starunselect3.png"), for: .normal)
            star4.setImage(UIImage.init(named: "starunselect4.png"), for: .normal)
            star5.setImage(UIImage.init(named: "starunselect5.png"), for: .normal)
            star2str = "0"
            passrating = ""
        }
    }
    
    @IBAction func star3button(_sender:UIButton)
    {
        
        if star3str == "0"
        {
            star1.setImage(UIImage.init(named: "starunselect1.png"), for: .normal)
            star2.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
            star3.setImage(UIImage.init(named: "starselect3.png"), for: .normal)
            star4.setImage(UIImage.init(named: "starselect4.png"), for: .normal)
            star5.setImage(UIImage.init(named: "starselect5.png"), for: .normal)
            star3str = "1"
            passrating = "3"
        }
        else
        {
            star1.setImage(UIImage.init(named: "starunselect1.png"), for: .normal)
            star2.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
            star3.setImage(UIImage.init(named: "starunselect3.png"), for: .normal)
            star4.setImage(UIImage.init(named: "starunselect4.png"), for: .normal)
            star5.setImage(UIImage.init(named: "starunselect5.png"), for: .normal)
            star3str = "0"
            passrating = ""
        }
    }
    
    @IBAction func star4button(_sender:UIButton)
    {
        
        if star4str == "0"
        {
            star1.setImage(UIImage.init(named: "starunselect1.png"), for: .normal)
            star2.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
            star3.setImage(UIImage.init(named: "starunselect3.png"), for: .normal)
            star4.setImage(UIImage.init(named: "starselect4.png"), for: .normal)
            star5.setImage(UIImage.init(named: "starselect5.png"), for: .normal)
            star4str = "1"
            passrating = "4"
        }
        else
        {
            star1.setImage(UIImage.init(named: "starunselect1.png"), for: .normal)
            star2.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
            star3.setImage(UIImage.init(named: "starunselect3.png"), for: .normal)
            star4.setImage(UIImage.init(named: "starunselect4.png"), for: .normal)
            star5.setImage(UIImage.init(named: "starunselect5.png"), for: .normal)
            star4str = "0"
            passrating = ""
        }
    }
    
    @IBAction func star5button(_sender:UIButton)
    {
        
        if star5str == "0"
        {
            star1.setImage(UIImage.init(named: "starunselect1.png"), for: .normal)
            star2.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
            star3.setImage(UIImage.init(named: "starunselect3.png"), for: .normal)
            star4.setImage(UIImage.init(named: "starunselect4.png"), for: .normal)
            star5.setImage(UIImage.init(named: "starselect5.png"), for: .normal)
            star5str = "1"
            passrating = "5"
        }
        else
        {
            star1.setImage(UIImage.init(named: "starunselect1.png"), for: .normal)
            star2.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
            star3.setImage(UIImage.init(named: "starunselect3.png"), for: .normal)
            star4.setImage(UIImage.init(named: "starunselect4.png"), for: .normal)
            star5.setImage(UIImage.init(named: "starunselect5.png"), for: .normal)
            star5str = "0"
            passrating = ""
        }
    }
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat)
    {
        print("Standard slider updated. Min Value: \(minValue) Max Value: \(maxValue)")
        if slider == rangesliderview
        {
            distancelab.font = UIFont.init(name: "Roboto-Bold", size: 14.0)
            distancelab.text = String(format: "%.2f Kms", maxValue)
            let ar = distancelab.text?.components(separatedBy: " ") as! NSArray
            let st = ar.object(at: 0) as! String
            passdistance = st
        }
        else
        {
            pricelab.font = UIFont.init(name: "Roboto-Bold", size: 14.0)
            pricelab.text = String(format: "%.1f CHF", maxValue)
            let ar = pricelab.text?.components(separatedBy: " ") as! NSArray
            let st = ar.object(at: 0) as! String
            passprice = st
        }
    
    }
    
    func didStartTouches(in slider: RangeSeekSlider) {
        print("did start touches")
        if slider == rangesliderview
        {
            distancelab.font = UIFont.init(name: "Roboto-Bold", size: 12.0)
        }
        else
        {
            pricelab.font = UIFont.init(name: "Roboto-Bold", size: 12.0)
        }
    }
    
    func didEndTouches(in slider: RangeSeekSlider)
    {
        print("did end touches")
        if slider == rangesliderview
        {
            distancelab.font = UIFont.init(name: "Roboto-Bold", size: 12.0)
        }
        else
        {
            pricelab.font = UIFont.init(name: "Roboto-Bold", size: 12.0)
        }
    }
    
    @IBAction func closebutton(_sender:UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func loadlink()
    {
        if AFNetworkReachabilityManager.shared().isReachable
        {
            let urlstring = String(format:"%@/filter_details.php",kBaseURL)
            print(urlstring)
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: true, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    if jsonObject.count != 0
                    {
                        self.filterarr.removeAllObjects()
                        self.filterarr.addObjects(from: jsonObject as [AnyObject])
                        print(self.filterarr)
                        if self.filterarr.count != 0
                        {
                            self.facilitiesarr.addObjects(from: (self.filterarr.object(at: 0) as AnyObject).value(forKey: "facility") as! [AnyObject])
                            self.accessibilityarr.addObjects(from: (self.filterarr.object(at: 0) as AnyObject).value(forKey: "accessbility") as! [AnyObject])
                            self.categoryarr.addObjects(from: (self.filterarr.object(at: 0) as AnyObject).value(forKey: "category") as! [AnyObject])
                        }
                        else
                        {
                            
                        }
                        print(self.facilitiesarr)
                        print(self.accessibilityarr)
                        print(self.categoryarr)
                        
                        if self.facilitiesarr.count != 0
                        {
                            let arr = NSMutableArray()
                            let mutdic = NSMutableDictionary()
                            for c in 0 ..< self.facilitiesarr.count
                            {
//                                arr.addObjects(from: (self.facilitiesarr.object(at: c) as AnyObject).value(forKey: "facilities") as! [AnyObject])
                                let datearrs = (self.facilitiesarr.object(at: c) as AnyObject).value(forKey: "facilities")
                                print(datearrs)
                                self.insidefacilities.add(datearrs)
                                print(self.insidefacilities)
                                
                            }
//                            arr.addObjects(from: self.insidefacilities as [AnyObject])
                            arr.add(self.insidefacilities)
                            print(arr)
                       
                        }
                        else
                        {
                            
                        }                        
                        print(self.insidefacilities)
                        
                        if UserDefaults.standard.object(forKey: "Filterdetail") == nil
                        {
                            self.rangesliderview.sizeToFit()
                            self.rangesliderview.delegate = self
                            self.rangesliderview.minValue = 0.0
                            self.rangesliderview.maxValue = 10.0
                            self.rangesliderview.selectedMinValue = 0.0
                            self.rangesliderview.selectedMaxValue = 10.0
                            self.rangesliderview.minDistance = 0.0
                            self.rangesliderview.maxDistance = 10.0
                            self.rangesliderview.handleDiameter = 30.0
                            self.rangesliderview.handleColor = UIColor.white
                            self.rangesliderview.backgroundColor = UIColor.clear
                            self.rangesliderview.selectedHandleDiameterMultiplier = 1.3
                            self.rangesliderview.numberFormatter.numberStyle = .none
                            //        rangesliderview.numberFormatter.locale = Locale(identifier: "en_US")
                            self.rangesliderview.numberFormatter.maximumFractionDigits = 1
                            self.rangesliderview.minLabelFont = UIFont(name: "Roboto-Regular", size: 10.0)!
                            self.rangesliderview.maxLabelFont = UIFont(name: "Roboto-Regular", size: 10.0)!
                            self.rangesliderview.lineHeight = 10.0
                            self.rangesliderview.minLabelColor = UIColor.clear
                            self.rangesliderview.maxLabelColor = UIColor.black
                            self.distancelab.font = UIFont.init(name: "Roboto-Bold", size: 12.0)
                            self.distancelab.text = String(format: "%.1f Kms", self.rangesliderview.selectedMaxValue)
                            
                            
                            self.priceranceslider.delegate = self
                            self.priceranceslider.minValue = 0.0
                            self.priceranceslider.maxValue = self.defaultDistance
                            self.priceranceslider.selectedMinValue = 0.0
                            self.priceranceslider.selectedMaxValue = 0.0
                            self.priceranceslider.minDistance = 0.0
                            self.priceranceslider.maxDistance = self.defaultDistance
                            self.priceranceslider.handleDiameter = 30.0
                            self.priceranceslider.handleColor = UIColor.white
                            self.priceranceslider.backgroundColor = UIColor.clear
                            self.priceranceslider.selectedHandleDiameterMultiplier = 1.3
                            self.priceranceslider.numberFormatter.numberStyle = .none
                            //        rangesliderview.numberFormatter.locale = Locale(identifier: "en_US")
                            self.priceranceslider.numberFormatter.maximumFractionDigits = 1
                            self.priceranceslider.minLabelFont = UIFont(name: "Roboto-Regular", size: 10.0)!
                            self.priceranceslider.maxLabelFont = UIFont(name: "Roboto-Regular", size: 10.0)!
                            self.priceranceslider.lineHeight = 10.0
                            self.priceranceslider.minLabelColor = UIColor.clear
                            self.priceranceslider.maxLabelColor = UIColor.black
                            //        priceranceslider.minlabel = String(format: "%@ Kms", numberFormatter.string(from: selectedMaxValue as NSNumber)!)
                            self.pricelab.font = UIFont.init(name: "Roboto-Bold", size: 12.0)
                            self.pricelab.text = String(format: "%.1f CHF", self.priceranceslider.selectedMaxValue)
                        }
                        else
                        {
                            let placesData = UserDefaults.standard.object(forKey: "Filterdetail") as? NSDictionary
                            self.getdic.removeAllObjects()
                            if placesData != nil{
                                self.getdic.addEntries(from: (placesData as? [String : Any])!)
                            }
                            print(self.getdic)
                            self.facilityidstore = ((self.getdic.value(forKey: "facility") ) ?? []) as! [String]
                            self.facility_cat_idstore = ((self.getdic.value(forKey: "facility_category") ) ?? []) as! [String]
                            self.accessibilityidstore = ((self.getdic.value(forKey: "accessibility") ) ?? []) as! [String]
                            self.categoryidstore = ((self.getdic.value(forKey: "category") ) ?? []) as! [String]
                            self.categorytype = ((self.getdic.value(forKey: "type")) ?? [] ) as! [String]
                            
                            self.passdistance = self.getdic.value(forKey: "distance") as? String
                            print(self.passdistance)
                            let myInt = (self.passdistance as? NSString)?.doubleValue
                         
                            self.rangesliderview.sizeToFit()
                            self.rangesliderview.delegate = self
                            self.rangesliderview.minValue = 0.0
                            self.rangesliderview.maxValue = 10.0
                            self.rangesliderview.selectedMinValue = 0.0
                            self.rangesliderview.selectedMaxValue = CGFloat(((self.passdistance as? NSString)?.doubleValue) ?? 0.0)
                            self.rangesliderview.minDistance = 0.0
                            self.rangesliderview.maxDistance = 10.0
                            self.rangesliderview.handleDiameter = 30.0
                            self.rangesliderview.handleColor = UIColor.white
                            self.rangesliderview.backgroundColor = UIColor.clear
                            self.rangesliderview.selectedHandleDiameterMultiplier = 1.3
                            self.rangesliderview.numberFormatter.numberStyle = .none
                            //        rangesliderview.numberFormatter.locale = Locale(identifier: "en_US")
                            self.rangesliderview.numberFormatter.maximumFractionDigits = 1
                            self.rangesliderview.minLabelFont = UIFont(name: "Roboto-Regular", size: 10.0)!
                            self.rangesliderview.maxLabelFont = UIFont(name: "Roboto-Regular", size: 10.0)!
                            self.rangesliderview.lineHeight = 10.0
                            self.rangesliderview.minLabelColor = UIColor.clear
                            self.rangesliderview.maxLabelColor = UIColor.black
                            self.distancelab.font = UIFont.init(name: "Roboto-Bold", size: 12.0)
                            self.distancelab.text = String(format: "%.1f Kms", self.rangesliderview.selectedMaxValue)
                            
                            self.passprice = self.getdic.value(forKey: "price") as? String
                            print(self.passprice)
                            let myInts = (self.passprice as? NSString)?.doubleValue
                            self.priceranceslider.delegate = self
                            self.priceranceslider.minValue = 0.0
                            self.priceranceslider.maxValue = self.defaultDistance
                            self.priceranceslider.selectedMinValue = 0.0
                            self.priceranceslider.selectedMaxValue = CGFloat(myInts ?? 0.0)
                            self.priceranceslider.minDistance = 0.0
                            self.priceranceslider.maxDistance = self.defaultDistance
                            self.priceranceslider.handleDiameter = 30.0
                            self.priceranceslider.handleColor = UIColor.white
                            self.priceranceslider.backgroundColor = UIColor.clear
                            self.priceranceslider.selectedHandleDiameterMultiplier = 1.3
                            self.priceranceslider.numberFormatter.numberStyle = .none
                            //        rangesliderview.numberFormatter.locale = Locale(identifier: "en_US")
                            self.priceranceslider.numberFormatter.maximumFractionDigits = 1
                            self.priceranceslider.minLabelFont = UIFont(name: "Roboto-Regular", size: 10.0)!
                            self.priceranceslider.maxLabelFont = UIFont(name: "Roboto-Regular", size: 10.0)!
                            self.priceranceslider.lineHeight = 10.0
                            self.priceranceslider.minLabelColor = UIColor.clear
                            self.priceranceslider.maxLabelColor = UIColor.black
                            //        priceranceslider.minlabel = String(format: "%@ Kms", numberFormatter.string(from: selectedMaxValue as NSNumber)!)
                            self.pricelab.font = UIFont.init(name: "Roboto-Bold", size: 12.0)
                            self.pricelab.text = String(format: "%.1f CHF", self.priceranceslider.selectedMaxValue)
                            
                            
                            //rating
                            self.passrating = self.getdic.value(forKey: "rating") as? String
                            print(self.passrating)
                            if self.passrating == "1"
                            {
                                self.star1.setImage(UIImage.init(named: "starselect1.png"), for: .normal)
                                self.star2.setImage(UIImage.init(named: "starselect2.png"), for: .normal)
                                self.star3.setImage(UIImage.init(named: "starselect3.png"), for: .normal)
                                self.star4.setImage(UIImage.init(named: "starselect4.png"), for: .normal)
                                self.star5.setImage(UIImage.init(named: "starselect5.png"), for: .normal)
                                self.star1str = "1"
                            }
                            else if self.passrating == "2"
                            {
                                self.star1.setImage(UIImage.init(named: "starunselect1.png"), for: .normal)
                                self.star2.setImage(UIImage.init(named: "starselect2.png"), for: .normal)
                                self.star3.setImage(UIImage.init(named: "starselect3.png"), for: .normal)
                                self.star4.setImage(UIImage.init(named: "starselect4.png"), for: .normal)
                                self.star5.setImage(UIImage.init(named: "starselect5.png"), for: .normal)
                                self.star2str = "1"
                            }
                            else if self.passrating == "3"
                            {
                                self.star1.setImage(UIImage.init(named: "starunselect1.png"), for: .normal)
                                self.star2.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
                                self.star3.setImage(UIImage.init(named: "starselect3.png"), for: .normal)
                                self.star4.setImage(UIImage.init(named: "starselect4.png"), for: .normal)
                                self.star5.setImage(UIImage.init(named: "starselect5.png"), for: .normal)
                                self.star3str = "1"
                            }
                            else if self.passrating == "4"
                            {
                                self.star1.setImage(UIImage.init(named: "starunselect1.png"), for: .normal)
                                self.star2.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
                                self.star3.setImage(UIImage.init(named: "starunselect3.png"), for: .normal)
                                self.star4.setImage(UIImage.init(named: "starselect4.png"), for: .normal)
                                self.star5.setImage(UIImage.init(named: "starselect5.png"), for: .normal)
                                self.star4str = "1"
                            }
                            else if self.passrating == "5"
                            {
                                self.star1.setImage(UIImage.init(named: "starunselect1.png"), for: .normal)
                                self.star2.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
                                self.star3.setImage(UIImage.init(named: "starunselect3.png"), for: .normal)
                                self.star4.setImage(UIImage.init(named: "starunselect4.png"), for: .normal)
                                self.star5.setImage(UIImage.init(named: "starselect5.png"), for: .normal)
                                self.star5str = "1"
                            }
                            else
                            {
                                self.star1.setImage(UIImage.init(named: "starunselect1.png"), for: .normal)
                                self.star2.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
                                self.star3.setImage(UIImage.init(named: "starunselect3.png"), for: .normal)
                                self.star4.setImage(UIImage.init(named: "starunselect4.png"), for: .normal)
                                self.star5.setImage(UIImage.init(named: "starunselect5.png"), for: .normal)
                                self.passrating = ""
                            }
                        }
                        self.priceranceslider.callRefresh()
                        self.rangesliderview.callRefresh()
                        
                        self.facilitycollection.reloadData()
                        self.accessibilitycollection.reloadData()
                        self.categorycollection.reloadData()
                    }
                }
                else
                {
                    NSLog("jsonObject=error");
                }
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        if collectionView == facilitycollection
        {
            return self.facilitiesarr.count
        }
        else if collectionView == accessibilitycollection
        {
            return 1
        }
        else
        {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == facilitycollection
        {
            let dic = self.insidefacilities.object(at: section) as! NSArray;
            print(dic)
            print("LOG:: COUNT \(dic.count)")
            return dic.count
        }
        else if collectionView == accessibilitycollection
        {
            return self.accessibilityarr.count
        }
        else
        {
            return self.categoryarr.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        //       let headerView = Bundle.main.loadNibNamed("FacilityHeaderCollectionReusableView", owner: self, options: nil)?[0] as! FacilityHeaderCollectionReusableView
        //        if collectionView == facilitycollection
        //        {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FacilityHeaderCollectionReusableView", for: indexPath) as! FacilityHeaderCollectionReusableView
        headerView.headernamelab.text = ""
        headerView.headerimg.isHidden = true
        let facilityCollectionCount = 3
        
        if self.facilitiesarr.count != 0 && (indexPath.section < facilityCollectionCount)
        {
            headerView.headerimg.isHidden = false
            let dataarr = (self.facilitiesarr.object(at: indexPath.section) as AnyObject).value(forKey: "facility_category_name") as! String
            let idarr = (self.facilitiesarr.object(at: indexPath.section) as AnyObject).value(forKey: "facility_category_id") as! String
            
            print("LOG:: \(dataarr.localized)")
            DispatchQueue.main.async {
                
                if indexPath.section == 0
                {
                    headerView.headernamelab.isHidden = false
                    headerView.headernamelab.text = dataarr.localized
                }
                else if indexPath.section == 1
                {
                    headerView.headernamelab.isHidden = false
                    headerView.headernamelab.text = dataarr.localized
                }
                else if indexPath.section == 2
                {
                    headerView.headernamelab.isHidden = false
                    headerView.headernamelab.text = dataarr.localized
                }
                else 
                {
                    headerView.headernamelab.text = ""
                    headerView.headernamelab.isHidden = true
                }
                
                print("LOG:: \(headerView.headernamelab)")
            }
            
            if idarr != ""
            {
                let imageurl = (self.facilitiesarr.object(at: indexPath.section) as AnyObject).value(forKey: "facility_category_enable_image") as! String
                if indexPath.section == 0
                {
                    if imageurl.characters.count == 0 || imageurl == ""
                    {
                        headerView.headerimg.image = UIImage.init(named: "default_image.png")
                    }
                    else
                    {
                        let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                            print(self)
                        }
                        headerView.headerimg.sd_setImage(with: NSURL(string:imageurl) as URL!, completed: block)
                        headerView.headerimg.contentMode = UIViewContentMode.scaleAspectFit;
                    }
                }
                else if indexPath.section == 1
                {
                    if imageurl.characters.count == 0 || imageurl == ""
                    {
                        headerView.headerimg.image = UIImage.init(named: "default_image.png")
                    }
                    else
                    {
                        let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                            print(self)
                        }
                        headerView.headerimg.sd_setImage(with: NSURL(string:imageurl) as URL!, completed: block)
                        headerView.headerimg.contentMode = UIViewContentMode.scaleAspectFit;
                    }
                    
                }
                else if indexPath.section == 2
                {
                    if imageurl.characters.count == 0 || imageurl == ""
                    {
                        headerView.headerimg.image = UIImage.init(named: "default_image.png")
                    }
                    else
                    {
                        let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                            print(self)
                        }
                        headerView.headerimg.sd_setImage(with: NSURL(string:imageurl) as URL!, completed: block)
                        headerView.headerimg.contentMode = UIViewContentMode.scaleAspectFit;
                    }
                    
                }
                //headerView.headerimg.image = UIImage.init(named: "checkboxselect.png")
            }
            else
            {
                //headerView.headerimg.image = UIImage.init(named: "checkboxunselect.png")
            }
        }
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == facilitycollection
        {
            if indexPath.section < 3 {
                
                let cell: FiltersCollectionViewCell = (collectionView.dequeueReusableCell(withReuseIdentifier: "FiltersCollectionViewCell", for: indexPath) as? FiltersCollectionViewCell)!
                if (cell.namelab != nil) {
                    cell.namelab.isHidden = false
                }
                
                if (cell.insideimg != nil) {
                    cell.insideimg.isHidden = false
                }
                
                if (cell.clickbutton != nil) {
                    cell.clickbutton.isHidden = false
                }
                
                self.collectionfacheight.constant = self.facilitycollection.contentSize.height
                
                dic = self.insidefacilities.object(at: indexPath.section) as! NSArray;
                if indexPath.section == 0
                {
                   let namestr = (dic.object(at: indexPath.item) as AnyObject).value(forKey: "facility_name") as! String
                    if namestr == ""
                    {
                        
                    }
                    else
                    {
                        cell.namelab.text = namestr.localized
                    }
                }
                else if indexPath.section == 1
                {
                    let namestr = (dic.object(at: indexPath.item) as AnyObject).value(forKey: "facility_name") as! String
                    if namestr == ""
                    {
                        
                    }
                    else
                    {
                        cell.namelab.text = namestr.localized
                    }
                }
                else if indexPath.section == 2
                {
                    let namestr = (dic.object(at: indexPath.item) as AnyObject).value(forKey: "facility_name") as! String
                    if namestr == ""
                    {
                        
                    }
                    else
                    {
                        cell.namelab.text = namestr.localized
                    }
                    
                }
                else if indexPath.section == 3
                {
                    let namestr = (dic.object(at: indexPath.item) as AnyObject).value(forKey: "facility_name") as! String
                    if namestr == ""
                    {
                        
                    }
                    else
                    {
                        cell.namelab.isHidden = true
                    }
                    
                }
                
                let apps = (dic.object(at: indexPath.item) as AnyObject).value(forKey: "facility_id") as! String
                print(apps)
                print(facilityidstore)
                let sectionKey = String(indexPath.section+1)
                if facilityidstore.contains(apps)
                {
                    let imageurl = (dic.object(at: indexPath.item) as AnyObject).value(forKey: "facility_enable_image") as! String
                    if imageurl.characters.count == 0 || imageurl == ""
                    {
                        cell.insideimg.image = UIImage.init(named: "default_image.png")
                    }
                    else
                    {
                        cell.insideimg.sd_setImage(with:URL(string: imageurl))
                        cell.insideimg.contentMode = UIViewContentMode.scaleAspectFit;
                    }
                    
                    print(facilityCategoryMapping)
                    if self.facilityCategoryMapping.value(forKey: sectionKey) != nil {
                        let value = facilityCategoryMapping.value(forKey: sectionKey) as? String
                        let newValue = Int(value!)! + 1
                        facilityCategoryMapping.setValue(String(newValue), forKey: sectionKey)
                    } else {
                       self.facilityCategoryMapping.setValue("1", forKey: sectionKey)
                    }
                    
                    print(facilityCategoryMapping)
                }
                else
                {
                    if indexPath.section == 0
                    {
                        let imageurl = (dic.object(at: indexPath.item) as AnyObject).value(forKey: "facility_disable_image") as! String
                        if imageurl.characters.count == 0 || imageurl == ""
                        {
                            cell.insideimg.image = UIImage.init(named: "default_image.png")
                        }
                        else
                        {
                            let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                                print(self)
                            }
                            cell.insideimg.sd_setImage(with: NSURL(string:imageurl) as URL!, completed: block)
                            cell.insideimg.contentMode = UIViewContentMode.scaleAspectFit;
                        }
                    }
                    else if indexPath.section == 1
                    {
                        let imageurl = (dic.object(at: indexPath.item) as AnyObject).value(forKey: "facility_disable_image") as! String
                        if imageurl.characters.count == 0 || imageurl == ""
                        {
                            cell.insideimg.image = UIImage.init(named: "default_image.png")
                        }
                        else
                        {
                            let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                                print(self)
                            }
                            cell.insideimg.sd_setImage(with: NSURL(string:imageurl) as URL!, completed: block)
                            cell.insideimg.contentMode = UIViewContentMode.scaleAspectFit;
                        }
                        
                    }
                    else if indexPath.section == 2
                    {
                        let imageurl = (dic.object(at: indexPath.item) as AnyObject).value(forKey: "facility_disable_image") as! String
                        if imageurl.characters.count == 0 || imageurl == ""
                        {
                            cell.insideimg.image = UIImage.init(named: "default_image.png")
                        }
                        else
                        {
                            let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                                print(self)
                            }
                            cell.insideimg.sd_setImage(with: NSURL(string:imageurl) as URL!, completed: block)
                            cell.insideimg.contentMode = UIViewContentMode.scaleAspectFit;
                        }
                        
                    }
                }
                return cell
            } else {
                let cell: FiltersCollectionViewCell = (collectionView.dequeueReusableCell(withReuseIdentifier: "FiltersCollectionViewCell", for: indexPath) as? FiltersCollectionViewCell)!
                if (cell.namelab != nil) {
                    cell.namelab.isHidden = true
                }
                
                if (cell.insideimg != nil) {
                    cell.insideimg.isHidden = true
                }
                
                if (cell.clickbutton != nil) {
                    cell.clickbutton.isHidden = true
                }
                return cell
            }
        }
            
        else if collectionView == accessibilitycollection
        {
            let cell: FiltersCollectionViewCell = (collectionView.dequeueReusableCell(withReuseIdentifier: "FiltersCollectionViewCell", for: indexPath) as? FiltersCollectionViewCell)!
            self.collectionaccheight.constant = self.accessibilitycollection.contentSize.height
            
            let apps = (self.accessibilityarr.object(at: indexPath.item) as AnyObject).value(forKey: "accessbility_id") as! String
            print(apps)
            print(accessibilityidstore)
            if accessibilityidstore.contains(apps)
            {
                let imageurl = (self.accessibilityarr.object(at: indexPath.item) as AnyObject).value(forKey: "accessbility_enable_image") as! String
                if imageurl.characters.count == 0 || imageurl == ""
                {
                    cell.insideimg.image = UIImage.init(named: "default_image.png")
                }
                else
                {
                    let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                        print(self)
                    }
                    cell.insideimg.sd_setImage(with: NSURL(string:imageurl) as URL!, completed: block)
                    cell.insideimg.contentMode = UIViewContentMode.scaleAspectFit;
                }
                
            }
            else
            {
                let imageurl = (self.accessibilityarr.object(at: indexPath.item) as AnyObject).value(forKey: "accessbility_disable_image") as! String
                if imageurl.characters.count == 0 || imageurl == ""
                {
                    cell.insideimg.image = UIImage.init(named: "default_image.png")
                }
                else
                {
                    let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                        print(self)
                    }
                    cell.insideimg.sd_setImage(with: NSURL(string:imageurl) as URL!, completed: block)
                    cell.insideimg.contentMode = UIViewContentMode.scaleAspectFit;
                }
            }
            
            let namestr = (self.accessibilityarr.object(at: indexPath.item) as AnyObject).value(forKey: "accessbility_name") as! String
            if namestr == ""
            {
                
            }
            else
            {
                cell.namelab.text = namestr.localized
            }
            return cell
        }
        
        else
        {
            let cell: FiltersCollectionViewCell = (collectionView.dequeueReusableCell(withReuseIdentifier: "FiltersCollectionViewCell", for: indexPath) as? FiltersCollectionViewCell)!
            self.collectioncatheight.constant = self.categorycollection.contentSize.height
            
            let apps = (self.categoryarr.object(at: indexPath.item) as AnyObject).value(forKey: "category_id") as! String
            print(apps)
            print(categoryidstore)
            if categoryidstore.contains(apps)
            {
                let imageurl = (self.categoryarr.object(at: indexPath.item) as AnyObject).value(forKey: "category_enable_image") as! String
                if imageurl.characters.count == 0 || imageurl == ""
                {
                    cell.insideimg.image = UIImage.init(named: "default_image.png")
                }
                else
                {
                    let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                        print(self)
                    }
                    cell.insideimg.sd_setImage(with: NSURL(string:imageurl) as URL!, completed: block)
                    cell.insideimg.contentMode = UIViewContentMode.scaleAspectFit;
                }
            }
            else
            {
                let imageurl = (self.categoryarr.object(at: indexPath.item) as AnyObject).value(forKey: "category_disable_image") as! String
                if imageurl.characters.count == 0 || imageurl == ""
                {
                    cell.insideimg.image = UIImage.init(named: "default_image.png")
                }
                else
                {
                    let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                        print(self)
                    }
                    
                    cell.insideimg.sd_setImage(with: NSURL(string:imageurl) as URL!, completed: block)
                    cell.insideimg.contentMode = UIViewContentMode.scaleAspectFit;
                }
            }
            
           
            
            let namestr = (self.categoryarr.object(at: indexPath.item) as AnyObject).value(forKey: "category_name") as! String
            if namestr == ""
            {
                
            }
            else
            {
                cell.namelab.text = namestr.localized
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == facilitycollection
        {
            dic = self.insidefacilities.object(at: indexPath.section) as! NSArray;
            print(dic)
            let ids = (dic.object(at: indexPath.item) as AnyObject).value(forKey: "facility_id") as! String
            
            print("ids \(ids)")
            
            if facilityidstore.contains("")
            {
                self.facilityidstore.remove(at:0)
            }
            
            if facility_cat_idstore.contains("")
            {
                self.facility_cat_idstore.remove(at:0)
            }
            let selectedSection = String(indexPath.section + 1) //facility list starts from 1
            if facilityidstore.contains(ids)
            {
                let indexforObject = facilityidstore.index(of: ids)
                self.facilityidstore.remove(at: indexforObject!)
                
                //The dictionary contains the list for mapping with number of selected sub sections
                // If subsection is 0 then it will be removed
                if (facilityCategoryMapping.value(forKey: selectedSection) != nil) {
                    let value = facilityCategoryMapping.value(forKey: selectedSection) as? String
                    if value == "1" {
                        facilityCategoryMapping.removeObject(forKey: selectedSection)
                        if self.facility_cat_idstore.contains(selectedSection) {
                            if let indexOfItem = self.facility_cat_idstore.index(of: selectedSection) {
                                self.facility_cat_idstore.remove(at: indexOfItem)
                            }
                        }
                    } else {
                        let newValue = Int(value!)! - 1
                        facilityCategoryMapping.setValue(String(newValue), forKey: selectedSection)
                    }
                } else {
                    facilityCategoryMapping.setValue("1", forKey: selectedSection)
                }
            }
            else
            {
                //Update sub category
                self.facilityidstore.append(ids)
                
                //Update Category
                if !self.facility_cat_idstore.contains(selectedSection) {
                    self.facility_cat_idstore.append(selectedSection)
                }
                //The dictionary contains the list for mapping with number of selected sub sections
                // If subsection is 0 then it will be removed
                if (facilityCategoryMapping.value(forKey: selectedSection) != nil) {
                    let value = facilityCategoryMapping.value(forKey: selectedSection) as? String
                    let newValue = Int(value!)! + 1
                    facilityCategoryMapping.setValue(String(newValue), forKey: selectedSection)
                } else {
                    facilityCategoryMapping.setValue("1", forKey: selectedSection)
                }   
            }
            let indexPath = IndexPath(row: indexPath.item, section: indexPath.section)
            self.facilitycollection.reloadItems(at: [indexPath])
            let indexSet = IndexSet(integer: indexPath.section)
            self.facilityCategoryMapping.removeObject(forKey: selectedSection)
            self.facilitycollection.reloadSections(indexSet)
            print(facilityCategoryMapping)
            
        }
        else if collectionView == accessibilitycollection
        {
            let ids = (self.accessibilityarr.object(at: indexPath.item) as AnyObject).value(forKey: "accessbility_id") as! String
            print(ids)
            //        let ids = dic.value(forKey: "facility_id") as? String;
            if accessibilityidstore.contains("")
            {
                self.accessibilityidstore.remove(at:0)
            }
            if accessibilityidstore.contains(ids)
            {
                let indexforObject = accessibilityidstore.index(of: ids)
                self.accessibilityidstore.remove(at: indexforObject!)
            }
            else
            {
                self.accessibilityidstore.append(ids)
            }
            let indexPath = IndexPath(row: indexPath.item, section: 0)
            self.accessibilitycollection.reloadItems(at: [indexPath])
        }
        else
        {
            let ids = (self.categoryarr.object(at: indexPath.item) as AnyObject).value(forKey: "category_id") as! String
            print(ids)
            if categoryidstore.contains("")
            {
                self.categoryidstore.remove(at:0)
            }
            if categoryidstore.contains(ids)
            {
                let indexforObject = categoryidstore.index(of: ids)
                self.categoryidstore.remove(at: indexforObject!)
            }
            else
            {
                self.categoryidstore.append(ids)
            }
            let indexPath = IndexPath(row: indexPath.item, section: 0)
            self.categorycollection.reloadItems(at: [indexPath])
        }
    }
    
    @IBAction func applybutton(_sender : UIButton)
    {
        let nsmutdic = NSMutableDictionary()
        nsmutdic.setObject(passrating, forKey:"rating" as NSCopying);
        nsmutdic.setObject(passdistance, forKey:"distance" as NSCopying);
        nsmutdic.setObject(passprice, forKey:"price" as NSCopying);
        nsmutdic.setObject(facilityidstore, forKey:"facility" as NSCopying);
        nsmutdic.setObject(facility_cat_idstore, forKey:"facility_category" as NSCopying);
        nsmutdic.setObject(accessibilityidstore, forKey:"accessibility" as NSCopying);
        nsmutdic.setObject(categoryidstore, forKey:"category" as NSCopying);
         nsmutdic.setObject(categorytype, forKey:"type" as NSCopying);
        print(nsmutdic)
        //let data = NSKeyedArchiver.archivedData(withRootObject: nsmutdic) as? NSMutableDictionary
        UserDefaults.standard.set(nsmutdic, forKey:"Filterdetail")
        UserDefaults.standard.synchronize()
        
        let valueToSave = "comingfromfilter"
        UserDefaults.standard.set(valueToSave, forKey: "screen")
        let userId = UserDefaults.standard.string(forKey: "user_id") ?? ""
        let userName = UserDefaults.standard.string(forKey: "user_name") ?? ""
        FIRAnalytics.logEvent(withName: "filter_applied", parameters: [
            "user_id": userId,
            "user_name": userName,
            "filter_type" : "filter_button",
            "filter_params" : nsmutdic,
            "login_device" : "iOS",
            "content_type" : "user_filter_applied"
            
            ])
        //        next.fromfilter
        self.dismiss(animated: true, completion: nil)

    }
    
//    func loadlink()
//    {
//        if AFNetworkReachabilityManager.shared().isReachable
//        {
//            let urlstring = String(format:"%@/filter_details.php",kBaseURL)
//            print(urlstring)
//            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: true, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
//                if Stats == true
//                {
//                    NSLog("jsonObject=%@", jsonObject);
//                    if jsonObject.count != 0
//                    {
//                        self.filterarr .removeAllObjects()
//                        self.filterarr.addObjects(from: jsonObject as [AnyObject])
//                        print(self.filterarr)
//                    }
//                }
//                else
//                {
//                    NSLog("jsonObject=error");
//                }
//            })
//        }
//        else
//        {
//            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("JEDE_r"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
//        }
//    }
    
    @IBAction func resetbutton(_sender: UIButton)
        
    {

        let valueToSave = "comingfromfilter"
        UserDefaults.standard.set(valueToSave, forKey: "screen")
        UserDefaults.standard.removeObject(forKey: "Filterdetail")
        UserDefaults.standard.synchronize()

        passdistance = "10.0"
        passprice = ""

        star1.setImage(UIImage.init(named: "starunselect1.png"), for: .normal)
        star2.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
        star3.setImage(UIImage.init(named: "starunselect3.png"), for: .normal)
        star4.setImage(UIImage.init(named: "starunselect4.png"), for: .normal)
        star5.setImage(UIImage.init(named: "starunselect5.png"), for: .normal)


        self.rangesliderview.sizeToFit()
        self.rangesliderview.delegate = self
        self.rangesliderview.minValue = 0.0
        self.rangesliderview.maxValue = 10.0
        self.rangesliderview.selectedMinValue = 0.0
        self.rangesliderview.selectedMaxValue = 10.0 //Change to 0 if distance is need to reset
        self.rangesliderview.minDistance = 0.0
        self.rangesliderview.maxDistance = 10.0
        self.rangesliderview.handleDiameter = 30.0
        self.rangesliderview.handleColor = UIColor.white
        self.rangesliderview.backgroundColor = UIColor.clear
        self.rangesliderview.selectedHandleDiameterMultiplier = 1.3
        self.rangesliderview.numberFormatter.numberStyle = .none
        //        rangesliderview.numberFormatter.locale = Locale(identifier: "en_US")
        self.rangesliderview.numberFormatter.maximumFractionDigits = 1
        self.rangesliderview.minLabelFont = UIFont(name: "Roboto-Regular", size: 10.0)!
        self.rangesliderview.maxLabelFont = UIFont(name: "Roboto-Regular", size: 10.0)!
        self.rangesliderview.lineHeight = 10.0
        self.rangesliderview.minLabelColor = UIColor.clear
        self.rangesliderview.maxLabelColor = UIColor.black
        self.distancelab.font = UIFont.init(name: "Roboto-Bold", size: 12.0)
        self.distancelab.text = String(format: "%.1f Kms", self.rangesliderview.selectedMaxValue)

        self.priceranceslider.delegate = self
        self.priceranceslider.minValue = 0.0
        self.priceranceslider.maxValue = defaultDistance
        self.priceranceslider.selectedMinValue = 0.0
        self.priceranceslider.selectedMaxValue = 0.0
        self.priceranceslider.minDistance = 0.0
        self.priceranceslider.maxDistance = defaultDistance
        self.priceranceslider.handleDiameter = 30.0
        self.priceranceslider.handleColor = UIColor.white
        self.priceranceslider.backgroundColor = UIColor.clear
        self.priceranceslider.selectedHandleDiameterMultiplier = 1.3
        self.priceranceslider.numberFormatter.numberStyle = .none
        self.priceranceslider.numberFormatter.maximumFractionDigits = 1
        self.priceranceslider.minLabelFont = UIFont(name: "Roboto-Regular", size: 10.0)!
        self.priceranceslider.maxLabelFont = UIFont(name: "Roboto-Regular", size: 10.0)!
        self.priceranceslider.lineHeight = 10.0
        self.priceranceslider.minLabelColor = UIColor.clear
        self.priceranceslider.maxLabelColor = UIColor.black
        self.pricelab.font = UIFont.init(name: "Roboto-Bold", size: 12.0)
        self.pricelab.text = String(format: "%.1f CHF", self.priceranceslider.selectedMaxValue)
        
        self.priceranceslider.callRefresh()
        self.rangesliderview.callRefresh()
        
        //Reset stored data!!
       
        self.facilityidstore.removeAll()
        self.facility_cat_idstore.removeAll()
        self.accessibilityidstore.removeAll()
        self.categoryidstore.removeAll()
        self.facilityCategoryMapping.removeAllObjects()
        
        
        //Run the service to fetch new data which implicitly has reload collection view
        //Reset the collection view data
        
        self.facilitiesarr.removeAllObjects()
        self.accessibilityarr.removeAllObjects()
        self.categoryarr.removeAllObjects()
        self.insidefacilities.removeAllObjects()
        self.loadlink()
        
        self.priceranceslider.callRefresh()
        self.rangesliderview.callRefresh()
        
        //Reload Collection view
//        self.facilitycollection.reloadData()
//        self.accessibilitycollection.reloadData()
        
  }
}
