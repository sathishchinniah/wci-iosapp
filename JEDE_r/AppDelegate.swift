
//
//  AppDelegate.swift
//  JEDE_r
//
//  Created by Exarcplus iOS 2 on 29/04/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import CoreLocation
import AFNetworking
import LGSideMenuController
import FBSDKLoginKit
import IQKeyboardManagerSwift
import UserNotifications
import SVProgressHUD
import Fabric
import Crashlytics
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    
    var SideMenu:LGSideMenuController!
    var SideMenuView:SidemenuViewController!
    var myclass:MyClass!
    var HUD : MBProgressHUD!
    
    var locationManager: CLLocationManager!
    let googleMapsApiKey = "AIzaSyCHRCZX_HC5MVr6pTBtQU7coRT-kvzgNho" //AS PER SOURCE
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FIRApp.configure()
        Fabric.with([Crashlytics.self])
        myclass = MyClass()
        AFNetworkReachabilityManager.shared().startMonitoring()
        Localization.sharedInstance().setPreferred("en", fallback: nil)
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        GMSServices.provideAPIKey(googleMapsApiKey)
        
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setDefaultAnimationType(.flat)
    
        //facebook
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        GIDSignIn.sharedInstance().clientID = KClientID
        
        UINavigationBar.appearance().tintColor = UIColor.black
        let launchedBefore = UserDefaults.standard.bool(forKey:"launchedBefore")
        
        if UserDefaultManager.sharedManager.userAgreedTerms()  {
            let mainview = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
            let nav = UINavigationController.init(rootViewController: mainview)
            SideMenu = LGSideMenuController.init(rootViewController: nav)
           
            SideMenu.setLeftViewEnabledWithWidth(280, presentationStyle: .slideAbove, alwaysVisibleOptions: [])
            SideMenuView = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "SidemenuViewController") as! SidemenuViewController
            
            if SideMenuView.vendorsts == ""
            {
                let refreshAlert = UIAlertController(title: "WCI", message: "Please Login into your Account", preferredStyle: UIAlertControllerStyle.alert)
                
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                    print("Handle Ok logic here")
                }))
                
                self.SideMenuView.present(refreshAlert, animated: true, completion: nil)
                
            }
            else
            {
                print("LOG: /(second Time)")
            }
            ////
            SideMenu.leftViewStatusBarVisibleOptions = .onAll
            SideMenu.leftViewStatusBarStyle = .lightContent
           
            var rect = SideMenuView.view.frame;
            rect.size.width = 280;
            SideMenuView.view.frame = rect
            SideMenu.leftView().addSubview(SideMenuView.view)
            
             // UserDefaults.standard.set(false,forKey:"launchedBefore")
            
        } else {
            let settingsViewController = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            let nav = UINavigationController.init(rootViewController: settingsViewController)
            UserDefaults.standard.set(true,forKey:"launchedBefore")
            self.window?.rootViewController = nav;
        }
        
        if #available(iOS 10.0, *){
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(granted, error) in
                if (granted)
                {
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
                else
                {
                    //Do stuff if unsuccessful...
                }
            })
        }
        else{ //If user is not on iOS 10 use the old methods we've been using
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
            
        }
        return true
    }
    
    //for facebook
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool
    {
        
        if (url.scheme?.contains("fb"))!
        {
            return FBSDKApplicationDelegate.sharedInstance().application(application, open: url as URL!, sourceApplication: sourceApplication, annotation: annotation)
        }
        else
        {
            //            return GPPURLHandler.handle(url as URL!,sourceApplication: sourceApplication,annotation: annotation)
            return GIDSignIn .sharedInstance() .handle(url , sourceApplication: sourceApplication, annotation: annotation)
        }
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //for notification
    // MARK: - Remote Notification
    func application(_ application: UIApplication,didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        //send this device token to server
        print("My token is: %@", deviceToken);
        let str = NSString(format:"%@",deviceToken as CVarArg);
        var token = str.replacingOccurrences(of: " ", with:"")
        token = token.replacingOccurrences(of: "<", with:"")
        token = token.replacingOccurrences(of: ">", with:"")
        NSLog("token->%@",token)
        
        UserDefaults.standard.set(token, forKey: "token")
        UserDefaults.standard.synchronize()
        
        if UserDefaults.standard.value(forKey: "Logindetail") == nil {

        } else {
            let Userdic = UserDefaults.standard.object(forKey: "Logindetail") as? NSDictionary
            let Userid = Userdic?.value(forKey: "user_id") as? String
            if UserDefaults.standard.object(forKey: "token") == nil
            {
                if AFNetworkReachabilityManager.shared().isReachable
                {
                    let manager = AFHTTPSessionManager();
                    manager.responseSerializer = AFJSONResponseSerializer()
                    manager.responseSerializer.acceptableContentTypes = ["text/html"]
                    let urlstring = String(format:"%@/update_ios_device.php?",kBaseURL);
                    let parameters = ["user_id":Userid,"device_id":token]
                    manager.get(urlstring.forurl(), parameters:parameters, success:{(task,responseObject) -> Void in
                        print("JSON: %@", responseObject as! NSDictionary);
                        let jsonResults = responseObject as! NSDictionary
                        if jsonResults.value(forKey: "result") != nil
                        {
                            let copyarr = jsonResults.value(forKey: "result") as! NSArray;
                            if (copyarr.object(at:0) as AnyObject).value(forKey: "message") as! String == "success"
                            {
                                UserDefaults.standard.set(token, forKey:"token")
                                UserDefaults.standard.synchronize()
                            }
                        }
                        
                    }, failure:{(task,error) -> Void in
                        print("Error: %@", error);
                    })
                }
                else
                {
                    
                }
            }
            else
            {
                let oldtoken = UserDefaults.standard.object(forKey: "token") as! String
                if token == oldtoken
                {
                    if AFNetworkReachabilityManager.shared().isReachable
                    {
                        let manager = AFHTTPSessionManager();
                        manager.responseSerializer = AFJSONResponseSerializer()
                        manager.responseSerializer.acceptableContentTypes = ["text/html"]
                        let urlstring = String(format:"%@/update_ios_device.php?",kBaseURL)
                        let parameters = ["user_id":Userid,"device_id":token]
                        manager.get(urlstring.forurl(), parameters:parameters, success:{(task,responseObject) -> Void in
                            print("JSON: %@", responseObject as! NSDictionary);
                            let jsonResults = responseObject as! NSDictionary
                            if jsonResults.value(forKey: "result") != nil
                            {
                                let copyarr = jsonResults.value(forKey: "result") as! NSArray;
                                if (copyarr.object(at:0) as AnyObject).value(forKey: "message") as! String == "sucess"
                                {
                                    UserDefaults.standard.set(token, forKey:"token")
                                    UserDefaults.standard.synchronize()
                                }
                            }
                            
                        }, failure:{(task,error) -> Void in
                            print("Error: %@", error);
                        })
                    }
                    else
                    {
                        
                    }
                }
            }
            
        }
    }
    //Called if unable to register for APNS.
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any])
    {
        if application.applicationState == .inactive || application.applicationState == .background {
            //opened from a push notification when the app was on background
        }
        else
        {
            if application.applicationState == .active
            {
                let myuserinfo = userInfo as NSDictionary
                print(myuserinfo)
                
                let tempDict:NSDictionary = (myuserinfo.value(forKey: "aps") as? NSDictionary)!
                let alert = UIAlertView(title: "alertView", message: (tempDict.value(forKey: "alert") as? String)!, delegate: nil, cancelButtonTitle:"Cancel", otherButtonTitles: "Done", "Delete")
                alert.show()
                let person:NSDictionary = (tempDict.value(forKey: "custom") as? NSDictionary)!
            }
        }
    }
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        print("GOT A NOTIFICATION")
        
    }
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void)
    {
        if UIApplication.shared.applicationState == .inactive || UIApplication.shared.applicationState == .background
        {
            //opened from a push notification when the app was on background
        }
        else
        {
            if UIApplication.shared.applicationState == .active
            {
                
                let myuserinfo = response.notification.request.content.userInfo as NSDictionary
                print(myuserinfo)
                let tempDict: NSDictionary = (myuserinfo.value(forKey: "aps") as? NSDictionary)!
                let alert = UIAlertView(title: "alertView", message: (tempDict.value(forKey: "Getting one notification") as? String)!, delegate: nil, cancelButtonTitle:"Cancel", otherButtonTitles: "Done", "Delete")
                alert.show()
                let person:NSDictionary = (tempDict.value(forKey: "custom") as? NSDictionary)!
            }
        }
    }
    
    
    func gotoview(dic:NSDictionary)
    {
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        completionHandler(.newData)
    }
}

extension String
{
    func forurl() -> String
    {
        return self.addingPercentEscapes(using: String.Encoding.utf8)! as String;
    }
}

