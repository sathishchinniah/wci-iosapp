//
//  ConfirmationBookingPopup.swift
//  JEDE_r
//
//  Created by Exarcplus on 31/05/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit

class ConfirmationBookingPopup: UIView {
    
    @IBOutlet weak var looimg : UIImageView!
    @IBOutlet weak var loonamelab : UILabel!
    @IBOutlet weak var datelab : UILabel!
    @IBOutlet weak var timelab : UILabel!
    @IBOutlet weak var bookbutton : UIButton!
    @IBOutlet weak var closebutton : UIButton!
    @IBOutlet weak var termsandconditionsbutton : UIButton!

    @IBOutlet weak var agreeMessage: UILabel!
    @IBOutlet weak var confirmBooking: UILabel!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var bookAndNavigateButton: UIButton!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
