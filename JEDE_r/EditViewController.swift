//
//  EditViewController.swift
//  JEDE_r
//
//  Created by Exarcplus on 22/05/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class EditViewController: UIViewController, UITextFieldDelegate
{
    var myclass : MyClass!
    var updatearr = NSMutableArray()
    
    @IBOutlet weak var scrollview : UIScrollView!
    @IBOutlet weak var fullnametext: SkyFloatingLabelTextField!
    @IBOutlet weak var mobilenotext: SkyFloatingLabelTextField!
    var previousdic : NSMutableDictionary!
    var passuserid : String!
    var passimage : String!

    @IBOutlet weak var updateBtn: UIButton!
    override func viewDidLoad()
    {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        myclass = MyClass()
        if previousdic != nil
        {
            print(previousdic)
            IQKeyboardManager.shared.enable = true
            IQKeyboardManager.shared.shouldResignOnTouchOutside = true
            IQKeyboardManager.shared.enableAutoToolbar = false
            
            mobilenotext.keyboardType = .namePhonePad
            fullnametext.placeholder = "profile_layout_user_name".localized
            mobilenotext.placeholder = "profile_layout_mobile".localized
            fullnametext.text = "profile_layout_user_name".localized
            mobilenotext.text = "profile_layout_mobile".localized
            self.title = "edit_profile_toolbar_text".localized
            self.updateBtn.setTitle("edit_profile_update".localized, for: .normal)
            
            fullnametext.delegate = self
            mobilenotext.delegate = self
            fullnametext.text = self.previousdic.value(forKey: "user_name") as? String
            mobilenotext.text = self.previousdic.value(forKey: "mobile_num") as? String
            passuserid = self.previousdic.value(forKey: "user_id") as? String
            passimage = self.previousdic.value(forKey: "image") as? String
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func loginbuttonclick(sender: UIButton)
    {
        if fullnametext.text == "" && mobilenotext.text == ""
        {
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.07
            animation.repeatCount = 4
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint(x: fullnametext.center.x - 10,y :fullnametext.center.y))
            animation.toValue = NSValue(cgPoint: CGPoint(x: fullnametext.center.x + 10,y :fullnametext.center.y))
            fullnametext.layer.add(animation, forKey: "position")
            
            let animation1 = CABasicAnimation(keyPath: "position")
            animation1.duration = 0.07
            animation1.repeatCount = 4
            animation1.autoreverses = true
            animation1.fromValue = NSValue(cgPoint: CGPoint(x: mobilenotext.center.x - 10,y :mobilenotext.center.y))
            animation1.toValue = NSValue(cgPoint: CGPoint(x: mobilenotext.center.x + 10,y :mobilenotext.center.y))
            mobilenotext.layer.add(animation1, forKey: "position")
        }
        else if fullnametext.text == ""
        {
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.07
            animation.repeatCount = 4
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint(x: fullnametext.center.x - 10,y :fullnametext.center.y))
            animation.toValue = NSValue(cgPoint: CGPoint(x: fullnametext.center.x + 10,y :fullnametext.center.y))
            fullnametext.layer.add(animation, forKey: "position")
        }
        else if mobilenotext.text == ""
        {
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.07
            animation.repeatCount = 4
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint(x: mobilenotext.center.x - 10,y :mobilenotext.center.y))
            animation.toValue = NSValue(cgPoint: CGPoint(x: mobilenotext.center.x + 10,y :mobilenotext.center.y))
            mobilenotext.layer.add(animation, forKey: "position")
        }
        else if fullnametext.text != ""
        {
           
                self.view.endEditing(true)
                let devicetoken : String!
                if UserDefaults.standard.string(forKey: "token") == nil
                {
                    devicetoken = ""
                }
                else
                {
                    devicetoken = UserDefaults.standard.string(forKey: "token") as String?
                }
                let urlstring = String(format:"%@/profile_update.php?user_id=%@&user_name=%@&mobile_num=%@&address=&image=%@",kBaseURL,passuserid,fullnametext.text!,mobilenotext.text!,passimage);
                print(urlstring)
                self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: true, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                    if Stats == true
                    {
                        NSLog("jsonObject=%@", jsonObject);
                        let message = (jsonObject.object(at: 0) as AnyObject).value(forKey:"message") as! String
                        if message == "success"
                        {
                            NSLog("jsonObject=%@", jsonObject);
                            self.updatearr = jsonObject.mutableCopy() as! NSMutableArray
                            let data = NSKeyedArchiver.archivedData(withRootObject: self.updatearr.object(at: 0) as! NSDictionary)
                            print("AFTER UPDATE")
                            print(self.updatearr)
                            UserDefaults.standard.set(self.updatearr.object(at: 0), forKey:"Logindetail")
                            UserDefaults.standard.synchronize()
                            self.navigationController?.popViewController(animated: true)
                   
                        }
                        else
                        {
                            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Please Check Your Username and Password"), withIdentifier:"")
                        }
                    }
                    else
                    {
                        
                    }
                })
        }
        
    }
    
    @IBAction func backbutton(_sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
