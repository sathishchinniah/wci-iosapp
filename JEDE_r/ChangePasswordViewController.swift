//
//  ChangePasswordViewController.swift
//  JEDE_r
//
//  Created by Exarcplus on 22/05/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class ChangePasswordViewController: UIViewController, UITextFieldDelegate {
    
    var myclass : MyClass!
    var previousdic : NSMutableDictionary!
    var updatearr = NSMutableArray()
    @IBOutlet weak var newpasswordtext : SkyFloatingLabelTextField!
    @IBOutlet weak var confirmpasswordtext : SkyFloatingLabelTextField!
    var passuserid : String!

    @IBOutlet weak var updateBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        myclass = MyClass()
        
        if previousdic != nil
        {
            IQKeyboardManager.shared.enable = true
            IQKeyboardManager.shared.shouldResignOnTouchOutside = true
            IQKeyboardManager.shared.enableAutoToolbar = false
            
            passuserid = self.previousdic.value(forKey: "user_id") as! String
            if(UserDefaultManager.sharedManager.getLanguageName() == "English"){
                newpasswordtext.placeholder = "Enter New Password"
                confirmpasswordtext.placeholder = "Enter Confirm Password"
                self.title = "Change Password"
                self.updateBtn.setTitle("UPDATE", for: .normal)
                
            }
            else if(UserDefaultManager.sharedManager.getLanguageName() == "German"){
                newpasswordtext.placeholder = "Neues Passwort eingeben"
                confirmpasswordtext.placeholder = "Geben Sie Passwort bestätigen ein"
                self.title = "Passwort ändern"
                self.updateBtn.setTitle("AKTUALISIEREN", for: .normal)
            }
            else if(UserDefaultManager.sharedManager.getLanguageName() == "French"){
                newpasswordtext.placeholder = "Entrez un nouveau mot de passe"
                confirmpasswordtext.placeholder = "Entrer le mot de passe"
                self.title = "Changer le mot de passe"
                self.updateBtn.setTitle("METTRE À JOUR", for: .normal)
            }
            
          
            newpasswordtext.delegate = self
            //            fullnametext.placeholderActiveColor = UIColor.white
            //            fullnametext.placeholderInactiveColor = UIColor.init(colorLiteralRed: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0)
            
            
            confirmpasswordtext.delegate = self
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginbuttonclick(sender: UIButton)
    {
        if newpasswordtext.text == "" && confirmpasswordtext.text == ""
        {
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.07
            animation.repeatCount = 4
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint(x: newpasswordtext.center.x - 10,y :newpasswordtext.center.y))
            animation.toValue = NSValue(cgPoint: CGPoint(x: newpasswordtext.center.x + 10,y :newpasswordtext.center.y))
            newpasswordtext.layer.add(animation, forKey: "position")
            
            let animation1 = CABasicAnimation(keyPath: "position")
            animation1.duration = 0.07
            animation1.repeatCount = 4
            animation1.autoreverses = true
            animation1.fromValue = NSValue(cgPoint: CGPoint(x: confirmpasswordtext.center.x - 10,y :confirmpasswordtext.center.y))
            animation1.toValue = NSValue(cgPoint: CGPoint(x: confirmpasswordtext.center.x + 10,y :confirmpasswordtext.center.y))
            confirmpasswordtext.layer.add(animation1, forKey: "position")
        }
        else if newpasswordtext.text == ""
        {
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.07
            animation.repeatCount = 4
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint(x: newpasswordtext.center.x - 10,y :newpasswordtext.center.y))
            animation.toValue = NSValue(cgPoint: CGPoint(x: newpasswordtext.center.x + 10,y :newpasswordtext.center.y))
            newpasswordtext.layer.add(animation, forKey: "position")
        }
        else if confirmpasswordtext.text == ""
        {
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.07
            animation.repeatCount = 4
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint(x: confirmpasswordtext.center.x - 10,y :confirmpasswordtext.center.y))
            animation.toValue = NSValue(cgPoint: CGPoint(x: confirmpasswordtext.center.x + 10,y :confirmpasswordtext.center.y))
            confirmpasswordtext.layer.add(animation, forKey: "position")
        }
        else if newpasswordtext.text != ""
        {
            if newpasswordtext.text == confirmpasswordtext.text
            {
                self.view.endEditing(true)
                let devicetoken : String!
                if UserDefaults.standard.string(forKey: "token") == nil
                {
                    devicetoken = ""
                }
                else
                {
                    devicetoken = UserDefaults.standard.string(forKey: "token") as String?
                }
                let urlstring = String(format:"%@/change_password.php?user_id=%@&password=%@",kBaseURL,passuserid,newpasswordtext.text!);
                print(urlstring)
                self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: true, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                    if Stats == true
                    {
                        NSLog("jsonObject=%@", jsonObject);
                        let message = (jsonObject.object(at: 0) as AnyObject).value(forKey:"message") as! String
                        if message == "success"
                        {
                            NSLog("jsonObject=%@", jsonObject);
                            self.updatearr = jsonObject.mutableCopy() as! NSMutableArray
                            print(self.updatearr)
//                            let data = NSKeyedArchiver.archivedData(withRootObject: self.updatearr.object(at: 0) as! NSDictionary)
//                            UserDefaults.standard.set(data, forKey:"Logindetail")
//                            UserDefaults.standard.synchronize()
                            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:"Your Password Updated Successfully", withIdentifier:"")
                            self.navigationController?.popViewController(animated: true)
                            
                        }
                        else
                        {
                            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Please Check Your Username and Password"), withIdentifier:"")
                        }
                    }
                    else
                    {
                        
                    }
                })
            }
            else
            {
                self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:"Your Password and Confirm Password does not match", withIdentifier:"")
            }
            
        }
        
    }
    
    @IBAction func backbutton(_sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
