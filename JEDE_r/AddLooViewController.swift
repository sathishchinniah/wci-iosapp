//
//  AddLooViewController.swift
//  JEDE_r
//
//  Created by Exarcplus on 30/05/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import AFNetworking
import SDWebImage
import KKActionSheet
import Agrume
import ImageSlideshow
import Alamofire
import Alamofire
import SwiftyJSON
import DatePickerDialog
import Crashlytics

class AddLooViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDataSource, UITableViewDelegate,UIActionSheetDelegate,PECropViewControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate, UITextViewDelegate, UIScrollViewDelegate,GMSMapViewDelegate,CLLocationManagerDelegate
{
    @IBOutlet weak var MoreInformationView: UIView!
    @IBOutlet weak var MoreInformationHeight: NSLayoutConstraint!
    var myclass : MyClass!
    @IBOutlet weak var facilitytable : UITableView!
    @IBOutlet weak var accessibilitycollection : UICollectionView!
    @IBOutlet weak var categorycollection : UICollectionView!
    @IBOutlet var tablefacheight : NSLayoutConstraint!
    @IBOutlet var collectionaccheight : NSLayoutConstraint!
    @IBOutlet  var collectioncatheight : NSLayoutConstraint!
    
    @IBOutlet weak var startDate: UIButton!
    @IBOutlet weak var endDate: UIButton!
    @IBOutlet weak var slidingImages: ImageSlideshow!
    
    @IBOutlet weak var priceView: UIView!
    
    @IBOutlet weak var datePickerView: UIView!
    @IBOutlet weak var nameWrapperView: UILabel!
    @IBOutlet weak var freeProviderView: UIView!
    @IBOutlet weak var dateTable: UITableView!
    
    var isEditMode = Bool()
    var vendorlooId:String = ""
    
    var day1 = [String]()
    var time1 = [String]()
    
    var myImage = [UIImage]()
    var myImageName = [String]()
    
    
    var looId = ""
    var looName = ""
    var looAddress = ""
    var looImage = ""
    var looPrice = ""
    var looLocation = ""
    var looType = ""
    var tandc = ""
    var looDays = ""
    var looTimings = ""
    
    var looCategory = [Dictionary<String,AnyObject>]()
    var looFacilities = [Dictionary<String,AnyObject>]()
    var looAccessbility = [Dictionary<String,AnyObject>]()
    
    var ds = [Dictionary<String,AnyObject>]()
    
    var cat = [String]()
    var fact = [String]()
    var count = [Double]()
    var detailsarr = NSMutableArray()
    
    var offerer_id = [String]()
    var accessibility_id = [String]()

    var image_to_s = ""
    
    var filterarr = NSMutableArray()
    var facilitiesarr = NSMutableArray()
    var insidefacilities = NSMutableArray()
    var accessibilityarr = NSMutableArray()
    var categoryarr = NSMutableArray()
    var facilityidstore = NSMutableArray()
    var facility_cat_idstore = NSMutableArray()
    var accessibilityidstore = NSMutableArray()
    var categoryidstore = NSMutableArray()
    var dic = NSMutableArray()
    var imageHud:MyHUDProgress!
    var checkimg : String!
    var Userdic = NSMutableDictionary()
    var passuserid : String!
    @IBOutlet weak var firstimageview : UIImageView!
    @IBOutlet weak var secondimageview : UIImageView!
    @IBOutlet weak var thirdimageview : UIImageView!
    @IBOutlet weak var bannerimageview : UIImageView!
    @IBOutlet weak var bannerimageview1 : UIImageView!
    @IBOutlet weak var bannerimageview2 : UIImageView!
    var firstimgstr : String!
    var secondimgstr : String!
    var thirdimgstr : String!
    @IBOutlet weak var termsandcondtextview : UITextView!
    @IBOutlet weak var addresstextview : UITextView!
    @IBOutlet weak var infotextview : UITextView!
    @IBOutlet weak var scrollView : UIScrollView!
    @IBOutlet weak var pagecontrol : UIPageControl!
    var bannerimageviewarr:[UIImage] = []
    @IBOutlet weak var firstimagebutton : UIButton!
    var selectArray = NSMutableArray()
    var timearr = NSMutableArray()
    @IBOutlet weak var daytable : UITableView!
    //var arrayText:NSMutableArray!;
    var arrayText = NSMutableArray()
    var lastid = NSMutableArray()
    
    var location_cordinates = ""
    var storedLocationData = ""
    
    var selectdic : NSMutableDictionary!
    
    @IBOutlet weak var goButtonImage: UIImageView!
    
    @IBOutlet weak var yourBusinessConditionView: UIView!
    @IBOutlet weak var viewmap: GMSMapView!
    var firstload = Bool()
    var locmanager:CLLocationManager!
    var currentlocation = CLLocation()
    var currentadddress:GMSAddress!
    var myaddressstr : String!
    var addssstr : String!
    var displaymarker:GMSMarker!
    var x:Int = 0
    
    var newds = [String: [String: [String: String]]]()
    
    @IBOutlet weak var looNameTF: SkyFloatingLabelTextField!
    
    @IBOutlet weak var looPriceTF: SkyFloatingLabelTextField!
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var uploadLabel: UIButton!
    @IBOutlet weak var catagoryLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var accessbilityLabel: UILabel!
    @IBOutlet weak var termsandconditionsLabel: UILabel!
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var checkBoxLabel: UILabel!
    
    var id : String!
    var isCheckBoxChecked = Bool()
    let checkboxUnSelected = "checkboxunselect"
    let checkboxSelected = "checkboxselect"
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        MoreInformationHeight.constant = 0
        MoreInformationView.isHidden = true
        dateTable.tableFooterView = UIView(frame: CGRect.zero)
        myclass = MyClass()
        timesave.removeAll()
        print(detailsarr)
        self.slidingImages.activityIndicator = DefaultActivityIndicator()
        
        print(detailsarr.count)
        
        if self.detailsarr.count>0{
        
        id = (self.detailsarr[0] as AnyObject).value(forKey: "loo_id") as! String
        
        }
        print(id)
        if id == nil
        {
            id = ""
        }
        self.navigationController?.navigationBar.isHidden = false
        // Do any additional setup after loading the view.
        self.addresstextview.isEditable = true
        
        slidingImages.setImageInputs([
            ImageSource(image: UIImage(named: "default_image")!),
            ])

        self.firstimgstr = ""
        self.secondimgstr = ""
        self.thirdimgstr = ""
        
        dateTable.delegate = self
        dateTable.dataSource = self
        dateTable.reloadData()
        
        
        //self.bannerimageview.image = UIImage.init(named: "default_image.png")
        
        facilitytable.register(UINib.init(nibName:"AddFacilityTableViewCell", bundle: nil),forCellReuseIdentifier: "AddFacilityTableViewCell")
        facilitytable.estimatedRowHeight = 48;
        facilitytable.rowHeight = 48
        facilitytable.tableFooterView?.isHidden = true
        
        checkimg = "firstimg"
        
        let view = UIView(frame:  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 20))
        view.backgroundColor = UIColor.init(red: 27.0/255.0, green: 34.0/255.0, blue: 40.0/255.0, alpha: 1.0)
        self.view.addSubview(view)
        self.navigationController?.navigationBar.isHidden = false
        if UserDefaults.standard.object(forKey: "Logindetail") == nil
        {
            passuserid = ""
        }
        else
        {
            
            let placesData = UserDefaults.standard.object(forKey: "Logindetail") as? NSDictionary
            self.Userdic.removeAllObjects()
            if placesData != nil{
                self.Userdic.addEntries(from: (placesData as? [String : Any])!)
            }
            passuserid = self.Userdic.value(forKey: "user_id") as? String
            print(passuserid)

        }

        
        if UserDefaults.standard.object(forKey: "CartList") == nil
        {
            
        }
        else
        {   UserDefaults.standard.removeObject(forKey: "CartList")
            UserDefaults.standard.synchronize()
            
        }
        
        termsandcondtextview.text = "add_loo_enter_terms_and_condition_here".localized
        //"Your Title/Headlines \n \u{2022} Type your conditions... "
        termsandcondtextview.textColor = UIColor.lightGray
        termsandcondtextview.selectedTextRange = termsandcondtextview.textRange(from: termsandcondtextview.beginningOfDocument, to: termsandcondtextview.beginningOfDocument)
        self.termsandcondtextview.delegate = self
        
        
        if myaddressstr != nil
        {
            //            let point = viewmap.center
            //            let coor = viewmap.projection .coordinate(for: point)
            //            let loc = CLLocation.init(latitude: coor.latitude, longitude: coor.longitude)
            
            
            
            print(myaddressstr)
            let arr = myaddressstr.components(separatedBy: ",") as NSArray
            let String1 = arr.object(at: 0) as! String
            let String2 = arr.object(at: 1) as! String
            let lat = Double(String1)
            let lon = Double(String2)
            
            let loc = CLLocation.init(latitude: lat!, longitude: lon!)
            currentlocation = loc;
        }
        else
        {
            let point = viewmap.center
            let coor = viewmap.projection .coordinate(for: point)
            let loc = CLLocation.init(latitude: coor.latitude, longitude: coor.longitude)
            currentlocation = loc;
        }
        
        viewmap.clear()
        locmanager = CLLocationManager()
        locmanager.delegate = self as CLLocationManagerDelegate
        locmanager.desiredAccuracy = kCLLocationAccuracyBest
        locmanager.requestAlwaysAuthorization()
        let dispatchTime2: DispatchTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime2, execute: {
            self.locmanager.startUpdatingLocation()
        })
        
        viewmap.settings.myLocationButton=false
        viewmap.isMyLocationEnabled = true
        viewmap.settings.compassButton=true
        viewmap.delegate = self;
        
        
        
        displaymarker = GMSMarker()
        displaymarker.position = CLLocationCoordinate2DMake(currentlocation.coordinate.latitude, currentlocation.coordinate.longitude)
        let camera = GMSCameraPosition.init(target: currentlocation.coordinate, zoom: 14, bearing: 0, viewingAngle: 0)
        viewmap .animate(to: camera)
        
        for c in 0 ..< 7
        {
            let nsmutdic = NSMutableDictionary()
            nsmutdic.setValue("", forKey: "time")
            self.timearr.add(nsmutdic)

        }
        
        self.looNameTF.placeholder = "add_loo_hint_text_loo_name".localized
        self.looPriceTF.placeholder = "add_loo_price".localized
        self.catagoryLabel.text = "filter_pop_layout_category".localized
        self.addressLabel.text = "loo_details_address".localized
        self.accessbilityLabel.text = "Accessibility".localized
        self.termsandconditionsLabel.text = "terms and conditions".localized
        self.timeLabel.text = "add_loo_days_and_time".localized
        self.checkBoxLabel.text = "add_loo_checkbox_label".localized
        isCheckBoxChecked = false
        
        //If the login user is city login then load the UI for city login
        //Otherwise hide the UI elements
        if self.isClientLoginUser() {
            self.loadUIForCityLogin()
        } else {
            self.hideUIForCityLogin()
        }
        
        self.loadlink()
        getDatastructure()
        
    }

    @IBAction func startDateAction(_ sender: Any) {
        //Picker view
        DatePickerDialog().show("start_date".localized, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                self.startDate.setTitle(formatter.string(from: dt), for: .normal)
            }
        }
    }
    
    @IBAction func endDateAction(_ sender: Any) {
        //Picker view
        DatePickerDialog().show("end_date".localized, doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                self.endDate.setTitle(formatter.string(from: dt), for: .normal)
            }
        }
    }
    
    
    func isClientLoginUser() -> Bool{
        let userDict = UserDefaults.standard.object(forKey: "Logindetail") as? NSDictionary
        let clientLogin = userDict?.value(forKey: "is_ClientLogin") as? String
        print("CLIENT LOGIN \(clientLogin)")
        return (clientLogin == "1") ? true : false
    }
    
    func subsetTheCategoryListForClientLogin(isCityLogin: Bool) {
        let cityType = "city"
        print("LOG:CATE1 \(self.categoryarr)")
        var objectIds = [Int]()
        var subsetArray = NSMutableArray()
        for i in 0..<self.categoryarr.count {
            let type = (self.categoryarr.object(at:i) as AnyObject).value(forKey: "type") as! String
            if isCityLogin {
                if type == cityType {
                    subsetArray.add(self.categoryarr.object(at:i) as AnyObject)
                    objectIds.append(i)
                }
            } else {
                if type != cityType {
                    subsetArray.add(self.categoryarr.object(at:i) as AnyObject)
                    objectIds.append(i)
                }
            }
        }
        self.categoryarr.removeAllObjects()
        self.categoryarr = subsetArray
        print("LOG:CATE2 \(self.categoryarr)")
        self.categorycollection.reloadData()
    }
    
    func hideUIForCityLogin() {
        freeProviderView.isHidden = false
        priceView.isHidden = false
        startDate.isHidden = true
        endDate.isHidden = true
        datePickerView.isHidden = true
        datePickerView.removeFromSuperview()
        
    }
    
    func loadUIForCityLogin() {
        //Free provider
        freeProviderView.isHidden = true
        freeProviderView.removeConstraints(freeProviderView.constraints)
        freeProviderView.frame = CGRect.zero
        
        //Price details remove
        priceView.isHidden = true
        priceView.removeConstraints(priceView.constraints)
        priceView.frame = CGRect.zero
        goButtonImage.isHidden = true
        
        //Your business conditions remove
        yourBusinessConditionView.isHidden = true
        yourBusinessConditionView.removeConstraints(yourBusinessConditionView.constraints)
        yourBusinessConditionView.frame = CGRect.zero
        
        //Name wrapper view
        nameWrapperView.frame = CGRect(x: nameWrapperView.frame.origin.x, y: nameWrapperView.frame.origin.y, width: nameWrapperView.frame.size.width, height: nameWrapperView.frame.size.height/2)
        
        self.timeLabel.text = "add_loo_days".localized
        self.timeLabel.numberOfLines = 0
        
        startDate.contentHorizontalAlignment = .center
        startDate.setTitle("start_date".localized, for: .normal)
        
        endDate.contentHorizontalAlignment = .center
        endDate.setTitle("end_date".localized, for: .normal)
        
        
    }


    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    func configurePageControl() {
        // The total number of pages that are available is based on how many available colors we have.
        
        self.pagecontrol.numberOfPages = 3
        self.pagecontrol.currentPage = 0
        self.view.addSubview(pagecontrol)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error:" + error.localizedDescription)
        if !CLLocationManager .locationServicesEnabled()
        {
            let alert: UIAlertView = UIAlertView(title: "Turn on Location Service!", message: "Please Turn on Location to known where you are. We promise to keep your location private.", delegate: self, cancelButtonTitle: "Settings")
            alert.delegate = self
            alert.restorationIdentifier = "userLocation"
            alert.tag = 100
            alert.show()
        }
        else
        {
            if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.denied
            {
                let alert: UIAlertView = UIAlertView(title: "Enable Location Service!", message: "Please enable Location Based Services for Loocater to known where you are. We promise to keep your location private.", delegate: self, cancelButtonTitle: "Settings")
                alert.delegate = self
                alert.restorationIdentifier = "userLocation"
                alert.tag = 200
                alert.show()
            }
            else
            {
                manager.startUpdatingLocation()
            }
        }
    }
    
    func alertView(_ View: UIAlertView, clickedButtonAt buttonIndex: Int)
    {
        switch buttonIndex{
        case 1:
            NSLog("Retry");
            break;
        case 0:
            NSLog("Dismiss");
            if View.restorationIdentifier=="userLocation"
            {
                if buttonIndex == 0
                {
                    if View.tag == 100
                    {
                        //                        UIApplication.shared.openURL(URL(string:"prefs:root=LOCATION_SERVICES")!)
                        UIApplication.shared.openURL(URL(string:UIApplicationOpenSettingsURLString)!)
                        //UIApplication .shared .openURL(NSURL (string: "prefs:root=LOCATION_SERVICES")! as URL)
                        
                    }
                    else if View.tag == 200
                    {
                        //                        UIApplication.shared.openURL(URL(string:"prefs:root=LOCATION_SERVICES")!)
                        UIApplication.shared.openURL(URL(string:UIApplicationOpenSettingsURLString)!)
                        //UIApplication .shared .openURL(NSURL (string: "prefs:root=LOCATION_SERVICES")! as URL)
                        //                       UIApplication .shared .openURL(NSURL (string: UIApplicationOpenSettingsURLString)! as URL);
                        
                    }
                }
            }
            break;
        default:
            NSLog("Default");
            break;
            //Some code here..
        }
    }
    
    
    func getAddressFromLatLon(location:CLLocation)
    {
        //Get address only when it is in add loo mode not in edit mode
        
        if !isEditMode {
            //        if AFNetworkReachabilityManager.shared().isReachable
            //        {
            let str = String(format: "%f,%f",location.coordinate.latitude,location.coordinate.longitude)
            print(str)
            
            location_cordinates = str
            
            print(location)
            let geoCoder = GMSGeocoder()
            geoCoder.reverseGeocodeCoordinate(location.coordinate, completionHandler: {(respones,err) in
                if err == nil
                {
                    if respones != nil
                    {
                        let gmaddr = respones?.firstResult()
                        print(gmaddr)
                        self.currentadddress = gmaddr
                        var addrarray : NSMutableArray! = NSMutableArray()
                        let count:Int = (gmaddr?.lines!.count)!
                        for c in 0 ..< count
                        {
                            addrarray.add(gmaddr?.lines![c] as! String)
                        }
                        
                        let formaddr = addrarray.componentsJoined(by: ", ");
                        NSLog("%@",formaddr)
                        addrarray = nil
                        self.addssstr = formaddr
                        
                        let throughfare : String!
                        let subloc : String!
                        let locali : String!
                        let administrivear : String!
                        let countrystr : String!
                        //fromloocater
                        
                        if gmaddr?.thoroughfare == nil || gmaddr?.thoroughfare == ""
                        {
                            throughfare = ""
                        }
                        else
                        {
                            throughfare = gmaddr?.value(forKey: "thoroughfare") as! String
                            print(throughfare)
                        }
                        
                        if gmaddr?.subLocality == nil || gmaddr?.subLocality == ""
                        {
                            subloc = ""
                        }
                            
                        else
                        {
                            subloc = gmaddr?.value(forKey: "subLocality") as! String
                            print(subloc)
                        }
                        
                        
                        if gmaddr?.locality == nil || gmaddr?.locality == ""
                        {
                            locali = ""
                        }
                        else
                        {
                            locali = gmaddr?.value(forKey: "locality") as! String
                            print(locali)
                        }
                        
                        if gmaddr?.administrativeArea == nil || gmaddr?.administrativeArea == ""
                        {
                            administrivear = ""
                        }
                        else
                        {
                            administrivear = gmaddr?.value(forKey: "administrativeArea") as! String
                            print(administrivear)
                        }
                        
                        if gmaddr?.country == nil || gmaddr?.country == ""
                        {
                            countrystr = ""
                        }
                        else
                        {
                            countrystr = gmaddr?.value(forKey: "country") as! String
                            print(countrystr)
                        }
                        
                        //self.addresstextview.text = String(format: "%@, %@", throughfare,subloc)
                        self.addresstextview.text = String(format: "%@, %@, %@, %@, %@", throughfare,subloc,locali, administrivear,countrystr)
                        
                    }
                    else
                    {
                        self.addssstr = "Address not found"
                    }
                }
                else
                {
                    //                    self.myclass.ShowsinglebutAlertwithTitle(title: self.myclass.StringfromKey(Key: "Loocater"), withAlert:self.myclass.StringfromKey(Key: "errorinretrive"), withIdentifier:"error")
                }
            })
        }
    }
    
    //Action when the delete button is pressed
    
    @IBAction func deleteImageAction(_ sender: Any) {
        print("LOG:im1 \(self.slidingImages)")
        let currentPage = self.slidingImages.currentPage
        var images = self.slidingImages.images
        if currentPage < self.slidingImages.images.count {
            images.remove(at: currentPage)
            self.slidingImages.setImageInputs(images)
            if currentPage < self.myImageName.count {
                self.myImageName.remove(at: currentPage)
            }
        }
        print("LOG:im2 \(self.slidingImages)")
    }
    
    @IBAction func openaddaddress(_ x:AnyObject)
    {
        if isEditMode {
            return
        }
        let viewController = kmainStoryboard.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
        //        let nav = UINavigationController.init(rootViewController: viewController)
        self.locmanager.stopUpdatingLocation()
        self.navigationController?.pushViewController(viewController, animated: true)
    }

    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        viewmap.settings.compassButton = true
        print("didUpdateToLocation: %@", locations.last!)
        if  firstload == false
        {
            firstload = true
            if myaddressstr != nil
            {
                
            }
            else
            {
                currentlocation = locations.last!
            }
            
            
            viewmap.clear()
            displaymarker.icon = UIImage.init(named: "LocationPin")
            displaymarker.position = CLLocationCoordinate2DMake(currentlocation.coordinate.latitude, currentlocation.coordinate.longitude)
            displaymarker.map  = viewmap
            
            let camera = GMSCameraPosition.init(target: currentlocation.coordinate, zoom: 14, bearing: 0, viewingAngle: 0)
            viewmap .animate(to: camera)
            
            let cameradisp = GMSCameraPosition.init(target: currentlocation.coordinate, zoom: 14, bearing: 0, viewingAngle: 0)
            viewmap.animate(to: cameradisp)
            
            self.getAddressFromLatLon(location: currentlocation)
        }
    }

    
    override func viewWillAppear(_ animated: Bool) {
        
        MoreInformationHeight.constant = 0
        MoreInformationView.isHidden = true
        dateTable.reloadData()
        self.navigationController?.navigationBar.isHidden = false
        
        //Localized title
        if isEditMode {
            self.title = "calender_main_layout_edit".localized
            self.uploadLabel.setTitle("edit_loo_save".localized, for: .normal)
            self.addresstextview.isEditable = false
            self.viewmap.isUserInteractionEnabled = false
        } else {
            self.title = "ADD_A_TOILET".localized
            self.uploadLabel.setTitle("Upload".localized, for: .normal)
            self.viewmap.isUserInteractionEnabled = true
            self.addresstextview.isEditable = false
        }
        
        if UserDefaults.standard.object(forKey: "storedateandtime") == nil
        {
            
        }
        else
        {
            let myretrievedic = NSMutableDictionary()
            let Data = UserDefaults.standard.object(forKey: "storedateandtime") as? NSData
            myretrievedic.addEntries(from: NSKeyedUnarchiver.unarchiveObject(with: Data! as Data) as! NSDictionary as [NSObject : AnyObject])
            print(Data)
            print(myretrievedic)
            print(myretrievedic["timings"])
            print(x)
        }
            
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == termsandcondtextview
        {
            if termsandcondtextview.textColor == UIColor.lightGray {
                termsandcondtextview.text = nil
                termsandcondtextview.textColor = UIColor.black
            }
        }
        else if textView == infotextview
        {
            if infotextview.textColor == UIColor.lightGray {
                infotextview.text = nil
                infotextview.textColor = UIColor.black
            }
        }
        else
        {
            if addresstextview.textColor == UIColor.lightGray {
                addresstextview.text = nil
                addresstextview.textColor = UIColor.black
            }
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView == termsandcondtextview
        {
            if termsandcondtextview.text.isEmpty {
                termsandcondtextview.text = "Your Title/Headlines \n \u{2022} Type your conditions... "
                termsandcondtextview.textColor = UIColor.lightGray
            }
        }
        else if textView == infotextview
        {
            if infotextview.text.isEmpty {
                infotextview.text = "Enter your Information about this Loo"
                infotextview.textColor = UIColor.lightGray
            }
        }
        
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        // If the replacement text is "\n" and the
        // text view is the one you want bullet points
        // forsdaf
        if (text == "\n") {
            // If the replacement text is being added to the end of the
            // text view, i.e. the new index is the length of the old
            // text view's text...
            
            
            if range.location == textView.text.characters.count {
                // Simply add the newline and bullet point to the end
                //                var updatedText: String = termsandcondtextview.text!.stringByAppendingString("\n \u{2022} ")
                let updatedText : String = termsandcondtextview.text!.appending("\n \u{2022} ")
                textView.text = updatedText
            }
            else {
                
                // Get the replacement range of the UITextView
                let beginning: UITextPosition = textView.beginningOfDocument
                let start: UITextPosition = termsandcondtextview.position(from: beginning, offset: range.location)!
                let end: UITextPosition = termsandcondtextview.position(from: start, offset: range.length)!
                let textRange : UITextRange = termsandcondtextview.textRange(from: start, to: end)!
                //                var start: UITextPosition = textView.positionFromPosition(beginning, offset: range.location)!
                //                var end: UITextPosition = textView.positionFromPosition(start, offset: range.length)!
                //                var textRange: UITextRange = textView.textRangeFromPosition(start, toPosition: end)!
                // Insert that newline character *and* a bullet point
                // at the point at which the user inputted just the
                // newline character
                termsandcondtextview.replace(textRange, withText: "\n \u{2022} ")
                //                textView.replaceRange(textRange, withText: "\n \u{2022} ")
                // Update the cursor position accordingly
                let cursor: NSRange = NSMakeRange(range.location + "\n \u{2022} ".characters.count, 0)
                //                var cursor: NSRange = NSMakeRange(range.location + "\n \u{2022} ".length, 0)
                termsandcondtextview.selectedRange = cursor
            }
            
            return false
            
            
        }
        // Else return yes
        return true
    }
    
    @IBAction func dateandtimebutton(_ sender: UIButton)
    {
        //For the client login the date picker is shown for the range of the dates
        if (self.isClientLoginUser()) {
            return
        }
        
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "DateandTimeViewController") as! DateandTimeViewController
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    @IBAction func OpenSideMenu(_ x:AnyObject)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.SideMenu.showLeftView(animated: true, completionHandler: nil)
          NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadsidemenu"), object: nil, userInfo: nil)
        
    }
    
    func config( x:[Dictionary<String,AnyObject>]){
        
        var x = x
        x[0]["loo_id"] = "" as AnyObject
        x[0]["loo_name"] = "" as AnyObject
        x[0]["loo_image"] = "" as AnyObject
        x[0]["price"] =  "" as AnyObject
        x[0]["loo_location"] = "" as AnyObject
        x[0]["type"] = "" as AnyObject
        //x[0]["category"] = "" as AnyObject
        x[0]["days"] = "" as AnyObject
        x[0]["timings"] = "" as AnyObject
        x[0]["terms_conditions"] = "" as AnyObject
        
        print(x)
      
    }
    
    func loadlink()
    {
        if AFNetworkReachabilityManager.shared().isReachable
        {
            let urlstring = String(format:"%@/filter_details.php",kBaseURL)
            print(urlstring)
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: true, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                
                
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    let x = jsonObject as? [Dictionary<String,AnyObject>]
                    print(x)
                    self.config(x: x!)
                    if jsonObject.count != 0
                    {
                        
                        self.filterarr .removeAllObjects()
                        self.filterarr.addObjects(from: jsonObject as [AnyObject])
                        print(self.filterarr)
                        if self.filterarr.count != 0 {
                            self.facilitiesarr.addObjects(from: (self.filterarr.object(at: 0) as AnyObject).value(forKey: "facility") as! [AnyObject])
                            self.accessibilityarr.addObjects(from: (self.filterarr.object(at: 0) as AnyObject).value(forKey: "accessbility") as! [AnyObject])
                            self.categoryarr.addObjects(from: (self.filterarr.object(at: 0) as AnyObject).value(forKey: "category") as! [AnyObject])
 
                        }
                        
                        print(self.facilitiesarr)
                        print(self.accessibilityarr)
                        print(self.categoryarr)
                        
                        if self.categoryarr.count > 0 {
                            if (self.isClientLoginUser()){
                                self.subsetTheCategoryListForClientLogin(isCityLogin: true)
                            } else {
                                self.subsetTheCategoryListForClientLogin(isCityLogin: false)
                            }
                        }
                        
                        var mydic = NSMutableDictionary()
                        if self.facilitiesarr.count != 0
                        {
                            let arr = NSMutableArray()
                            print(self.facilitiesarr.count)
                            for c in 0 ..< self.facilitiesarr.count
                            {
                                //                                arr.addObjects(from: (self.facilitiesarr.object(at: c) as AnyObject).value(forKey: "facilities") as! [AnyObject])
                                let datearrs = (self.facilitiesarr.object(at: c) as AnyObject).value(forKey: "facilities") as! NSMutableArray
                                print(datearrs)
                                
                                for x in 0 ..< datearrs.count
                                {
                                    mydic = datearrs.object(at: x) as! NSMutableDictionary
                                    mydic.setObject("0", forKey: "Count" as NSCopying)
                                    if datearrs.contains(mydic)
                                    {
                                        
                                    }
                                    else
                                    {
                                        datearrs.add(mydic)
                                    }
                                    
                                }
                                if self.insidefacilities.contains(datearrs)
                                {
                                    
                                }
                                else
                                {
                                    self.insidefacilities.add(datearrs)
                                    print(self.insidefacilities)
                                }
                            }
                            arr.add(self.insidefacilities)
                            print(arr)
                        }
                        else
                        {
                            
                        }
                        
                        print(self.insidefacilities)
                        self.facilitytable.reloadData()
                        self.accessibilitycollection.reloadData()
                        self.categorycollection.reloadData()
                        
                        if self.isEditMode {
                            self.getEditData()
                        }
                    }
                        
                }
                else
                {
                    NSLog("jsonObject=error");
                }
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
//        if collectionView == facilitytable
//        {
//            return self.facilitiesarr.count
//        }
        if collectionView == accessibilitycollection
        {
            return 1
        }
        else
        {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
//        if collectionView == facilitycollection
//        {
//            let dic = self.insidefacilities.object(at: section) as! NSArray;
//            print(dic)
//            return dic.count
//        }
        if collectionView == accessibilitycollection
        {
            print(accessibilityarr)
            return self.accessibilityarr.count
        }
        else
        {
            print(categoryarr)
            return self.categoryarr.count
        }
        
    }

    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        if collectionView == accessibilitycollection
        {
            let cell: FiltersCollectionViewCell = (collectionView.dequeueReusableCell(withReuseIdentifier: "FiltersCollectionViewCell", for: indexPath) as? FiltersCollectionViewCell)!
            self.collectionaccheight.constant = self.accessibilitycollection.contentSize.height
            
            let apps = (self.accessibilityarr.object(at: indexPath.item) as AnyObject).value(forKey: "accessbility_id") as! String
            print(apps)
            print(ds)
            print(indexPath.item)
            
            //print(ds[0]["accessbility"]?["\(indexPath.item)"]?!["status"]?++ )
            
            print(accessibilityidstore)
            if accessibilityidstore.contains(apps)
            {
                let imageurl = (self.accessibilityarr.object(at: indexPath.item) as AnyObject).value(forKey: "accessbility_enable_image") as! String
                if imageurl.characters.count == 0 || imageurl == ""
                {
                    cell.insideimg.image = UIImage.init(named: "default_image.png")
                }
                else
                {
                    let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                        print(self)
                    }
                    cell.insideimg.sd_setImage(with: NSURL(string:imageurl) as URL!, completed: block)
                    cell.insideimg.contentMode = UIViewContentMode.scaleToFill;
                }
                
            }
            else
            {
                let imageurl = (self.accessibilityarr.object(at: indexPath.item) as AnyObject).value(forKey: "accessbility_disable_image") as! String
                if imageurl.characters.count == 0 || imageurl == ""
                {
                    cell.insideimg.image = UIImage.init(named: "default_image.png")
                }
                else
                {
                    let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                        print(self)
                    }
                    cell.insideimg.sd_setImage(with: NSURL(string:imageurl) as URL!, completed: block)
                    cell.insideimg.contentMode = UIViewContentMode.scaleToFill;
                }
            }
            
            let namestr = (self.accessibilityarr.object(at: indexPath.item) as AnyObject).value(forKey: "accessbility_name") as! String
            if namestr == ""
            {
                
            }
            else
            {
                cell.namelab.text = namestr.localized
            }
            return cell
        }
            
        else
        {
            let cell: FiltersCollectionViewCell = (collectionView.dequeueReusableCell(withReuseIdentifier: "FiltersCollectionViewCell", for: indexPath) as? FiltersCollectionViewCell)!
            self.collectioncatheight.constant = self.categorycollection.contentSize.height
            
            let apps = (self.categoryarr.object(at: indexPath.item) as AnyObject).value(forKey: "category_id") as! String
            print(apps)
            print(categoryidstore)
            if categoryidstore.contains(apps)
            {
                let imageurl = (self.categoryarr.object(at: indexPath.item) as AnyObject).value(forKey: "category_enable_image") as! String
                if imageurl.characters.count == 0 || imageurl == ""
                {
                    cell.insideimg.image = UIImage.init(named: "default_image.png")
                }
                else
                {
                    let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                        print(self)
                    }
                    cell.insideimg.sd_setImage(with: NSURL(string:imageurl) as URL!, completed: block)
                    cell.insideimg.contentMode = UIViewContentMode.scaleToFill;
                }
            }
            else
            {
                let imageurl = (self.categoryarr.object(at: indexPath.item) as AnyObject).value(forKey: "category_disable_image") as! String
                if imageurl.characters.count == 0 || imageurl == ""
                {
                    cell.insideimg.image = UIImage.init(named: "default_image.png")
                }
                else
                {
                    
                    let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                        print(self)
                    }
                    cell.insideimg.sd_setImage(with: NSURL(string:imageurl) as URL!, completed: block)
                    cell.insideimg.contentMode = UIViewContentMode.scaleToFill;
                }
            }
            
            let namestr = (self.categoryarr.object(at: indexPath.item) as AnyObject).value(forKey: "category_name") as! String
            if namestr == ""
            {
                
            }
            else
            {
                cell.namelab.text = namestr.localized
            }
            
            
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == accessibilitycollection
        {
            let ids = (self.accessibilityarr.object(at: indexPath.item) as AnyObject).value(forKey: "accessbility_id") as! String
            
            print(ids)
            
            if !accessibility_id.contains(ids){
                accessibility_id.append(ids)
            } else {
                let index = accessibility_id.index(of: ids)
                accessibility_id.remove(at: index!)
            }
            
            if accessibilityidstore.contains(ids)
            {
                self.accessibilityidstore.remove(ids)
            }
            else
            {
                self.accessibilityidstore.add(ids)
            }
            let indexPath = IndexPath(row: indexPath.item, section: 0)
            self.accessibilitycollection.reloadItems(at: [indexPath])
        }
        else
        {
            //Category collection view
            //Categories shouldn’t be editable in the edit mode
            if isEditMode {
                return
            }
            
            let ids = (self.categoryarr.object(at: indexPath.item) as AnyObject).value(forKey: "category_id") as! String
            
            print(ids)
            
            offerer_id.removeAll()
            offerer_id.append(ids)
            
            if categoryidstore.contains(ids)
            {
                self.categoryidstore.removeAllObjects()
                self.categoryidstore.remove(ids)
            }
            else
            {
                self.categoryidstore.removeAllObjects()
                self.categoryidstore.add(ids)
            }
            self.categorycollection.reloadData()
        }
    }
    
    
    ///tableview
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if tableView == self.dateTable
        {
            return 1
        }else{
            
            print(facilitiesarr)
            return self.facilitiesarr.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        print(tableView)
        if tableView == self.dateTable {
            return timesave.count
        } else {
            let dic = self.insidefacilities.object(at: section) as! NSArray;
            print("dic \(dic)")
            return dic.count
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        
        //        print(self.datearr.count)
        if tableView == self.dateTable
        {
            return nil
        }
        else
        {
            let headerView = Bundle.main.loadNibNamed("FacilityHeaderView", owner: self, options: nil)?[0] as! FacilityHeaderView
            if self.facilitiesarr.count != 0
            {
                let dataarr = (self.facilitiesarr.object(at: section) as AnyObject).value(forKey: "facility_category_name") as! String
                let idarr = (self.facilitiesarr.object(at: section) as AnyObject).value(forKey: "facility_category_id") as! String
                headerView.headernamelab.text = dataarr.localized
                if facility_cat_idstore.contains(idarr) {
                    self.facility_cat_idstore.remove(idarr)
                } else {
                    self.facility_cat_idstore.add(idarr)
                }
                
                if idarr != ""
                {
                    
                    let imageurl = (self.facilitiesarr.object(at: section) as AnyObject).value(forKey: "facility_category_enable_image") as! String
                    if imageurl.characters.count == 0 || imageurl == ""
                    {
                        headerView.headerimg.image = UIImage.init(named: "default_image.png")
                    }
                    else
                    {
                        let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                            print(self)
                        }
                        headerView.headerimg.sd_setImage(with: NSURL(string:imageurl) as URL!, completed: block)
                        headerView.headerimg.contentMode = UIViewContentMode.scaleToFill;
                    }
                }
            }
            return headerView
        }
       
    }
    
    func tableView(_  tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if tableView == self.dateTable
        {
            return UITableViewAutomaticDimension
        }
        else
        {
            return 48.0
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if tableView == self.dateTable
        {
            return UITableViewAutomaticDimension
        }
        else
        {
            return 50.0
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == self.dateTable {
            let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cells")
            cell.textLabel?.adjustsFontSizeToFitWidth = true
            let timeTable = timesave[indexPath.row]
            print("LOG: TIME \(timesave[indexPath.row])")
            cell.textLabel?.text = timeTable.localized
            return cell
        } else {
            let simpleTableIdentifier  = NSString(format:"AddFacilityTableViewCell")
            let cell:AddFacilityTableViewCell?=tableView.dequeueReusableCell(withIdentifier: simpleTableIdentifier as String) as? AddFacilityTableViewCell
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            self.tablefacheight.constant = self.facilitytable.contentSize.height
            
            print(insidefacilities)
            
            dic = self.insidefacilities.object(at: indexPath.section) as! NSMutableArray;
            
            print(dic)
            
            let namestr = (dic.object(at: indexPath.row) as AnyObject).value(forKey: "facility_name") as! String
            if namestr == ""
            {
                
            }
            else
            {
                cell?.namelab.text = namestr.localized
            }
            
            cell?.CountView.tag = indexPath.section + 123
            
            cell?.CountView.accessibilityHint = String(format: "%d",indexPath.row + 123)
            cell?.CountView.addTarget(self, action: #selector(AddLooViewController.stepperValueChanged), for: .valueChanged)
            print(dic)
            if let count = (dic.object(at: indexPath.row) as AnyObject).value(forKey: "Count") as? String {
                print("LOG: CountView\(Double(count)!)")
                cell?.CountView.value = Double(count)!
            }
            return (cell)!
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    
    // MARK: - StepperValuChanged
    func stepperValueChanged(stepper: GMStepper)
    {
        
        // print(facilitiesarr)
        print(categoryarr)
        let index = Int(stepper.accessibilityHint!)! - 123
        let sectionindex = stepper.tag - 123
        print("LOG:index \(index)")
        print("LOG:sectionindex \(sectionindex)")
        print("LOG:self.categoryarr \(self.categoryarr)")
        
        let cat_id = String(sectionindex + Int(1))
        print("cat_id \(cat_id)")
        let tablecell = facilitytable.cellForRow(at:IndexPath.init(row: index, section: 0))
        if tablecell != nil {
            let cell = tablecell as! AddFacilityTableViewCell
        }
        dic = self.insidefacilities.object(at: sectionindex) as! NSArray as! NSMutableArray;
        print("self.insidefacilities \(self.insidefacilities)")
        print("dic \(dic)")
        let psdic = dic.object(at: index) as! NSDictionary
        print(UserDefaults.standard.object(forKey: "CartList"))
        let seledtedid = psdic.value(forKey: "facility_id") as! String
        print(cat_id,seledtedid)
        print(stepper.value)
        print("psdic \(psdic)")
        
        if !cat.contains(cat_id) || !fact.contains(seledtedid) {
            cat.append(cat_id)
            fact.append(seledtedid)
            print(stepper.value)
            count.append(stepper.value)
        } else {
            for i in 0..<cat.count {
                if cat[i] == cat_id && fact[i] == seledtedid {
                    count[i] = stepper.value
                }
            }
        }
        
        print("CATE \(cat)")
        print("FACT \(fact)")
        
        if stepper.value == 0 {
            let seledtedid = psdic.value(forKey: "facility_id") as! String
        } else {
            print(psdic)
            let seledtedid = psdic.value(forKey: "facility_id") as! String
            print(seledtedid)
            if UserDefaults.standard.object(forKey: "CartList") == nil
            {
                let cartarray = NSMutableArray()
                let Pdic = NSMutableDictionary()
                Pdic.setObject(psdic.object(forKey:"facility_id") as! String, forKey: "facility_id" as NSCopying)
                Pdic.setObject(psdic.object(forKey:"facility_name") as! String, forKey: "facility_name" as NSCopying)
                Pdic.setObject(psdic.object(forKey:"facility_enable_image") as! String, forKey: "facility_enable_image" as NSCopying)
                Pdic.setObject(psdic.object(forKey:"facility_disable_image") as! String, forKey: "facility_disable_image" as NSCopying)
                Pdic.setObject("\(stepper.value)", forKey: "Count" as NSCopying)
                
                print(Pdic)
                cartarray.add(Pdic.mutableCopy())
                UserDefaults.standard.set(cartarray.mutableCopy(), forKey:"CartList")
            }
            else
            {
                let curid = seledtedid
                let cartarray = NSMutableArray()
                let nsarra = UserDefaults.standard.object(forKey: "CartList") as! NSArray
                cartarray.addObjects(from: nsarra.mutableCopy() as! [AnyObject])
                let idarray = cartarray.value(forKey: "facility_id") as! NSArray
                
                if idarray.contains(curid)
                {
                    let pindex = idarray.index(of: curid)
                    let pdsddic = cartarray.object(at: pindex) as! NSDictionary
                    let Pdic = NSMutableDictionary()
                    Pdic.addEntries(from: pdsddic.mutableCopy() as! [AnyHashable : Any])
                    Pdic.setObject("\(stepper.value)", forKey: "Count" as NSCopying)
                    cartarray.replaceObject(at: pindex, with: Pdic.mutableCopy())
                    dic.replaceObject(at: index, with:  Pdic.mutableCopy())
                    UserDefaults.standard.set(cartarray.mutableCopy(), forKey:"CartList")
                }
                else
                {
                    let Pdic = NSMutableDictionary()
                    Pdic.setObject(psdic.object(forKey:"facility_id") as! String, forKey: "facility_id" as NSCopying)
                    Pdic.setObject(psdic.object(forKey:"facility_name") as! String, forKey: "facility_name" as NSCopying)
                    Pdic.setObject(psdic.object(forKey:"facility_enable_image") as! String, forKey: "facility_enable_image" as NSCopying)
                    Pdic.setObject(psdic.object(forKey:"facility_disable_image") as! String, forKey: "facility_disable_image" as NSCopying)
                    Pdic.setObject("\(stepper.value)", forKey: "Count" as NSCopying)
                    cartarray.add(Pdic.mutableCopy())
                    UserDefaults.standard.set(cartarray.mutableCopy(), forKey:"CartList")
                    print(Pdic)
                    
                }
            }
        }
    }
    
    @IBAction func firstimagebutton(_ sender: UIButton)
    {
        if (x<3){
            
        
        let actionsheet = KKActionSheet.init(title:"Change Picture", delegate:self, cancelButtonTitle:"Cancel", destructiveButtonTitle:nil ,otherButtonTitles:"take_photo".localized,"choose_from_gallery".localized)
                        actionsheet.show(in: self.view)
                        self.view.endEditing(true)
        }
        else
        {
            print("You can only select 3 Images")
        }
    }
    
    // MARK: - Release 1.2
    @IBAction func checkBoxAction(_ sender: Any) {
        if isCheckBoxChecked {
            [checkBoxButton .setImage(UIImage(named: checkboxUnSelected), for: .normal)]
        } else {
            [checkBoxButton .setImage(UIImage(named: checkboxSelected), for: .normal)]
        }
        isCheckBoxChecked = !isCheckBoxChecked
    }
    
    // MARK: - Action Sheet
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        if buttonIndex == 1
        {
            let imagePickerController = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
                imagePickerController.delegate = self
                imagePickerController.sourceType = UIImagePickerControllerSourceType.camera;
                imagePickerController.allowsEditing = false
                UIApplication.shared.statusBarStyle = UIStatusBarStyle.default;
                self.present(imagePickerController, animated: true, completion: nil)
                UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent;
            }
                
        }
        else if buttonIndex == 2 {
            let imagePickerController = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
                imagePickerController.delegate = self
                imagePickerController.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
                imagePickerController.allowsEditing = false
                UIApplication.shared.statusBarStyle = UIStatusBarStyle.default;
                self.present(imagePickerController, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: - Image Picker Delegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        picker .dismiss(animated: true, completion: nil)
        let media =  info[UIImagePickerControllerOriginalImage] as? UIImage
        
        let controller = PECropViewController();
        controller.delegate = self;
        controller.image = media;
        controller.toolbarItems=nil;
        let ratio:CGFloat = 16.0 / 9.0;
        controller.cropAspectRatio = ratio;
        
        let navigationController = UINavigationController.init(rootViewController: controller);
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            navigationController.modalPresentationStyle = UIModalPresentationStyle.formSheet;
        }
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker .dismiss(animated: true, completion: nil)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent;
    }
    
    func cropViewController(_ controller: PECropViewController!, didFinishCroppingImage croppedImage: UIImage!, transform: CGAffineTransform, cropRect: CGRect)
    {
        let ims:UIImage=croppedImage
        controller.dismiss(animated: true, completion:nil)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent;
        self.UploadimageWithImage(image: ims, size:CGSize(width: 200, height: 200))
    }
    func cropViewControllerDidCancel(_ controller: PECropViewController!)
    {
        controller.dismiss(animated: true, completion:nil)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent;
    }
    
    func UploadimageWithImage(image:UIImage,size:CGSize)
    {
        UIGraphicsBeginImageContext(size)
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let newimage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        let date=NSDate() as NSDate;
        let form = DateFormatter() as DateFormatter
        form.dateFormat="yyyy-MM-dd"
        let form1 = DateFormatter() as DateFormatter
        form1.dateFormat="HH:MM:SS"
        let datesstr = form.string(from: date as Date);
        let timestr = form1.string(from: date as Date);
        let datearr = datesstr.components(separatedBy: "-") as NSArray
        let timearr = timestr.components(separatedBy: ":") as NSArray
        let imageData = UIImageJPEGRepresentation(newimage,0.7);
        let imagename = String(format:"Pro_%@_%@_%@_%@_%@_%@.png",
                               (datearr.object(at: 0) as! String).trimmingCharacters(in: .whitespacesAndNewlines),
                               (datearr.object(at: 1) as! String).trimmingCharacters(in: .whitespacesAndNewlines),
                               (datearr.object(at: 2) as! String).trimmingCharacters(in: .whitespacesAndNewlines),
                               (timearr.object(at: 0) as! String).trimmingCharacters(in: .whitespacesAndNewlines),
                               (timearr.object(at: 1) as! String).trimmingCharacters(in: .whitespacesAndNewlines),
                               (timearr.object(at: 2) as! String).trimmingCharacters(in: .whitespacesAndNewlines))
        let imageNameTrimmed = imagename.replacingOccurrences(of: " ", with: "_")
        let imageUrlWithEncoding = imageNameTrimmed.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        Crashlytics.sharedInstance().setObjectValue(imageUrlWithEncoding, forKey: "LOG:ADDLOO:imageUrlWithEncoding")
        if AFNetworkReachabilityManager.shared().isReachable
        {
            let  yval = UserDefaults()
            yval.setValue("20", forKey: "yval")
            yval.setValue("#0063AD", forKey:"loadcolor")
            yval.setValue("photo", forKey: "Type")
            
            imageHud = MyHUDProgress.init(view: self.view, yavl: 20);
            imageHud.hidehud(20.0)
            //            imageHud.frame = CGRectMake(0,0,UIScreen.main.bounds.size.width,UIScreen.mainScreen.bounds.size.height);
            imageHud.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            self.view.addSubview(imageHud)
            
            let request : NSMutableURLRequest = AFHTTPRequestSerializer().multipartFormRequest(withMethod: "POST", urlString:kBaseDomainUrl + "/upload_loo_image.php", parameters: nil, constructingBodyWith: { (formData:AFMultipartFormData) -> Void in
                
                formData.appendPart(withFileData: imageData!, name:"uploaded_file", fileName:imageUrlWithEncoding!, mimeType: "image/png")
                
            }, error: nil)
            
            let conf : URLSessionConfiguration = URLSessionConfiguration.default
            
            let manager : AFURLSessionManager = AFURLSessionManager(sessionConfiguration: conf)
            
            manager.responseSerializer = AFHTTPResponseSerializer()
            let uploadTask:URLSessionUploadTask = manager.uploadTask(withStreamedRequest: request as URLRequest, progress:{(uploadProgress:Progress!) -> Void in
                
                DispatchQueue.main.async {
                    
                    self.imageHud.progress.setProgress(Float(uploadProgress.fractionCompleted), animated: true)
                    
                }
                
                //                dispatch_get_main_queue().asynchronously() { () -> Void in
                //
                //                    // done, back to main thread
                //                }
            })
            {(response, responseObject, error) -> Void in
                
                
                print(error)
                self.imageHud.removeFromSuperview();
                self.imageHud = nil;
                if((error == nil))
                {
                    print("%@ %@", response, responseObject);
                    //                    self.Sendimageurlwithimagename(imagename,image:UIImage.init(data: imageData!)!)
                    
                    self.myImage.append(image)
                    self.myImageName.append(imageUrlWithEncoding!)
                    self.selectArray.add(imageUrlWithEncoding)
                    
                    
                    var images = [ImageSource]()
                    
                    for i in 0 ..< self.myImage.count {
                        
                        let imgSource = ImageSource(image:self.myImage[i])
                        images.append(imgSource)
                    }
                    
                    self.slidingImages.setImageInputs(images)
                    self.slidingImages.setCurrentPage( self.x, animated: true)
                    // self.slidingImages.scrollViewPage = 3
                    // self.slidingImages.currentPage = self.x
                    self.slidingImages.circular = false
                    
                    self.x=self.x+1
                    
                    print(self.myImageName)
                }
                else
                {
                    self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert: self.myclass.StringfromKey("Errorwhieleupload"), withIdentifier:"error")
                }
            }
            
            uploadTask.resume()
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
    
    func relodBackground(caddress:String)
    {
        myaddressstr = caddress
        if myaddressstr != nil
        {
            //            let point = viewmap.center
            //            let coor = viewmap.projection .coordinate(for: point)
            //            let loc = CLLocation.init(latitude: coor.latitude, longitude: coor.longitude)
            print(myaddressstr)
            let arr = myaddressstr.components(separatedBy: ",") as NSArray
            let String1 = arr.object(at: 0) as! String
            let String2 = arr.object(at: 1) as! String
            let lat = Double(String1)
            let lon = Double(String2)
            
            let loc = CLLocation.init(latitude: lat!, longitude: lon!)
            currentlocation = loc;
        }
        else
        {
            let point = viewmap.center
            let coor = viewmap.projection .coordinate(for: point)
            let loc = CLLocation.init(latitude: coor.latitude, longitude: coor.longitude)
            currentlocation = loc;
        }
        self.locmanager.startUpdatingLocation()
        displaymarker = GMSMarker()
        firstload = false

    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) ->  Int {
        return arrayText.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath:  NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectedDayTableViewCell", for: indexPath as IndexPath)
        let dataDic = arrayText[indexPath.row] as! NSMutableArray
        
        print(dataDic)
        //cell.textLabel?.text = dataDic.valueforkey("storedateandtime") as String;
        return cell
    }

    
//    @IBAction func OpenSideMenu(_ x:AnyObject)
//    {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.SideMenu.showLeftView(animated: true, completionHandler: nil)
//    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func postButton(_ sender: Any) {
        
        print("postButton called!!")
//        self.uploadLabel.setTitle("Upload".localized, for: .normal)
        var vendor_id = self.Userdic.value(forKey: "user_id") as? String
        var loo_name = looNameTF.text
        var price = (looPriceTF.text?.isEmpty)! ? "0" : looPriceTF.text;
        var address = addresstextview.text
        var tandc = termsandcondtextview.text
        var loo_location = isEditMode ? storedLocationData : location_cordinates

        if (looNameTF.text?.isEmpty)! {
            showAlert("WCi", message: "error_add_toilet_name".localized, actionMessage: "OK")
            return
        }
        
        if addresstextview.text.isEmpty {
            showAlert("WCi", message: "error_add_toilet_address".localized, actionMessage: "OK")
            return
        }
        
        
        //Valdation for Client login
        if (self.isClientLoginUser()) {
            let startDateText = startDate.titleLabel?.text
            let endDataText = endDate.titleLabel?.text
            
            if (startDateText != "start_date".localized || endDataText != "end_date".localized) {
                if (startDateText == "start_date".localized) {
                    showAlert("WCi", message: "error_select_start_date".localized, actionMessage: "OK")
                    return
                }
                
                if (endDataText == "end_date".localized) {
                    showAlert("WCi", message: "error_select_end_date".localized, actionMessage: "OK")
                    return
                }
                
                let startDateTimeStamp:Int = self.getTimeStampForDateString(dateString: startDateText!, forCurrentTime: false)
                let endDateTimeStamp:Int = self.getTimeStampForDateString(dateString: endDataText!, forCurrentTime: false)
                let currentDateTimeStamp = self.getTimeStampForDateString(dateString: "", forCurrentTime: true)
                
                if (startDateTimeStamp < currentDateTimeStamp || endDateTimeStamp < currentDateTimeStamp) {
                    showAlert("WCi", message: "error_start_date_greater".localized, actionMessage: "OK")
                    return
                }
                
                if startDateTimeStamp > endDateTimeStamp {
                    showAlert("WCi", message: "error_end_date_greater".localized, actionMessage: "OK")
                    return
                }
            }
        } else { //Validation for Normal login
            if ((looPriceTF.text?.isEmpty)! && !isCheckBoxChecked) {
                showAlert("WCi", message: "error_add_toilet_price".localized, actionMessage: "OK")
                return
            }
            
            let looPriceInInt:Int = Int(looPriceTF.text!)!
            if ((looPriceInInt < 0 || looPriceInInt > 5) && !isCheckBoxChecked) {
                showAlert("WCi", message: "error_add_toilet_valid_price".localized, actionMessage: "OK")
                return
            }
            
            if self.categoryidstore.count == 0 {
                showAlert("WCi", message: "error_add_toilet_Offers".localized, actionMessage: "OK")
                return
            }
        
            //Timings is mandatory to add a toilet
            if timesave.count == 0 {
                showAlert("WCi", message: "error_add_toilet_timings".localized, actionMessage: "OK")
                return
            }
        }

        var image_to_send = [String]()
        
        for i in 0..<self.myImageName.count
        {
            var str = kBaseDomainUrl + "/images/loo/\(self.myImageName[i])"
            
            image_to_send.append(str)
        }
        
        self.image_to_s = image_to_send.joined(separator: ",")
        
        print(timesave)
        var dayArr = [String]()
        var timeArr = [String]()
        var timesaveArr = [String]()
        
        for i in 0..<timesave.count{
            print(timesave[i])
            timesaveArr = timesave[i].components(separatedBy: "- |")
            
            print(timesave)
            print(timesaveArr)
            print(timesaveArr[0])
            print(timesaveArr[1])
            
            dayArr.append(timesaveArr[0].trimmingCharacters(in: .whitespacesAndNewlines))
            timeArr.append(timesaveArr[1].trimmingCharacters(in: .whitespacesAndNewlines))

        }
        
        
        let dayStr = dayArr.joined(separator: ", ")
        let dateStr = timeArr.joined(separator: ", ")
        
        print(dayStr)
        print(dateStr)
        print("count \(count)")
        while count.contains(0.0){
            
            if let a = count.index(of: 0.0){

            cat.remove(at: a)
            fact.remove(at: a)
            count.remove(at: a)
            
            }
            
            print(cat)
            print(fact)
            print(count)
        }

        print(cat)
        print(fact)
        print(count)
        
       let  zipped = zip(cat,fact)
        
       print("zipped \(zipped)")
       
        for i in 0..<cat.count{
            
            for j in 0..<cat.count{
                if cat[i]<cat[j]
                {
                    swap(&cat[i], &cat[j])
                    swap(&fact[i], &fact[j])
                    swap(&count[i], &count[j])
                }
            }
        }
        
       print(cat)
       print(fact)
       print(count)
        
       var cat_to_send = cat.unique()
       var cat_to_s = cat_to_send.joined(separator: ",")
       var fact_to_s = fact.joined(separator: ",")
        
        var count_to_send = count.map
        {
            String($0)
        }
        var count_to_s = count_to_send.joined(separator: ",")
        print("cat_to_s \(cat_to_s)")
        print("fact_to_s \(fact_to_s)")
        print(count_to_s)
        print(image_to_s)
        
        print(accessibility_id.joined(separator: ","))
        print(offerer_id.joined(separator: ","))
        
        let offerer_to_s = offerer_id.joined(separator: ",")
        let access_to_s = accessibility_id.joined(separator: ",")
        
        var startDateValue = ""
        var endDateValue = ""
        if (self.isClientLoginUser()) {
            if(startDate.titleLabel?.text != "start_date".localized) {
                startDateValue = (startDate.titleLabel?.text)!
            }
            
            if (endDate.titleLabel?.text != "end_date".localized) {
                endDateValue = (endDate.titleLabel?.text)!
            }
        }
        
        var url:URL
        var params = [String:String]()
        let freeProviderValue = isCheckBoxChecked ? "1" : "0";
        let toiletType = self.isClientLoginUser() ? kToiletCategoryTypePublic : kToiletCategoryOfferers;
        let priceDetails = self.isClientLoginUser() ? "" : price!;
        let daysValue = self.isClientLoginUser() ? "" : dayStr;
        let timingsValue = self.isClientLoginUser() ? "" : dateStr;
        let fromDateValue = self.isClientLoginUser() ? startDateValue : "";
        let toDateValue = self.isClientLoginUser() ? endDateValue : "";
        if isEditMode {
            
            url = URL(string: "\(kBaseURL)update_loo.php")!
            //id = vendor_id
            params = ["loo_id":vendorlooId,
                      "loo_name":loo_name!,
                      "address":address!,
                      "loo_location":loo_location,
                      "price":priceDetails,
                      "facility_category":cat_to_s,
                      "facilities":fact_to_s,
                      "count":count_to_s,
                      "accessbility":access_to_s,
                      "image":image_to_s,
                      "category": offerer_to_s,
                      "days":daysValue,
                      "timings":timingsValue,
                      "terms_conditions":tandc!,
                      "free_provider":freeProviderValue,
                      "from_date":fromDateValue,
                      "to_date":toDateValue] as! [String : String]
        }
        else
        {
            url = URL(string: "\(kBaseURL)add_loo.php")!
            params = ["vendor_id":vendor_id!,
                      "loo_name":loo_name!,
                      "address":address!,
                      "loo_location":loo_location,
                      "price":priceDetails,
                      "facility_category":cat_to_s,
                      "facilities":fact_to_s,
                      "count":count_to_s,
                      "accessbility":access_to_s,
                      "image":image_to_s,
                      "category": offerer_to_s,
                      "days":daysValue,
                      "timings":timingsValue,
                      "terms_conditions":tandc!,
                      "free_provider":freeProviderValue,
                      "type":toiletType,
                      "from_date":fromDateValue,
                      "to_date":toDateValue] as! [String : String]
        }
   
            Alamofire.request(url,method:.get,parameters:params).responseJSON { response in

                print(params)
                let result = response.result
                print(response.request)
                print(result)
                print(response.value)
                    
                    if let dict = result.value as? Dictionary<String,AnyObject>{
                        
                        if let dat = dict["result"] as? [Dictionary<String,AnyObject>]  {
                            print(dat)
                            print(dat[0]["message"])
                            for obj in dat
                            {
                                
                                if let message = obj["message"] as? String{
                                    
                                    print(message)
                                    
                                    if message == "success"{
                                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                        let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
                                        appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
                                        let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
                                        currentview.pushViewController(mainview, animated: false)
                                        
                                        let alert = UIAlertController(title: "Waiting", message: "For Admin Approval", preferredStyle: UIAlertControllerStyle.alert)
                                        
                                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: nil))
                                        
                                        self.present(alert, animated: true, completion: nil)
                                        
                                        
                                    }
                                }
                            }
                            
                        }
                        
                    }
                    

            }

    }
    
    
    
    func getEditData(){
        let params:[String:String] = ["loo_id":vendorlooId]
        Alamofire.request(kBaseURL + "/vendor_loo_details.php", method: .get, parameters: params).responseJSON { response in
            print(kBaseURL + "/vendor_loo_details.php")
            print(params)
            print(response)
            let result = response.result
            if let dict = result.value as? Dictionary<String,AnyObject>{
                if let dat = dict["result"] as? [Dictionary<String,AnyObject>]  {
                    print(dat)
                    for obj in dat{
                        if let loo_name = obj["loo_name"] as? String{
                            print(loo_name)
                            self.looName = loo_name
                            self.looNameTF.text = loo_name
                        }
                        if let loo_address = obj["loo_address"] as? String {
                            print(loo_address)
                            self.looAddress = loo_address
                            self.addresstextview.text = loo_address
                        }
                        
                        if let loo_image = obj["loo_image"] as? String{
                            print("LOG: loo_image \(loo_image)")
                            self.looImage = loo_image
                            if loo_image != "" {
                                var newarr = loo_image.components(separatedBy: ",")
                                print("LOG: newarr \(newarr)")
                                var images = [AlamofireSource]()
                                for i in 0 ..< newarr.count {
                                    print("LOG: newarr[i] \(newarr[i])")
                                    let alamofireSource = AlamofireSource(urlString:newarr[i])!
                                    images.append(alamofireSource)
                                    let imageName = newarr[i].components(separatedBy: "/").last
                                    self.myImageName.append(imageName!)
                                }
                                self.slidingImages.setImageInputs(images)
                            }
                        }
                        
                        if let price = obj["price"] as? String{
                            print(price)
                            self.looPrice = price
                            self.looPriceTF.text = price
                        }
                        
                        if let loo_location = obj["loo_location"] as? String{
                            print(loo_location)
                            self.looLocation = loo_location
                            self.location_cordinates = loo_location
                            self.storedLocationData = loo_location
                        } else {
                            self.location_cordinates = ""
                            self.storedLocationData = ""
                            self.looLocation = ""
                        }
                        
                        if let type = obj["type"] as? String{
                            print(type)
                            self.looType = type
                        }
                        
                        if let days = obj["days"] as? String{
                            print(days)
                            self.looDays = days
                            self.day1 = days.components(separatedBy: ",")
                        }
                        if let timings = obj["timings"] as? String{
                            print(timings)
                        }
                        
                        self.dateTable.reloadData()
                        
                        if let terms = obj["terms_conditions"] as? String{
                            self.termsandcondtextview.text = terms
                        }
                        
                        if let category = obj["category"] as? [Dictionary<String,AnyObject>]{
                            
                            print(category)
                            print(category[0])
                            
                            self.looCategory = category
                            self.offerer_id.removeAll()
                            for newobj in category{
                                if let status = newobj["status"] as? String{
                                    if let category_id = newobj["category_id"] as? String
                                    {
                                        print(status)
                                        print(category_id)
                                        if status == "1" {
                                            self.categoryidstore.add(category_id)
                                            self.offerer_id.append(category_id)
                                            print(self.categoryidstore)
                                        }
                                    }
                                }
                            }
                            self.categorycollection.reloadData()
                        }
                        
                        if let facilities = obj["facilities"] as? [Dictionary<String,AnyObject>] {
                            print(facilities)
                            print(facilities[0])
                            
                            self.looFacilities = facilities
                            var i=0
                            var arr1 = [[Int]]()
                            for newobj in  facilities{
                                
                                if let newfacilities = newobj["facility"] as? [Dictionary<String,AnyObject>]{
                                    
                                    print(newfacilities)
                                    print(newfacilities[0])
                                    var arr = [Int]()
                                    var j=0
                                    print(self.dic)
                                    self.dic = self.insidefacilities.object(at: i) as! NSMutableArray
                                    for anobj in newfacilities
                                    {
                                        if let value = anobj["count"] as? String {
                                            print(value)
                                            print(i,j)
                                            print(arr)
                                            
                                            (self.dic[j] as AnyObject).setObject(value, forKey: "Count" as NSCopying)
                                            print(self.dic[j])
                                            print(value)
                                            print(arr)
                                            print(Int(value))
                                            arr.append((Int(Double(value)!)))
                                        }
                                        j=j+1
                                        
                                    }
                                    arr1.append(arr)
                                }
                                i=i+1
                            }
                            
                            self.facilitytable.reloadData()
                            print(arr1)
                        }
                        
                        //If free provider
                        if let isFreeProvider = obj["free_provider"] as? String {
                            if isFreeProvider == "1" {
                                self.isCheckBoxChecked = true
                                [self.checkBoxButton .setImage(UIImage(named: self.checkboxSelected), for: .normal)]
                            } else {
                                self.isCheckBoxChecked = false
                                [self.checkBoxButton .setImage(UIImage(named: self.checkboxUnSelected), for: .normal)]
                            }
                        }
                        
                        if let accessbility = obj["accessbility"] as? [Dictionary<String,AnyObject>]{
 
                            print(accessbility)
                            print(accessbility[0])
                            self.looAccessbility = accessbility
                            
                            self.accessibility_id.removeAll()
                            for newobj in accessbility{
                                
                                if let status = newobj["status"] as? String{
                                    
                                    if let access_id = newobj["accessbility_id"] as? String
                                    {
                                        print(status)
                                        print(access_id)
                                        if status == "1" {
                                            self.accessibilityidstore.add(access_id)
                                            self.accessibility_id.append(access_id)
                                        }
                                    }
                                }
                            }
                            self.accessibilitycollection.reloadData()
                        }
                        
                        if (self.isClientLoginUser()) {
                            if let fromDate = obj["from_date"] as? String {
                                if fromDate.isEmpty {
                                    self.startDate.setTitle("start_date".localized, for: .normal)
                                } else{
                                    self.startDate.setTitle(fromDate, for: .normal)
                                }
                            } else {
                                self.startDate.setTitle("start_date".localized, for: .normal)
                            }
                            
                            if let toDate = obj["to_date"] as? String {
                                if toDate.isEmpty {
                                    self.endDate.setTitle("end_date".localized, for: .normal)
                                } else {
                                    self.endDate.setTitle(toDate, for: .normal)
                                }
                            } else {
                                self.endDate.setTitle("end_date".localized, for: .normal)
                            }
                        }
                    }
                }
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        editLoo = 0
    }

    
    
    func getDatastructure(){
        
        let params:[String:String] = ["loo_id":id]
        
        Alamofire.request(kBaseURL + "/ios_vender_loo_details.php", method: .get,parameters: params).responseJSON { response in
            
            print(params)
            print(response)
            print(response.result.value)
            
            if response.result.isSuccess {
                let loo = GettingStarted.from(data: response.data! )
                print(loo?.result)
                print(loo?.result[0].looId)
                print(loo?.result[0].category[0].catDisableImage)
                print(loo?.result[0].facilities[0].facility[0].facilityName)
            }
            else {
                print("Error \(String(describing: response.result.error))")
            }
        }
    }
    
    
    func updateData(json : JSON) {

    }
    
    func getTimeStampForDateString(dateString: String, forCurrentTime:Bool) -> Int {
        var date = Date()
        if (!forCurrentTime) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            date = dateFormatter.date(from: dateString)!
        } else {
            let today = Date()
            date = Calendar.current.date(byAdding: .day, value: -1, to: today)!
            print("TODAY:\(today)")
            print("date:\(date)")
        }
        let timeStamp : TimeInterval = (date.timeIntervalSince1970)
        return Int(timeStamp)
    }
}

extension Sequence where Iterator.Element: Hashable {
    func unique() -> [Iterator.Element] {
        var alreadyAdded = Set<Iterator.Element>()
        return self.filter { alreadyAdded.insert($0).inserted }
    }
}
