//
//  TimeAddingTableViewCell.swift
//  JEDE_r
//
//  Created by Exarcplus on 07/06/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit

class TimeAddingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var showCloseLabel: UILabel!
    @IBOutlet weak var showOpenLabel: UILabel!
    @IBOutlet weak var opentime : UILabel!
    @IBOutlet weak var closetime : UILabel!
    @IBOutlet weak var openbutton : UIButton!
    @IBOutlet weak var closebutton : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        

//        self.opentime.text = "pop_date_time_open".localized
//        self.closetime.text = "pop_date_time_close".localized
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
