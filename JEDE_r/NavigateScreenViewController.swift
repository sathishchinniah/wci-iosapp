//
//  NavigateScreenViewController.swift
//  JEDE_r
//
//  Created by Vignesh Waran on 5/13/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit

class NavigateScreenViewController: UIViewController {
    
    var selectdic : NSMutableDictionary!
    @IBOutlet weak var viewmap: GMSMapView!
    let marker = GMSMarker()
    
    @IBOutlet weak var looaddresslab : UILabel!
    @IBOutlet weak var exactloclab : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let view = UIView(frame:  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 20))
        view.backgroundColor = UIColor.init(red: 27.0/255.0, green: 34.0/255.0, blue: 40.0/255.0, alpha: 1.0)
        self.navigationController?.view.addSubview(view)

        //map
        if self.selectdic != nil
        {
            print(self.selectdic)
            let location = self.selectdic.value(forKey: "loo_location") as? String
            if let address = self.selectdic.value(forKey: "loo_address") as? String {
            looaddresslab.text = address
            } else {
                if let address2 = self.selectdic.value(forKey: "address") as? String {
                    looaddresslab.text = address2
                } else {
                    looaddresslab.text = ""
                }
            }
            
            print(looaddresslab.text!)
            
            let type = self.selectdic.value(forKey: "category_name") as? String
            if type == ""
            {
                self.marker.icon = UIImage.init(named: "Public")
                //                                                    marker.accessibilityHint = "Yes"
                exactloclab.isHidden = true
            }
            else if type == "Gas Station"
            {
                exactloclab.isHidden = false
            }
            else if type == "Hotel"
            {
                exactloclab.isHidden = false
            }
            else if type == "Restaurant"
            {
                exactloclab.isHidden = false
            }
            //draw circle
            if location?.characters.count != 0
            {
                if let locationDetails = location {
                    let locar:NSArray = locationDetails.components(separatedBy: ",") as NSArray
                    if locar.count == 2 {
                        let lat = Double(locar.object(at: 0) as! String)
                        let lon = Double(locar.object(at: 1) as! String)
                        if let latitude = lat {
                            if let longitude = lon {
                                let loosloc = CLLocation.init(latitude: latitude, longitude: longitude)
                                self.marker.position = CLLocationCoordinate2DMake(loosloc.coordinate.latitude, loosloc.coordinate.longitude)
                                
                                if type == "" {
                                    self.marker.zIndex = 0 ;
                                    self.marker.map  = self.viewmap
                                } else if type == "Gas Station" {
                                    let geoFenceCircle = GMSCircle()
                                    geoFenceCircle.radius = 100
                                    geoFenceCircle.position = CLLocationCoordinate2DMake(loosloc.coordinate.latitude, loosloc.coordinate.longitude)
                                    geoFenceCircle.fillColor = UIColor.init(red: 0.941, green: 0.675, blue: 0.671, alpha: 1.0)
                                    geoFenceCircle.strokeWidth = 0
                                    geoFenceCircle.strokeColor = UIColor.red
                                    geoFenceCircle.map = self.viewmap
                                } else if type == "Hotel" {
                                    let geoFenceCircle = GMSCircle()
                                    geoFenceCircle.radius = 100
                                    geoFenceCircle.position = CLLocationCoordinate2DMake(loosloc.coordinate.latitude, loosloc.coordinate.longitude)
                                    geoFenceCircle.fillColor = UIColor.init(red: 0.000, green: 0.655, blue: 0.976, alpha: 0.4)
                                    geoFenceCircle.strokeWidth = 1
                                    geoFenceCircle.strokeColor = UIColor.init(red: 0.000, green: 0.655, blue: 0.976, alpha: 1.0)
                                    geoFenceCircle.map = self.viewmap
                                } else if type == "Restaurant" {
                                    let geoFenceCircle = GMSCircle()
                                    geoFenceCircle.radius = 100
                                    geoFenceCircle.position = CLLocationCoordinate2DMake(loosloc.coordinate.latitude, loosloc.coordinate.longitude)
                                    geoFenceCircle.fillColor = UIColor.init(red: 1.000, green: 0.435, blue: 0.149, alpha: 0.4)
                                    geoFenceCircle.strokeWidth = 1
                                    geoFenceCircle.strokeColor = UIColor.init(red: 1.000, green: 0.435, blue: 0.149, alpha: 1.0)
                                    geoFenceCircle.map = self.viewmap
                                }
                            }
                        }
                    }
                }
            }
            self.focusMapToShowAllMarkers()
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func closebuttonclick(_sender:UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func focusMapToShowAllMarkers() {
        let myLocation: CLLocationCoordinate2D = self.marker.position
        var bounds: GMSCoordinateBounds = GMSCoordinateBounds(coordinate: myLocation, coordinate: myLocation)
        //        for marker in self.markers {
        bounds = bounds.includingCoordinate(marker.position)
        self.viewmap.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 100.0))
        self.viewmap.animate(toZoom: 16.0)
        //        }
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
