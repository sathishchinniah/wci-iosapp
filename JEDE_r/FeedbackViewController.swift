//
//  FeedbackViewController.swift
//  JEDE_r
//
//  Created by Pixselo Global Solutions on 11/04/18.
//  
//

import UIKit
import Alamofire
import Firebase

class FeedbackViewController: UIViewController {
    
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var usernameTextfield: UITextField!
    @IBOutlet weak var emailIdTextfield: UITextField!
    
    @IBOutlet weak var feebbackMessage: LRTextField!
    //    @IBOutlet weak var feedbackTextview: UITextView!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.submitButton.endEditing(true)
        self.submitButton.layer.cornerRadius = 20.0
        self.submitButton.clipsToBounds = true
        //self.navigationController?.navigationBar.isHidden = true
//        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = false
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = UIColor.white.cgColor
        border.frame = CGRect(x: 0, y: usernameTextfield.frame.size.height - width, width:  usernameTextfield.frame.size.width, height: usernameTextfield.frame.size.height)
        
        border.borderWidth = width
        usernameTextfield.layer.addSublayer(border)
        usernameTextfield.placeholder = "name_optional".localized
        usernameTextfield.layer.masksToBounds = true
        
        let borderr = CALayer()
        let widthr = CGFloat(2.0)
        borderr.borderColor = UIColor.white.cgColor
        borderr.frame = CGRect(x: 0, y: emailIdTextfield.frame.size.height - widthr, width:  emailIdTextfield.frame.size.width, height: emailIdTextfield.frame.size.height)
        
        borderr.borderWidth = widthr
        emailIdTextfield.layer.addSublayer(borderr)
        emailIdTextfield.placeholder = "email_address_optional".localized
        emailIdTextfield.layer.masksToBounds = true
        let borders = CALayer()
        let widths = CGFloat(2.0)
        borders.borderColor = UIColor.white.cgColor
        borders.frame = CGRect(x: 0, y: feebbackMessage.frame.size.height - widths, width:  feebbackMessage.frame.size.width, height: feebbackMessage.frame.size.height)
        
        borders.borderWidth = widths
        feebbackMessage.layer.addSublayer(borders)
        feebbackMessage.placeholder = "message".localized
        feebbackMessage.layer.animation(forKey: "Message")
        feebbackMessage.layer.masksToBounds = true
        self.submitButton.setTitle("submit".localized, for: UIControlState.normal)
        
        //Do any additional setup after loading the view.
    }
    
    @IBAction func capthaAction(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Please Wait", message: "You Are Not A Robot", preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        
        // change to desired number of seconds (in this case 5 seconds)
        let when = DispatchTime.now() + 5
        DispatchQueue.main.asyncAfter(deadline: when){
            // your code with delay
            alert.dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func backToSidemenu(_ sender: UIBarButtonItem) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.SideMenu.showLeftView(animated: true, completionHandler: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadsidemenu"), object: nil, userInfo: nil)
    }
    @IBAction func SubmitAction(_ sender: UIButton) {
    
        if (isValidEmail(self.emailIdTextfield.text!) != true) {
            showAlert("Email Input Error", message: "Please make sure you have entered a valid email.", actionMessage: "OK")
        }
    
        if  feebbackMessage.text == "" {
            showAlert("Feedback Message Error", message: "Please enter Feedback Message.", actionMessage: "OK")
            
        }
        else{
            
            let parameters: [String: String] = [
                "name" : usernameTextfield.text ?? "",
                "email" : emailIdTextfield.text ?? "",
                "message" : feebbackMessage.text ?? "",
                ]
            let UserId =  UserDefaults.standard.string(forKey: "user_id")
            FIRAnalytics.logEvent(withName: "user_feedback", parameters: [
                "user_id" : UserId,
                "email_id" : emailIdTextfield.text ?? "NA",
                "user_feedback" : feebbackMessage.text ?? "NA",
                "login_device" : "iOS",
                "action_name" : "button_action"
                ])
            let feedbackUrl = kBaseURL+"feedback.php"
            Alamofire.request(feedbackUrl,method:.post,parameters:parameters,encoding: URLEncoding()).responseJSON{ response in
                let result = response.result
                print(response)
                print(result)
                
                if let dict = result.value as? Dictionary<String,AnyObject>{
                    
                    if let values = dict["result"] as? [Dictionary<String,AnyObject>]
                    {
                        let alert = UIAlertController(title: "Thank You", message: "For Your Feedback", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
                        appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
                        let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
                        currentview.pushViewController(mainview, animated: false)
                        self.present(alert, animated: true, completion: nil)
                        self.usernameTextfield.text = ""
                        self.emailIdTextfield.text = ""
                        self.feebbackMessage.text = ""
                        
                        
                    }
                    else{
                        // error alert
                        let alert = UIAlertController(title: "Error Alert", message: "For Your Feedback", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    
    
    func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "activity_main_feedback".localized
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
//        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
