//
//  MyCard.swift
//  CardStack
//
//  Created by Zhou Hao on 11/3/17.
//  Copyright © 2017 Zhou Hao. All rights reserved.
//

import UIKit
import WOWCardStackView


class MyCard: CardView {
    @IBOutlet weak var loonamelab : UILabel!
    @IBOutlet weak var ratingview : EZRatingView!
    @IBOutlet weak var moreInfoButton: UIButton!
    @IBOutlet weak var looimg : UIImageView!
    @IBOutlet weak var maleimg : UIImageView!
    @IBOutlet weak var femaleimg : UIImageView!
    @IBOutlet weak var uniseximg : UIImageView!
    @IBOutlet weak var addresslab : UILabel!
    @IBOutlet weak var distancelab : UILabel!
    @IBOutlet weak var bookbutton : UIButton!
    
    @IBOutlet weak var viewMoreButton: UIButton!
    var id: String

    init(id: String) {
        self.id = id
        super.init(frame: CGRect.zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.id = "0"
        super.init(coder: aDecoder)
    }
    
}
