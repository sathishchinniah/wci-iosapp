//
//  TermsAndConditionsViewController.swift
//  JEDE_r
//
//  Created by Exarcplus on 14/09/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import AFNetworking
import SDWebImage

class TermsAndConditionsViewController: UIViewController,  UIActionSheetDelegate,UINavigationControllerDelegate, UIGestureRecognizerDelegate, UIWebViewDelegate {
    
    var myclass : MyClass!
    @IBOutlet weak var continueButton : UIButton!
    @IBOutlet weak var agreeButton : UIButton!
    @IBOutlet weak var webView : UIWebView!
    @IBOutlet weak var waitinglab : UILabel!
    @IBOutlet weak var activityLoader: UIActivityIndicatorView!
    @IBOutlet weak var headingLabel: UILabel!
    
    //Variables
    var webViewarr : NSMutableArray!
    var agreeButtonstr : String!
    var webViewid : String!
    var webarr : NSMutableArray!
    var selectdicthis : NSDictionary!
    var detailsarr = NSMutableArray()
    var passlooid : String!
    var passuserid : String!
    var passcurrentloc : String!
    var Userdic = NSMutableDictionary()
    var isCheckBoxSelected = Bool()
    
    let checkboxUnSelected = "checkboxunselect"
    let checkboxSelected = "checkboxselect"
   
    @IBOutlet weak var checkBoxButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        print("TermsAndConditionsViewController called!!")
        print("LANGUAGE CODE \(Language.language)")
        myclass = MyClass()
        self.navigationController?.navigationBar.isHidden = false
        agreeButtonstr = "0"
        self.activityLoader.hide()
        //Load Terms and conditions web view
        self.hideOrShowActivityIndicator(toHide: true)
        self.activityLoader.hidesWhenStopped = true
        self.loadWebViewFromServer()
        self.checkBoxButton.setBackgroundImage(UIImage.init(named: checkboxUnSelected), for: .normal)
        self.checkBoxButton.setTitle("", for: .normal)
        self.headingLabel.text  = "term_con_layout_following1".localized
        self.agreeButton.setTitle("i_agree_this_terms_and_conditions".localized, for: .normal)
        self.title = "terms and conditions".localized
        
        isCheckBoxSelected = false
        
        if selectdicthis != nil
        {
            print(selectdicthis)
            let urtermsandconditions = selectdicthis.value(forKey:"terms_conditions") as! String
            print(urtermsandconditions)
            var returnStr: String = urtermsandconditions
            
            returnStr = returnStr.replacingOccurrences(of: "521", with: "•")
            returnStr = returnStr.replacingOccurrences(of: "", with: "\n")
            print(returnStr)
            webView.loadHTMLString(returnStr, baseURL: nil)
            self.webView.isOpaque = false;
            self.webView.backgroundColor = UIColor.clear
            
            print(selectdicthis)
            passlooid = selectdicthis.value(forKey: "loo_id") as! String
            print(passlooid)
            passcurrentloc = selectdicthis.value(forKey: "loo_location") as! String
            print(passcurrentloc)
        }

        else
        {
            //            self.scrollview.addTwitterCover(with: UIImage(named: "Toilets-Defualt.png"))
        }
        
        if UserDefaults.standard.object(forKey: "Logindetail") == nil
        {
            passuserid = ""
        }
        else
        {
            let placesData = UserDefaults.standard.object(forKey: "Logindetail") as? NSDictionary
            self.Userdic.removeAllObjects()
            if placesData != nil{
                self.Userdic.addEntries(from: (placesData as? [String : Any])!)
            }
            print(self.Userdic)
            passuserid = self.Userdic.value(forKey: "user_id") as? String
            print(passuserid)
        }

    }
    
    
    
    /// Load the webview on load of the terms and conditions view
    func loadWebViewFromServer() {
        
        let webUrl = URL(string: kTermsAndConditionsUrl)
        let webUrlRequest:URLRequest = URLRequest(url: webUrl!)
        webView.loadRequest(webUrlRequest)
        self.hideOrShowActivityIndicator(toHide: false)
        webView.layer.borderWidth = 1.0
        webView.layer.borderColor = UIColor.lightGray.cgColor
        
    }
    
    func hideOrShowActivityIndicator(toHide:Bool) {
        if toHide {
            print("HIDE ACTIVITY INDICATOR")
            self.activityLoader.stopAnimating()
            self.activityLoader.hide()
        } else {
            print("SHOW ACTIVITY INDICATOR")
            self.activityLoader.startAnimating()
            self.activityLoader.show()
        }
    }
    
    //MARK: Webview delegate
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("webViewDidFinishLoad: LOADED SUCCESSFULLY")
        DispatchQueue.main.async {
            self.hideOrShowActivityIndicator(toHide: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func checkBoxTouchAction(_ sender: Any) {
        if isCheckBoxSelected {
            self.checkBoxButton.setBackgroundImage(UIImage.init(named: checkboxUnSelected), for: .normal)
        } else {
            self.checkBoxButton.setBackgroundImage(UIImage.init(named: checkboxSelected), for: .normal)
        }
        isCheckBoxSelected = !isCheckBoxSelected
        
    }
    
    
    
    @IBAction func backButton(_sender:UIButton)
    {
        //self.dismiss(animated:true, completion: nil)
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func continueButton(_sender: UIButton)
    {
        if(!isCheckBoxSelected) {
            let alert = UIAlertController(title: "terms_agree_button".localized, message: "term_con_layout_following1".localized, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else{
            UserDefaults.standard.set("I AGREE", forKey: "AGREE")
            UserDefaults.standard.synchronize()
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "IntroViewController") as! IntroViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
    }
}
