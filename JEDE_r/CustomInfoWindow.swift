//
//  CustomInfoWindow.swift
//  Geofleetz
//
//  Created by Exarcplus iOS 2 on 01/03/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit

class CustomInfoWindow: UIView {
    
    @IBOutlet weak var namelabel : UILabel!
    @IBOutlet weak var categorylab : UILabel!
    @IBOutlet weak var pricelabel : UILabel!
    @IBOutlet weak var imageview : UIImageView!
    @IBOutlet weak var ratingview : EZRatingView!
    @IBOutlet weak var bgview : UIView!
    @IBOutlet weak var Malepic: UIImageView!
    @IBOutlet weak var Femalepic: UIImageView!
    @IBOutlet weak var Unisexpic: UIImageView!
    @IBOutlet weak var carPic: UIImageView!
    @IBOutlet weak var wheelchairPic: UIImageView!
    @IBOutlet weak var ClickForMore: UILabel!
    
    var imageData:UIImage!
    
    public func updateImage(imageUrl:String) {
        //cell.insideimg.sd_setImage(with:URL(string: imageurl))
        let imageCompleteUrl = URL(string: imageUrl) as? URL
        let placeHolder = UIImage.init(named:"default_image.png")
       // let typestr = dat.value(forKey: "category_name") as! String;
        print("imageCompleteUrl \(imageCompleteUrl)")
        if let imageUrlWithCompletList = imageCompleteUrl {
            self.imageview.sd_setImage(with: imageUrlWithCompletList, placeholderImage: placeHolder, options: .refreshCached, progress: nil) { (image, error, cacheType, imageUrl) in
                if image != nil {
                    print(cacheType)
                    DispatchQueue.main.async {
                        print("Image downloaded successfully")
                        print("INSIDE MAIN QUEUE")
                        self.imageview.image = image
                        self.imageview.contentMode = UIViewContentMode.scaleToFill;
                        print("Image SET")
                    }
                }
                if error != nil {
                    print("ERROREL FILE FOR IUMAGE \(error)")
                }
                if imageUrl != nil {
                    print(imageUrl)
                }
            }
        }
    }
}

