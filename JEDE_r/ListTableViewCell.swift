//
//  TableViewCell.swift
//  Loocator
//
//  Created by Exarcplus iOS 1 on 25/08/16.
//  Copyright © 2016 Interactive Thumbz. All rights reserved.
//

import UIKit
//import ChangeImageView
class ListTableViewCell: UITableViewCell {

    @IBOutlet var borderview: UIView!
    @IBOutlet var List_name:UILabel!
    @IBOutlet var List_image: UIImageView!
    @IBOutlet var pricelab:UILabel!
    @IBOutlet var timelab:UILabel!
    @IBOutlet var distancelab:UILabel!
    @IBOutlet var ratingview : EZRatingView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        borderview.layer.shadowColor = UIColor.black.cgColor
        borderview.layer.shadowOpacity = 0.4
        borderview.layer.shadowOffset = CGSize.zero
        borderview.layer.shadowRadius = 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
