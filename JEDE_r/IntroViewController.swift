//
//  IntroViewController.swift
//  JEDE_r
//
//  Created by apple on 26/05/18.
//  Copyright © 2018 Exarcplus. All rights reserved.
//

import UIKit
import LGSideMenuController

class IntroViewController: UIViewController {

    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var gotItButton: UIButton!
    @IBOutlet weak var IntroImages: UIImageView!
    @IBOutlet weak var IntroScreen: UIScrollView!
    var SideMenu:LGSideMenuController!
    var SideMenuView:SidemenuViewController!
    
    var imgeOne:UIImageView?
    var imgTwo:UIImageView?
    var pageIndex:Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.IntroScreen.frame = CGRect(x:0, y:0, width:self.view.frame.width, height:self.view.frame.height-36)
        let IntroScreenWidth:CGFloat = self.IntroScreen.frame.width
        let IntroScreenHeight:CGFloat = self.IntroScreen.frame.height
        
        //If the language is in German use German images
        //Otherwise use English for others
        var imageArray = Array<String>()
        if(UserDefaultManager.sharedManager.getLanguageName() == "German"){
            imageArray = ["German_intro_filter","German_intro_moreinfo","German_intro_rating",
                          "German_intro_menu","German_intro_login","German_intro_moreinfo_WeDoGiveAShit"]
        } else {
            imageArray = ["ExplanationFilter","MoreInfo_english (1)","Rating_english (1)","MenuExplanation","Login_english","WeDoGiveAShit_english"]
        }
        
        //3
        imgeOne = UIImageView(frame: CGRect(x:0, y:0,width:IntroScreenWidth, height:IntroScreenHeight))
        imgeOne?.image = UIImage(named: imageArray[0])
        imgeOne?.contentMode = .scaleAspectFit
        imgeOne?.tag = 0
        imgTwo = UIImageView(frame: CGRect(x:IntroScreenWidth, y:0,width:IntroScreenWidth, height:IntroScreenHeight))
      
        imgTwo?.image = UIImage(named: imageArray[1])
        imgTwo?.tag = 1
        imgTwo?.contentMode = .scaleAspectFit
        let imgThree = UIImageView(frame: CGRect(x:IntroScreenWidth*2, y:0,width:IntroScreenWidth, height:IntroScreenHeight))
        imgThree.image = UIImage(named: imageArray[2])
        let imgFour = UIImageView(frame: CGRect(x:IntroScreenWidth*3, y:0,width:IntroScreenWidth, height:IntroScreenHeight))
        imgFour.image = UIImage(named: imageArray[3])
        let imgFive = UIImageView(frame: CGRect(x:IntroScreenWidth*4, y:0,width:IntroScreenWidth, height:IntroScreenHeight))
        imgFive.image = UIImage(named: imageArray[4])
        let imgSix = UIImageView(frame: CGRect(x:IntroScreenWidth*5, y:0,width:IntroScreenWidth, height:IntroScreenHeight))
        imgSix.image = UIImage(named: imageArray[5])
        
        imgThree.contentMode = .scaleAspectFit
        imgFour.contentMode = .scaleAspectFit
        imgFive.contentMode = .scaleAspectFit
        imgSix.contentMode = .scaleAspectFit
        
        self.IntroScreen.addSubview(imgeOne!)
       
        self.IntroScreen.addSubview(imgTwo!)
        self.IntroScreen.addSubview(imgThree)
        self.IntroScreen.addSubview(imgFour)
        self.IntroScreen.addSubview(imgFive)
        self.IntroScreen.addSubview(imgSix)
        //4
        self.IntroScreen.contentSize = CGSize(width:self.IntroScreen.frame.width * 6, height:self.IntroScreen.frame.height-36)
        self.pageControl.currentPage = 0
        pageIndex = 0
        
    }
    
    
    func CGRectMake(_ x: CGFloat, _ y: CGFloat, _ width: CGFloat, _ height: CGFloat) -> CGRect {
        return CGRect(x: x, y: y, width: width, height: height)
    }
     
    

    @IBAction func SkipAction(_ sender: UIButton) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let mainview = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
        let valueToSave = "noneedrunlink"
        let nav = UINavigationController.init(rootViewController: mainview)
        appDelegate.SideMenu = LGSideMenuController.init(rootViewController: nav)
        appDelegate.SideMenu.setLeftViewEnabledWithWidth(280, presentationStyle: .slideAbove, alwaysVisibleOptions:[])
        appDelegate.SideMenuView = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "SidemenuViewController") as! SidemenuViewController
        appDelegate.SideMenu.leftViewStatusBarVisibleOptions = .onAll
        appDelegate.SideMenu.leftViewStatusBarStyle = .lightContent
         UserDefaults.standard.set(true,forKey:"launchedBefore")
        //        appDelegate.SideMenuView.selectedpage = 0;
        var rect = appDelegate.SideMenuView.view.frame;
        rect.size.width = 280;
        appDelegate.SideMenuView.view.frame = rect
        appDelegate.SideMenu.leftView().addSubview(appDelegate.SideMenuView.view)
        print(appDelegate.SideMenu)
        self.present(appDelegate.SideMenu, animated:true, completion: nil)
        
    }
    
    @IBAction func GotItAction(_ sender: UIButton) {
        if IntroScreen.contentOffset.x <= IntroScreen.contentSize.width {
            let frame:CGRect = CGRectMake(IntroScreen.contentOffset.x + IntroScreen.frame.size.width, 0, self.view.frame.width, self.view.frame.height-36)
            IntroScreen.scrollRectToVisible(frame, animated: true)
            let pageWidth:CGFloat = IntroScreen.frame.width
            let currentPage:CGFloat = floor((IntroScreen.contentOffset.x-pageWidth/2)/pageWidth)+1
            self.pageControl.currentPage = Int(currentPage) + 1;
            if  Int(currentPage) == 4{
                gotItButton.setTitle("GOT IT", for: UIControlState.normal)
            }
            if Int(currentPage) == 5{
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let mainview = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
                let valueToSave = "noneedrunlink"
                let nav = UINavigationController.init(rootViewController: mainview)
                appDelegate.SideMenu = LGSideMenuController.init(rootViewController: nav)
                appDelegate.SideMenu.setLeftViewEnabledWithWidth(280, presentationStyle: .slideAbove, alwaysVisibleOptions:[])
                appDelegate.SideMenuView = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "SidemenuViewController") as! SidemenuViewController
                appDelegate.SideMenu.leftViewStatusBarVisibleOptions = .onAll
                appDelegate.SideMenu.leftViewStatusBarStyle = .lightContent
                 UserDefaults.standard.set(true,forKey:"launchedBefore")
                var rect = appDelegate.SideMenuView.view.frame;
                rect.size.width = 280;
                appDelegate.SideMenuView.view.frame = rect
                appDelegate.SideMenu.leftView().addSubview(appDelegate.SideMenuView.view)
                print(appDelegate.SideMenu)
                self.present(appDelegate.SideMenu, animated:true, completion: nil)
            }
        }
        
 
 
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
private typealias ScrollView = IntroViewController
extension ScrollView
{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        // Test the offset and calculate the current page after scrolling ends
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        // Change the indicator
        self.pageControl.currentPage = Int(currentPage);
        // Change the text accordingly
        if Int(self.pageControl.currentPage) == 5{
           gotItButton.setTitle("GOT IT", for: UIControlState.normal)
        }
        else{
             gotItButton.setTitle("GO TO NEXT", for: UIControlState.normal)
        }

    }
}

