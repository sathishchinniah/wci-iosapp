//
//  AddFacilityTableViewCell.swift
//  JEDE_r
//
//  Created by Exarcplus on 29/06/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit

class AddFacilityTableViewCell: UITableViewCell {
    
    @IBOutlet weak var CountView: GMStepper!
    @IBOutlet weak var namelab: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        CountView.labelFont = UIFont(name: "Roboto-Regular", size: 13)!
        CountView.backgroundColor = UIColor.clear
        CountView.leftButton.backgroundColor = UIColor.clear
        CountView.rightButton.backgroundColor = UIColor.clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
