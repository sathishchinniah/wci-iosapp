//
//  RateViewController.swift
//  JEDE_r
//
//  Created by Exarcplus on 25/05/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import AFNetworking
import LGSideMenuController

class RateViewController: UIViewController
{

    @IBOutlet weak var sliderView : StepSlider!
    @IBOutlet weak var cleanratingview = EZRatingView()
    @IBOutlet weak var accessratingview = EZRatingView()
    @IBOutlet weak var waitingtimeratingview = EZRatingView()
    @IBOutlet weak var commentratingview = EZRatingView()
    var myclass : MyClass!
    @IBOutlet weak var scrollview : UIScrollView!
    var getdic : NSMutableDictionary!
    @IBOutlet weak var updownimg : UIImageView!
    @IBOutlet weak var myheight : NSLayoutConstraint!
    var checkstr : String!
    @IBOutlet weak var fourrateview : UIView!
    
    var Userdic = NSMutableDictionary()
    var passuserid : String!
    var passlooid : String!
    
    var passoverall : String!
    var passcleanliness : String!
    var passaccess : String!
    var passwaitingtime : String!
    var passcomment : String!
    
    @IBOutlet weak var likeButton: SparkButton!
    @IBOutlet weak var textview : UITextView!
    @IBOutlet weak var ratebutton : UIButton!
    @IBOutlet weak var notnowbutton : UIButton!
    @IBOutlet weak var loonamelab : UILabel!
    var isLiked:Bool!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        myclass = MyClass()
        self.navigationController?.navigationBar.isHidden = true
        
//        scrollview.delegate=self
        if getdic != nil
        {
            print(getdic)
            loonamelab.text = getdic.value(forKey: "loo_name") as? String
            let typestr = getdic.value(forKey: "type") as! String
            if typestr == "Public"
            {
                self.notnowbutton.isHidden = false
                self.ratebutton.isHidden = false
            }
            else
            {
                self.ratebutton.isHidden = false
                self.notnowbutton.isHidden = true
            }
            passlooid =  getdic.value(forKey: "loo_id") as? String
            let imageurl = getdic.value(forKey: "loo_image") as? String
           // print(imageurl!)
            if imageurl == "" || imageurl?.characters.count == 0 || imageurl == nil
            {
                scrollview.addTwitterCover(with: UIImage(named: "Toilets-Defualt.png"))
            }
            else
            {
                let url = URL(string: imageurl!)
                let data = try? Data(contentsOf: url!) //make                                                    sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                scrollview.addTwitterCover(with: UIImage(data: data!))
            }
            
//            let favouritestr = getdic.value(forKey: "favourite_status") as? String
//            print(favouritestr!)
//            if favouritestr == "0"
//            {
//                self.isLiked = false
//                self.likeButton.setImage(UIImage(named: "Fav.png"), for: UIControlState())
//                self.likeButton.unLikeBounce(0.4)
//            }
//            else
//            {
//                self.isLiked = true
//                self.likeButton.setImage(UIImage(named: "LIKE.png"), for: UIControlState())
//                self.likeButton.likeBounce(0.6)
//            }
            
            self.isLiked = false
            self.likeButton.setImage(UIImage(named: "Fav.png"), for: UIControlState())
            self.likeButton.unLikeBounce(0.4)

        }
        else
        {
            self.isLiked = false
            self.likeButton.setImage(UIImage(named: "Fav.png"), for: UIControlState())
            self.likeButton.unLikeBounce(0.4)
            passlooid = ""
            scrollview.addTwitterCover(with: UIImage(named: "Toilets-Defualt.png"))
        }
        
        if UserDefaults.standard.object(forKey: "Logindetail") == nil
        {
            passuserid = ""
        }
        else
        {
//            let placesData = UserDefaults.standard.object(forKey: "Logindetail") as? NSData
//            self.Userdic.removeAllObjects()
//            self.Userdic.addEntries(from: NSKeyedUnarchiver.unarchiveObject(with: placesData! as Data) as! NSDictionary as [NSObject : AnyObject])
            let placesData = UserDefaults.standard.object(forKey: "Logindetail") as? NSDictionary
            self.Userdic.removeAllObjects()
            if placesData != nil{
                self.Userdic.addEntries(from: (placesData as? [String : Any])!)
            }
            print(self.Userdic)
            passuserid = self.Userdic.value(forKey: "user_id") as? String
            print(passuserid)
        }

        self.sliderView.index = 2
        passoverall = String(format: "%d", self.sliderView.index+1)
        print(passoverall)
        
        updownimg.image = UIImage.init(named: "down-arrow.png")
        //myheight.constant = 0
        fourrateview.isHidden = true
        checkstr = "close"
        
        self.sliderView.sliderCircleImage = UIImage.init(named: "button_compass.png")
        sliderView.labels = ["Awful", "Poor", "Average", "Good", "Excellent"]
        
        self.cleanratingview?.value = 0
        self.cleanratingview?.stepInterval = 1.0
        self.cleanratingview?.isUserInteractionEnabled = true
        self.cleanratingview?.isSelected = true
        
        
        self.accessratingview?.value = 0
        self.accessratingview?.stepInterval = 1.0
        self.accessratingview?.isUserInteractionEnabled = true
        self.accessratingview?.isSelected = true
        
        self.waitingtimeratingview?.value = 0
        self.waitingtimeratingview?.stepInterval = 1.0
        self.waitingtimeratingview?.isUserInteractionEnabled = true
        self.waitingtimeratingview?.isSelected = true
        
        self.commentratingview?.value = 0
        self.commentratingview?.stepInterval = 1.0
        self.commentratingview?.isUserInteractionEnabled = true
        self.commentratingview?.isSelected = true
        
         passcleanliness = ""
         passaccess = ""
         passwaitingtime = ""
         passcomment = ""
        // Do any additional setup after loading the view.
    }
    

    @IBAction func changeValue(_ sender: StepSlider)
    {
        print("Selected index: \(UInt(sender.index))")
        passoverall = String(format: "%d",(UInt(sender.index+1)))
        print(passoverall)
        
        if UInt(sender.index) == 0
        {
            fourrateview.isHidden = false
            //myheight.constant = 157
            updownimg.image = UIImage.init(named: "up-arrow.png")
            checkstr = "open"
        }
        else if UInt(sender.index) == 1
        {
            fourrateview.isHidden = false
            //myheight.constant = 157
            updownimg.image = UIImage.init(named: "up-arrow.png")
            checkstr = "open"
        }
        else if UInt(sender.index) == 2
        {
            fourrateview.isHidden = true
            //myheight.constant = 0
            updownimg.image = UIImage.init(named: "down-arrow.png")
            checkstr = "close"
        }
        else if UInt(sender.index) == 3
        {
            fourrateview.isHidden = true
            //myheight.constant = 0
            checkstr = "close"
            updownimg.image = UIImage.init(named: "down-arrow.png")
        }
        else if UInt(sender.index) == 4
        {
            fourrateview.isHidden = true
            //myheight.constant = 0
            checkstr = "close"
            updownimg.image = UIImage.init(named: "down-arrow.png")
        }
//        label.text = "Selected index: \(UInt(sender.index))"
    }
    
//    - (void)ratingChanged:(EZRatingView *)sender
//    {
//    [self.slider setValue:[sender value]];
//    [self.label setText:[NSString stringWithFormat:@"set and get: %.2f", sender.value]];
//    }
    
    @IBAction func updownimgbutton(_ sender : UIButton)
    {
        if checkstr == "close"
        {
            //myheight.constant = 157
            updownimg.image = UIImage.init(named: "up-arrow.png")
            checkstr = "open"
            fourrateview.isHidden = false
        }
        else
        {
            checkstr = "close"
            updownimg.image = UIImage.init(named: "down-arrow.png")
            //myheight.constant = 0
            fourrateview.isHidden = true
        }
    }
    
    @IBAction func cleanratingChange(_ sender: EZRatingView)
    {
        print("Selected rating: \(UInt(sender.value))")
        passcleanliness = String(format: "%d",(UInt(sender.value)))
        print(passcleanliness)
    }
    
    @IBAction func accessratingChange(_ sender: EZRatingView)
    {
        print("Selected rating: \(UInt(sender.value))")
        passaccess = String(format: "%d",(UInt(sender.value)))
        print(passaccess)
    }
    
    @IBAction func waitingtimeratingChange(_ sender: EZRatingView)
    {
        print("Selected rating: \(UInt(sender.value))")
        passwaitingtime = String(format: "%d",(UInt(sender.value)))
        print(passwaitingtime)
    }
    
    @IBAction func commentratingChange(_ sender: EZRatingView)
    {
        print("Selected rating: \(UInt(sender.value))")
        passcomment = String(format: "%d",(UInt(sender.value)))
        print(passcomment)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func ratebutton(_sender: UIButton)
    {
        self.runlink()
    }
    
    
    func runlink()
    {
        if AFNetworkReachabilityManager.shared().isReachable
        {
            print(passuserid)
            print(passlooid)
            print(passoverall)
            print(passcleanliness)
            if passcleanliness == ""
            {
                passcleanliness = "0"
            }
            print(passaccess)
            if passaccess == ""
            {
                passaccess = "0"
            }
            print(passwaitingtime)
            if passwaitingtime == ""
            {
                passwaitingtime = "0"
            }
            print(passcomment)
            if passcomment == ""
            {
                passcomment = "0"
            }
            print(self.textview.text)
            let urlstring = String(format:"%@/add_rating.php?user_id=%@&loo_id=%@&overall=%@&cleanliness=%@&access=%@&waiting_time=%@&comment=%@&review=%@",kBaseURL,passuserid,passlooid,passoverall,passcleanliness,passaccess,passwaitingtime,passcomment,self.textview.text)
            print(urlstring)
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: true, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    if jsonObject.count != 0
                    {
                        let str = (jsonObject.object(at: 0) as AnyObject).value(forKey: "message") as? String
                        if str == "success"
                        {
                            self.myclass.ShowsinglebutAlertwithTitle(self.loonamelab.text!, withAlert:"You have Rated Successfully", withIdentifier:"")
                            let categorystring = NSMutableArray()
                            let nsmutdic = NSMutableDictionary()
                            nsmutdic.setObject("", forKey:"gender" as NSCopying);
                            nsmutdic.setObject("", forKey:"KM" as NSCopying);
                            nsmutdic.setObject("", forKey:"wheelchair" as NSCopying);
                            nsmutdic.setObject("", forKey:"parking" as NSCopying);
                            nsmutdic.setObject("", forKey:"rating" as NSCopying);
                            nsmutdic.setObject("", forKey:"distance" as NSCopying);
                            nsmutdic.setObject("", forKey:"price" as NSCopying);
                            nsmutdic.setObject(categorystring, forKey:"category" as NSCopying);
                            print(nsmutdic)
                            
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            let mainview = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
                            let valueToSave = "noneedrunlink"
                            UserDefaults.standard.set(valueToSave, forKey: "runlink")
                            mainview.getdic = nsmutdic
                            let nav = UINavigationController.init(rootViewController: mainview)
                            appDelegate.SideMenu = LGSideMenuController.init(rootViewController: nav)
                            appDelegate.SideMenu.setLeftViewEnabledWithWidth(280, presentationStyle: .slideAbove, alwaysVisibleOptions:[])
                            appDelegate.SideMenuView = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "SidemenuViewController") as! SidemenuViewController
                            appDelegate.SideMenu.leftViewStatusBarVisibleOptions = .onAll
                            appDelegate.SideMenu.leftViewStatusBarStyle = .lightContent
                            //        appDelegate.SideMenuView.selectedpage = 0;
                            var rect = appDelegate.SideMenuView.view.frame;
                            rect.size.width = 280;
                            appDelegate.SideMenuView.view.frame = rect
                            appDelegate.SideMenu.leftView().addSubview(appDelegate.SideMenuView.view)
                            print(appDelegate.SideMenu)
                            self.present(appDelegate.SideMenu, animated:true, completion: nil)
                        }
                        else
                        {
                            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:str!, withIdentifier:"")
                        }
                        
                    }
                }
                else
                {
                    NSLog("jsonObject=error");
                }
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
    
    
    @IBAction func addfavouritebutton(_sender : UIButton)
    {
        if UserDefaults.standard.object(forKey: "Logindetail") == nil
        {
            let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(mainview, animated: true)
        }
        else
        {
            isLiked = !isLiked
            if isLiked == true
            {
                likeButton.setImage(UIImage(named: "LIKE.png"), for: UIControlState())
                likeButton.likeBounce(0.6)
                likeButton.animate()
                isLiked = false
            }
            else
            {
                likeButton.setImage(UIImage(named: "Fav.png"), for: UIControlState())
                likeButton.unLikeBounce(0.4)
                isLiked = true
            }
            self.favlink()
        }
    }
    func favlink()
    {
        if AFNetworkReachabilityManager.shared().isReachable
        {
            let urlstring : String!
            isLiked = !isLiked
            if isLiked == true
            {
                urlstring = String(format:"%@/fav.php?user_id=%@&loo_id=%@&fav_status=1",kBaseURL,passuserid,passlooid)
                print(urlstring)
            }
            else
            {
                urlstring = String(format:"%@/fav.php?user_id=%@&loo_id=%@&fav_status=0",kBaseURL,passuserid,passlooid)
                print(urlstring)
            }
            
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: false, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    if jsonObject.count != 0
                    {
                        let copyarr = jsonObject;
                        print(copyarr)
                        
                    }
                }
                else
                {
                    NSLog("jsonObject=error");
                }
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
    
    @IBAction func backbuttonclick(_sender:UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nothanksbuttonclick(_sender:UIButton)
    {
        let categorystring = NSMutableArray()
        let nsmutdic = NSMutableDictionary()
        nsmutdic.setObject("", forKey:"gender" as NSCopying);
        nsmutdic.setObject("", forKey:"KM" as NSCopying);
        nsmutdic.setObject("", forKey:"wheelchair" as NSCopying);
        nsmutdic.setObject("", forKey:"parking" as NSCopying);
        nsmutdic.setObject("", forKey:"rating" as NSCopying);
        nsmutdic.setObject("", forKey:"distance" as NSCopying);
        nsmutdic.setObject("", forKey:"price" as NSCopying);
        nsmutdic.setObject(categorystring, forKey:"category" as NSCopying);
        print(nsmutdic)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let mainview = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
        let valueToSave = "noneedrunlink"
        UserDefaults.standard.set(valueToSave, forKey: "runlink")
        mainview.getdic = nsmutdic
        let nav = UINavigationController.init(rootViewController: mainview)
        appDelegate.SideMenu = LGSideMenuController.init(rootViewController: nav)
        appDelegate.SideMenu.setLeftViewEnabledWithWidth(280, presentationStyle: .slideAbove, alwaysVisibleOptions:[])
        appDelegate.SideMenuView = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "SidemenuViewController") as! SidemenuViewController
        appDelegate.SideMenu.leftViewStatusBarVisibleOptions = .onAll
        appDelegate.SideMenu.leftViewStatusBarStyle = .lightContent
                                    //        appDelegate.SideMenuView.selectedpage = 0;
        var rect = appDelegate.SideMenuView.view.frame;
        rect.size.width = 280;
        appDelegate.SideMenuView.view.frame = rect
        appDelegate.SideMenu.leftView().addSubview(appDelegate.SideMenuView.view)
        print(appDelegate.SideMenu)
        self.present(appDelegate.SideMenu, animated:true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
