//
//  SidemenuTableViewCell.swift
//  JEDE_r
//
//  Created by Exarcplus iOS 2 on 03/05/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit

class SidemenuTableViewCell: UITableViewCell {
    
    @IBOutlet weak var Title:UILabel!
    @IBOutlet weak var imagee:UIImageView!
    @IBOutlet weak var switchButton : UISwitch!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
