//
//  MyFiltersViewController.swift
//  JEDE_r
//
//  Created by Exarcplus on 19/05/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import AFNetworking

class MyFiltersViewController: UIViewController, NHRangeSliderViewDelegate, RangeSeekSliderDelegate {
    
    var myclass : MyClass!
    @IBOutlet weak var radio1 : UIImageView!
    @IBOutlet weak var radio2 : UIImageView!
    @IBOutlet weak var radio3 : UIImageView!
    @IBOutlet weak var radio4 : UIImageView!

    @IBOutlet weak var rangesliderview : RangeSeekSlider!
    @IBOutlet weak var pricerangesliderview : NHRangeSliderView!
    var genderstring : String!
    @IBOutlet weak var femaleimg : UIImageView!
    @IBOutlet weak var maleimg : UIImageView!
    @IBOutlet weak var uniseximg : UIImageView!
    
    @IBOutlet weak var wheelchairbgimg : UIImageView!
    @IBOutlet weak var parkingbgimg : UIImageView!
    @IBOutlet weak var wheelchairlab : UILabel!
    @IBOutlet weak var parkinglab : UILabel!
    //    @IBOutlet weak var wheelchairview : RoundView!
    //    @IBOutlet weak var parkingview : RoundView!
    @IBOutlet weak var tagListView : JCTagListView!
    

    var wheelchairstr : String!
    var parkingstr : String!
    
    var radio1str : String!
    var radio2str : String!
    var radio3str : String!
    var radio4str : String!
    
    var passrating : String!
    var passdistance : String!
    var passprice : String!
    
    var mydic = NSMutableDictionary()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        myclass = MyClass()
        let view = UIView(frame:  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 20))
        view.backgroundColor = UIColor.init(red: 27.0/255.0, green: 34.0/255.0, blue: 40.0/255.0, alpha: 1.0)
        self.navigationController?.view.addSubview(view)
        
        
        
        
        if UserDefaults.standard.object(forKey: "Filterdetail") != nil
        {
//            let placesData = UserDefaults.standard.object(forKey: "Filterdetail") as? NSData
//            self.mydic.removeAllObjects()
//            self.mydic.addEntries(from: NSKeyedUnarchiver.unarchiveObject(with: placesData! as Data) as! NSDictionary as [NSObject : AnyObject])
            let placesData = UserDefaults.standard.object(forKey: "Filterdetail") as? NSDictionary
            self.mydic.removeAllObjects()
            if placesData != nil{
                self.mydic.addEntries(from: (placesData as? [String : Any])!)
            }
            print(self.mydic)
            
            femaleimg.image = UIImage.init(named: "FemaleGrey.png")
            maleimg.image = UIImage.init(named: "MaleGrey.png")
            uniseximg.image = UIImage.init(named: "UnisexGrey.png")
            genderstring = mydic.value(forKey: "gender") as! String
            print(genderstring)
            if genderstring == "Male"
            {
                maleimg.image = UIImage.init(named: "MaleGrey.png")
            }
            else if genderstring == "Female"
            {
                femaleimg.image = UIImage.init(named: "FemaleGrey.png")
            }
            else if genderstring == "Unisex"
            {
                uniseximg.image = UIImage.init(named: "UnisexGrey.png")
            }
            else
            {
                genderstring = "0"
//                femaleimg.image = UIImage.init(named: "FemaleGrey.png")
//                maleimg.image = UIImage.init(named: "MaleGrey.png")
//                uniseximg.image = UIImage.init(named: "UnisexGrey.png")
            }
            
            wheelchairstr = mydic.value(forKey: "wheelchair") as! String
            if wheelchairstr == "0"
            {
                wheelchairbgimg.image = UIImage.init(named: "wheelchairunselected.png")
            }
            else if wheelchairstr == "1"
            {
                wheelchairbgimg.image = UIImage.init(named: "wheelchairselected.png")
            }
            else
            {
                wheelchairbgimg.image = UIImage.init(named: "wheelchairunselected.png")
            }
            
            parkingstr = mydic.value(forKey: "parking") as! String
            print(parkingstr)
            if parkingstr == "0"
            {
                parkingbgimg.image = UIImage.init(named: "parkingunselected.png")
            }
            else if parkingstr == "1"
            {
                parkingbgimg.image = UIImage.init(named: "parkingselected.png")
            }
            else
            {
                parkingbgimg.image = UIImage.init(named: "parkingunselected.png")
            }
            
            
            passrating = mydic.value(forKey: "rating") as! String
            print(passrating)
            if passrating == "1"
            {
                radio1.image = UIImage.init(named: "radioselected.png")
                radio2.image = UIImage.init(named: "radiounselected.png")
                radio3.image = UIImage.init(named: "radiounselected.png")
                radio4.image = UIImage.init(named: "radiounselected.png")
                radio1str = "1"
                radio4str = "0"
                radio3str = "0"
                radio2str = "0"
            }
            else if passrating == "2"
            {
                radio2.image = UIImage.init(named: "radioselected.png")
                radio1.image = UIImage.init(named: "radiounselected.png")
                radio3.image = UIImage.init(named: "radiounselected.png")
                radio4.image = UIImage.init(named: "radiounselected.png")
                radio2str = "1"
                radio4str = "0"
                radio3str = "0"
                radio1str = "0"
            }
            else if passrating == "3"
            {
                radio3.image = UIImage.init(named: "radioselected.png")
                radio1.image = UIImage.init(named: "radiounselected.png")
                radio2.image = UIImage.init(named: "radiounselected.png")
                radio4.image = UIImage.init(named: "radiounselected.png")
                radio3str = "1"
                radio4str = "0"
                radio2str = "0"
                radio1str = "0"
            }
            else if passrating == "4"
            {
                radio4.image = UIImage.init(named: "radioselected.png")
                radio1.image = UIImage.init(named: "radiounselected.png")
                radio2.image = UIImage.init(named: "radiounselected.png")
                radio3.image = UIImage.init(named: "radiounselected.png")
                radio4str = "1"
                radio3str = "0"
                radio2str = "0"
                radio1str = "0"
            }
            else
            {
                radio4.image = UIImage.init(named: "radiounselected.png")
                radio1.image = UIImage.init(named: "radiounselected.png")
                radio2.image = UIImage.init(named: "radiounselected.png")
                radio3.image = UIImage.init(named: "radiounselected.png")
                radio4str = "0"
                radio3str = "0"
                radio2str = "0"
                radio1str = "0"
                passrating = ""
            }
            
            
            passdistance = mydic.value(forKey: "distance") as! String
            print(passdistance)
            if passdistance == ""
            {
                rangesliderview.sizeToFit()
                rangesliderview.delegate = self
                rangesliderview.minValue = 0.0
                rangesliderview.maxValue = 5.0
                rangesliderview.selectedMinValue = 0.0
                rangesliderview.selectedMaxValue = 0.5
                rangesliderview.minDistance = 0.5
                rangesliderview.maxDistance = 5.0
                rangesliderview.handleDiameter = 30.0
                rangesliderview.handleColor = UIColor.lightGray
                rangesliderview.backgroundColor = UIColor.white
                rangesliderview.selectedHandleDiameterMultiplier = 1.3
                rangesliderview.numberFormatter.numberStyle = .none
                //        rangesliderview.numberFormatter.locale = Locale(identifier: "en_US")
                rangesliderview.numberFormatter.maximumFractionDigits = 1
                rangesliderview.minLabelFont = UIFont(name: "Roboto-Regular", size: 14.0)!
                rangesliderview.maxLabelFont = UIFont(name: "Roboto-Regular", size: 14.0)!
                rangesliderview.lineHeight = 10.0
                rangesliderview.handleImage = UIImage.init(named: "handleimg.png")
                rangesliderview.minLabelColor = UIColor.black
                rangesliderview.maxLabelColor = UIColor.black
            }
            else{
                let myInt = (passdistance as NSString).doubleValue
                rangesliderview.sizeToFit()
                rangesliderview.delegate = self
                rangesliderview.minValue = 0.0
                rangesliderview.maxValue = 5.0
                rangesliderview.selectedMinValue = 0.0
                rangesliderview.selectedMaxValue = CGFloat(myInt)
                rangesliderview.minDistance = 0.0
                rangesliderview.maxDistance = 5.0
                rangesliderview.handleDiameter = 30.0
                rangesliderview.handleColor = UIColor.lightGray
                rangesliderview.backgroundColor = UIColor.white
                rangesliderview.selectedHandleDiameterMultiplier = 1.3
                rangesliderview.numberFormatter.numberStyle = .none
                //        rangesliderview.numberFormatter.locale = Locale(identifier: "en_US")
                rangesliderview.numberFormatter.maximumFractionDigits = 1
                rangesliderview.minLabelFont = UIFont(name: "Roboto-Regular", size: 14.0)!
                rangesliderview.maxLabelFont = UIFont(name: "Roboto-Regular", size: 14.0)!
                rangesliderview.lineHeight = 10.0
                rangesliderview.handleImage = UIImage.init(named: "handleimg.png")
                rangesliderview.minLabelColor = UIColor.black
                rangesliderview.maxLabelColor = UIColor.black
            }
            
            
            
            passprice = mydic.value(forKey: "price") as! String
            print(passprice)
            if passprice != "5"
            {
                self.pricerangesliderview.sizeToFit()
                self.pricerangesliderview.delegate = self
                //pricerangesliderview.trackHighlightTintColor = UIColor.red
                pricerangesliderview.lowerValue = 0.0
                pricerangesliderview.minimumValue = 0.0
                pricerangesliderview.maximumValue = 5.0
                pricerangesliderview.upperValue = 5.0
                pricerangesliderview.stepValue = 1.0
                pricerangesliderview.gapBetweenThumbs = 0.5
                pricerangesliderview.lowerDisplayStringFormat = "CHF %.0f"
                pricerangesliderview.upperDisplayStringFormat = "CHF %.0f"
                passprice = String(format: "%d", pricerangesliderview.maximumValue)
            }
            else
            {
                let myInts = (passprice as NSString).integerValue
                self.pricerangesliderview.sizeToFit()
                self.pricerangesliderview.delegate = self
                //pricerangesliderview.trackHighlightTintColor = UIColor.red
                pricerangesliderview.lowerValue = 0.0
                pricerangesliderview.minimumValue = 0.0
                pricerangesliderview.maximumValue = 5.0
                pricerangesliderview.upperValue = Float(myInts)
                pricerangesliderview.stepValue = 1.0
                pricerangesliderview.gapBetweenThumbs = 0.5
                pricerangesliderview.lowerDisplayStringFormat = "CHF %.0f"
                pricerangesliderview.upperDisplayStringFormat = "CHF %.0f"
                passprice = String(format: "%d", pricerangesliderview.upperValue)
            }
            
//            let catar = mydic.value(forKey: "category") as! NSArray
//            print(catar)
//            passcategory = catar.componentsJoined(by: ",")
        }
        else
        {
            rangesliderview.sizeToFit()
            rangesliderview.delegate = self
            rangesliderview.minValue = 0.0
            rangesliderview.maxValue = 5.0
            rangesliderview.selectedMinValue = 0.0
            rangesliderview.selectedMaxValue = 0.5
            rangesliderview.minDistance = 0.0
            rangesliderview.maxDistance = 5.0
            rangesliderview.handleDiameter = 30.0
            rangesliderview.handleColor = UIColor.lightGray
            rangesliderview.backgroundColor = UIColor.white
            rangesliderview.selectedHandleDiameterMultiplier = 1.3
            rangesliderview.numberFormatter.numberStyle = .none
            //        rangesliderview.numberFormatter.locale = Locale(identifier: "en_US")
            rangesliderview.numberFormatter.maximumFractionDigits = 1
            rangesliderview.minLabelFont = UIFont(name: "Roboto-Regular", size: 14.0)!
            rangesliderview.maxLabelFont = UIFont(name: "Roboto-Regular", size: 14.0)!
            rangesliderview.lineHeight = 10.0
            rangesliderview.handleImage = UIImage.init(named: "handleimg.png")
            rangesliderview.minLabelColor = UIColor.black
            rangesliderview.maxLabelColor = UIColor.black
            
            
            self.pricerangesliderview.sizeToFit()
            self.pricerangesliderview.delegate = self
            //pricerangesliderview.trackHighlightTintColor = UIColor.red
            pricerangesliderview.lowerValue = 0.0
            pricerangesliderview.minimumValue = 0.0
            pricerangesliderview.maximumValue = 5.0
            pricerangesliderview.upperValue = 5.0
            pricerangesliderview.stepValue = 1.0
            pricerangesliderview.gapBetweenThumbs = 0.5
            pricerangesliderview.lowerDisplayStringFormat = "CHF %.0f"
            pricerangesliderview.upperDisplayStringFormat = "CHF %.0f"
            genderstring = "0"
            wheelchairstr = "0"
            parkingstr = "0"
            
            radio1str = "0"
            radio2str = "0"
            radio3str = "0"
            radio4str = "0"
            passrating = ""
            passdistance = String(format: "%d", rangesliderview.maxValue)
            passprice = String(format: "%d", pricerangesliderview.maximumValue)
            
            femaleimg.image = UIImage.init(named: "FemaleGrey.png")
            maleimg.image = UIImage.init(named: "MaleGrey.png")
            uniseximg.image = UIImage.init(named: "UnisexGrey.png")
            
            wheelchairbgimg.image = UIImage.init(named: "wheelchairunselected.png")
            parkingbgimg.image = UIImage.init(named: "parkingunselected.png")
        }
        
        self.catlink()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat)
    {
        print("Standard slider updated. Min Value: \(minValue) Max Value: \(maxValue)")
        rangesliderview.minLabelAccessibilityLabel = String(format: "%dRS",slider.minValue)
        rangesliderview.minLabelAccessibilityLabel = String(format: "%dRS",slider.maxValue)
        passdistance = String(format: "%.1f", maxValue)
        print(passdistance)
    }
    
    func didStartTouches(in slider: RangeSeekSlider) {
        print("did start touches")
    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
        print("did end touches")
    }
    
    func sliderValueChanged(slider: NHRangeSlider?)
    {
        let a:Double = (slider!.upperValue)
        let arr = String(format: "%.1f", a)
        let arra = arr.components(separatedBy: ".") as NSArray
        passprice = arra.object(at: 0) as! String
        print(passprice)
    }
    
    
    @IBAction func radio1button(_sender:UIButton)
    {
        if radio1str == "0"
        {
            radio1.image = UIImage.init(named: "radioselected.png")
            radio2.image = UIImage.init(named: "radiounselected.png")
            radio3.image = UIImage.init(named: "radiounselected.png")
            radio4.image = UIImage.init(named: "radiounselected.png")
            radio1str = "1"
            radio4str = "0"
            radio3str = "0"
            radio2str = "0"
            passrating = "1"
        }
        else
        {
            radio1.image = UIImage.init(named: "radiounselected.png")
            radio1str = "0"
            passrating = ""
        }
    }
    @IBAction func radio2button(_sender:UIButton)
    {
        if radio2str == "0"
        {
            radio2.image = UIImage.init(named: "radioselected.png")
            radio1.image = UIImage.init(named: "radiounselected.png")
            radio3.image = UIImage.init(named: "radiounselected.png")
            radio4.image = UIImage.init(named: "radiounselected.png")
            radio2str = "1"
            radio4str = "0"
            radio3str = "0"
            radio1str = "0"
            passrating = "2"
        }
        else
        {
            radio2.image = UIImage.init(named: "radiounselected.png")
            radio2str = "0"
            passrating = ""
        }
    }
    @IBAction func radio3button(_sender:UIButton)
    {
        if radio3str == "0"
        {
            radio3.image = UIImage.init(named: "radioselected.png")
            radio1.image = UIImage.init(named: "radiounselected.png")
            radio2.image = UIImage.init(named: "radiounselected.png")
            radio4.image = UIImage.init(named: "radiounselected.png")
            radio3str = "1"
            radio4str = "0"
            radio2str = "0"
            radio1str = "0"
            passrating = "3"
        }
        else
        {
            radio3.image = UIImage.init(named: "radiounselected.png")
            radio3str = "0"
            passrating = ""
        }
    }
    @IBAction func radio4button(_sender:UIButton)
    {
        if radio4str == "0"
        {
            radio4.image = UIImage.init(named: "radioselected.png")
            radio1.image = UIImage.init(named: "radiounselected.png")
            radio2.image = UIImage.init(named: "radiounselected.png")
            radio3.image = UIImage.init(named: "radiounselected.png")
            radio4str = "1"
            radio3str = "0"
            radio2str = "0"
            radio1str = "0"
            passrating = "4"
        }
        else
        {
            radio4.image = UIImage.init(named: "radiounselected.png")
            radio4str = "0"
            passrating = ""
        }
    }
    
    
    @IBAction func wheelchairbutton(_sender:UIButton)
    {
        if wheelchairstr == "0"
        {
            wheelchairbgimg.image = UIImage.init(named: "wheelchairselected.png")
            wheelchairstr = "1"
        }
        else
        {
            wheelchairbgimg.image = UIImage.init(named: "wheelchairunselected.png")
            wheelchairstr = "0"
        }
    }
    
    @IBAction func parkingbutton(_sender:UIButton)
    {
        if parkingstr == "0"
        {
            parkingbgimg.image = UIImage.init(named: "parkingselected.png")
            parkingstr = "1"
        }
        else
        {
            parkingbgimg.image = UIImage.init(named: "parkingunselected.png")
            parkingstr = "0"
        }
    }
    @IBAction func femalebutton(_sender : UIButton)
    {
        if femaleimg.image == UIImage.init(named: "FemaleGrey.png")
        {
            femaleimg.image = UIImage.init(named: "Female_select.png")
            genderstring = "Female"
            maleimg.image = UIImage.init(named: "MaleGrey.png")
            uniseximg.image = UIImage.init(named: "UnisexGrey.png")
        }
        else
        {
            femaleimg.image = UIImage.init(named: "FemaleGrey.png")
        }
    }
    
    @IBAction func malebutton(_sender : UIButton)
    {
        if maleimg.image == UIImage.init(named: "MaleGrey.png")
        {
            maleimg.image = UIImage.init(named: "Male_select.png")
            genderstring = "Male"
            femaleimg.image = UIImage.init(named: "FemaleGrey.png")
            uniseximg.image = UIImage.init(named: "UnisexGrey.png")
        }
        else
        {
            maleimg.image = UIImage.init(named: "MaleGrey.png")
            genderstring = ""
        }
    }
    
    @IBAction func unisexbutton(_sender : UIButton)
    {
        if uniseximg.image == UIImage.init(named: "UnisexGrey.png")
        {
            maleimg.image = UIImage.init(named: "MaleGrey.png")
            femaleimg.image = UIImage.init(named: "FemaleGrey.png")
            uniseximg.image = UIImage.init(named: "Unisex_select.png")
            genderstring = "Unisex"
        }
        else
        {
            uniseximg.image = UIImage.init(named: "UnisexGrey.png")
            genderstring = ""
        }
    }
    
    
    
    @IBAction func closebutton(_sender:UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func catlink()
    {
        if AFNetworkReachabilityManager.shared().isReachable
        {
            let urlstring = String(format:"%@/category.php",kBaseURL)
            print(urlstring)
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: true, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    if jsonObject.count != 0
                    {
                        let copyarr = jsonObject;
                        print(copyarr)
                        
                        self.tagListView.canSelectTags = true
                        self.tagListView.tagSelectedTextColor = UIColor.white
                        self.tagListView.tagSelectedBackgroundColor = UIColor.init(red: 0.000, green: 0.753, blue: 0.737, alpha: 1.0)
                        self.tagListView.tagCornerRadius = 12.0
                        
                        let cnaearr = copyarr.value(forKey: "category_name") as! NSArray
                        self.tagListView.tags.addObjects(from:cnaearr.mutableCopy() as! [AnyObject])
                        self.tagListView.collectionView.reloadData()
                        
                        if UserDefaults.standard.object(forKey: "Filterdetail") != nil
                        {
//                            let placesData = UserDefaults.standard.object(forKey: "Filterdetail") as? NSData
//                            self.mydic.removeAllObjects()
//                            self.mydic.addEntries(from: NSKeyedUnarchiver.unarchiveObject(with: placesData! as Data) as! NSDictionary as [NSObject : AnyObject])
                            let placesData = UserDefaults.standard.object(forKey: "Logindetail") as? NSDictionary
                            self.mydic.removeAllObjects()
                            if placesData != nil{
                                self.mydic.addEntries(from: (placesData as? [String : Any])!)
                            }
                            print(self.mydic)
                            let catar = self.mydic.value(forKey: "category") as! NSArray
                            print(catar)
//                            let str = catar.componentsJoined(by: ",")
//                            print(str)
//                            
//                            let checkstr = cnaearr.componentsJoined(by: ",")
//                            print(checkstr)
//                            
//                            if str == checkstr
//                            {
                                self.tagListView.selectedTags = catar.mutableCopy() as! NSMutableArray
                                self.tagListView.tagCornerRadius = 22.0
                                self.tagListView.collectionView.reloadData()
//                            }
                        }
                        else
                        {
                            
                        }
                        
                        self.tagListView.setCompletionBlockWithSelected({(_ index: Int) -> Void in
                            print("______\(Int(index))______")
                            
                        })
                        
                    }
                }
                else
                {
                    NSLog("jsonObject=error");
                }
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
    
    @IBAction func applybutton(_sender : UIButton)
    {
        print(self.tagListView.selectedTags)

        let nsmutdic = NSMutableDictionary()
        nsmutdic.setObject(genderstring, forKey:"gender" as NSCopying);
        nsmutdic.setObject(wheelchairstr, forKey:"wheelchair" as NSCopying);
        nsmutdic.setObject(parkingstr, forKey:"parking" as NSCopying);
        nsmutdic.setObject(passrating, forKey:"rating" as NSCopying);
        nsmutdic.setObject(passdistance, forKey:"distance" as NSCopying);
        nsmutdic.setObject(passprice, forKey:"price" as NSCopying);
        nsmutdic.setObject(self.tagListView.selectedTags, forKey:"category" as NSCopying);
        print(nsmutdic)

        let data = NSKeyedArchiver.archivedData(withRootObject: nsmutdic)
        UserDefaults.standard.set(data, forKey:"Filterdetail")
        UserDefaults.standard.synchronize()

        let valueToSave = "comingfromfilter"
        UserDefaults.standard.set(valueToSave, forKey: "screen")

//        next.fromfilter
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func resetbutton(_sender: UIButton)
    {
        let valueToSave = "comingfromfilter"
        UserDefaults.standard.set(valueToSave, forKey: "screen")
        UserDefaults.standard.removeObject(forKey: "Filterdetail")
        UserDefaults.standard.synchronize()
        
        self.dismiss(animated: true, completion: nil)
    }
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
