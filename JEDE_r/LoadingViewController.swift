//
//  LoadingViewController.swift
//  JEDE_r
//
//  Created by Exarcplus iOS 2 on 29/04/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import LGSideMenuController

class LoadingViewController: UIViewController
{
    var categorystring = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        print("NEW LOG: CALLED HERE!!!")
        let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(0.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute:
            {  
                let valueToSaves = "comingfromgetstart"
                UserDefaults.standard.set(valueToSaves, forKey: "screen")
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let mainview = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
                let valueToSave = "noneedrunlink"
                UserDefaults.standard.set(valueToSave, forKey: "runlink")
//                mainview.getdic = nsmutdic
                let nav = UINavigationController.init(rootViewController: mainview)
                appDelegate.SideMenu = LGSideMenuController.init(rootViewController: nav)
                appDelegate.SideMenu.setLeftViewEnabledWithWidth(280, presentationStyle: .slideAbove, alwaysVisibleOptions:[])
                appDelegate.SideMenuView = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "SidemenuViewController") as! SidemenuViewController
                appDelegate.SideMenu.leftViewStatusBarVisibleOptions = .onAll
                appDelegate.SideMenu.leftViewStatusBarStyle = .lightContent
                //        appDelegate.SideMenuView.selectedpage = 0;
                var rect = appDelegate.SideMenuView.view.frame;
                rect.size.width = 280;
                appDelegate.SideMenuView.view.frame = rect
                appDelegate.SideMenu.leftView().addSubview(appDelegate.SideMenuView.view)
                print(appDelegate.SideMenu)
                self.present(appDelegate.SideMenu, animated:true, completion: nil)
        })
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
