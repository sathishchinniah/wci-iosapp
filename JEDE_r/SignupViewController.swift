//
//  SignupViewController.swift
//  JEDE_r
//
//  Created by Vignesh Waran on 5/15/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import LGSideMenuController
import Firebase

class SignupViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var fullnametext: LRTextField!
    @IBOutlet weak var emailtext: LRTextField!
    @IBOutlet weak var passwordtext: LRTextField!
    @IBOutlet weak var confirmpasswordtext: LRTextField!
    @IBOutlet var passwordviewheightconstraint : NSLayoutConstraint!
    @IBOutlet var passwordviewbottomconstraint : NSLayoutConstraint!
    @IBOutlet var Cpasswordviewheightconstraint : NSLayoutConstraint!
    @IBOutlet var Cpasswordviewbottomconstraint : NSLayoutConstraint!
    @IBOutlet weak var passwordview : UIView!
    @IBOutlet weak var confirmpasswordview : UIView!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var unisexIcon: UIButton!
    @IBOutlet weak var femaleIcon: UIButton!
    
    @IBOutlet weak var maleLabel: UILabel!
    @IBOutlet weak var femaleLabel: UILabel!
    @IBOutlet weak var maleIcon: UIButton!
    
    @IBOutlet weak var unisexLabel: UILabel!
    var myclass:MyClass!
    var passemailstr : String!
    var registerarr = NSMutableArray()
    
    var regdic : NSMutableDictionary!
    var passsocialmediaid : String!
    var passtype : String!
    var passimage : String!
    var fromFacebookSignIn:Bool?
    var isFemaleSelected = Bool()
    var isMaleSelected = Bool()
    var isUnisexSelected = Bool()
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if regdic != nil
        {
            fromFacebookSignIn = true
            print(regdic)
            fullnametext.text = regdic.value(forKey: "Name") as? String
            emailtext.text = regdic.value(forKey: "Email") as? String
            passsocialmediaid = regdic.value(forKey: "Sid") as? String
            passtype = regdic.value(forKey: "Rtype") as? String
            passimage = regdic.value(forKey: "Pimage") as? String
            
            if passtype == "WCi" || passtype == ""
            {
                passwordviewheightconstraint.constant = 60
                passwordviewbottomconstraint.constant = 8
                Cpasswordviewbottomconstraint.constant = 8
                Cpasswordviewheightconstraint.constant = 60
                passwordview.isHidden = false
                confirmpasswordview.isHidden = false
            }
            else
            {
                passwordviewheightconstraint.constant = 0
                passwordviewbottomconstraint.constant = 0
                Cpasswordviewbottomconstraint.constant = 0
                Cpasswordviewheightconstraint.constant = 0
                passwordview.isHidden = true
                confirmpasswordview.isHidden = true
            }
            
        }
        else
        {
            passsocialmediaid = ""
            passtype = "WCi"
            passimage = ""
            passwordviewheightconstraint.constant = 60
            passwordviewbottomconstraint.constant = 8
            Cpasswordviewbottomconstraint.constant = 8
            Cpasswordviewheightconstraint.constant = 60
            passwordview.isHidden = false
            confirmpasswordview.isHidden = false
        }
        
        myclass = MyClass()
        self.navigationController?.navigationBar.isHidden = true
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        
        fullnametext.placeholder = "signup_full_name".localized
        fullnametext.delegate = self
        fullnametext.placeholderActiveColor = UIColor.white
        fullnametext.placeholderInactiveColor = UIColor.init(colorLiteralRed: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0)
        
        emailtext.placeholder = "edit_profile_email_address_hint".localized
        emailtext.delegate = self
        emailtext.placeholderActiveColor = UIColor.white
        emailtext.placeholderInactiveColor = UIColor.init(colorLiteralRed: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0)
        
        passwordtext.placeholder = "Password".localized
        passwordtext.delegate = self
        passwordtext.placeholderActiveColor = UIColor.white
        passwordtext.placeholderInactiveColor = UIColor.init(colorLiteralRed: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0)
        passwordtext.clearsOnBeginEditing = false;
        
        confirmpasswordtext.placeholder = "edit_profile_confirm_password_hint".localized
        confirmpasswordtext.delegate = self
        confirmpasswordtext.placeholderActiveColor = UIColor.white
        confirmpasswordtext.placeholderInactiveColor = UIColor.init(colorLiteralRed: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0)
        confirmpasswordtext.clearsOnBeginEditing = false;
        
        // Do any additional setup after loading the view.
        genderLabel.text = "sign_up_screen_select_gender".localized
        femaleIcon.setTitle("", for: .normal)
        femaleIcon.setBackgroundImage(UIImage.init(named: "Male_unselect.png"), for: .normal)
        femaleIcon.addTarget(self, action: #selector(femaleIconClicked), for: .touchUpInside)
        femaleLabel.text = "sign_up_screen_female".localized
        //
        
        maleIcon.setTitle("", for: .normal)
        maleIcon.setBackgroundImage(UIImage.init(named: "Female_unselect.png"), for: .normal)
        maleIcon.addTarget(self, action: #selector(maleIconClicked), for: .touchUpInside)
        maleLabel.text = "sign_up_screen_male".localized
        //
        
        unisexIcon.setTitle("", for: .normal)
        unisexIcon.setBackgroundImage(UIImage.init(named: "Unisex_unselect.png"), for: .normal)
        unisexIcon.addTarget(self, action: #selector(unisexIconClicked), for: .touchUpInside)
        unisexLabel.text = "sign_up_screen_unisex".localized
        //
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func validateEmail(_ candidate: String) -> Bool
    {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
    }
    
    
    func femaleIconState(toDisable:Bool) {
        if toDisable {
            femaleIcon.setBackgroundImage(UIImage.init(named: "Male_unselect.png"), for: .normal)
            isFemaleSelected = false
        } else {
            femaleIcon.setBackgroundImage(UIImage.init(named: "Male_select.png"), for: .normal)
            isFemaleSelected = true
        }
    }
    
    func maleIconState(toDisable:Bool) {
        if toDisable {
            maleIcon.setBackgroundImage(UIImage.init(named: "Female_unselect.png"), for: .normal)
            isMaleSelected = false
        } else {
            maleIcon.setBackgroundImage(UIImage.init(named: "Female_select.png"), for: .normal)
            isMaleSelected = true
        }
    }
    
    func unisexIconState(toDisable:Bool) {
        if toDisable {
           unisexIcon.setBackgroundImage(UIImage.init(named: "Unisex_unselect.png"), for: .normal)
            isUnisexSelected = false
        } else {
            unisexIcon.setBackgroundImage(UIImage.init(named: "Unisex_select.png"), for: .normal)
            isUnisexSelected = true
        }
    }
    
    func femaleIconClicked() {
        if isFemaleSelected {
            self.femaleIconState(toDisable: true)
        } else {
            self.femaleIconState(toDisable: false)
            
            self.maleIconState(toDisable:true)
            self.unisexIconState(toDisable: true)
        }
    }
    
    func maleIconClicked() {
        if isMaleSelected {
            self.maleIconState(toDisable: true)
        } else {
            self.maleIconState(toDisable: false)
            
            self.femaleIconState(toDisable: true)
            self.unisexIconState(toDisable: true)
        }
    }
    
    func unisexIconClicked() {
        if isUnisexSelected {
            self.unisexIconState(toDisable: true)
        } else {
            self.unisexIconState(toDisable: false)
            self.maleIconState(toDisable:true)
            self.femaleIconState(toDisable: true)
        }
    }
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        if textField == fullnametext
        {
            let nsString1 = fullnametext.text as NSString?
            if  ((fullnametext.text) != nil)
            {
                fullnametext.text = nsString1?.replacingCharacters(in: range, with: string)
            }
            else
            {
                fullnametext.text = string
            }
        }
        else if textField == emailtext
        {
            let nsString1 = emailtext.text as NSString?
            if  ((emailtext.text) != nil)
            {
                emailtext.text = nsString1?.replacingCharacters(in: range, with: string)
            }
            else
            {
                emailtext.text = string
            }
        }
        else if textField == passwordtext
        {
            let nsString = passwordtext.text as NSString?
            if  ((passwordtext.text) != nil)
            {
                passwordtext.text = nsString?.replacingCharacters(in: range, with: string)
            }
            else
            {
                passwordtext.text = string
            }
        }
        else if textField == confirmpasswordtext
        {
            let nsString1 = confirmpasswordtext.text as NSString?
            if  ((confirmpasswordtext.text) != nil)
            {
                confirmpasswordtext.text = nsString1?.replacingCharacters(in: range, with: string)
            }
            else
            {
                confirmpasswordtext.text = string
            }
        }
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.view.endEditing(true)
        return false
    }
    
    @IBAction func registerbuttonclick(sender: UIButton)
    {
        let mobileNumberText = ""
        if(fromFacebookSignIn == true){
            
            
            let urlstring = String(format:"%@/register.php?name=%@&email=%@&password=%@&mobile_num=%@&socialmedia_id=%@&image=%@&login_using=%@",kBaseURL,fullnametext.text!,emailtext.text!,passwordtext.text!,mobileNumberText,passsocialmediaid,passimage,passtype);
            print(urlstring)
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: true, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    let message = (jsonObject.object(at: 0) as AnyObject).value(forKey:"message") as! String
                    if message == "success"
                    {
                        NSLog("jsonObject=%@", jsonObject);
                        self.registerarr = jsonObject.mutableCopy() as! NSMutableArray
                        
                        let data = NSKeyedArchiver.archivedData(withRootObject: self.registerarr.object(at: 0) as! NSDictionary)
                        UserDefaults.standard.set(self.registerarr.object(at: 0), forKey:"Logindetail")
                        UserDefaults.standard.synchronize()
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadlogin"), object: nil, userInfo: nil)
                        
                        let str = (self.registerarr.object(at: 0) as AnyObject).value(forKey:"user_id") as! String
                        let str1 = (self.registerarr.object(at: 0) as AnyObject).value(forKey:"user_name") as! String
                        print(str)
                        UserDefaults.standard.set(str, forKey: "user_id")
                        UserDefaults.standard.set(str1, forKey: "user_id")
                        UserDefaults.standard.synchronize()
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
                        let nav = UINavigationController.init(rootViewController: mainview)
                        appDelegate.SideMenu = LGSideMenuController.init(rootViewController: nav)
                        appDelegate.SideMenu.setLeftViewEnabledWithWidth(280, presentationStyle:.slideAbove, alwaysVisibleOptions:[])
                        appDelegate.SideMenuView = kmainStoryboard.instantiateViewController(withIdentifier: "SidemenuViewController") as! SidemenuViewController
                        appDelegate.SideMenu.leftViewStatusBarVisibleOptions = .onAll
                        appDelegate.SideMenu.leftViewStatusBarStyle = .lightContent
                        var rect = appDelegate.SideMenuView.view.frame;
                        rect.size.width = 280;
                        appDelegate.SideMenuView.view.frame = rect
                        appDelegate.SideMenu.leftView().addSubview(appDelegate.SideMenuView.view)
                        print(appDelegate.SideMenu)
                        self.present(appDelegate.SideMenu, animated:true, completion: nil)
                    }
                    else if message == "email already exist"
                    {
                        let type = (self.registerarr.object(at: 0) as AnyObject).value(forKey: "login_using") as! String
                        self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:String(format: "Your Email ID already Exist with %@",type), withIdentifier:"")
                    }
                    else
                    {
                        self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Please Check Your Username and Password"), withIdentifier:"")
                    }
                }
                
            })
            
        }
        else{
            if passwordtext.text == ""
            {
                let animation = CABasicAnimation(keyPath: "position")
                animation.duration = 0.07
                animation.repeatCount = 4
                animation.autoreverses = true
                animation.fromValue = NSValue(cgPoint: CGPoint(x: passwordtext.center.x - 10,y :passwordtext.center.y))
                animation.toValue = NSValue(cgPoint: CGPoint(x: passwordtext.center.x + 10,y :passwordtext.center.y))
                passwordtext.layer.add(animation, forKey: "position")
            }
            else if confirmpasswordtext.text == ""
            {
                let animation = CABasicAnimation(keyPath: "position")
                animation.duration = 0.07
                animation.repeatCount = 4
                animation.autoreverses = true
                animation.fromValue = NSValue(cgPoint: CGPoint(x: confirmpasswordtext.center.x - 10,y :confirmpasswordtext.center.y))
                animation.toValue = NSValue(cgPoint: CGPoint(x: confirmpasswordtext.center.x + 10,y :confirmpasswordtext.center.y))
                confirmpasswordtext.layer.add(animation, forKey: "position")
            }
            else
            {
                
                if passtype == "WCi"
                {
                    if fullnametext.text == ""
                    {
                        let animation = CABasicAnimation(keyPath: "position")
                        animation.duration = 0.07
                        animation.repeatCount = 4
                        animation.autoreverses = true
                        animation.fromValue = NSValue(cgPoint: CGPoint(x: fullnametext.center.x - 10,y :fullnametext.center.y))
                        animation.toValue = NSValue(cgPoint: CGPoint(x: fullnametext.center.x + 10,y :fullnametext.center.y))
                        fullnametext.layer.add(animation, forKey: "position")
                    }
                    else if emailtext.text == ""
                    {
                        let animation = CABasicAnimation(keyPath: "position")
                        animation.duration = 0.07
                        animation.repeatCount = 4
                        animation.autoreverses = true
                        animation.fromValue = NSValue(cgPoint: CGPoint(x: emailtext.center.x - 10,y :emailtext.center.y))
                        animation.toValue = NSValue(cgPoint: CGPoint(x: emailtext.center.x + 10,y :emailtext.center.y))
                        emailtext.layer.add(animation, forKey: "position")
                    }
                    if (passwordtext.text?.characters.count)! < 6
                    {
                        let actionSheetController: UIAlertController = UIAlertController(title:"Password Alert", message:"Your Password must have atleast 6 characters", preferredStyle: .alert)
                        let cancelAction: UIAlertAction = UIAlertAction(title:"OK", style: .cancel) { action -> Void in
                            //Just dismiss the action sheet
                        }
                        actionSheetController.addAction(cancelAction)
                        self.present(actionSheetController, animated: true, completion: nil)
                    }
                    else if passwordtext.text != confirmpasswordtext.text
                    {
                        let actionSheetController: UIAlertController = UIAlertController(title:"Password Alert", message:"Your Password and Confirm Password does not match", preferredStyle: .alert)
                        let cancelAction: UIAlertAction = UIAlertAction(title:"OK", style: .cancel) { action -> Void in
                            //Just dismiss the action sheet
                        }
                        actionSheetController.addAction(cancelAction)
                        self.present(actionSheetController, animated: true, completion: nil)
                    }
                    else if passwordtext.text == ""
                    {
                        let animation = CABasicAnimation(keyPath: "position")
                        animation.duration = 0.07
                        animation.repeatCount = 4
                        animation.autoreverses = true
                        animation.fromValue = NSValue(cgPoint: CGPoint(x: passwordtext.center.x - 10,y :passwordtext.center.y))
                        animation.toValue = NSValue(cgPoint: CGPoint(x: passwordtext.center.x + 10,y :passwordtext.center.y))
                        passwordtext.layer.add(animation, forKey: "position")
                    }
                    else if confirmpasswordtext.text == ""
                    {
                        let animation = CABasicAnimation(keyPath: "position")
                        animation.duration = 0.07
                        animation.repeatCount = 4
                        animation.autoreverses = true
                        animation.fromValue = NSValue(cgPoint: CGPoint(x: confirmpasswordtext.center.x - 10,y :confirmpasswordtext.center.y))
                        animation.toValue = NSValue(cgPoint: CGPoint(x: confirmpasswordtext.center.x + 10,y :confirmpasswordtext.center.y))
                        confirmpasswordtext.layer.add(animation, forKey: "position")
                    }
                    else
                    {
                        if  emailtext.text != ""
                        {
                            
                            if !validateEmail(emailtext.text!)
                            {
                                let actionSheetController: UIAlertController = UIAlertController(title:"Parking My Car", message:"Enter Valid Email Id", preferredStyle: .alert)
                                let cancelAction: UIAlertAction = UIAlertAction(title:"OK", style: .cancel) { action -> Void in
                                    //Just dismiss the action sheet
                                }
                                actionSheetController.addAction(cancelAction)
                                self.present(actionSheetController, animated: true, completion: nil)
                            }
                            else
                            {
                                self.view.endEditing(true)
                                print(fullnametext.text!)
                                print(emailtext.text!)
                                print(passwordtext.text!)
                                print(passsocialmediaid)
                                print(passimage)
                                print(passtype)
                                //self.registerlink()
                            }
                        }
                        else
                        {
                            self.view.endEditing(true)
                            print(fullnametext.text!)
                            print(emailtext.text!)
                            print(passwordtext.text!)
                            print(passsocialmediaid)
                            print(passimage)
                            print(passtype)
                            //self.registerlink()
                        }
                    }
                }
                else
                {
                    if fullnametext.text == ""
                    {
                        let animation = CABasicAnimation(keyPath: "position")
                        animation.duration = 0.07
                        animation.repeatCount = 4
                        animation.autoreverses = true
                        animation.fromValue = NSValue(cgPoint: CGPoint(x: fullnametext.center.x - 10,y :fullnametext.center.y))
                        animation.toValue = NSValue(cgPoint: CGPoint(x: fullnametext.center.x + 10,y :fullnametext.center.y))
                        fullnametext.layer.add(animation, forKey: "position")
                    }
                    else if emailtext.text == ""
                    {
                        let animation = CABasicAnimation(keyPath: "position")
                        animation.duration = 0.07
                        animation.repeatCount = 4
                        animation.autoreverses = true
                        animation.fromValue = NSValue(cgPoint: CGPoint(x: emailtext.center.x - 10,y :emailtext.center.y))
                        animation.toValue = NSValue(cgPoint: CGPoint(x: emailtext.center.x + 10,y :emailtext.center.y))
                        emailtext.layer.add(animation, forKey: "position")
                    }
                    else if  emailtext.text != ""
                    {
                        if !validateEmail(emailtext.text!)
                        {
                            let actionSheetController: UIAlertController = UIAlertController(title:"Parking My Car", message:"Enter Valid Email Id", preferredStyle: .alert)
                            let cancelAction: UIAlertAction = UIAlertAction(title:"OK", style: .cancel) { action -> Void in
                                //Just dismiss the action sheet
                            }
                            actionSheetController.addAction(cancelAction)
                            self.present(actionSheetController, animated: true, completion: nil)
                        }
                        else
                        {
                            self.view.endEditing(true)
                            print(fullnametext.text!)
                            print(emailtext.text!)
                            print(passwordtext.text!)
                            print(passsocialmediaid)
                            print(passimage)
                            print(passtype)
                            //self.registerlink()
                        }
                    }
                    else
                    {
                        self.view.endEditing(true)
                        print(fullnametext.text!)
                        print(emailtext.text!)
                        print(passwordtext.text!)
                        print(passsocialmediaid)
                        print(passimage)
                        print(passtype)
                        // self.registerlink()
                    }
                }
                
                
                
                
                
                if (passwordtext.text?.characters.count)! < 6
                {
                    self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Your Password must have atleast 6 characters"), withIdentifier:"")
                }
                else if passwordtext.text != confirmpasswordtext.text
                {
                    self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Your Password and Confirm Password does not match"), withIdentifier:"")
                }
                else if emailtext.text != ""
                {
                    if !validateEmail(emailtext.text!)
                    {
                        self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert: "Enter Valid Email Id", withIdentifier:"")
                    }
                    else
                    {
                        self.view.endEditing(true)
                        
                        print(fullnametext.text!)
                        print(emailtext.text!)
                        print(passwordtext.text!)
                        print(passsocialmediaid)
                        print(passimage)
                        print(passtype)
                        
                        let urlstring = String(format:"%@/register.php?name=%@&email=%@&password=%@&mobile_num=%@&socialmedia_id=%@&image=%@&login_using=%@",kBaseURL,fullnametext.text!,emailtext.text!,passwordtext.text!,mobileNumberText,passsocialmediaid,passimage,passtype);
                        print(urlstring)
                        self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: true, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                            if Stats == true
                            {
                                NSLog("jsonObject=%@", jsonObject);
                                let message = (jsonObject.object(at: 0) as AnyObject).value(forKey:"message") as! String
                                let userId = (jsonObject.object(at: 0) as AnyObject).value(forKey:"user_id") as! String
                                let userName = (jsonObject.object(at: 0) as AnyObject).value(forKey:"user_name") as! String
                                FIRAnalytics.logEvent(withName: "Signup", parameters: [
                                    "user_id": userId,
                                    "user_name": userName,
                                    "login_device" : "iOS",
                                    "action_name" : "signup_click"
                                    
                                    ])
                                if message == "success"
                                {
                                    NSLog("jsonObject=%@", jsonObject);
                                    self.registerarr = jsonObject.mutableCopy() as! NSMutableArray
                                    
                                    let data = NSKeyedArchiver.archivedData(withRootObject: self.registerarr.object(at: 0) as! NSDictionary)
                                    UserDefaults.standard.set(self.registerarr.object(at: 0), forKey:"Logindetail")
                                    UserDefaults.standard.synchronize()
                                    
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadlogin"), object: nil, userInfo: nil)
                                    
                                    let str = (self.registerarr.object(at: 0) as AnyObject).value(forKey:"user_id") as! String
                                    let str1 = (self.registerarr.object(at: 0) as AnyObject).value(forKey:"user_name") as! String
                                    print(str)
                                    UserDefaults.standard.set(str, forKey: "user_id")
                                     UserDefaults.standard.set(str1, forKey: "user_name")
                                    UserDefaults.standard.synchronize()
                                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
                                    let nav = UINavigationController.init(rootViewController: mainview)
                                    appDelegate.SideMenu = LGSideMenuController.init(rootViewController: nav)
                                    appDelegate.SideMenu.setLeftViewEnabledWithWidth(280, presentationStyle:.slideAbove, alwaysVisibleOptions:[])
                                    appDelegate.SideMenuView = kmainStoryboard.instantiateViewController(withIdentifier: "SidemenuViewController") as! SidemenuViewController
                                    appDelegate.SideMenu.leftViewStatusBarVisibleOptions = .onAll
                                    appDelegate.SideMenu.leftViewStatusBarStyle = .lightContent
                                    var rect = appDelegate.SideMenuView.view.frame;
                                    rect.size.width = 280;
                                    appDelegate.SideMenuView.view.frame = rect
                                    appDelegate.SideMenu.leftView().addSubview(appDelegate.SideMenuView.view)
                                    print(appDelegate.SideMenu)
                                    self.present(appDelegate.SideMenu, animated:true, completion: nil)
                                }
                                else if message == "email already exist"
                                {
                                    var message = "Your Email ID already Exist"
                                    if self.registerarr.count > 0 {
                                        let type = (self.registerarr.object(at: 0) as AnyObject).value(forKey: "login_using") as! String
                                        message = message + "with \(type)"
                                    }
                                    self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:message, withIdentifier:"")
                                }
                                else
                                {
                                    self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Please Check Your Username and Password"), withIdentifier:"")
                                }
                            }
                            else
                            {
                                
                            }
                        })
                    }
                }
                else
                {
                    
                }
            }
        }
    }
    
    func backbuttonclick(sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func registerlink()
    {
        let devicetoken : String!
        let mobileNumber = ""
        if UserDefaults.standard.string(forKey: "token") == nil
        {
            devicetoken = ""
        }
        else
        {
            devicetoken = UserDefaults.standard.string(forKey: "token") as String?
        }
        let urlstring = String(format:"%@/register.php?name=%@&email=%@&password=%@&mobile_num=%@&socialmedia_id=%@&image=%@&login_using=%@&device_id=%@",kBaseURL,fullnametext.text!,emailtext.text!,passwordtext.text!,mobileNumber,passsocialmediaid,passimage,passtype,devicetoken!);
        print(urlstring)
        self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: true, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
            if Stats == true
            {
                NSLog("jsonObject=%@", jsonObject);
                let message = (jsonObject.object(at: 0) as AnyObject).value(forKey:"message") as! String
                if message == "success"
                {
                    NSLog("jsonObject=%@", jsonObject);
                    self.registerarr = jsonObject.mutableCopy() as! NSMutableArray
                    
                    let data = NSKeyedArchiver.archivedData(withRootObject: self.registerarr.object(at: 0) as! NSDictionary)
                    UserDefaults.standard.set(self.registerarr.object(at: 0), forKey:"Logindetail")
                    UserDefaults.standard.synchronize()
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadlogin"), object: nil, userInfo: nil)
                    
                    let str = (self.registerarr.object(at: 0) as AnyObject).value(forKey:"user_id") as! String
                    print(str)
                    UserDefaults.standard.set(str, forKey: "user_id")
                    UserDefaults.standard.synchronize()
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
                    let nav = UINavigationController.init(rootViewController: mainview)
                    appDelegate.SideMenu = LGSideMenuController.init(rootViewController: nav)
                    appDelegate.SideMenu.setLeftViewEnabledWithWidth(280, presentationStyle:.slideAbove, alwaysVisibleOptions:[])
                    appDelegate.SideMenuView = kmainStoryboard.instantiateViewController(withIdentifier: "SidemenuViewController") as! SidemenuViewController
                    appDelegate.SideMenu.leftViewStatusBarVisibleOptions = .onAll
                    appDelegate.SideMenu.leftViewStatusBarStyle = .lightContent
                    var rect = appDelegate.SideMenuView.view.frame;
                    rect.size.width = 280;
                    appDelegate.SideMenuView.view.frame = rect
                    appDelegate.SideMenu.leftView().addSubview(appDelegate.SideMenuView.view)
                    print(appDelegate.SideMenu)
                    self.present(appDelegate.SideMenu, animated:true, completion: nil)
                }
                else if message == "email already exist"
                {
                    let type = (self.registerarr.object(at: 0) as AnyObject).value(forKey: "login_using") as! String
                    self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:String(format: "Your Email ID already Exist with %@",type), withIdentifier:"")
                }
                else
                {
                    self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Please Check Your Username and Password"), withIdentifier:"")
                }
            }
            else
            {
                
            }
        })
    }
}

