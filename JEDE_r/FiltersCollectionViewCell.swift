//
//  FiltersCollectionViewCell.swift
//  JEDE_r
//
//  Created by Exarcplus on 21/06/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit

class FiltersCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var insideimg : UIImageView!
    @IBOutlet var namelab : UILabel!
    @IBOutlet var clickbutton : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
