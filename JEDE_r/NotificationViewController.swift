//
//  NotificationViewController.swift
//  JEDE_r
//
//  Created by Exarcplus on 28/05/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import SDWebImage
import AFNetworking

class NotificationViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    var myclass : MyClass!
    var bgload:Bool! = true
    @IBOutlet weak var tablelist : UITableView!
    var datasarr =  NSMutableArray()
    var refreshControl: UIRefreshControl!
    var Userdic = NSMutableDictionary()
    var passuserid : String!
    var datearr = NSMutableArray()
    var detailsarr = NSMutableArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myclass = MyClass()
        self.navigationController?.navigationBar.isHidden = false
        self.tablelist.reloadData()
        //self.notificationlink()
        
        if UserDefaults.standard.object(forKey: "Logindetail") == nil
        {
            passuserid = ""
        }
        else
        {
            let placesData = UserDefaults.standard.object(forKey: "Logindetail") as? NSDictionary
            self.Userdic.removeAllObjects()
            self.Userdic.addEntries(from: (placesData as? [String : Any])!)
            print(self.Userdic)
            passuserid = self.Userdic.value(forKey: "user_id") as? String
            print(passuserid)
        }
        self.tablelist.tableFooterView = UIView()
        self.navigationController?.navigationBar.isHidden = false
        
        self.loadData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        if(UserDefaultManager.sharedManager.getLanguageName() == "English"){
            self.title = "NOTIFICATION"
        }
        else if(UserDefaultManager.sharedManager.getLanguageName() == "German"){
            self.title = "BENACHRICHTIGUNG"
        }
        else if(UserDefaultManager.sharedManager.getLanguageName() == "French"){
            self.title = "NOTIFICATION"
        }
        self.navigationController?.navigationBar.isHidden = false
    }

    
    @IBAction func OpenSideMenu(_ x:AnyObject)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.SideMenu.showLeftView(animated: true, completionHandler: nil)
          NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadsidemenu"), object: nil, userInfo: nil)
    }
    
    func loadData()
    {
        Loader.addLoaderTo(self.tablelist)
        if AFNetworkReachabilityManager.shared().isReachable
        {
            let urlstring = String(format:"%@/user_notification_list.php?user_id=%@",kBaseURL,passuserid)
            print(urlstring)
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: false, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    if jsonObject.count != 0
                    {
                        self.datasarr .removeAllObjects()
                        self.datasarr.addObjects(from: jsonObject as [AnyObject])
                        print(self.datasarr)
                      
                        //self.tablelist.estimatedRowHeight = 143;
                        
                        //                        let ss = self.datasarr.value(forKey: "date") as! NSArray
                        //                        self.datearr.addObjects(from: ss.mutableCopy() as! [AnyObject])
                        //                        print(self.datearr)
                        
//                        for c in 0 ..< self.datasarr.count
//                        {
//                            let dats = (self.datasarr.object(at: c) as AnyObject).value(forKey: "date_time") as! String
//                            print(dats)
//                            
//                            let dateFormatterGet = DateFormatter()
//                            dateFormatterGet.dateFormat = "yyyy-MM-dd,hh:mm:ss"
////                            dateFormatterGet.dateFormat = "yyyy-MM-dd"
//                            
//                            let dateFormatter = DateFormatter()
//                            dateFormatter.dateFormat = "dd-MMM-yyyy hh:mm:ss"
//                            
//                            let date: Date? = dateFormatterGet.date(from: dats)
//                            print(dateFormatter.string(from: date!))
//                            let dddi = String(format: "%@", dateFormatter.string(from: date!))
//                            self.datearr.add(dddi)
//                            print(self.datearr)
//                        }
//                        print(self.datearr)
                        
                        
                        //                        for i in 0 ..< self.datasarr.count
                        //                        {
                        //                            let datearrs = (self.datasarr.object(at: i) as AnyObject).value(forKey: "details") as! NSArray
                        //                            print(datearrs)
                        //                            self.detailsarr.addObjects(from: datearrs.mutableCopy() as! [AnyObject])
                        //                            print(self.detailsarr)
                        //                        }
                        
//                        for i in 0 ..< self.datasarr.count
//                        {
//                            let datearrs = (self.datasarr.object(at: i) as AnyObject).value(forKey: "details")
//                            print(datearrs)
//                            self.detailsarr.add(datearrs)
//                            print(self.detailsarr)
//                        }
//                        
//                        print(self.datearr)
//                        print(self.detailsarr)
                        
                        //                        let dd = self.datasarr.value(forKey: "details") as! NSArray
                        //                        self.detailsarr.addObjects(from: dd.mutableCopy() as! [AnyObject])
                        //                        print(self.detailsarr)
                        
                        //                        let dates = self.datasarr.value(forKey: "date") as! NSArray
                        //                        print(dates)
                        //
                        //                        let deeeta = self.datasarr.value(forKey: "details") as! NSArray
                        //                        print(deeeta)
                        //
                        //                        let mdic = NSMutableDictionary()
                        //                        let daar = NSMutableArray()
                        //                        for c in 0 ..< dates.count
                        //                        {
                        //                            let datearrs = dates.object(at: c)
                        //                            print(datearrs)
                        //                            daar.add(datearrs)
                        //                            print(daar)
                        //                        }
                        //                        mdic.setValue(daar, forKey: "Heading")
                        //                        self.datearr.add(mdic)
                        //                        print(self.datearr)
                        //
                        //                        let sdic = NSMutableDictionary()
                        //                        let deesr = NSMutableArray()
                        //                        for i in 0 ..< deeeta.count
                        //                        {
                        //                            let datearrs = deeeta.object(at: i)
                        //                            print(datearrs)
                        //                            deesr.add(datearrs)
                        //                            print(deesr)
                        //                        }
                        //                        sdic.setValue(deesr, forKey: "List")
                        //                        self.detailsarr.add(sdic)
                        //                        print(self.detailsarr)
                        //                        
                        //                        
                        //                        print(self.datearr)
                        //                        print(self.detailsarr)
                        
                        self.tablelist.reloadData()
//                        Loader.removeLoaderFrom(self.tablelist)
                    }
                }
                else
                {
                    NSLog("jsonObject=error");
                }
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
    
//    func numberOfSections(in tableView: UITableView) -> Int
//    {
//        print(self.datearr)
//        print(self.datearr.count)
//        return self.datearr.count
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return datasarr.count
    }
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
//    {
//        
//        //        print(self.datearr.count)
//        let sponser = Bundle.main.loadNibNamed("NotificationTableViewCell", owner: self, options: nil)?[0] as! NotificationTableViewCell
//        sponser.dateandtimelab.text = self.datearr.object(at: section) as? String
//        //sponser.dateandtimelab.text = self.datearr.object(at: section) as? String
//        
////        sponser.borderview.layer.shadowColor = UIColor.black.cgColor
////        sponser.borderview.layer.shadowOpacity = 0.4
////        sponser.borderview.layer.shadowOffset = CGSize.zero
////        sponser.borderview.layer.shadowRadius = 2
//        return sponser
//    }
//    
    func tableView(_  tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100
    }
//
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
//    {
//        return 40.0
//    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell", for: indexPath) as! NotificationTableViewCell
        cell.selectionStyle = .none
        
         //self.datasarr.object(at: indexPath.section) as! NSArray;
        
        //        for i in 0 ..< dic.count
        //        {
        let imageurl = (datasarr.object(at: indexPath.row) as AnyObject).value(forKey: "image") as? String
        print(imageurl!)
        if imageurl?.characters.count == 0 || imageurl == ""
        {
            cell.imageview.image = UIImage.init(named: "default_image.png")
        }
        else
        {
            let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                print(self)
            }
            cell.imageview.sd_setImage(with: NSURL(string:imageurl!) as URL!, completed: block)
            cell.imageview.contentMode = UIViewContentMode.scaleToFill;
        }
        
        let namestr = (datasarr.object(at: indexPath.row) as AnyObject).value(forKey: "name") as? String
        if namestr == nil || namestr == ""
        {
            cell.namelab.text = ""
        }
        else
        {
            cell.namelab.text = namestr
        }
        
//        let price = (dic.object(at: indexPath.row) as AnyObject).value(forKey: "price") as? String
//        if price != ""
//        {
//            cell?.pricelab.text = String(format: "CHF %@",price!)
//        }
//        else
//        {
//            cell?.pricelab.text = "FREE"
//        }
        
        
        let time = (datasarr.object(at: indexPath.row) as AnyObject).value(forKey: "date_time") as? String
        if time != ""
        {
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd,HH:mm:ss"
            //                            dateFormatterGet.dateFormat = "yyyy-MM-dd"
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MMM-yyyy hh:mm a"
            
            let date: Date? = dateFormatterGet.date(from: time!)
            print(dateFormatter.string(from: date!))
            cell.dateandtimelab.text = String(format: "%@", dateFormatter.string(from: date!))
        }
        else
        {
            cell.dateandtimelab.text = ""
        }
        
        
//        let type = (dic.object(at: indexPath.row) as AnyObject).value(forKey: "category_name") as? String
//        print(type)
//        if type == ""
//        {
//            cell?.categoryimg.image = UIImage.init(named: "Public")
//        }
//        else if type == "Gas Station"
//        {
//            cell?.categoryimg.image = UIImage.init(named: "Gas")
//        }
//        else if type == "Hotel"
//        {
//            cell?.categoryimg.image = UIImage.init(named: "Hotels")
//        }
//        else if type == "Restaurant"
//        {
//            cell?.categoryimg.image = UIImage.init(named: "Restaurant")
//        }
//        else if type == "Shopping Mall"
//        {
//            cell?.categoryimg.image = UIImage.init(named: "shoping_marker")
//        }
//        else if type == "Bar"
//        {
//            cell?.categoryimg.image = UIImage.init(named: "bar_marker")
//        }
//        
//        
//        let stas = (dic.object(at: indexPath.row) as AnyObject).value(forKey: "status") as? String
//        if stas != ""
//        {
//            cell?.statuslab.text = stas
//        }
//        else
//        {
//            cell?.statuslab.text = ""
//        }
        
        
        let addre = (datasarr.object(at: indexPath.row) as AnyObject).value(forKey: "msg") as? String
        if addre == nil || addre == ""
        {
            cell.messagelab.text = ""
        }
        else
        {
            cell.messagelab.text = addre
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
       // datasarr.object(at: indexPath.section) as! NSArray;
        //print(dic)
//        passloc = String(format: "%f,%f", movelocation.coordinate.latitude,movelocation.coordinate.longitude)
//        print(passloc)
//        //self.canclelink()
//        let next = self.storyboard?.instantiateViewController(withIdentifier: "MovetoNavigateViewController") as! MovetoNavigateViewController
//        next.selectdic = dic.object(at: indexPath.row) as! NSMutableDictionary
//        next.bookingidstr = (dic.object(at: indexPath.row) as AnyObject).value(forKey: "booking_id") as? String
//        //next.passcurrentloc = passloc
//        self.navigationController?.pushViewController(next, animated: true)
        
        //mainview.looid = self.passlooid
        //        self.navigationController?.pushViewController(mainview, animated: true)
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
            
            self.datasarr.remove(at: indexPath.row)
            self.tablelist.reloadData()
        
        
    }
  
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
