//
//  WebForTermsViewController.swift
//  JEDE_r
//
//  Created by apple on 27/05/18.
//  Copyright © 2018 Exarcplus. All rights reserved.
//

import UIKit

class WebForTermsViewController: UIViewController,UIWebViewDelegate{

    @IBOutlet weak var TermsAndConditions: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        //TermsAndConditions = UIWebView(frame: CGRectMake(0, y, screenSize.width, screenSize.height-y))
        let url : NSURL! = NSURL(string: "http://wc-i.net/terms-and-conditions/")
        TermsAndConditions?.loadRequest(URLRequest(url: url as URL))
        TermsAndConditions?.isOpaque = false;
        TermsAndConditions?.backgroundColor = UIColor.clear
        TermsAndConditions?.scalesPageToFit = true;
        TermsAndConditions?.delegate = self// Add this line to set the delegate of webView
        self.view.addSubview(TermsAndConditions!)
        func webViewDidFinishLoad(webView : UIWebView) {
            //Page is loaded do what you want
        }

        // Do any additional setup after loading the view.
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
