//
//  ReviewsViewController.swift
//  JEDE_r
//
//  Created by Exarcplus on 27/06/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import AFNetworking
import SDWebImage

class ReviewsViewController: UIViewController, iCarouselDataSource, iCarouselDelegate {
    
    var myclass : MyClass!
    @IBOutlet weak var carousel: iCarousel!
    @IBOutlet weak var noreviews: UILabel!
    var reviewsarr = NSMutableArray()
    var looid : String!

    override func viewDidLoad() {
        super.viewDidLoad()
        myclass = MyClass()
        self.navigationController?.navigationBar.isHidden = false
        carousel.dataSource = self
        carousel.delegate = self
        carousel.type = .coverFlow2
        carousel.isPagingEnabled = true
        noreviews.isHidden = true
        self.loadData()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated:true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadData()
    {
        if AFNetworkReachabilityManager.shared().isReachable
        {
            let urlstring = String(format:"%@/reviews.php?loo_id=%@",kBaseURL,looid)
            print(urlstring)
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: true, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    if jsonObject.count != 0
                    {
                        self.reviewsarr .removeAllObjects()
                        self.reviewsarr.addObjects(from: jsonObject as [AnyObject])
                        print(self.reviewsarr)
                        if self.reviewsarr.count != 0
                        {
                            self.carousel.reloadData();
                            self.noreviews.isHidden = true
                        }
                        else
                        {
                            self.noreviews.isHidden = false
                        }
                    }
                }
                else
                {
                    NSLog("jsonObject=error");
                    self.noreviews.isHidden = false
                }
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
    
    
     //finalpagescroll
     func numberOfItems(in carousel: iCarousel) -> Int {
        return self.reviewsarr.count
     }
     
     func carouselDidEndScrollingAnimation(_ carousel: iCarousel)
     {
    
     }
     
     func carouselDidScroll(_ carousel: iCarousel)
     {
    
     }     
     
     func carousel(_ carousel: iCarousel, didSelectItemAt index: Int)
     {

     }
     
     func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView
     {
        let dic = self.reviewsarr.object(at: Int(index)) as! NSDictionary;
        print(dic)
        
        let sponser = Bundle.main.loadNibNamed("ReviewsListView", owner: self, options: nil)?[0] as! ReviewsListView
        sponser.frame = CGRect(x: 0, y: 0, width: 264, height: 264)
        
        sponser.layer.shadowColor = UIColor.black.cgColor
        sponser.layer.shadowOpacity = 0.8
        sponser.layer.shadowOffset = CGSize.zero
        sponser.layer.shadowRadius = 2
        
        //Reset the data
        sponser.namelab.text = ""
        sponser.reviewstext.text = ""
        sponser.reviewstext.isEditable = false
        
        let imageurl = dic.value(forKey: "user_image") as? String;
        if imageurl != nil
        {
            if imageurl?.characters.count == 0 || imageurl == "" || imageurl == "http://stitchplus.com/loocaterpanel/"
            {
                sponser.imgview.image = UIImage.init(named: "ios_default_image.png")
            }
            else
            {
                let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                    print(self)
                }
                sponser.imgview.sd_setImage(with: NSURL(string:imageurl!) as! URL, completed: block)
                sponser.imgview.contentMode = UIViewContentMode.scaleToFill;
            }
        }
        
        sponser.imgbgview.contentMode = UIViewContentMode.scaleToFill;
        sponser.imgbgview.layer.cornerRadius = sponser.imgbgview.frame.size.width / 2
        sponser.imgbgview.clipsToBounds = true
        sponser.imgview.contentMode = UIViewContentMode.scaleToFill;
        sponser.imgview.layer.cornerRadius = sponser.imgview.frame.size.width / 2
        sponser.imgview.clipsToBounds = true
        sponser.imgview.layer.borderWidth = 1.0
        sponser.imgview.layer.borderColor = UIColor.clear.cgColor
        
        //sponser.namelab.text = dic.value(forKey: "user_name") as? String;
        var reviewname = dic.value(forKey: "user_name") as? String
        if reviewname == nil {
            sponser.namelab.text = ""
        }
        else {
            var components = reviewname?.components(separatedBy: " ")
            
            if(components!.count > 0)
            {
                let firstname = components!.removeFirst()
                let lastname = components?.joined(separator: " ")
                debugPrint(firstname ?? " ")
                debugPrint(lastname ?? " ")
                sponser.namelab.text = firstname as? String
            }
            //sponser.reviewstext.text = dic.value(forKey: "review") as? String;
            let reviewText = dic.value(forKey: "review") as? String
            if reviewText == nil
            {
                sponser.reviewstext.text = ""
            }
            else{
                sponser.reviewstext.text = reviewText
            }
            sponser.reviewstext.isEditable = false
            
        }
        let rating = dic.value(forKey: "rating") as! NSNumber
        print(rating)
        print(CGFloat(rating.doubleValue))
        //                        let myInteger = Int(String(format: "%@", rating))
        //                        let myNumber = NSNumber(value:myInteger!)
        //                        print(myNumber)
        if rating.intValue != 0
        {
            //                    cell?.Pricebg.backgroundColor = myclass.colorWithHexString(hex: "#01a550",Alpha: 0.7)
            //                            self.ratingview.stepInterval = CGFloat(rating.intValue)
            //                            self.ratingview.isHighlighted = true
            //                            self.ratingview.stepInterval = 0.0
            sponser.ratingview.value = CGFloat(rating.doubleValue)
        }
        else
        {
            //                    cell?.Pricebg.backgroundColor = myclass.colorWithHexString(hex: "#ec0d1a",Alpha: 0.7)
            //                            self.ratingview.stepInterval = 0.0
            //                            self.ratingview.isHighlighted = false
            sponser.ratingview.value = 0
        }
        return sponser;
    }
    
     func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat
     {
        if (option == .spacing)
        {
            return value * 2.2
        }
        return value
     }
    
    @IBAction func backbutton(_sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
