//
//  NewRatingViewController.swift
//  JEDE_r
//
//  Created by Exarcplus on 19/08/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import AFNetworking
import SDWebImage
import Firebase


class NewRatingViewController: UIViewController, TagListViewDelegate, NHRangeSliderViewDelegate, RangeSeekSliderDelegate {
    
    var myclass : MyClass!
    
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var male : UIButton!
    @IBOutlet weak var female : UIButton!
    @IBOutlet weak var unisex : UIButton!
    @IBOutlet weak var wheelchair : UIButton!
    @IBOutlet weak var star1 : UIButton!
    @IBOutlet weak var star2 : UIButton!
    @IBOutlet weak var star3 : UIButton!
    @IBOutlet weak var star4 : UIButton!
    @IBOutlet weak var star5 : UIButton!
    @IBOutlet weak var down : UIButton!
    @IBOutlet weak var addBtn : SparkButton!
    @IBOutlet weak var looname: UILabel!
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var ratingView: UITextView!
    @IBOutlet weak var newratingopupview : NewRatingPopUp!
    
    @IBOutlet weak var seatCoverView: EZRatingView!
    @IBOutlet weak var sanitaryBagView: UIView!
    @IBOutlet weak var mirrorView: UIView!
    @IBOutlet weak var handDryerView: UIView!
    @IBOutlet weak var toiletPaperView: UIView!
    @IBOutlet weak var soapView: EZRatingView!
    
    @IBOutlet weak var rateButton: UIButton!
    @IBOutlet weak var nothanksButton: UIButton!
    @IBOutlet weak var cleanratingview: EZRatingView!
    @IBOutlet weak var accessratingview: EZRatingView!
    @IBOutlet weak var waitingtimeratingview: EZRatingView!
    @IBOutlet weak var commentratingview: EZRatingView!
    @IBOutlet var secondDownButton: UIButton!
    @IBOutlet var firstDownButton: UIButton!
    @IBOutlet var secondViewHeight: NSLayoutConstraint!
    @IBOutlet var firstViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var soapYes: UIButton!
    
    @IBOutlet weak var soapNo: UIButton!
    @IBOutlet weak var handDryerYes: UIButton!
    
    @IBOutlet weak var handDryerNo: UIButton!
    @IBOutlet weak var mirrorYes: UIButton!
    
    @IBOutlet weak var mirrorNo: UIButton!
    @IBOutlet weak var sanitaryBageYes: UIButton!
    
    @IBOutlet weak var sanitaryBagsNo: UIButton!
    
    @IBOutlet weak var seatCoverYes: UIButton!
    @IBOutlet weak var seatCoverNo: UIButton!
    
    @IBOutlet weak var toiletPaperYes: UIButton!
    
    @IBOutlet weak var toiletPaperNo: UIButton!
    
    @IBOutlet weak var OverAllRating: UILabel!
    @IBOutlet weak var FirstRate: UILabel!
    @IBOutlet weak var SecondRate: UILabel!
    @IBOutlet weak var ThirdRate: UILabel!
    @IBOutlet weak var FourthRate: UILabel!
    @IBOutlet weak var FirstLabel: UILabel!
    @IBOutlet weak var SecondLabel: UILabel!
    @IBOutlet weak var waitingLabel: UILabel!
    @IBOutlet weak var ComfortLAbel: UILabel!
    @IBOutlet weak var AcessLabel: UILabel!
    @IBOutlet weak var ClienlessLabel: UILabel!
    @IBOutlet weak var SoapLabel: UILabel!
    @IBOutlet weak var MirrorLabel: UILabel!
    @IBOutlet weak var SeatLabel: UILabel!
    @IBOutlet weak var SanitaryLabel: UILabel!
    @IBOutlet weak var ToiletLabel: UILabel!
    @IBOutlet weak var HandDryerLabel: UILabel!
    @IBOutlet weak var EquipmentLabel: UILabel!
    @IBOutlet weak var WriteAReviewLabel: UILabel!
    @IBOutlet weak var FifthRate: UILabel!
    
    var soapSelected:Bool?
    
    var checkstr : String!
    var star1str : String!
    var star2str : String!
    var star3str : String!
    var star4str : String!
    var star5str : String!
    var malestr : String!
    var femalestr : String!
    var unisexstr : String!
    var wheelchairstr : String!
    var passcleanliness : String = ""
    var passwaitingtime : String!
    var passcomment : String!
    var passaccess : String!
    var passoverall : String!
    var isLiked : Bool!
    var Userdic = NSMutableDictionary()
    var passuserid : String!
    var passlooid : String!
    var bookingidstr : String!
    var getdic : NSMutableDictionary!
    var downstr : String!
    var passSoap : String!
    var passHandDryer : String!
    var passMirror : String!
    var passSanitary : String!
    var passSeatCover : String!
    var passToiletPaper : String!
    var passGender : String!
    
    //CReated booleans to store the state of button image
    var maleState: Bool = false
    var femaleState: Bool = false
    var unisexState: Bool = false
    var wheelChairState: Bool = false
    
    var restrictMaleSelect:Bool = true
    var restrictFemaleSelect:Bool = true
    var restrictUnisexSelect:Bool = true
    var restrictWheelChairSelect:Bool = true
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        accessratingview.isUserInteractionEnabled = true
        myclass = MyClass()
        let view = UIView(frame:  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 20))
        view.backgroundColor = UIColor.init(red: 27.0/255.0, green: 34.0/255.0, blue: 40.0/255.0, alpha: 1.0)
        self.navigationController?.view.addSubview(view)
        
        //let borderColor = UIColor.whiteColor.Cgcolor()
        let borderColor = UIColor.black.cgColor
        
        ratingView.layer.borderColor = borderColor.copy(alpha: 0.1)
        ratingView.layer.borderWidth = 1.0;
        ratingView.layer.cornerRadius = 5.0;
        
        male.addTarget(self, action: #selector(selectedFacilityIconMale), for: .touchUpInside)
        female.addTarget(self, action: #selector(selectedFacilityIconFemale), for: .touchUpInside)
        unisex.addTarget(self, action: #selector(selectedFacilityIconUnisex), for: .touchUpInside)
        wheelchair.addTarget(self, action: #selector(selectedFacilityIconWheelChair), for: .touchUpInside)
        
        
        if bookingidstr != nil
        {
            print(bookingidstr)
        }
        else
        {
            bookingidstr = ""
        }
        
        
        
        if getdic != nil
        {
            var facilitiesstr = getdic.value(forKey: "facility_category") as? String
            if facilitiesstr == nil {
                facilitiesstr = self.getFacilityList(dict: getdic)
            }
            var accessbilitystr = getdic.value(forKey: "accessbility") as? String
            if ((facilitiesstr?.contains("1"))!) {
                self.male.setImage(UIImage.init(named: "unselectmale.png") as? UIImage, for: UIControlState.normal)
                maleState = true
                restrictMaleSelect = false
            }
            if ((facilitiesstr?.contains("2"))! ) {
                self.female.setImage(UIImage.init(named: "unselectfemale.png") as? UIImage, for: UIControlState.normal)
                femaleState = true
                restrictFemaleSelect = false
            }
            if ((facilitiesstr?.contains("3"))!) {
                self.unisex.setImage(UIImage.init(named: "unselectunisex.png") as? UIImage, for: UIControlState.normal)
                unisexState = true
                restrictUnisexSelect = false
            }
            if ((facilitiesstr?.contains("4"))!) {
                self.wheelchair.setImage(UIImage.init(named: "unselectwheelchair.png") as? UIImage, for: UIControlState.normal)
                wheelChairState = true
                restrictWheelChairSelect = false
            }
            
            if (accessbilitystr != nil) {
                if (accessbilitystr?.contains("1"))! {
                    self.wheelchair.setImage(UIImage.init(named: "unselectwheelchair.png") as? UIImage, for: UIControlState.normal)
                    wheelChairState = true
                    restrictWheelChairSelect = false
                }
            }
            
            print(getdic)
            //loonamelab.text = getdic.value(forKey: "loo_name") as? String
            let typestr = getdic.value(forKey: "type") as! String
            if typestr == "Public"
            {
                self.nothanksButton.isHidden = false
                self.rateButton.isHidden = false
            }
            else
            {
                self.rateButton.isHidden = false
                self.nothanksButton.isHidden = false
            }
            passlooid =  getdic.value(forKey: "loo_id") as? String
            looname.text = getdic.value(forKey: "loo_name") as? String
            let imageurl = getdic.value(forKey: "loo_image") as? String
            let imagearr = imageurl?.components(separatedBy: ",") as! NSArray
            //print(imageurl!)
            if imageurl == "" || imageurl?.characters.count == 0 || imageurl == nil
            {
                if let categoryType = getdic.value(forKey: "category_name") as? String {
                    let categoryImage = self.getDefaultImageForCategory(categoryName: categoryType)
                    bannerImage.image = UIImage.init(named: categoryImage)
                } else {
                    bannerImage.image = UIImage.init(named: "default_image.png")
                }
            }
            else
            {
                //                let url = URL(string: imageurl!)
                //                let data = try? Data(contentsOf: url!)
                let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                    print(self)
                }
                bannerImage.sd_setImage(with: NSURL(string:(imagearr.object(at: 0) as? String)!) as URL!, completed: block)
                bannerImage.contentMode = UIViewContentMode.scaleToFill;
                
            }
            
            self.isLiked = false
            self.addBtn.setImage(UIImage(named: "Fav.png"), for: UIControlState())
            //self.addBtn.unLikeBounce(0.4)
            
        }
        else
        {
            self.isLiked = false
            self.addBtn.setImage(UIImage(named: "Fav.png"), for: UIControlState())
            //self.addBtn.unLikeBounce(0.4)
            passlooid = ""
            //  scrollview.addTwitterCover(with: UIImage(named: "Toilets-Defualt.png"))
            bannerImage.image = UIImage.init(named: "default_image.png")
            
        }
        
        if UserDefaults.standard.object(forKey: "Logindetail") == nil
        {
            passuserid = ""
        }
        else
        {
            
        }
        
        
        firstViewHeight.constant = 0
        secondViewHeight.constant = 0
        firstView.isHidden = true
        secondView.isHidden = true
        checkstr = "close"
        
        star1.setImage(UIImage.init(named: "starunselect1.png"), for: .normal)
        star2.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
        star3.setImage(UIImage.init(named: "starunselect3.png"), for: .normal)
        star4.setImage(UIImage.init(named: "starunselect4.png"), for: .normal)
        star5.setImage(UIImage.init(named: "starunselect5.png"), for: .normal)
        
        self.cleanratingview?.value = 0
        //self.cleanratingview?.stepInterval = 1.0
        self.cleanratingview?.isUserInteractionEnabled = true
        self.cleanratingview?.isSelected = true
        
 
        self.waitingtimeratingview?.value = 0
        //self.waitingtimeratingview?.stepInterval = 1.0
        self.waitingtimeratingview?.isUserInteractionEnabled = true
        self.waitingtimeratingview?.isSelected = true
        
        self.commentratingview?.value = 0
        //self.commentratingview?.stepInterval = 1.0
        self.commentratingview?.isUserInteractionEnabled = true
        self.commentratingview?.isSelected = true
        
        passcleanliness = ""
        passaccess = ""
        passwaitingtime = ""
        passcomment = ""
        
        star1str = "0"
        star2str = "0"
        star3str = "0"
        star4str = "0"
        star5str = "0"
        passSoap = "null"
        passMirror = "null"
        passSanitary = "null"
        passToiletPaper = "null"
        passSeatCover = "null"
        passHandDryer = "null"
        
        downstr = "0"
        
        // Do any additional setup after loading the view.
        self.OverAllRating.text = "rating_layout_overall_rating".localized
        self.FirstRate.text = "rating_layout_awful".localized
        self.SecondRate.text = "rating_layout_poor".localized
        self.ThirdRate.text = "rating_layout_average".localized
        self.FourthRate.text = "rating_layout_good".localized
        self.FifthRate.text = "rating_layout_excellent".localized
        self.FirstLabel.text = "would_you_like_to_add_something".localized
        self.ClienlessLabel.text = "filter_pop_layout_cleanliness".localized
        self.ComfortLAbel.text = "rating_layout_comfort_and_equipment".localized
        self.waitingLabel.text = "filter_pop_layout_waiting_time".localized
        self.AcessLabel.text = "filter_pop_layout_access".localized
        self.SecondLabel.text = "are_you_missing_some_important_infrastructure".localized
        self.SoapLabel.text = "soap".localized
        self.HandDryerLabel.text = "hand_dryer".localized
        self.MirrorLabel.text = "mirror".localized
        self.SeatLabel.text = "seat_cover".localized
        self.ToiletLabel.text = "toilet_paper".localized
        self.SanitaryLabel.text = "sanitary_bags".localized
        self.WriteAReviewLabel.text = "rating_layout_write_a_review".localized
        self.EquipmentLabel.text = "equipments".localized
        self.rateButton.setTitle("rating_layout_rate".localized, for: .normal)
        self.nothanksButton.setTitle("rating_layout_no_thanks".localized, for: .normal)
        

         let looid = getdic.value(forKey: "loo_id") as? String ?? "NA"
        let userID = UserDefaults.standard.string(forKey: "user_id") ?? "NA"
         let looName = getdic.value(forKey: "loo_name") as? String ?? "NA"
         let looAddress = getdic.value(forKey: "loo_address") as? String ?? "NA"
         let looImage = getdic.value(forKey: "loo_image") as? String ?? "NA"
        
        FIRAnalytics.logEvent(withName: "rating_opened", parameters: [
            "loo_id": looid,
            "user_id" : userID,
            "loo_name" : looName,
            "loo_address" : looAddress,
            "login_device" : "iOS",
            "loo_image_url" : looImage
            ])
    }
    
    @IBAction func firstDown(_ sender: UIButton) {
        if(firstDownButton.currentImage == UIImage.init(named: "up-arrow")){
            UIView.animate(withDuration: 1.0) {
                self.firstView.isHidden = true
                self.firstViewHeight.constant = 0
            }
            firstDownButton.setImage(UIImage.init(named: "down-arrow"), for: UIControlState.normal)
        }
        else{
            
            UIView.animate(withDuration: 1.0) {
                print("LOG: ANIMATE METHOD CALLED")
                self.firstView.isHidden = false
                self.firstViewHeight.constant = 120
            }
            
            firstDownButton.setImage(UIImage.init(named: "up-arrow"), for: UIControlState.normal)
        }
    }
    @IBAction func secondDown(_ sender: Any) {
        if(secondDownButton.currentImage == UIImage.init(named: "up-arrow")){
            secondView.isHidden = true
            secondViewHeight.constant = 0
            secondDownButton.setImage(UIImage.init(named: "down-arrow"), for: UIControlState.normal)
        }
        else{
            secondView.isHidden = false
            secondViewHeight.constant = 173
            secondDownButton.setImage(UIImage.init(named: "up-arrow"), for: UIControlState.normal)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func getFacilityList(dict:NSMutableDictionary) -> String {
        var facilityString = ""
        if let facilityDict = dict.value(forKey: "facilities") as? NSArray {
            for case let facilityItem as NSDictionary in facilityDict {
                let facilityCategory = facilityItem.value(forKey: "facility_category_id") as? String
                facilityString.append(facilityCategory!)
            }
        }
        return facilityString

    }
    
 
    
    
    @IBAction func star1button(_sender:UIButton)
    {
        if star1str == "0"
        {
            star1.setImage(UIImage.init(named: "starselect1.png"), for: .normal)
            star2.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
            star3.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
            star4.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
            star5.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
            star1str = "1"
            self.cleanratingview?.value = 1.0
            self.accessratingview?.value = 1.0
            self.waitingtimeratingview?.value = 1.0
            self.commentratingview?.value = 1.0
            print("Selected rating: \(UInt((cleanratingview?.value)!))")
            passcleanliness = String(format: "%d",(UInt((cleanratingview?.value)!)))
            print(passcleanliness)
            print("Selected rating: \(UInt((accessratingview?.value)!))")
            passaccess = String(format: "%d",(UInt((accessratingview?.value)!)))
            print(passaccess)
            print("Selected rating: \(UInt((waitingtimeratingview?.value)!))")
            passwaitingtime = String(format: "%d",(UInt((waitingtimeratingview?.value)!)))
            print(passwaitingtime)
            print("Selected rating: \(UInt((commentratingview?.value)!))")
            passcomment = String(format: "%d",(UInt((commentratingview?.value)!)))
            print(passcomment)
            
        }
        else
        {
            star1.setImage(UIImage.init(named: "starunselect1.png"), for: .normal)
            star2.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
            star3.setImage(UIImage.init(named: "starunselect3.png"), for: .normal)
            star4.setImage(UIImage.init(named: "starunselect4.png"), for: .normal)
            star5.setImage(UIImage.init(named: "starunselect5.png"), for: .normal)
            star1str = "0"
            
        }
        self.avglink()
    }
    
    @IBAction func star2button(_sender:UIButton)
    {
        
        if star2str == "0"
        {
            star1.setImage(UIImage.init(named: "starselect1.png"), for: .normal)
            star2.setImage(UIImage.init(named: "starselect2.png"), for: .normal)
            star3.setImage(UIImage.init(named: "starunselect3.png"), for: .normal)
            star4.setImage(UIImage.init(named: "starunselect4.png"), for: .normal)
            star5.setImage(UIImage.init(named: "starunselect5.png"), for: .normal)
            star2str = "2"
            self.cleanratingview?.value = 2.0
            self.accessratingview?.value = 2.0
            self.waitingtimeratingview?.value = 2.0
            self.commentratingview?.value = 2.0
            print("Selected rating: \(UInt((cleanratingview?.value)!))")
            passcleanliness = String(format: "%d",(UInt((cleanratingview?.value)!)))
            print(passcleanliness)
            print("Selected rating: \(UInt((accessratingview?.value)!))")
            passaccess = String(format: "%d",(UInt((accessratingview?.value)!)))
            print(passaccess)
            print("Selected rating: \(UInt((waitingtimeratingview?.value)!))")
            passwaitingtime = String(format: "%d",(UInt((waitingtimeratingview?.value)!)))
            print(passwaitingtime)
            print("Selected rating: \(UInt((commentratingview?.value)!))")
            passcomment = String(format: "%d",(UInt((commentratingview?.value)!)))
            print(passcomment)
            
        }
        else
        {
            star1.setImage(UIImage.init(named: "starunselect1.png"), for: .normal)
            star2.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
            star3.setImage(UIImage.init(named: "starunselect3.png"), for: .normal)
            star4.setImage(UIImage.init(named: "starunselect4.png"), for: .normal)
            star5.setImage(UIImage.init(named: "starunselect5.png"), for: .normal)
            star2str = "0"
            
        }
        self.avglink()
    }
    
    @IBAction func star3button(_sender:UIButton)
    {
        
        if star3str == "0"
        {
            star1.setImage(UIImage.init(named: "starselect1.png"), for: .normal)
            star2.setImage(UIImage.init(named: "starselect2.png"), for: .normal)
            star3.setImage(UIImage.init(named: "starselect3.png"), for: .normal)
            star4.setImage(UIImage.init(named: "starunselect4.png"), for: .normal)
            star5.setImage(UIImage.init(named: "starunselect5.png"), for: .normal)
            star3str = "3"
            self.cleanratingview?.value = 3.0
            self.accessratingview?.value = 3.0
            self.waitingtimeratingview?.value = 3.0
            self.commentratingview?.value = 3.0
            print("Selected rating: \(UInt((cleanratingview?.value)!))")
            passcleanliness = String(format: "%d",(UInt((cleanratingview?.value)!)))
            print(passcleanliness)
            print("Selected rating: \(UInt((accessratingview?.value)!))")
            passaccess = String(format: "%d",(UInt((accessratingview?.value)!)))
            print(passaccess)
            print("Selected rating: \(UInt((waitingtimeratingview?.value)!))")
            passwaitingtime = String(format: "%d",(UInt((waitingtimeratingview?.value)!)))
            print(passwaitingtime)
            print("Selected rating: \(UInt((commentratingview?.value)!))")
            passcomment = String(format: "%d",(UInt((commentratingview?.value)!)))
            print(passcomment)
            
        }
        else
        {
            star1.setImage(UIImage.init(named: "starunselect1.png"), for: .normal)
            star2.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
            star3.setImage(UIImage.init(named: "starunselect3.png"), for: .normal)
            star4.setImage(UIImage.init(named: "starunselect4.png"), for: .normal)
            star5.setImage(UIImage.init(named: "starunselect5.png"), for: .normal)
            star3str = "0"
            
        }
        self.avglink()
    }
    
    @IBAction func star4button(_sender:UIButton)
    {
        
        if star4str == "0"
        {
            star1.setImage(UIImage.init(named: "starselect1.png"), for: .normal)
            star2.setImage(UIImage.init(named: "starselect2.png"), for: .normal)
            star3.setImage(UIImage.init(named: "starselect3.png"), for: .normal)
            star4.setImage(UIImage.init(named: "starselect4.png"), for: .normal)
            star5.setImage(UIImage.init(named: "starunselect5.png"), for: .normal)
            star4str = "4"
            self.cleanratingview?.value = 4.0
            self.accessratingview?.value = 4.0
            self.waitingtimeratingview?.value = 4.0
            self.commentratingview?.value = 4.0
            print("Selected rating: \(UInt((cleanratingview?.value)!))")
            passcleanliness = String(format: "%d",(UInt((cleanratingview?.value)!)))
            print(passcleanliness)
            print("Selected rating: \(UInt((accessratingview?.value)!))")
            passaccess = String(format: "%d",(UInt((accessratingview?.value)!)))
            print(passaccess)
            print("Selected rating: \(UInt((waitingtimeratingview?.value)!))")
            passwaitingtime = String(format: "%d",(UInt((waitingtimeratingview?.value)!)))
            print(passwaitingtime)
            print("Selected rating: \(UInt((commentratingview?.value)!))")
            passcomment = String(format: "%d",(UInt((commentratingview?.value)!)))
            print(passcomment)
            
        }
        else
        {
            star1.setImage(UIImage.init(named: "starunselect1.png"), for: .normal)
            star2.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
            star3.setImage(UIImage.init(named: "starunselect3.png"), for: .normal)
            star4.setImage(UIImage.init(named: "starunselect4.png"), for: .normal)
            star5.setImage(UIImage.init(named: "starunselect5.png"), for: .normal)
            star4str = "0"
            
        }
        self.avglink()
    }
    
    @IBAction func star5button(_sender:UIButton)
    {
        
        if star5str == "0"
        {
            star1.setImage(UIImage.init(named: "starselect1.png"), for: .normal)
            star2.setImage(UIImage.init(named: "starselect2.png"), for: .normal)
            star3.setImage(UIImage.init(named: "starselect3.png"), for: .normal)
            star4.setImage(UIImage.init(named: "starselect4.png"), for: .normal)
            star5.setImage(UIImage.init(named: "starselect5.png"), for: .normal)
            star5str = "5"
            self.cleanratingview?.value = 5.0
            self.accessratingview?.value = 5.0
            self.waitingtimeratingview?.value = 5.0
            self.commentratingview?.value = 5.0
            print("Selected rating: \(UInt((cleanratingview?.value)!))")
            passcleanliness = String(format: "%d",(UInt((cleanratingview?.value)!)))
            print(passcleanliness)
            print("Selected rating: \(UInt((accessratingview?.value)!))")
            passaccess = String(format: "%d",(UInt((accessratingview?.value)!)))
            print(passaccess)
            print("Selected rating: \(UInt((waitingtimeratingview?.value)!))")
            passwaitingtime = String(format: "%d",(UInt((waitingtimeratingview?.value)!)))
            print(passwaitingtime)
            print("Selected rating: \(UInt((commentratingview?.value)!))")
            passcomment = String(format: "%d",(UInt((commentratingview?.value)!)))
            print(passcomment)
            
        }
        else
        {
            star1.setImage(UIImage.init(named: "starunselect1.png"), for: .normal)
            star2.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
            star3.setImage(UIImage.init(named: "starunselect3.png"), for: .normal)
            star4.setImage(UIImage.init(named: "starunselect4.png"), for: .normal)
            star5.setImage(UIImage.init(named: "starunselect5.png"), for: .normal)
            star5str = "0"
            
        }
        self.avglink()
    }
    func favlink()
    {
        if AFNetworkReachabilityManager.shared().isReachable
        {
            let urlstring : String!
            isLiked = !isLiked
            if isLiked == true
            {
                urlstring = String(format:"%@/fav.php?user_id=%@&loo_id=%@&fav_status=1",kBaseURL,passuserid,passlooid)
                print(urlstring)
            }
            else
            {
                urlstring = String(format:"%@/fav.php?user_id=%@&loo_id=%@&fav_status=0",kBaseURL,passuserid,passlooid)
                print(urlstring)
            }
            
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: false, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    if jsonObject.count != 0
                    {
                        let copyarr = jsonObject;
                        print(copyarr)
                        
                    }
                }
                else
                {
                    NSLog("jsonObject=error");
                }
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
    
    public func resetSelectedIcons() {
        if !restrictMaleSelect {
            self.male.setImage(UIImage.init(named: "unselectmale.png") as? UIImage, for: UIControlState.normal)
            self.maleState = false
        }
        if !restrictFemaleSelect {
            self.female.setImage(UIImage.init(named: "unselectfemale.png") as? UIImage, for: UIControlState.normal)
            self.femaleState = false
        }
        if !restrictUnisexSelect {
            self.unisex.setImage(UIImage.init(named: "unselectunisex.png") as? UIImage, for: UIControlState.normal)
            self.unisexState = false
        }
        if !restrictWheelChairSelect {
            self.wheelchair.setImage(UIImage.init(named: "unselectwheelchair.png") as? UIImage, for: UIControlState.normal)
            self.wheelChairState = false
        }
    }
    
    public func selectedFacilityIconMale() {
        
        if !restrictMaleSelect {
            if self.maleState {
                self.male.setImage(UIImage.init(named: "unselectmale.png") as? UIImage, for: UIControlState.normal)
            } else {
                resetSelectedIcons()
                self.male.setImage(UIImage.init(named: "maleselect.png") as? UIImage, for: UIControlState.normal)
            }
            self.maleState = !self.maleState
        }
    }
    
    public func selectedFacilityIconFemale() {
        
        if !restrictFemaleSelect {
         
            if self.femaleState {
                self.female.setImage(UIImage.init(named: "unselectfemale.png") as? UIImage, for: UIControlState.normal)
            } else {
                resetSelectedIcons()
                self.female.setImage(UIImage.init(named: "Male_select.png") as? UIImage, for: UIControlState.normal)
            }
            self.femaleState = !self.femaleState
        }
    }
    
    public func selectedFacilityIconUnisex() {
        if !restrictUnisexSelect {
         
            if self.unisexState {
                self.unisex.setImage(UIImage.init(named: "unselectunisex.png") as? UIImage, for: UIControlState.normal)
            } else {
                resetSelectedIcons()
                self.unisex.setImage(UIImage.init(named: "Unisex_select.png") as? UIImage, for: UIControlState.normal)
            }
            self.unisexState = !self.unisexState
        }
    }
    
    public func selectedFacilityIconWheelChair() {
        if !restrictWheelChairSelect {
            
            if self.wheelChairState {
                self.wheelchair.setImage(UIImage.init(named: "unselectwheelchair.png") as? UIImage, for: UIControlState.normal)
            } else {
                resetSelectedIcons()
                self.wheelchair.setImage(UIImage.init(named: "Wheel-Chair_select.png") as? UIImage, for: UIControlState.normal)
            }
            self.wheelChairState = !self.wheelChairState
        }
    }
    
    func getDefaultImageForCategory(categoryName:String) -> String {
        var imageName = "public"
        
        if categoryName == "" {
            imageName = "public"
        } else if categoryName == "Gas Station" {
            imageName = "Gas"
        } else if categoryName == "Hotel" {
            imageName = "Hotels"
        } else if categoryName == "Restaurant" {
            imageName = "Restaurant"
        } else if categoryName == "Shopping Mall" {
            imageName = "shoping_marker"
        } else if categoryName == "Bar" {
            imageName = "bar_marker"
        }
        
        return imageName
    }

    
    @IBAction func backtoSudeMenu(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.SideMenu.showLeftView(animated: true, completionHandler: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadsidemenu"), object: nil, userInfo: nil)
    }
    @IBAction func noThanks(_ sender: UIButton) {
        KGModal.sharedInstance().hide(animated: true)
        
        let looid = getdic.value(forKey: "loo_id") as? String ?? "NA"
        let userID = UserDefaults.standard.string(forKey: "user_id") ?? "NA"
        let looName = getdic.value(forKey: "loo_name") as? String ?? "NA"
        let looAddress = getdic.value(forKey: "loo_address") as? String ?? "NA"
        let looImage = getdic.value(forKey: "loo_image") as? String ?? "NA"
        
        FIRAnalytics.logEvent(withName: "rating_no_thanks", parameters: [
            "button_id_name" : "no_thanks",
            "loo_id": looid,
            "user_id" : userID,
            "loo_name" : looName,
            "button_name" : "No_Thanks_Click",
            "login_device" : "iOS",
            "content_type" : "Button_Action"
            ])
        
        
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //appDelegate.SideMenu.showLeftView(animated: true, completionHandler: nil)
        let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
        //let nav = UINavigationController.init(rootViewController: mainview)
        self.navigationController?.pushViewController(mainview, animated: false)
       // self.changelink()
    }
    func changelink()
    {
        if AFNetworkReachabilityManager.shared().isReachable
        {
            let urlstring = String(format:"%@/change_rate_notification.php?booking_id=%@",kBaseURL,self.bookingidstr)
            print(urlstring)
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: true, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    if jsonObject.count != 0
                    {
                        let str = (jsonObject.object(at: 0) as AnyObject).value(forKey: "message") as? String
                        if str == "success"
                        {
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
                            appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
                            currentview.isNavigationBarHidden = false
                            let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
                            currentview.pushViewController(mainview, animated: false)
                        }
                        else
                        {
                            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:str!, withIdentifier:"")
                        }
                        
                    }
                }
                else
                {
                    NSLog("jsonObject=error");
                }
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
    
    func avglink()
    {
        let a = cleanratingview?.value
        let b = accessratingview?.value
        let c = waitingtimeratingview?.value
        let d = commentratingview?.value
       
       
        var z = a!+b!+c!+d!
        if a == 0
        {
            z = 0
        }
        else if b == 0
        {
            z = 0
        }
        else if c == 0
        {
            z = 0
        }
        else if d == 0
        {
            z = 0
        }

        print(z)
        let x = z/4
        print(x)
        print("Selected index: \(x)")

        passoverall = String(format: "%d",(UInt(x)))
        print(passoverall)
        
        
        
        if x < 0.5
        {
            star1.setImage(UIImage.init(named: "starunselect1.png"), for: .normal)
            star2.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
            star3.setImage(UIImage.init(named: "starunselect3.png"), for: .normal)
            star4.setImage(UIImage.init(named: "starunselect4.png"), for: .normal)
            star5.setImage(UIImage.init(named: "starunselect5.png"), for: .normal)
        }
        else if x < 1.5
        {
            star1.setImage(UIImage.init(named: "starselect1.png"), for: .normal)
            //passaccess = String(format: "%d",(UInt((cleanratingview?.value)!)))
        }
        else if x < 2.5
        {
            star2.setImage(UIImage.init(named: "starselect2.png"), for: .normal)
        }
        else if x < 3.5
        {
            star3.setImage(UIImage.init(named: "starselect3.png"), for: .normal)
        }
        else if x < 4.5
        {
            star4.setImage(UIImage.init(named: "starselect4.png"), for: .normal)
        }
        else if x < 5.5
        {
            star5.setImage(UIImage.init(named: "starselect5.png"), for: .normal)
        }
//        firstViewHeight.constant = 0
//        secondViewHeight.constant = 0
//        firstView.isHidden = true
//        secondView.isHidden = true
    }
    @IBAction func Rate(_ sender: UIButton) {
        self.runlink()
    }
    
    func runlink()
    {
        if AFNetworkReachabilityManager.shared().isReachable
        {
            print(passuserid)
            print(passlooid)
            print(passoverall)
            print(passcleanliness)
//            print(passSoap ?? 0)
//            print(passMirror)
//            print(passSanitary)
//            print(passSeatCover)
//            print(passToiletPaper)
//            print(passHandDryer)
            if passcleanliness.characters.count == 0
            {
                passcleanliness = "0"
            }
            print(passaccess)
            if passaccess.characters.count == 0
            {
                passaccess = "0"
            }
            print(passwaitingtime)
            if passwaitingtime.characters.count == 0
            {
                passwaitingtime = "0"
            }
            print(passcomment)
            if passcomment.characters.count == 0
            {
                passcomment = "0"
            }
            
            passGender = getdic.value(forKey: "facility_category") as? String
            var userId = passuserid
            if UserDefaults.standard.object(forKey: "Logindetail") == nil {
                userId = "null"
            }
            print(self.ratingView.text)
            let urlstring = String(format:"%@/add_rating.php?user_id=%@&loo_id=%@&overall=%@&gender=%@&soap=%@&hand_dryer=%@&mirror=%@&sanitary_bags=%@&seat_cover=%@&toilet_paper=%@&cleanliness=%@&access=%@&waiting_time=%@&comment=%@&review=%@&booking_id=",kBaseURL,userId!,passlooid ?? "",passoverall ?? "",passGender ?? "",passSoap ?? "", passHandDryer ?? "", passMirror ?? "",passSanitary ?? "" ,passSeatCover ?? "",passToiletPaper ?? "", passcleanliness ?? "",passaccess ?? "",passwaitingtime ?? "",passcomment ?? "",self.ratingView.text ?? "" ,bookingidstr ?? "")
        //let urlstring = String(format:"%@/add_rating.php?user_id=%@&loo_id=%@&overall=%@&cleanliness=%@&access=%@&waiting_time=%@&comment=%@&review=%@",kBaseURL ?? "",passuserid ?? "",passlooid ?? "",passoverall ?? "",passcleanliness ?? "",passaccess ?? "",passwaitingtime ?? "",passcomment ?? "",self.ratingView.text ?? "")
            print(urlstring)
            
            let looid = getdic.value(forKey: "loo_id") as? String ?? "NA"
            let userID = UserDefaults.standard.string(forKey: "user_id") ?? "NA"
            let looName = getdic.value(forKey: "loo_name") as? String ?? "NA"
            FIRAnalytics.logEvent(withName: "user_rating", parameters: [
                "loo_id": looid,
                "user_id" : userID,
                "loo_name" : looName,
                "rating_details" : passoverall,
                "gender" : passGender,
                "login_device" : "iOS"
                ])
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: true, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    if jsonObject.count != 0
                    {
                        let str = (jsonObject.object(at: 0) as AnyObject).value(forKey: "message") as? String
                        
                        if str == "success"
                        {
                            
                            KGModal.sharedInstance().hide(animated: true)
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                         
                            let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
                            
                            self.navigationController?.pushViewController(mainview, animated: false)
                            
                            
                        }
                        else
                        {
                            let alertMessage = "Please_Rate_all_Sub_Ratings"
                            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:alertMessage.localized, withIdentifier:"")
                        }
                        
                    }
                }
                else
                {
                    NSLog("jsonObject=error");
                }
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
    
    //MARK: - SOAP
    @IBAction func soapYesAction(_ sender: UIButton) {
        self.soapYesKnowState(isYes: true)
    }
    
    @IBAction func soapNoAction(_ sender: UIButton) {
        self.soapYesKnowState(isYes: false)
    }
    
    func soapYesKnowState(isYes:Bool) {
        //If the value is null or already no then set the value to YES
        if (isYes) {
            self.passSoap = "yes";
            soapYes.setImage(UIImage.init(named: "yes"), for: UIControlState.normal)
            soapNo.setImage(UIImage.init(named: "no"), for: UIControlState.normal)
        } else {
            //Set the value to NO
            self.passSoap = "no";
            soapYes.setImage(UIImage.init(named: "no"), for: UIControlState.normal)
            soapNo.setImage(UIImage.init(named: "yes"), for: UIControlState.normal)
        }
    }
    
    //MARK: - HandDryer
    @IBAction func handDryerYesAction(_ sender: UIButton) {
        self.handDryerDataState(isYes: true)
    }
    
    @IBAction func handDryerNoAction(_ sender: UIButton) {
        self.handDryerDataState(isYes: false)
    }
    
    func handDryerDataState(isYes:Bool)  {
        if (isYes) {
            self.passHandDryer = "yes"
            handDryerYes.setImage(UIImage.init(named: "yes"), for: UIControlState.normal)
            handDryerNo.setImage(UIImage.init(named: "no"), for: UIControlState.normal)
        } else {
            self.passHandDryer = "no"
            handDryerYes.setImage(UIImage.init(named: "no"), for: UIControlState.normal)
            handDryerNo.setImage(UIImage.init(named: "yes"), for: UIControlState.normal)
        }
    }
    
    //MARK: - Sanity Bag
    @IBAction func sanitaryBageYesAction(_ sender: UIButton) {
        self.sanitaryBagStateTransfer(isYes: true)
    }
    
    @IBAction func sanitaryBageNoAction(_ sender: UIButton) {
        self.sanitaryBagStateTransfer(isYes: false)
    }
    
    func sanitaryBagStateTransfer(isYes:Bool)  {
        if (isYes) {
            self.passSanitary = "yes"
            sanitaryBageYes.setImage(UIImage.init(named: "yes"), for: UIControlState.normal)
            sanitaryBagsNo.setImage(UIImage.init(named: "no"), for: UIControlState.normal)
        } else {
            self.passSanitary = "no"
            sanitaryBageYes.setImage(UIImage.init(named: "no"), for: UIControlState.normal)
            sanitaryBagsNo.setImage(UIImage.init(named: "yes"), for: UIControlState.normal)
        }
    }
    
    //MARK: - Seat cover
    @IBAction func seatCoverYesAction(_ sender: UIButton) {
        self.seatCoverStateTransfer(isYes: true)
    }
    
    @IBAction func seatCoverNoAction(_ sender: UIButton) {
        self.seatCoverStateTransfer(isYes: false)
    }
    
    func seatCoverStateTransfer(isYes:Bool) {
        if (isYes) {
            self.passSeatCover = "yes"
            seatCoverYes.setImage(UIImage.init(named: "yes"), for: UIControlState.normal)
            seatCoverNo.setImage(UIImage.init(named: "no"), for: UIControlState.normal)
        } else {
            self.passSeatCover = "no"
            seatCoverYes.setImage(UIImage.init(named: "no"), for: UIControlState.normal)
            seatCoverNo.setImage(UIImage.init(named: "yes"), for: UIControlState.normal)
        }
    }
    
    //MARK: - Toilet paper
    @IBAction func toiletPaperYesAction(_ sender: UIButton) {
       self.toiletPaperStateTransfer(isYes: true)
    }
    
    @IBAction func toiletPaperNoAction(_ sender: UIButton) {
        self.toiletPaperStateTransfer(isYes: false)
    }
    
    func toiletPaperStateTransfer(isYes:Bool)  {
        if (isYes) {
            toiletPaperYes.setImage(UIImage.init(named: "yes"), for: UIControlState.normal)
            toiletPaperNo.setImage(UIImage.init(named: "no"), for: UIControlState.normal)
            self.passToiletPaper = "yes"
        } else {
            toiletPaperYes.setImage(UIImage.init(named: "no"), for: UIControlState.normal)
            toiletPaperNo.setImage(UIImage.init(named: "yes"), for: UIControlState.normal)
            self.passToiletPaper = "no"
        }
    }
    
    
    //MARK: - MIRROR
    @IBAction func mirrorYesAction(_ sender: UIButton) {
        self.mirrorStateTransfer(isYes: true)
    }
    
    @IBAction func mirrorNoAction(_ sender: UIButton) {
        self.mirrorStateTransfer(isYes: false)
    }
    
    func mirrorStateTransfer(isYes:Bool) {
        if (isYes) {
            mirrorYes.setImage(UIImage.init(named: "yes"), for: UIControlState.normal)
            mirrorNo.setImage(UIImage.init(named: "no"), for: UIControlState.normal)
            self.passMirror = "yes"
        } else {
            mirrorYes.setImage(UIImage.init(named: "no"), for: UIControlState.normal)
            mirrorNo.setImage(UIImage.init(named: "yes"), for: UIControlState.normal)
            self.passMirror = "no"
        }
    }
    
    @IBAction func cleanness(_ sender: EZRatingView) {
        print("cleanness called!!")
        print("Selected rating: \(UInt(sender.value))")
        passcleanliness = String(format: "%d",(UInt(sender.value)))
        print(passcleanliness)
    }
    
    @IBAction func acessRating(_ sender: EZRatingView) {
        print("acessRating called!!")
        print("Selected rating: \(UInt(sender.value))")
        passaccess = String(format: "%d",(UInt(sender.value)))
        print(passaccess)
    }
    
    @IBAction func waitingTime(_ sender: EZRatingView) {
        print("waitingTime called!!")
        print("Selected rating: \(UInt(sender.value))")
        passwaitingtime = String(format: "%d",(UInt(sender.value)))
        print(passwaitingtime)
    }
    
    @IBAction func commentRating(_ sender: EZRatingView) {
        print("commentRating called!!")
        print("Selected rating: \(UInt(sender.value))")
        passcomment = String(format: "%d",(UInt(sender.value)))
        print(passcomment)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let placesData = UserDefaults.standard.object(forKey: "Logindetail") as? NSDictionary
        self.Userdic.removeAllObjects()
        if placesData != nil{
            self.Userdic.addEntries(from: (placesData as? [String : Any])!)
        }
        print(self.Userdic)
        passuserid = self.Userdic.value(forKey: "user_id") as? String
        print(passuserid)
        rateButton.layer.cornerRadius = 20.0
        rateButton.clipsToBounds = true
       
    }
    
    @IBAction func addfavourite(_ sender: UIButton) {
                if UserDefaults.standard.object(forKey: "Logindetail") == nil
                {
                    let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    self.navigationController?.pushViewController(mainview, animated: true)
                }
                else
                {
                    isLiked = !isLiked
                    if isLiked == true
                    {
                        addBtn.setImage(UIImage(named: "LIKE.png"), for: UIControlState())
                       addBtn.likeBounce(0.6)
                        addBtn.animate()
                        isLiked = false
                    }
                    else
                    {
                        addBtn.setImage(UIImage(named: "Fav.png"), for: UIControlState())
                        addBtn.unLikeBounce(0.4)
                        isLiked = true
                    }
                    self.favlink()
                }
        
   
    }
    @IBAction func addBtnbutton(sender:UIButton!)
    {
        
    }
    
}
