//
//  calenderVC.swift
//  JEDE_r
//
//  Created by Exarcplus on 9/21/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit

import ScrollableDatepicker
import Alamofire
import Toast





class calenderVC: UIViewController,UITableViewDelegate,UITableViewDataSource,IQActionSheetPickerViewDelegate,BSHourPickerDelegate{
    
    
    
    @IBOutlet weak var calenderTableView: UITableView!
    var id:String = ""
    var newTimings = [String]()
    var timingsStr:String = ""
    var ultimateTimings:String = ""
    var timearr = NSMutableArray()
    var dayarr = NSMutableArray()
    var selecteddayarr = NSMutableArray()
    var selectedtime:Int!
    var tableindex:Int!
    var str : String!
    var checkopenorclose : String!
    var openclosetimearr = NSMutableArray()
    var newSelectedDate = ""
    
    var looOpenStatus = ""
    
    @IBOutlet weak var looSwitch: UISwitch!
    var timePicker = MIDatePicker.getFromNib()
    var dateFormatter = DateFormatter()
    var txtHours = BSHourPicker()
    
    var time = ["12:00 AM","01:00 AM","02:00 AM","03:00 AM","04:00 AM","05:00 AM","06:00 AM","07:00 AM","08:00 AM","09:00 AM","10:00 AM","11:00 AM","12:00 PM","01:00 PM","02:00 PM","03:00 PM","04:00 PM","05:00 PM","06:00 PM","07:00 PM","08:00 PM","09:00 PM","10:00 PM","11:00 PM"]
    
    var timings:String = ""
    
    var timeIntervel = [String]()
    var sponser : TimeAddingPopup!
    var timeColor = [Int](repeating: 0, count: 25)
    let dateCellIdentifier = "DateCellIdentifier"
    let contentCellIdentifier = "ContentCellIdentifier"
    let newdic = NSMutableDictionary()
    
    
    @IBOutlet weak var datepicker: ScrollableDatepicker! {
        didSet {
            var dates = [Date]()
            for day in -3...3 {
                
                dates.append(Date(timeIntervalSinceNow: Double(day * 86400)))
            }
            
            datepicker.dates = dates
            datepicker.selectedDate = Date()
            datepicker.delegate = self
            calenderTableView.reloadData()
            showSelectedDate()
        
            
            var configuration = Configuration()
            
            // weekend customization
            configuration.weekendDayStyle.dateTextColor = UIColor(red: 242.0/255.0, green: 93.0/255.0, blue: 28.0/255.0, alpha: 1.0)
            configuration.weekendDayStyle.dateTextFont = UIFont.boldSystemFont(ofSize: 20)
            configuration.weekendDayStyle.weekDayTextColor = UIColor(red: 242.0/255.0, green: 93.0/255.0, blue: 28.0/255.0, alpha: 1.0)
            
            // selected date customization
            configuration.selectedDayStyle.backgroundColor = UIColor(white: 0.1, alpha: 1)
            configuration.daySizeCalculation = .numberOfVisibleItems(7)
            
            datepicker.configuration = configuration
        }
    }
    //@IBOutlet weak var selectedDateLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        timearr.add("12:00 AM");
        timearr.add("01:00 AM");
        timearr.add("02:00 AM");
        timearr.add("03:00 AM");
        timearr.add("04:00 AM");
        timearr.add("05:00 AM");
        timearr.add("06:00 AM");
        timearr.add("07:00 AM");
        timearr.add("08:00 AM");
        timearr.add("09:00 AM");
        timearr.add("10:00 AM");
        timearr.add("11:00 AM");
        timearr.add("12:00 PM");
        timearr.add("01:00 PM");
        timearr.add("02:00 PM");
        timearr.add("03:00 PM");
        timearr.add("04:00 PM");
        timearr.add("05:00 PM");
        timearr.add("06:00 PM");
        timearr.add("07:00 PM");
        timearr.add("08:00 PM");
        timearr.add("09:00 PM");
        timearr.add("10:00 PM");
        timearr.add("11:00 PM");
        
        
        str = "show"
        
        self.navigationController?.navigationBar.isHidden = false
        
        calenderTableView.delegate = self
        calenderTableView.dataSource = self
        print(id)
        print(timeColor)
        DispatchQueue.main.async {
            self.showSelectedDate()
            self.datepicker.scrollToSelectedDate(animated: false)
        }
        //self.calenderTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cells")
        
        newdic.setValue("____ : ____", forKey: "open_time")
        newdic.setValue("____ : ____", forKey: "close_time")
        self.openclosetimearr.add(newdic)
    }
    
    fileprivate func showSelectedDate() {
        guard let selectedDate = datepicker.selectedDate else {
            return
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
       // selectedDateLabel.text = formatter.string(from: selectedDate)
        print(formatter.string(from: selectedDate))
        newSelectedDate = formatter.string(from: selectedDate)
        downloadTimings(looIdVal: id,dateVal: formatter.string(from: selectedDate))
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == calenderTableView{
            
            return 24
        }
            
        else{
            
           return self.openclosetimearr.count
        }
        
        
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == calenderTableView{
        // create a new cell if needed or reuse an old one
        if let cell = tableView.dequeueReusableCell(withIdentifier:"cells") as? CalenderTableViewCell
        {
            
            cell.timeLabel.text = time[indexPath.row]
            if timeColor[indexPath.row] == 1
            {
            cell.colouredView.backgroundColor = UIColor(red: 32.0/255, green: 215.0/255, blue: 179.0/255, alpha: 1.0)
            }
            else{
                cell.colouredView.backgroundColor = UIColor.white
            }
            
        return cell
        }
        else{
            return UITableViewCell()
    
        }
        }
        else
        {
            let simpleTableIdentifier  = NSString(format:"TimeAddingTableViewCell")
            let cell:TimeAddingTableViewCell?=tableView.dequeueReusableCell(withIdentifier: simpleTableIdentifier as String) as? TimeAddingTableViewCell
            cell?.selectionStyle = UITableViewCellSelectionStyle.none
            let openstr = (self.openclosetimearr.object(at: indexPath.row) as AnyObject).value(forKey: "open_time") as? String
            cell?.opentime.text = openstr
            cell?.closetime.text = (self.openclosetimearr.object(at: indexPath.row) as AnyObject).value(forKey: "close_time") as? String
            cell?.openbutton.tag = indexPath.row + 123
            cell?.openbutton.addTarget(self, action: #selector(DateandTimeViewController.openbutton(sender:)), for: UIControlEvents.touchUpInside)
            cell?.closebutton.tag = indexPath.row + 123
            cell?.closebutton.addTarget(self, action: #selector(DateandTimeViewController.closebutton(sender:)), for: UIControlEvents.touchUpInside)
            return cell!        }

    }

    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
    
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        if tableView == calenderTableView{
            return false
        }
        else{
            return true
        }
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if (editingStyle == UITableViewCellEditingStyle.delete)
        {
            // you might want to delete the item at the array first before calling this function
            //            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        let deleteAction = UITableViewRowAction(style: .default, title: "Delete", handler: {action in
            //handle delete
            
            self.sponser.tablelist.register(UINib.init(nibName:"TimeAddingTableViewCell", bundle: nil),forCellReuseIdentifier: "TimeAddingTableViewCell")
            self.sponser.tablelist.dataSource = self
            self.sponser.tablelist.delegate = self
            //                        self.openclosetimearr.remove(indexPath.row)
            self.openclosetimearr.removeObject(at: indexPath.row)
            self.sponser.tablelist.reloadData()
        })
        return [deleteAction]
    }
    
    
    
    
    
    func downloadTimings(looIdVal:String,dateVal:String){
        
        let params:[String:String] = ["loo_id":looIdVal,"date":dateVal]
        var timingsArr = [String]()
        timeColor = [Int](repeating: 0, count: 25)
        print(timeColor)
        print(timingsArr.count)
        print(params)
        let Url = URL(string:kBaseURL + "/calender_details.php")!
        Alamofire.request(Url,method:.get,parameters:params,encoding: URLEncoding()).responseJSON{ response in
            let result = response.result
            print(response)
            print(result)
            
            if let dict = result.value as? Dictionary<String,AnyObject>{
                
                if let values = dict["result"] as? [Dictionary<String,AnyObject>]
                {
                    for obj in values {
                        if let timings = obj["timings"] as? String
                        {
                            self.timings = timings
                        }
                    }
                }
            }
            print(self.timings)
            if (self.timings != "")
            {
               timingsArr = self.timings.components(separatedBy: ",")
            }
            print(timingsArr.count)
            print(timingsArr)
            
            if (timingsArr.count > 0)
            {
            for i in 0...timingsArr.count-1{
                
                self.timeIntervel = timingsArr[i].components(separatedBy: "-")
                //print(self.timeIntervel[0].trim())
                //print(self.timeIntervel[1].trim())
               var  x = self.timeIntervel[0].trim()
               var  y = self.timeIntervel[1].trim()
                
                for var i in 0...self.time.count-1{
                    
                    print(self.time[i],x)
                    if self.time[i] == x{
                        repeat {
                            print(self.time[i],x,"inside")
                            self.timeColor[i] = 1
                            print(self.timeColor)
                            i=i+1
                        } while self.time[i] != y && i<23
                        self.timeColor[i] = 1
                        print(self.timeColor)
                        break
                    }
                    
                    
                }
                
            }
            print(self.timeColor)
            print("hai")
            }
            self.calenderTableView.reloadData()
            //HUD.hide()
            
            //            var sectionNames: [String] {
            //                //return Set(self.objectArray.valueForKeyPath("race") as! [String]).sort()
            //            }
            
           
        }
        
        
        
    }
    
    
    @IBAction func backPressed(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func editButtonPressed(_ sender: Any) {
        
        self.openclosetimearr.removeAllObjects()
        newdic.setValue("____ : ____", forKey: "open_time")
        newdic.setValue("____ : ____", forKey: "close_time")
        self.openclosetimearr.add(newdic)

        
        sponser = Bundle.main.loadNibNamed("TimeAddingPopup", owner: self, options: nil)?[0] as! TimeAddingPopup
        sponser.layer.zPosition = 2
        KGModal.sharedInstance().show(withContentView: sponser, andAnimated: true)
        KGModal.sharedInstance().tapOutsideToDismiss = false
        
        self.view.endEditing(true)
        sponser.mybgview.layer.cornerRadius = 20.0
        sponser.mybgview.clipsToBounds = true
        sponser.addbutton.addTarget(self, action: #selector(calenderVC.addbottombutton(sender:)), for: UIControlEvents.touchUpInside)
        sponser.popupclose.addTarget(self, action: #selector(calenderVC.popupclosebutton(sender:)), for: UIControlEvents.touchUpInside)
        
        
        sponser.clearbutton.addTarget(self, action: #selector(calenderVC.popupclearbutton(sender:)), for: UIControlEvents.touchUpInside)
        sponser.savebutton.addTarget(self, action: #selector(calenderVC.popupsavebutton(sender:)), for: UIControlEvents.touchUpInside)
        
        //img
        sponser.mondayimg.isHidden = true
        sponser.mondaystr = ""
        
        sponser.tuesdayimg.isHidden = true
        sponser.tuesdaystr = ""
        
        sponser.wednesdayimg.isHidden = true
        sponser.wednesdaystr = ""
        
        sponser.thursdayimg.isHidden = true
        sponser.thursdaystr = ""
        
        sponser.fridayimg.isHidden = true
        sponser.fridaystr = ""
        
        sponser.saturdayimg.isHidden = true
        sponser.saturdaystr = ""
        
        sponser.sundayimg.isHidden = true
        sponser.sundaystr = ""
//
                  //            else if indexPath.section == 8
            //            {
            //                sponser.sundayimg.image = UIImage.init(named: "sselect.png")
            //                sponser.sundaystr = "1"
  
        sponser.tablelist.dataSource = self
        sponser.tablelist.delegate = self
        sponser.tablelist.register(UINib.init(nibName:"TimeAddingTableViewCell", bundle: nil),forCellReuseIdentifier: "TimeAddingTableViewCell")
        
       
        
        sponser.tablelist.dataSource = self
        sponser.tablelist.delegate = self
        
        sponser.tablelist.reloadData()
        let lastRow: Int = sponser.tablelist.numberOfRows(inSection: 0) - 1
        let indexPath = IndexPath(row: lastRow, section: 0);
        //sponser.tablelist.scrollToRow(at: indexPath, at: .top, animated: true)
          }
    
    
    func popupclosebutton(sender:UIButton!)
    {
        KGModal.sharedInstance().hide(animated: true)
        self.view.endEditing(true)
    }
    
    func addbottombutton(sender:UIButton!)
    {
        
        sponser.tablelist.register(UINib.init(nibName:"TimeAddingTableViewCell", bundle: nil),forCellReuseIdentifier: "TimeAddingTableViewCell")
       // let newdic = NSMutableDictionary()
        newdic.setValue("____ : ____", forKey: "open_time")
        newdic.setValue("____ : ____", forKey: "close_time")
        print(newdic)
        self.openclosetimearr.add(newdic)
        print(self.openclosetimearr)
        sponser.tablelist.dataSource = self
        sponser.tablelist.delegate = self
        
        sponser.tablelist.reloadData()
        let lastRow: Int = sponser.tablelist.numberOfRows(inSection: 0) - 1
        let indexPath = IndexPath(row: lastRow, section: 0);
        sponser.tablelist.scrollToRow(at: indexPath, at: .top, animated: true)
        
    }
    
    
    func popupsavebutton(sender:UIButton!)
    {
        print("hai")
        //(self.openclosetimearr.object(at: 0) as AnyObject).value(forKey: "close_time") as? String
       // print(self.openclosetimearr)
        
        print("helo")
        print(openclosetimearr.count)
       // print(self.openclosetimearr[0]["close_time"])
        if (openclosetimearr.count > 0)
        {
        
        for i in 0...openclosetimearr.count-1{
            
            let openTime = (self.openclosetimearr.object(at: i) as AnyObject).value(forKey: "open_time") as? String
            let closeTime = (self.openclosetimearr.object(at: i) as AnyObject).value(forKey: "close_time") as? String
            
            if (!(openTime?.contains("____"))! && !(closeTime?.contains("____"))!)
            {
            
            let x = time.index(of: openTime!)
            let y = time.index(of: closeTime!)
            
            if (x!>=y!)
            {
                self.view.makeToast("Invalid Time")
            }
            else{
            
            timingsStr = openTime!+" - "+closeTime!
            self.newTimings.append(timingsStr)
            }
                
            }
            
        }
       
      print(newTimings)
        
        //let stringArray = ["Bob", "Dan", "Bryan"]
        ultimateTimings = newTimings.joined(separator: ",")
        
        
        print(ultimateTimings)
            
            
            if ultimateTimings.contains("____"){
                print("invalid Time")
                self.view.makeToast("Invalid Time")
                
                
                
                
            }
                
            else
            {
                
                
                
                timingsSaved()
            }
            
            
            
        }
        else{
            self.view.makeToast("Invalid Time")
        }
        
       
        self.openclosetimearr.removeAllObjects()
        
    }
    
    func popupclearbutton(sender:UIButton!)
    {
        
        self.openclosetimearr.removeAllObjects()
        
        
        newdic.setValue("____ : ____", forKey: "open_time")
        newdic.setValue("____ : ____", forKey: "close_time")
        
        self.sponser.tablelist.reloadData()
        
        
        
    }
    
    func shouldShow(_ picker: BSHourPicker!) -> Bool
    {
        return true
    }
    
    func time(_ picker: BSHourPicker!, selctedItem item: String!)
    {
        print(item)
    }
    
    func closebutton(sender:UIButton!)
    {
        checkopenorclose = "close"
        tableindex = sender.tag-123
        print(tableindex)
        //        let picker = IQActionSheetPickerView(title: "Time Picker", delegate: self)
        //        picker?.tag = 8
        //        picker?.actionSheetPickerStyle = IQActionSheetPickerStyle.timePicker
        //        picker?.show()
        let picker = IQActionSheetPickerView(title: "Hour Picker", delegate: self)
        picker?.tag = 8
        picker?.titlesForComponents = [["12:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00"], ["AM", "PM"]]
        picker?.show()
        
        
    }
    
    func openbutton(sender:UIButton!)
    {

        checkopenorclose = "open"
        tableindex = sender.tag-123
        print(tableindex)
        let picker = IQActionSheetPickerView(title: "Hour Picker", delegate: self)
        picker?.tag = 8
        picker?.titlesForComponents = [["12:00", "01:00", "02:00", "03:00", "04:00", "05:00", "06:00", "07:00", "08:00", "09:00", "10:00", "11:00"], ["AM", "PM"]]
        //        [picker setTitlesForComponents:@[@[@"First 1", @"Second 1", @"Third 1", @"Four 1", @"Five 1"],
        //            @[@"First 2", @"Second 2", @"Third 2", @"Four 2", @"Five 2"]]];
        picker?.show()
        
       
        
        
        
        
    }
    
    
    
    
    func timingsSaved(){
        
        let params:[String:String] = ["loo_id":id,"date":newSelectedDate,"timings":ultimateTimings,"open_status":"1"]
        print(params)
        
        var timingsArr = [String]()
        timeColor = [Int](repeating: 0, count: 25)
        Alamofire.request(kBaseURL + "/calender_update.php",method:.get,parameters:params,encoding: URLEncoding()).responseJSON { response in
            
            let result = response.result
            print(response.request)
            print(response)
            print(result)
            
            if let dict = result.value as? Dictionary<String,AnyObject>{
                
                if let values = dict["result"] as? [Dictionary<String,AnyObject>]
                {
                    
                     for obj in values {
                        
                        if let timings = obj["timings"] as? String
                        {
                            self.timings = timings
                            
                        }
                    }
                    
                }
            }
            print(self.timings)
            if (self.timings != "")
            {
                
                timingsArr = self.timings.components(separatedBy: ",")
                
            }
            print(timingsArr.count)
            print(timingsArr)
            
            if (timingsArr.count > 0)
            {
                for i in 0...timingsArr.count-1{
                    
                    self.timeIntervel = timingsArr[i].components(separatedBy: "-")
                    print(self.timeIntervel[0].trim())
                    print(self.timeIntervel[1].trim())
                    var  x = self.timeIntervel[0].trim()
                    var  y = self.timeIntervel[1].trim()
                    for var i in 0...self.time.count-1{
                        print(self.time[i],x)
                        if self.time[i] == x{
                            repeat {
                                
                                //print(i)
                                self.timeColor[i] = 1
                                
                                i=i+1
                            } while self.time[i] != y && i<23
                                self.timeColor[i] = 1
                            
                            break
                        }
                        
                        
                    }
                    
                }
                print(self.timeColor)
                print("hai")
            }
            self.calenderTableView.reloadData()
            //HUD.hide()
            
            KGModal.sharedInstance().hide(animated: true)
            self.view.endEditing(true)

            
            
        }
    }

    
    
    func actionSheetPickerViewDidCancel(_ pickerView: IQActionSheetPickerView!)
    {
        
    }
    
    
    
    
    func actionSheetPickerView(_ pickerView: IQActionSheetPickerView!, didSelectTitles titles: NSArray!)
    {
        if checkopenorclose == "open"
        {
            let formatter = DateFormatter()
            formatter.dateStyle = .none
            formatter.timeStyle = .short
            //        sun_am_lab.text = formatter.string(from: date)
            //        print("\(sun_am_lab.text)")
            sponser.tablelist.register(UINib.init(nibName:"TimeAddingTableViewCell", bundle: nil),forCellReuseIdentifier: "TimeAddingTableViewCell")
            
            
            sponser.tablelist.dataSource = self
            sponser.tablelist.delegate = self
            
            let str = titles.componentsJoined(by: " ")
            print(str)
            let nsmutdic = NSMutableDictionary()
            
            let closetimestr = (self.openclosetimearr.object(at: tableindex) as AnyObject).value(forKey: "close_time") as? String
            nsmutdic.setValue(str, forKey: "open_time")
            nsmutdic.setValue(closetimestr, forKey: "close_time")
            nsmutdic.setValue("", forKey: "timelist")
            nsmutdic.setValue("", forKey: "timestat")
            print(nsmutdic)
            //        self.openclosetimearr.add(nsmutdic)
            self.openclosetimearr.replaceObject(at: tableindex, with: nsmutdic)
            print(self.openclosetimearr)
            let indexPath = IndexPath(row: tableindex, section: 0)
            sponser.tablelist.reloadRows(at: [indexPath], with: .none)
        }
        else
        {
            let formatter = DateFormatter()
            formatter.dateStyle = .none
            formatter.timeStyle = .short
            //        sun_am_lab.text = formatter.string(from: date)
            //        print("\(sun_am_lab.text)")
            sponser.tablelist.register(UINib.init(nibName:"TimeAddingTableViewCell", bundle: nil),forCellReuseIdentifier: "TimeAddingTableViewCell")
            
            
            sponser.tablelist.dataSource = self
            sponser.tablelist.delegate = self
            let str = titles.componentsJoined(by: " ")
            print(str)
            let nsmutdic = NSMutableDictionary()
            let opentimestr = (self.openclosetimearr.object(at: tableindex) as AnyObject).value(forKey: "open_time") as? String
            
            let timeformat = DateFormatter()
            timeformat.dateFormat = "hh:mm a"
            var starttime = timeformat.date(from:opentimestr!)
            let endtime = timeformat.date(from:str)
            
            let calendar = NSCalendar.current as NSCalendar
            
            // Replace the hour (time) of both dates with 00:00
            
            let flags = NSCalendar.Unit.hour
            let components = calendar.components(flags, from: starttime!, to: endtime!, options: [])
            
            print(components.hour!)
            
            var timelist = String()
            var timestat = String()
            
            if (components.hour! > 0)
            {
            for c in 0 ..< components.hour!+1
            {
                if c == 0
                {
                    timelist.append("\(timeformat.string(from: starttime!))")
                    timestat.append("1")
                }
                else
                {
                    starttime = starttime?.addingTimeInterval(3600)
                    timelist.append(" , \(timeformat.string(from: starttime!))")
                    if c == components.hour!
                    {
                        timestat.append(",1")
                    }
                    else{
                        timestat.append(",0")
                    }
                }
            }
            }
            else{
                print("Enter a Valid Time")
            }
            
         
            print(timelist)
            print(timestat)
            
            if (opentimestr == "____:____") || (str == "____:____")
            {
                print("Enter a Valid Time")
            }
            else{
            
            nsmutdic.setValue(opentimestr, forKey: "open_time")
            nsmutdic.setValue(str, forKey: "close_time")
            nsmutdic.setValue(timelist, forKey: "timelist")
            nsmutdic.setValue(timestat, forKey: "timestat")
            
            print(nsmutdic)
            
            
           
            
            
            
            //        self.openclosetimearr.add(nsmutdic)
            self.openclosetimearr.replaceObject(at: tableindex, with: nsmutdic)
            print(self.openclosetimearr)
            let indexPath = IndexPath(row: tableindex, section: 0)
            sponser.tablelist.reloadRows(at: [indexPath], with: .none)
            }
            
           
            
            
        }
    }
    
    
    
    
    

    
    

    
}





// MARK: - ScrollableDatepickerDelegate
extension calenderVC: ScrollableDatepickerDelegate {
    
    func datepicker(_ datepicker: ScrollableDatepicker, didSelectDate date: Date) {
        showSelectedDate()
        
    }
    
    
}

extension String
{
    func trim() -> String
    {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
}
