//
//  Constant.swift
//  Sphere
//
//  Created by Exarcplus on 16/04/16.
//  Copyright © 2016 Exarcplus. All rights reserved.
//

import Foundation

let isProd = true

let kmainStoryboard = UIStoryboard(name:"Main", bundle: nil)
let kDevUrl = "http://inchpartners.ch/JEDE_r/json/"
let kProductionURL = "https://wc-i.org/WCi/json/"
let kBaseURL = isProd ? kProductionURL : kDevUrl;
let kBaseDomainUrl = isProd ? "https://wc-i.org/WCi/" : "http://inchpartners.ch/JEDE_r/";
let kTermsAndConditionsUrl = "http://wc-i.net/terms-and-conditions/"
let KEmail = "[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
let KClientID = "918893875150-kfvmk3bscb1rdt5su6254a0sidhs0oea.apps.googleusercontent.com"
let KgoogTrackid = "UA-81494919-1"
let kAppstoreLink = "https://itunes.apple.com/in/app/wci-where-can-i/id1393848544?mt=8"
let kGoogleDirectionAPIKey = "AIzaSyDZIP7vtjj-nL8mw2Y90cdvAP-ASnP6ADg"
