//
//  DetailsViewController.swift
//  JEDE_r
//
//  Created by Exarcplus iOS 2 on 10/05/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import AFNetworking
import SDWebImage
import KKActionSheet

class DetailsViewController: UIViewController, UIScrollViewDelegate, UIGestureRecognizerDelegate, PECropViewControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate,UINavigationControllerDelegate {
    
    var imageHud:MyHUDProgress!
    
    var myclass : MyClass!
    @IBOutlet weak var scrollview : UIScrollView!
    @IBOutlet weak var upperview : UIView!
    @IBOutlet weak var gradientview : UIView!
    @IBOutlet weak var bottomview : UIView!
    @IBOutlet weak var likeButton: SparkButton!
    var isLiked:Bool!
    @IBOutlet weak var sharebut : UIButton!
    @IBOutlet weak var malebut : UIButton!
    @IBOutlet weak var femalebut : UIButton!
    @IBOutlet weak var unisexbut : UIButton!
    @IBOutlet weak var wheelbut : UIButton!

    var detailsarr = NSMutableArray()
    var selectdic : NSMutableDictionary!
    var passlooid : String!
    var passuserid : String!
    var passcurrentloc : String!
    @IBOutlet weak var viewmap: GMSMapView!
    let marker = GMSMarker()
    var Userdic = NSMutableDictionary()
    
    @IBOutlet weak var loonamelab : UILabel!
    @IBOutlet weak var reviewslab : UILabel!
    @IBOutlet weak var distancelab : UILabel!
    @IBOutlet weak var pricelab : UILabel!
    @IBOutlet weak var locationlab : UILabel!
    @IBOutlet weak var infolab : UILabel!
    @IBOutlet weak var crossimg : UIImageView!
    @IBOutlet weak var backview : UIView!
    @IBOutlet weak var ratingview : EZRatingView!
    
    //femaleview
    @IBOutlet weak var femaleviewicon : UIImageView!
    @IBOutlet weak var femaleviewlab : UILabel!
    @IBOutlet weak var femalebgimg : UIImageView!
    @IBOutlet weak var femalecrossimg : UIImageView!
    
    //maleview
    @IBOutlet weak var maleviewicon : UIImageView!
    @IBOutlet weak var maleviewlab : UILabel!
    @IBOutlet weak var malebgimg : UIImageView!
    @IBOutlet weak var malecrossimg : UIImageView!
    
    //unisexview
    @IBOutlet weak var unisexviewicon : UIImageView!
    @IBOutlet weak var unisexviewlab : UILabel!
    @IBOutlet weak var unisexbgimg : UIImageView!
    @IBOutlet weak var unisexcrossimg : UIImageView!
    
    //wheelchairview
    @IBOutlet weak var wheelchairviewicon : UIImageView!
    @IBOutlet weak var wheelchairviewlab : UILabel!
    @IBOutlet weak var wheelchairbgimg : UIImageView!
    @IBOutlet weak var wheelchaircrossimg : UIImageView!
    
    //carparkingview
    @IBOutlet weak var carparkingviewicon : UIImageView!
    @IBOutlet weak var carparkingviewlab : UILabel!
    @IBOutlet weak var carparkingbgimg : UIImageView!
    @IBOutlet weak var carparkingcrossimg : UIImageView!
    
    @IBOutlet weak var bookview : UIView!
    @IBOutlet weak var navigateview : UIView!
    
    let serverResponseForAlreadyBooked = "already Booked"
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        myclass = MyClass()
        self.navigationController?.navigationBar.isHidden = true
        
        scrollview.delegate=self
        
//        scrollview.addTwitterCover(with: UIImage(named: "Toilets-Defualt.png"))
        if selectdic != nil
        {
            print(selectdic)
            passlooid = selectdic.value(forKey: "loo_id") as! String
            print(passlooid)
            print(passcurrentloc)
            
            
            let imageurl = selectdic.value(forKey: "loo_image") as? String
            print(imageurl!)
            if imageurl == "" || imageurl?.characters.count == 0 || imageurl == nil
            {
                scrollview.addTwitterCover(with: UIImage(named: "Toilets-Defualt.png"))
                
            }
            else
            {
                let url = URL(string: imageurl!)
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                scrollview.addTwitterCover(with: UIImage(data: data!))
            }
        }
        else
        {
            self.scrollview.addTwitterCover(with: UIImage(named: "Toilets-Defualt.png"))
        }
        
        if UserDefaults.standard.object(forKey: "Logindetail") == nil
        {
            passuserid = ""
        }
        else
        {
//            let placesData = UserDefaults.standard.object(forKey: "Logindetail") as? NSData
//            self.Userdic.removeAllObjects()
//            self.Userdic.addEntries(from: NSKeyedUnarchiver.unarchiveObject(with: placesData! as Data) as! NSDictionary as [NSObject : AnyObject])
            let placesData = UserDefaults.standard.object(forKey: "Logindetail") as? NSDictionary
            self.Userdic.removeAllObjects()
            if placesData != nil{
                self.Userdic.addEntries(from: (placesData as? [String : Any])!)
            }
            print(self.Userdic)
            passuserid = self.Userdic.value(forKey: "user_id") as? String
            print(passuserid)
        }
        
        let view = UIView(frame:  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 20))
        view.backgroundColor = UIColor.init(red: 27.0/255.0, green: 34.0/255.0, blue: 40.0/255.0, alpha: 1.0)
        self.navigationController?.view.addSubview(view)
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        
        self.maleviewicon.alpha = 0.3
        self.maleviewlab.alpha = 0.3
        self.malebgimg.alpha = 0.3
        self.malecrossimg.alpha = 1.0
  
        self.femaleviewicon.alpha = 0.3
        self.femaleviewlab.alpha = 0.3
        self.femalebgimg.alpha = 0.3
        self.femalecrossimg.alpha = 1.0
   
        self.unisexviewicon.alpha = 0.3
        self.unisexviewlab.alpha = 0.3
        self.unisexbgimg.alpha = 0.3
        self.unisexcrossimg.alpha = 1.0
        
        self.wheelchairviewicon.alpha = 0.3
        self.wheelchairviewlab.alpha = 0.3
        self.wheelchairbgimg.alpha = 0.3
        self.wheelchaircrossimg.alpha = 1.0
   
        self.carparkingviewicon.alpha = 0.3
        self.carparkingviewlab.alpha = 0.3
        self.carparkingbgimg.alpha = 0.3
        self.carparkingcrossimg.alpha = 1.0
    
    
        
//        let image = UIImage(named: "header_bg");
//        let screenSize: CGRect = UIScreen.main.bounds
//        image.frame = CGRect(x: 0, y: 0, width: 50, height: screenSize.height * 0.2)
//        upperimage.image = image
        
//        scrollview.addTwitterCover(with: UIImage(named: "header_bg"), withTopView: upperview)
        
        
//        scrollview.addSubview(bottomview)
//        self.view.addSubview(scrollview)

//        self.view.backgroundColor = [UIColor whiteColor];
//        scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
//        [scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, 600)];
//        [scrollView addTwitterCoverWithImage:[UIImage imageNamed:@"cover.png"]];
//        [self.view addSubview:scrollView];
//        
//        [scrollView addSubview:({
//            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, CHTwitterCoverViewHeight, self.view.bounds.size.width - 40, 600 - CHTwitterCoverViewHeight)];
//            label.numberOfLines = 0;
//            label.font = [UIFont systemFontOfSize:22];
//            label.text = @"TwitterCover is a parallax top view with real time blur effect to any UIScrollView, inspired by Twitter for iOS.\n\nCompletely created using UIKit framework.\n\nEasy to drop into your project.\n\nYou can add this feature to your own project, TwitterCover is easy-to-use.";
//            label;
//            })];
        // Do any additional setup after loading the view.
        
        self.bookview.isHidden = true
        self.navigateview.isHidden = true
        
        self.runlink()
    }
    
//    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
//        self.navigationController?.setNavigationBarHidden(false, animated: true)
//        self.backview.isHidden = true
//        self.navigationItem.title = self.selectdic.value(forKey: "loo_name") as? String
//        scrollview.removeTwitterCover()
//        return true
//    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        //self.navigationItem.hidesBackButton = true
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.SideMenu.isLeftViewSwipeGestureEnabled = false;
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil;
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let currentOffset: CGFloat = scrollView.contentOffset.y
        let maximumOffset: CGFloat = scrollView.contentSize.height - scrollView.frame.size.height
        // Change 10.0 to adjust the distance from bottom
        if maximumOffset - currentOffset <= 10.0 {
            UIView.animate(withDuration: 2.5, delay: 0, options: UIViewAnimationOptions(), animations: {
                self.navigationController?.setNavigationBarHidden(false, animated: true)
                print("UNHide")
                self.backview.isHidden = true
                self.navigationItem.title = self.selectdic.value(forKey: "loo_name") as? String
            }, completion: nil)
            
        }
        else {
            UIView.animate(withDuration: 2.5, delay: 0, options: UIViewAnimationOptions(), animations: {
                self.navigationController?.setNavigationBarHidden(true, animated: true)
                self.backview.isHidden = false
                print("hide")
            }, completion: nil)
        }
    }

    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func runlink()
    {
        if AFNetworkReachabilityManager.shared().isReachable
        {
//            let urlstring = String(format:"%@/loo_details.php?loo_id=%@&current_location=%@&user_id=%@",kBaseURL,passlooid,passcurrentloc,passuserid)
            let urlstring = String(format:"%@/loo_details.php?loo_id=%@&current_location=%@&user_id=%@",kBaseURL,passlooid,passcurrentloc,passuserid)
            print(urlstring)
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: true, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    if jsonObject.count != 0
                    {
                        //                        let sts = (jsonObject.object(at: 0) as AnyObject).value(forKey: "user_status") as! String
                        //                        if sts == "Empty" || sts == "Active"
                        //                        {
                        if UserDefaults.standard.object(forKey: "Logindetail") == nil
                        {
                            
                        }
                        else
                        {
                            self.addrecentlyviewlink()
                        }
                        self.detailsarr .removeAllObjects()
                        self.detailsarr.addObjects(from: jsonObject as [AnyObject])
                        print(self.detailsarr)
                        self.scrollview.removeTwitterCover()
                        let imageurl = (self.detailsarr.object(at: 0) as AnyObject).value(forKey: "loo_image") as? String
                        print(imageurl!)
                        if imageurl == "" || imageurl?.characters.count == 0 || imageurl == nil
                        {
                            self.scrollview.addTwitterCover(with: UIImage(named: "Toilets-Defualt.png"))
                        }
                        else
                        {
                            let url = URL(string: imageurl!)
                            let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                            self.scrollview.addTwitterCover(with: UIImage(data: data!))
                        }
                        
                        let loonamestr = (self.detailsarr.object(at: 0) as AnyObject).value(forKey: "loo_name") as? String
                        if loonamestr == nil
                        {
                            self.loonamelab.text = ""
                        }
                        else
                        {
                            self.loonamelab.text = loonamestr
                        }
                        
                        let favouritestr = (self.detailsarr.object(at: 0) as AnyObject).value(forKey: "favourite_status") as? String
                        print(favouritestr!)
                        if favouritestr == "0"
                        {
                            self.isLiked = false
                            self.likeButton.setImage(UIImage(named: "Fav.png"), for: UIControlState())
                            self.likeButton.unLikeBounce(0.4)
                        }
                        else
                        {
                            self.isLiked = true
                            self.likeButton.setImage(UIImage(named: "LIKE.png"), for: UIControlState())
                            self.likeButton.likeBounce(0.6)
                        }
                        
                        
                        let rating = (self.detailsarr.object(at: 0) as AnyObject).value(forKey: "rating") as! NSNumber
                        print(rating)
                        print(CGFloat(rating.doubleValue))
//                        let myInteger = Int(String(format: "%@", rating))
//                        let myNumber = NSNumber(value:myInteger!)
//                        print(myNumber)
                        if rating.intValue != 0
                        {
                                //                    cell?.Pricebg.backgroundColor = myclass.colorWithHexString(hex: "#01a550",Alpha: 0.7)
//                            self.ratingview.stepInterval = CGFloat(rating.intValue)
//                            self.ratingview.isHighlighted = true
//                            self.ratingview.stepInterval = 0.0
                            self.ratingview.value = CGFloat(rating.doubleValue)
                        }
                        else
                        {
                                //                    cell?.Pricebg.backgroundColor = myclass.colorWithHexString(hex: "#ec0d1a",Alpha: 0.7)
//                            self.ratingview.stepInterval = 0.0
//                            self.ratingview.isHighlighted = false
                            self.ratingview.value = 0
                        }
                       
                        let reviewsstrcount = (self.detailsarr.object(at: 0) as AnyObject).value(forKey: "reviews_count") as? String
                        if reviewsstrcount == nil || reviewsstrcount == ""
                        {
                            self.reviewslab.text = "0"
                        }
                        else
                        {
                            self.reviewslab.text = reviewsstrcount
                        }
                        
                        let distancestr = (self.detailsarr.object(at: 0) as AnyObject).value(forKey: "distance") as? String
                        if distancestr == nil || distancestr == ""
                        {
                            self.distancelab.text = ""
                        }
                        else
                        {
                            self.distancelab.text = String(format: "%@ KM", distancestr!)
                        }
                        let pricestr = (self.detailsarr.object(at: 0) as AnyObject).value(forKey: "price") as? String
                        if pricestr == nil || pricestr == ""
                        {
                            self.pricelab.text = "FREE"
                        }
                        else
                        {
                            self.pricelab.text = String(format: "CHF %@", pricestr!)
                        }
                        let locationstr = (self.detailsarr.object(at: 0) as AnyObject).value(forKey: "loo_address") as? String
                        if locationstr == nil
                        {
                            self.locationlab.text = ""
                        }
                        else
                        {
                            self.locationlab.text = locationstr
                        }
                        
                        //facilities
                        let facilitiesarr = (self.detailsarr.object(at: 0) as AnyObject).value(forKey: "facilities") as! NSArray
                        if facilitiesarr.count == 3
                        {
                            let index0 = facilitiesarr.object(at: 0) as! String
                            print(index0)
                            if index0 == "Male"
                            {
                                self.maleviewicon.alpha = 1.0
                                self.maleviewlab.alpha = 1.0
                                self.malebgimg.alpha = 1.0
                                self.malecrossimg.alpha = 0.0
                                
                            }
                            else if index0 == "Female"
                            {
                                self.femaleviewicon.alpha = 1.0
                                self.femaleviewlab.alpha = 1.0
                                self.femalebgimg.alpha = 1.0
                                self.femalecrossimg.alpha = 0.0
                            }
                            else if index0 == "Unisex"
                            {
                                self.unisexviewicon.alpha = 1.0
                                self.unisexviewlab.alpha = 1.0
                                self.unisexbgimg.alpha = 1.0
                                self.unisexcrossimg.alpha = 0.0
                            }
                            
                            let index1 = facilitiesarr.object(at: 1) as! String
                            print(index1)
                            if index1 == "Male"
                            {
                                self.maleviewicon.alpha = 1.0
                                self.maleviewlab.alpha = 1.0
                                self.malebgimg.alpha = 1.0
                                self.malecrossimg.alpha = 0.0
                            }
                            else if index1 == "Female"
                            {
                                self.femaleviewicon.alpha = 1.0
                                self.femaleviewlab.alpha = 1.0
                                self.femalebgimg.alpha = 1.0
                                self.femalecrossimg.alpha = 0.0
                            }
                            else if index1 == "Unisex"
                            {
                                self.unisexviewicon.alpha = 1.0
                                self.unisexviewlab.alpha = 1.0
                                self.unisexbgimg.alpha = 1.0
                                self.unisexcrossimg.alpha = 0.0
                            }
                            
                            let index2 = facilitiesarr.object(at: 2) as! String
                            print(index2)
                            if index2 == "Male"
                            {
                                self.maleviewicon.alpha = 1.0
                                self.maleviewlab.alpha = 1.0
                                self.malebgimg.alpha = 1.0
                                self.malecrossimg.alpha = 0.0
                            }
                            else if index2 == "Female"
                            {
                                self.femaleviewicon.alpha = 1.0
                                self.femaleviewlab.alpha = 1.0
                                self.femalebgimg.alpha = 1.0
                                self.femalecrossimg.alpha = 0.0
                            }
                            else if index2 == "Unisex"
                            {
                                self.unisexviewicon.alpha = 1.0
                                self.unisexviewlab.alpha = 1.0
                                self.unisexbgimg.alpha = 1.0
                                self.unisexcrossimg.alpha = 0.0
                            }
                        }
                        else if facilitiesarr.count == 2
                        {
                            let index0 = facilitiesarr.object(at: 0) as! String
                            print(index0)
                            if index0 == "Male"
                            {
                                self.maleviewicon.alpha = 1.0
                                self.maleviewlab.alpha = 1.0
                                self.malebgimg.alpha = 1.0
                                self.malecrossimg.alpha = 0.0
                            }
                            else if index0 == "Female"
                            {
                                self.femaleviewicon.alpha = 1.0
                                self.femaleviewlab.alpha = 1.0
                                self.femalebgimg.alpha = 1.0
                                self.femalecrossimg.alpha = 0.0
                            }
                            else if index0 == "Unisex"
                            {
                                self.unisexviewicon.alpha = 1.0
                                self.unisexviewlab.alpha = 1.0
                                self.unisexbgimg.alpha = 1.0
                                self.unisexcrossimg.alpha = 0.0
                            }
                            
                            let index1 = facilitiesarr.object(at: 1) as! String
                            print(index1)
                            if index1 == "Male"
                            {
                                self.maleviewicon.alpha = 1.0
                                self.maleviewlab.alpha = 1.0
                                self.malebgimg.alpha = 1.0
                                self.malecrossimg.alpha = 0.0
                            }
                            else if index1 == "Female"
                            {
                                self.femaleviewicon.alpha = 1.0
                                self.femaleviewlab.alpha = 1.0
                                self.femalebgimg.alpha = 1.0
                                self.femalecrossimg.alpha = 0.0
                            }
                            else if index1 == "Unisex"
                            {
                                self.unisexviewicon.alpha = 1.0
                                self.unisexviewlab.alpha = 1.0
                                self.unisexbgimg.alpha = 1.0
                                self.unisexcrossimg.alpha = 0.0
                            }
                        }
                        else if facilitiesarr.count == 1
                        {
                            let index0 = facilitiesarr.object(at: 0) as! String
                            print(index0)
                            if index0 == "Male"
                            {
                                self.maleviewicon.alpha = 1.0
                                self.maleviewlab.alpha = 1.0
                                self.malebgimg.alpha = 1.0
                                self.malecrossimg.alpha = 0.0
                            }
                            else if index0 == "Female"
                            {
                                self.femaleviewicon.alpha = 1.0
                                self.femaleviewlab.alpha = 1.0
                                self.femalebgimg.alpha = 1.0
                                self.femalecrossimg.alpha = 0.0
                            }
                            else if index0 == "Unisex"
                            {
                                self.unisexviewicon.alpha = 1.0
                                self.unisexviewlab.alpha = 1.0
                                self.unisexbgimg.alpha = 1.0
                                self.unisexcrossimg.alpha = 0.0
                            }
                        }
                        else
                        {
                            
                        }
                        
                        //accessibility
                        let accessibilitystr = (self.detailsarr.object(at: 0) as AnyObject).value(forKey: "accessibility") as? String
                        let accessibilityarr = accessibilitystr?.components(separatedBy: ",") as! NSArray
                        if accessibilityarr.count == 2
                        {
                            let index0 = accessibilityarr.object(at: 0) as! String
                            print(index0)
                            if index0 == "wheelchair"
                            {
                                self.wheelchairviewicon.alpha = 1.0
                                self.wheelchairviewlab.alpha = 1.0
                                self.wheelchairbgimg.alpha = 1.0
                                self.wheelchaircrossimg.alpha = 0.0
                            }
                            else if index0 == "Carparking"
                            {
                                self.carparkingviewicon.alpha = 1.0
                                self.carparkingviewlab.alpha = 1.0
                                self.carparkingbgimg.alpha = 1.0
                                self.carparkingcrossimg.alpha = 0.0
                            }
                            
                            let index1 = accessibilityarr.object(at: 1) as! String
                            print(index1)
                            if index1 == "wheelchair"
                            {
                                self.wheelchairviewicon.alpha = 1.0
                                self.wheelchairviewlab.alpha = 1.0
                                self.wheelchairbgimg.alpha = 1.0
                                self.wheelchaircrossimg.alpha = 0.0
                            }
                            else if index1 == "Carparking"
                            {
                                self.carparkingviewicon.alpha = 1.0
                                self.carparkingviewlab.alpha = 1.0
                                self.carparkingbgimg.alpha = 1.0
                                self.carparkingcrossimg.alpha = 0.0
                            }
                        }
                        else if accessibilityarr.count == 1
                        {
                            let index0 = accessibilityarr.object(at: 0) as! String
                            print(index0)
                            if index0 == "wheelchair"
                            {
                                self.wheelchairviewicon.alpha = 1.0
                                self.wheelchairviewlab.alpha = 1.0
                                self.wheelchairbgimg.alpha = 1.0
                                self.wheelchaircrossimg.alpha = 0.0
                            }
                            else if index0 == "Carparking"
                            {
                                self.carparkingviewicon.alpha = 1.0
                                self.carparkingviewlab.alpha = 1.0
                                self.carparkingbgimg.alpha = 1.0
                                self.carparkingcrossimg.alpha = 0.0
                            }
                        }
                        else
                        {
                            
                        }
                        
                        if self.selectdic != nil
                        {
                        //map
                            let type = self.selectdic.value(forKey: "category_name") as? String
                        let location = self.selectdic.value(forKey: "loo_location") as? String
                        
                            if type == ""
                            {
                                self.marker.icon = UIImage.init(named: "Public")
                                self.bookview.isHidden = true
                                self.navigateview.isHidden = false
                                //                                                    marker.accessibilityHint = "Yes"
                            }
                            else if type == "Gas Station"
                            {
                                self.marker.icon = UIImage.init(named: "Gas")
                                self.bookview.isHidden = false
                                self.navigateview.isHidden = true
                                //                                                    marker.accessibilityHint = "No"
                            }
                            else if type == "Hotel"
                            {
                                self.marker.icon = UIImage.init(named: "Hotels")
                                self.bookview.isHidden = false
                                self.navigateview.isHidden = true
                            }
                            else if type == "Restaurant"
                            {
                                self.marker.icon = UIImage.init(named: "Restaurant")
                                self.bookview.isHidden = false
                                self.navigateview.isHidden = true
                                //                                                    marker.accessibilityHint = "No"
                            }
                            
                            
                        if location?.characters.count != 0
                        {
                            let locar:NSArray = (location?.components(separatedBy:(",")))! as NSArray ;
                            let lat = NumberFormatter().number(from: locar.object(at: 0) as! String)
                            let lon = NumberFormatter().number(from: locar.object(at: 1) as! String)
                            let loosloc = CLLocation.init(latitude: (lat?.doubleValue)!, longitude: (lon?.doubleValue)!)
                            self.marker.position = CLLocationCoordinate2DMake(loosloc.coordinate.latitude, loosloc.coordinate.longitude)
                        }
                        self.marker.zIndex = 0 ;
                        self.marker.map  = self.viewmap
                        
                        print(self.selectdic)
                            
                            //draw circle
                            if location?.characters.count != 0
                            {
                                if type == ""
                                {
                                    
                                }
                                else if type == "Gas Station"
                                {
                                    let geoFenceCircle = GMSCircle()
                                    let locar:NSArray = (location?.components(separatedBy:(",")))! as NSArray ;
                                    let lat = NumberFormatter().number(from: locar.object(at: 0) as! String)
                                    let lon = NumberFormatter().number(from: locar.object(at: 1) as! String)
                                    let loosloc = CLLocation.init(latitude: (lat?.doubleValue)!, longitude: (lon?.doubleValue)!)
                                    geoFenceCircle.radius = 100
                                    // Meters
                                    geoFenceCircle.position = CLLocationCoordinate2DMake(loosloc.coordinate.latitude, loosloc.coordinate.longitude)
                                    // Some CLLocationCoordinate2D position
                                    geoFenceCircle.fillColor = UIColor.init(red: 0.941, green: 0.675, blue: 0.671, alpha: 1.0)
                                    geoFenceCircle.strokeWidth = 1
                                    geoFenceCircle.strokeColor = UIColor.init(red: 0.941, green: 0.675, blue: 0.671, alpha: 1.0)
                                    geoFenceCircle.map = self.viewmap
                                }
                                else if type == "Hotel"
                                {
                                    let geoFenceCircle = GMSCircle()
                                    let locar:NSArray = (location?.components(separatedBy:(",")))! as NSArray ;
                                    let lat = NumberFormatter().number(from: locar.object(at: 0) as! String)
                                    let lon = NumberFormatter().number(from: locar.object(at: 1) as! String)
                                    let loosloc = CLLocation.init(latitude: (lat?.doubleValue)!, longitude: (lon?.doubleValue)!)
                                    geoFenceCircle.radius = 100
                                    // Meters
                                    geoFenceCircle.position = CLLocationCoordinate2DMake(loosloc.coordinate.latitude, loosloc.coordinate.longitude)
                                    // Some CLLocationCoordinate2D position
                                    geoFenceCircle.fillColor = UIColor.init(red: 0.000, green: 0.655, blue: 0.976, alpha: 0.4)
                                    //                                            geoFenceCircle.fillColor = UIColor.blue.withAlphaComponent(0.4)
                                    geoFenceCircle.strokeWidth = 1
                                    geoFenceCircle.strokeColor = UIColor.init(red: 0.000, green: 0.655, blue: 0.976, alpha: 1.0)
                                    geoFenceCircle.map = self.viewmap
                                }
                                else if type == "Restaurant"
                                {
                                    let geoFenceCircle = GMSCircle()
                                    let locar:NSArray = (location?.components(separatedBy:(",")))! as NSArray;
                                    let lat = NumberFormatter().number(from: locar.object(at: 0) as! String)
                                    let lon = NumberFormatter().number(from: locar.object(at: 1) as! String)
                                    let loosloc = CLLocation.init(latitude: (lat?.doubleValue)!, longitude: (lon?.doubleValue)!)
                                    geoFenceCircle.radius = 100
                                    // Meters
                                    geoFenceCircle.position = CLLocationCoordinate2DMake(loosloc.coordinate.latitude, loosloc.coordinate.longitude)
                                    // Some CLLocationCoordinate2D position
                                    geoFenceCircle.fillColor = UIColor.init(red: 1.000, green: 0.435, blue: 0.149, alpha: 0.4)
                                    //                                            geoFenceCircle.fillColor = UIColor.orange.withAlphaComponent(0.4)
                                    geoFenceCircle.strokeWidth = 1
                                    geoFenceCircle.strokeColor = UIColor.init(red: 1.000, green: 0.435, blue: 0.149, alpha: 1.0)
                                    geoFenceCircle.map = self.viewmap
                                }
                            }
                        }
                        
//                        let imageurl = (self.detailsarr.object(at: 0) as AnyObject).value(forKey: "loo_image") as? String
//                        print(imageurl!)
//                        if imageurl == "" || imageurl?.characters.count == 0 || imageurl == nil
//                        {
//                            self.scrollview.addTwitterCover(with: UIImage(named: "Toilets-Defualt.png"))
//                        }
//                        else
//                        {
//                            self.scrollview.addTwitterCover(with:  UIImage(named: imageurl!))
//                        }
                        self.focusMapToShowAllMarkers()
                    }
                }
                else
                {
                    NSLog("jsonObject=error");
                }
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }

    
    func addrecentlyviewlink()
    {
        if AFNetworkReachabilityManager.shared().isReachable
        {
            let urlstring : String!
            urlstring = String(format:"%@/add_recently_view.php?user_id=%@&loo_id=%@",kBaseURL,passuserid,passlooid)
            print(urlstring)
            
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: false, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    if jsonObject.count != 0
                    {
                        let copyarr = jsonObject;
                        print(copyarr)
                        let str = (jsonObject.object(at: 0) as AnyObject).value(forKey: "message") as? String
                        if str == "success"
                        {
                            
                        }
                        else
                        {
                            
                            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:str!, withIdentifier:"")
                        }
                        
                    }
                }
                else
                {
                    NSLog("jsonObject=error");
                }
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
    
    
    func focusMapToShowAllMarkers() {
        let myLocation: CLLocationCoordinate2D = self.marker.position
        var bounds: GMSCoordinateBounds = GMSCoordinateBounds(coordinate: myLocation, coordinate: myLocation)
//        for marker in self.markers {
            bounds = bounds.includingCoordinate(marker.position)
            self.viewmap.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 1.0))
            self.viewmap.animate(toZoom: 16.0)
//        }
    }
    
    @IBAction func backbutton(_sender: UIButton)
    {
        let valueToSaves = "comingfromdetails"
        UserDefaults.standard.set(valueToSaves, forKey: "screen")
        
        self.scrollview.removeTwitterCover()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        let valueToSave = "noneedrunlink"
        UserDefaults.standard.set(valueToSave, forKey: "runlink")
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addfavouritebutton(_sender : UIButton)
    {
        if UserDefaults.standard.object(forKey: "Logindetail") == nil
        {
            let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(mainview, animated: true)
        }
        else
        {
            isLiked = !isLiked
            if isLiked == true
            {
                likeButton.setImage(UIImage(named: "LIKE.png"), for: UIControlState())
                likeButton.likeBounce(0.6)
                likeButton.animate()
                isLiked = false
            }
            else
            {
                likeButton.setImage(UIImage(named: "Fav.png"), for: UIControlState())
                likeButton.unLikeBounce(0.4)
                isLiked = true
            }
            self.favlink()
        }
    }
    
    func favlink()
    {
        if AFNetworkReachabilityManager.shared().isReachable
        {
            let urlstring : String!
            isLiked = !isLiked
            if isLiked == true
            {
                urlstring = String(format:"%@/fav.php?user_id=%@&loo_id=%@&fav_status=1",kBaseURL,passuserid,passlooid)
                print(urlstring)
            }
            else
            {
                urlstring = String(format:"%@/fav.php?user_id=%@&loo_id=%@&fav_status=0",kBaseURL,passuserid,passlooid)
                print(urlstring)
            }
            
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: false, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    if jsonObject.count != 0
                    {
                        let copyarr = jsonObject;
                        
                    }
                }
                else
                {
                    NSLog("jsonObject=error");
                }
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
    
    @IBAction func movetonavigationscreen(_sender: UIButton)
    {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "NavigateScreenViewController") as! NavigateScreenViewController
        next.selectdic = self.selectdic
        let nav = UINavigationController.init(rootViewController: next)
        self.present(nav, animated: true, completion: nil)
    }
    
    @IBAction func bookbutton(_sender: UIButton)
    {
        if UserDefaults.standard.object(forKey: "Logindetail") == nil
        {
            let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(mainview, animated: true)
        }
        else
        {
            print(self.selectdic)
            let typestr = self.selectdic.value(forKey: "type") as! String
            if typestr == "Public"
            {
                self.bookinglink()
            }
            else
            {
                let sponser = Bundle.main.loadNibNamed("ConfirmationBookingPopup", owner: self, options: nil)?[0] as! ConfirmationBookingPopup
                KGModal.sharedInstance().show(withContentView: sponser, andAnimated: true)
                self.view.endEditing(true)
                sponser.layer.cornerRadius = 20.0
                sponser.clipsToBounds = true
                print(self.selectdic)
                let imageurl = self.selectdic.value(forKey: "image") as? String;
                if imageurl?.characters.count == 0 || imageurl == "" || imageurl == "http://stitchplus.com/JEDE_rpanel/"
                {
                    sponser.looimg.image = UIImage.init(named: "no-image.png")
                }
                else
                {
                    let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                        print(self)
                    }
                    sponser.looimg.sd_setImage(with: NSURL(string:imageurl!) as URL!, completed: block)
                    sponser.looimg.contentMode = UIViewContentMode.scaleToFill;
                }
                let looname = self.selectdic.value(forKey: "loo_name") as? String;
                if looname == ""
                {
                    sponser.loonamelab.text = looname
                }
                else
                {
                    sponser.loonamelab.text = looname
                }
                
                
                let date = Date()
                let formatter = DateFormatter()
                
                
                formatter.dateFormat = "dd-MMM-yyyy h:mm a"
                formatter.amSymbol = "AM"
                formatter.pmSymbol = "PM"
                
                let result = formatter.string(from: date)
                let dattimearr = result.components(separatedBy: " ") as NSArray
                let dispdate = dattimearr.object(at: 0) as! String
                let dipstime = dattimearr.object(at: 1) as! String
                let ampm = dattimearr.object(at: 2) as! String
                
                
                
                sponser.datelab.text = dispdate
                sponser.timelab.text = String(format: "%@ %@", dipstime,ampm)
                
                sponser.closebutton.addTarget(self, action: #selector(DetailsViewController.closepopup(sender:)), for: UIControlEvents.touchUpInside)
                sponser.termsandconditionsbutton.addTarget(self, action: #selector(DetailsViewController.termsandconditionbut(sender:)), for: UIControlEvents.touchUpInside)
                sponser.bookbutton.addTarget(self, action: #selector(DetailsViewController.bookandnavigatebut(sender:)), for: UIControlEvents.touchUpInside)
            }
        }
    }
    
    @IBAction func movetonavigation2screen(_sender: UIButton)
    {
        if UserDefaults.standard.object(forKey: "Logindetail") == nil
        {
            let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(mainview, animated: true)
        }
        else
        {
            print(self.selectdic)
            let typestr = self.selectdic.value(forKey: "type") as! String
            if typestr == "Public"
            {
                self.bookinglink()
            }
            else
            {
                let sponser = Bundle.main.loadNibNamed("ConfirmationBookingPopup", owner: self, options: nil)?[0] as! ConfirmationBookingPopup
                KGModal.sharedInstance().show(withContentView: sponser, andAnimated: true)
                self.view.endEditing(true)
                sponser.layer.cornerRadius = 20.0
                sponser.clipsToBounds = true
                print(self.selectdic)
                let imageurl = self.selectdic.value(forKey: "image") as? String;
                if imageurl?.characters.count == 0 || imageurl == "" || imageurl == "http://stitchplus.com/JEDE_rpanel/"
                {
                    sponser.looimg.image = UIImage.init(named: "no-image.png")
                }
                else
                {
                    let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                        print(self)
                    }
                    sponser.looimg.sd_setImage(with: NSURL(string:imageurl!) as URL!, completed: block)
                    sponser.looimg.contentMode = UIViewContentMode.scaleToFill;
                }
                let looname = self.selectdic.value(forKey: "loo_name") as? String;
                if looname == ""
                {
                    sponser.loonamelab.text = looname
                }
                else
                {
                    sponser.loonamelab.text = looname
                }
                
                
                let date = Date()
                let formatter = DateFormatter()
                
                
                formatter.dateFormat = "dd-MMM-yyyy h:mm a"
                formatter.amSymbol = "AM"
                formatter.pmSymbol = "PM"
                
                let result = formatter.string(from: date)
                let dattimearr = result.components(separatedBy: " ") as NSArray
                let dispdate = dattimearr.object(at: 0) as! String
                let dipstime = dattimearr.object(at: 1) as! String
                let ampm = dattimearr.object(at: 2) as! String
                
                
                
                sponser.datelab.text = dispdate
                sponser.timelab.text = String(format: "%@ %@", dipstime,ampm)
                
                sponser.closebutton.addTarget(self, action: #selector(DetailsViewController.closepopup(sender:)), for: UIControlEvents.touchUpInside)
                sponser.termsandconditionsbutton.addTarget(self, action: #selector(DetailsViewController.termsandconditionbut(sender:)), for: UIControlEvents.touchUpInside)
                sponser.bookbutton.addTarget(self, action: #selector(DetailsViewController.bookandnavigatebut(sender:)), for: UIControlEvents.touchUpInside)
            }
        }
        
    }
    
    func closepopup(sender:UIButton!)
    {
        print("Button Clicked")
        KGModal.sharedInstance().hide(animated: true)
    }
    
    func termsandconditionbut(sender:UIButton!)
    {
        print("terms and condition Button Clicked")
        KGModal.sharedInstance().hide(animated: true)
    }
    
    func bookandnavigatebut(sender:UIButton!)
    {
        print("Button Clicked")
        KGModal.sharedInstance().hide(animated: true)
        self.bookinglink()
       
    }
    
    @IBAction func sharebuttonclick(_sender : UIButton)
    {
        let sharestr = (self.detailsarr.object(at: 0) as AnyObject).value(forKey: "loo_url") as? String
        let objectsToShare = ["", URL(string: sharestr!)!] as [Any]
        
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.excludedActivityTypes = [UIActivityType.addToReadingList, UIActivityType.postToVimeo, UIActivityType.print, UIActivityType.saveToCameraRoll]
        
        activityVC.popoverPresentationController?.sourceView = _sender
        self.present(activityVC, animated: true, completion: nil)
    }
    
    func bookinglink()
    {
        if AFNetworkReachabilityManager.shared().isReachable
        {
            let urlstring = String(format:"%@/add_booking.php?user_id=%@&loo_id=%@",kBaseURL,passuserid,passlooid)
            print(urlstring)
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: false, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    if jsonObject.count != 0
                    {
                        var str = (jsonObject.object(at: 0) as AnyObject).value(forKey: "message") as? String
                        if str == "success"
                        {
                            let next = self.storyboard?.instantiateViewController(withIdentifier: "MovetoNavigateViewController") as! MovetoNavigateViewController
                            next.selectdic = self.detailsarr.object(at: 0) as! NSMutableDictionary
                            print("LOG: selectdic \(next.selectdic)")
                            next.bookingidstr = (jsonObject.object(at: 0) as AnyObject).value(forKey: "booking_id") as? String
                            self.navigationController?.pushViewController(next, animated: true)
                        }
                        else
                        {
                            if str == self.serverResponseForAlreadyBooked {
                                str = "Ongoing_booking_message".localized
                            }
                            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:str!, withIdentifier:"")
                        }
                    }
                }
                else
                {
                    NSLog("jsonObject=error");
                }
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
    
    @IBAction func Upload(x:AnyObject)
    {
        self.view.endEditing(true)
        let actionsheet = KKActionSheet.init(title:myclass.StringfromKey("Add Images") as String, delegate:self, cancelButtonTitle:myclass.StringfromKey("Cancel") as String, destructiveButtonTitle:nil ,otherButtonTitles:myclass.StringfromKey("Camera") as String,myclass.StringfromKey("Gallery") as String)
        actionsheet.show(in: self.view)
    }
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        if buttonIndex == 1
        {
                let imagePickerController = UIImagePickerController()
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
                {
                    imagePickerController.delegate = self
                    imagePickerController.sourceType = UIImagePickerControllerSourceType.camera;
                    imagePickerController.allowsEditing = false
                    UIApplication.shared.statusBarStyle = UIStatusBarStyle.default;
                    self.present(imagePickerController, animated: true, completion: nil)
                    UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent;
                }
        }
        else if buttonIndex == 2
        {
                let imagePickerController = UIImagePickerController()
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum)
                {
                    imagePickerController.delegate = self
                    imagePickerController.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
                    imagePickerController.allowsEditing = false
                    UIApplication.shared.statusBarStyle = UIStatusBarStyle.default;
                    self.present(imagePickerController, animated: true, completion: nil)
                }
        }

    }
    
    // MARK: - Image Picker Delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        picker .dismiss(animated: true, completion: nil)
        let media =  info[UIImagePickerControllerOriginalImage] as? UIImage
        
        let controller = PECropViewController();
        controller.delegate = self;
        controller.image = media;
        controller.toolbarItems=nil;
        let ratio:CGFloat = 4.0 / 4.0;
        controller.cropAspectRatio = ratio;
        
        controller.dismiss(animated: true, completion:nil)
        
        let navigationController = UINavigationController.init(rootViewController: controller);
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            navigationController.modalPresentationStyle = UIModalPresentationStyle.formSheet;
        }
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker .dismiss(animated: true, completion: nil)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent;
    }
    
    func cropViewController(_ controller: PECropViewController!, didFinishCroppingImage croppedImage: UIImage!, transform: CGAffineTransform, cropRect: CGRect)
    {
        let ims:UIImage=croppedImage
        controller.dismiss(animated: true, completion:nil)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent;
        self.UploadimageWithImage(image: ims, size:CGSize(width: 1000, height: 1000))
    }
    func cropViewControllerDidCancel(_ controller: PECropViewController!)
    {
        controller.dismiss(animated: true, completion:nil)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent;
    }
    
    func UploadimageWithImage(image:UIImage,size:CGSize)
    {
        UIGraphicsBeginImageContext(size)
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let newimage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        let date=NSDate() as NSDate;
        let form = DateFormatter() as DateFormatter
        form.dateFormat="yyyy-MM-dd"
        let form1 = DateFormatter() as DateFormatter
        form1.dateFormat="HH:MM:SS"
        let datesstr = form.string(from: date as Date);
        let timestr = form1.string(from: date as Date);
        let datearr = datesstr.components(separatedBy: "-") as NSArray
        let timearr = timestr.components(separatedBy: ":") as NSArray
        let imageData = UIImageJPEGRepresentation(newimage,0.7);
        let imagename = String(format:"pic_%@_%@_%@_%@_%@_%@.png",datearr.object(at: 0) as! String,datearr.object(at: 1) as! String,datearr.object(at: 2) as! String,timearr.object(at: 0) as! String,timearr.object(at: 1) as! String,timearr.object(at: 2) as! String)
        if AFNetworkReachabilityManager.shared().isReachable
        {
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            hud = MBProgressHUD.showAdded(to: (navigationController?.view)!, animated: true)
            //            // Set the annular determinate mode to show task progress.
            //            hud.mode = MBProgressHUDMode.annularDeterminate
            //            hud.label.text = NSLocalizedString("Loading...", comment: "HUD loading title")
            //            hud.show(animated: true)
            
            //            HUD = MBProgressHUD .showAdded(to: (self.navigationController?.view)!, animated: true)
            //            HUD.label.text = NSLocalizedString("Loading", comment: "HUD loading title")
            //            HUD.show(animated: true)
            
            
            
            
            //            let  yval = UserDefaults()
            //            yval.setValue("10", forKey: "yval")
            //            yval.setValue("#0063AD", forKey:"loadcolor")
            //            yval.setValue("photo", forKey: "Type")
            
            //            imageHud = MyHUDProgress.init(view: self.view, yavl: 10);
            //            imageHud.hidehud(10.0)
            //            //            imageHud.frame = CGRectMake(0,0,UIScreen.main.bounds.size.width,UIScreen.mainScreen.bounds.size.height);
            //            imageHud.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            //            self.view.addSubview(imageHud)
            
            let request : NSMutableURLRequest = AFHTTPRequestSerializer().multipartFormRequest(withMethod: "POST", urlString:kBaseDomainUrl + "/upload_loo_image.php", parameters: nil, constructingBodyWith: { (formData:AFMultipartFormData) -> Void in
                
                formData.appendPart(withFileData: imageData!, name:"uploaded_file", fileName:imagename, mimeType: "image/png")
                
            }, error: nil)
            
            let conf : URLSessionConfiguration = URLSessionConfiguration.default
            
            let manager : AFURLSessionManager = AFURLSessionManager(sessionConfiguration: conf)
            
            manager.responseSerializer = AFHTTPResponseSerializer()
            let uploadTask:URLSessionUploadTask = manager.uploadTask(withStreamedRequest: request as URLRequest, progress:{(uploadProgress:Progress!) -> Void in
                
                DispatchQueue.main.async {
                    //  self.imageHud.progress.setProgress(Float(uploadProgress.fractionCompleted), animated: true)
                }
                
            })
            {(response, responseObject, error) -> Void in
                MBProgressHUD.hide(for: self.view, animated: true)
                
                //                print(error)
                //                self.imageHud.removeFromSuperview();
                //                self.imageHud = nil;
                if((error == nil))
                {
//                    self.storarr.add(String(format:"http://reliefz.com/doctor/images/pain/%@",imagename))
//                    print(self.storarr)
//                    print(self.storarr.count)
//                    if self.storarr.count != 0
//                    {
//                        self.storeimgviewheight.constant = 146
//                    }
//                    else
//                    {
//                        self.storeimgviewheight.constant = 120
//                    }
//                    
//                    self.collectionimaage.reloadData()
                    if AFNetworkReachabilityManager.shared().isReachable
                    {
                        let urlstring = String(format:kBaseURL + "/loo_image_update.php?loo_id=%@&image="
                            + kBaseDomainUrl + "/images/loo/%@",self.passlooid,imagename)
                        print(urlstring)
                        self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: false, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                            if Stats == true
                            {
                                NSLog("jsonObject=%@", jsonObject);
                                if jsonObject.count != 0
                                {
                                    //                        self.datasarr .removeAllObjects()
                                    //                        self.datasarr.addObjects(from: jsonObject as [AnyObject])
                                    //                        print(self.datasarr)
                                    //                        self.cardStackView.dataSource = self
                                    //                        self.cardStackView.delegate = self
                                    //                        self.orderNo = self.datasarr.count
                                    let sts = (jsonObject.object(at: 0) as AnyObject).value(forKey: "message") as? String
                                    if sts == "success"
                                    {
                                        self.scrollview.removeTwitterCover()
                                        self.scrollview.addTwitterCover(with: image)
                                    }
                                    else
                                    {
                                        self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:"Error Occured", withIdentifier:"internet")
                                    }
                                }
                            }
                            else
                            {
                                NSLog("jsonObject=error");
                            }
                        })
                    }
                    else
                    {
                        self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
                    }
                }
                else
                {
                    self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert: self.myclass.StringfromKey("Errorwhieleupload"), withIdentifier:"error")
                }
            }
            
            uploadTask.resume()
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert: self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }

  
    
      /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
