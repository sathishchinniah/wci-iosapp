//
//  NotificationTableViewCell.swift
//  JEDE_r
//
//  Created by Exarcplus on 21/09/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    
    @IBOutlet var imageview : UIImageView!
    @IBOutlet var messagelab : UILabel!
    @IBOutlet var dateandtimelab : UILabel!
    @IBOutlet var namelab : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
