//
//  NewRatingPopUp.swift
//  JEDE_r
//
//  Created by Exarcplus on 21/08/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit

class NewRatingPopUp: UIView{
    
    @IBOutlet weak var maleBtn : UIButton!
    @IBOutlet weak var femaleBtn : UIButton!
    @IBOutlet weak var unisexBtn : UIButton!
    @IBOutlet weak var wheelchairBtn : UIButton!
    @IBOutlet weak var male : UILabel!
    @IBOutlet weak var female : UILabel!
    @IBOutlet weak var unisex : UILabel!
    @IBOutlet weak var wheelchair : UILabel!

    
}
