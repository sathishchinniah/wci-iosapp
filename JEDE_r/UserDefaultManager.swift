//
//  UserDefaultManager.swift
//  JEDE_r
//
//  Created by Developer on 4/8/18.
//  Copyright © 2018 Exarcplus. All rights reserved.
//

import Foundation
class UserDefaultManager {
    static let sharedManager = UserDefaultManager()
    let kLanguageName = "Languagename"
    let wciLanguageKeySet = "WciLanguageKeySet"
    let iAgreeKey = "AGREE"
    
    //Languagename
    func setLanguageName(languageName:String)  {
        let defaults = UserDefaults.standard
        defaults.set(languageName,forKey: kLanguageName)
        defaults.synchronize()
    }
    
    func getLanguageName() -> String {
        let defaults = UserDefaults.standard
        return (defaults.object(forKey: kLanguageName) as? String) ?? "English"
    }
    
    func removeLanguageName() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: kLanguageName)
    }
    
    func setLanguageKeySet() {
        let defaults = UserDefaults.standard
        defaults.set("LANGUAGE", forKey: wciLanguageKeySet)
    }
    
    func getLanguageKeySet() -> Bool {
        let defaults = UserDefaults.standard
        let value = defaults.object(forKey: wciLanguageKeySet)
        if (value != nil) {
            return true
        } else{
            return false
        }
        return false
    }
    
    func userAgreedTerms() -> Bool {
        let defaults = UserDefaults.standard
        let value = defaults.object(forKey: iAgreeKey)
        if (value != nil) {
            return true
        } else{
            return false
        }
        return false
    }
}
