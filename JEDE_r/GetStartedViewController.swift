//
//  GetStartedViewController.swift
//  JEDE_r
//
//  Created by Exarcplus iOS 2 on 01/05/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import LGSideMenuController


class GetStartedViewController: UIViewController, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var femaleimg : UIImageView!
    @IBOutlet weak var maleimg : UIImageView!
    @IBOutlet weak var uniseximg : UIImageView!
    @IBOutlet weak var wheelchairimg : UIImageView!
    @IBOutlet weak var carparkingimg : UIImageView!
    
    @IBOutlet weak var getstartedbutton : UIButton!
    
    var genderstring : String!
    var wheelchairstring : String!
    var parkingstring : String!
    var categorystring = NSMutableArray()
    
    
    var app = AppDelegate()
    
    let transition = CircularTransition()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        femaleimg.image = UIImage.init(named: "Female_unselect.png")
        maleimg.image = UIImage.init(named: "Male_unselect.png")
        uniseximg.image = UIImage.init(named: "Unisex_unselect.png")
        wheelchairimg.image = UIImage.init(named: "Wheel-Chair_unselect.png")
        carparkingimg.image = UIImage.init(named: "Car_unselect.png")
        
        getstartedbutton.layer.cornerRadius = getstartedbutton.frame.size.width / 2
        
        genderstring = ""
        wheelchairstring = ""
        parkingstring = ""
        
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    //
    //        let mainVC = segue.destination as! MainScreenViewController
    ////        let nav = UINavigationController.init(rootViewController: mainVC)
    //        mainVC.transitioningDelegate = self
    //        mainVC.modalPresentationStyle = .custom
    //
    ////        let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
    ////        let nav = UINavigationController.init(rootViewController: mainview)
    ////        let appDelegate = UIApplication.shared.delegate as! AppDelegate
    ////        appDelegate.SideMenu.rootViewController = nav
    ////
    ////        self.present(appDelegate.SideMenu, animated:false, completion: nil)
    //
    //    }
    //
    //    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    //        transition.transitionMode = .present
    //        transition.startingPoint = getstartedbutton.center
    //        //transition.circleColor = getstartedbutton.backgroundColor!
    //
    //        return transition
    //    }
    //
    //    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    //
    //        transition.transitionMode = .dismiss
    //        transition.startingPoint = getstartedbutton.center
    //        //transition.circleColor = getstartedbutton.backgroundColor!
    //
    //        return transition
    //
    //    }
    
    
    @IBAction func femalebutton(_sender : UIButton)
    {
        if femaleimg.image == UIImage.init(named: "Female_unselect.png")
        {
            femaleimg.image = UIImage.init(named: "Female_select.png")
            //            genderstring.add("Female")
            genderstring = "Female"
            maleimg.image = UIImage.init(named: "Male_unselect.png")
            uniseximg.image = UIImage.init(named: "Unisex_unselect.png")
        }
        else
        {
            femaleimg.image = UIImage.init(named: "Female_unselect.png")
            //            genderstring.remove("Female")
            genderstring = ""
        }
    }
    
    @IBAction func malebutton(_sender : UIButton)
    {
        if maleimg.image == UIImage.init(named: "Male_unselect.png")
        {
            maleimg.image = UIImage.init(named: "Male_select.png")
            //            genderstring.add("Male")
            genderstring = "Male"
            femaleimg.image = UIImage.init(named: "Female_unselect.png")
            uniseximg.image = UIImage.init(named: "Unisex_unselect.png")
        }
        else
        {
            maleimg.image = UIImage.init(named: "Male_unselect.png")
            //            genderstring.remove("Male")
            genderstring = ""
        }
    }
    
    @IBAction func unisexbutton(_sender : UIButton)
    {
        if uniseximg.image == UIImage.init(named: "Unisex_unselect.png")
        {
            maleimg.image = UIImage.init(named: "Male_unselect.png")
            femaleimg.image = UIImage.init(named: "Female_unselect.png")
            uniseximg.image = UIImage.init(named: "Unisex_select.png")
            //            genderstring.add("Unisex")
            genderstring = "Unisex"
        }
        else
        {
            uniseximg.image = UIImage.init(named: "Unisex_unselect.png")
            //            genderstring.remove("Unisex")
            genderstring = ""
        }
    }
    
    @IBAction func wheelchairbutton(_sender : UIButton)
    {
        if wheelchairimg.image == UIImage.init(named: "Wheel-Chair_unselect.png")
        {
            wheelchairimg.image = UIImage.init(named: "Wheel-Chair_select.png")
            wheelchairstring = "1"
        }
        else
        {
            wheelchairimg.image = UIImage.init(named: "Wheel-Chair_unselect.png")
            wheelchairstring = ""
        }
    }
    
    @IBAction func carparkingbutton(_sender : UIButton)
    {
        if carparkingimg.image == UIImage.init(named: "Car_unselect.png")
        {
            carparkingimg.image = UIImage.init(named: "Car_select.png")
            parkingstring = "1"
        }
        else
        {
            carparkingimg.image = UIImage.init(named: "Car_unselect.png")
            parkingstring = ""
        }
    }
    
    @IBAction func getstartedbutton(_sender : UIButton)
    {
        //        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //        let expandedViewController = storyboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
        //
        ////        expandedViewController.viewBgColor = _sender.backgroundColor!
        //        expandedViewController.transitioningDelegate = self as? UIViewControllerTransitioningDelegate
        //        expandedViewController.modalPresentationStyle = .custom
        //
        //        self.modalPresentationStyle = .custom
        //        self.present(expandedViewController, animated: true, completion: nil)
        
        //        let next = self.storyboard?.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
        //        let nav = UINavigationController.init(rootViewController: next)
        //        self.present(nav, animated: true, completion: nil)
        
        let nsmutdic = NSMutableDictionary()
        nsmutdic.setObject(genderstring, forKey:"gender" as NSCopying);
        nsmutdic.setObject("", forKey:"KM" as NSCopying);
        nsmutdic.setObject(wheelchairstring, forKey:"wheelchair" as NSCopying);
        nsmutdic.setObject(parkingstring, forKey:"parking" as NSCopying);
        nsmutdic.setObject("", forKey:"rating" as NSCopying);
        nsmutdic.setObject("", forKey:"distance" as NSCopying);
        nsmutdic.setObject("", forKey:"price" as NSCopying);
        nsmutdic.setObject(categorystring, forKey:"category" as NSCopying);
        print(nsmutdic)
        let data = NSKeyedArchiver.archivedData(withRootObject: nsmutdic)
        UserDefaults.standard.set(data, forKey:"Filterdetail")
        UserDefaults.standard.synchronize()
        
        let valueToSaves = "comingfromgetstart"
        UserDefaults.standard.set(valueToSaves, forKey: "screen")
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let mainview = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
        let valueToSave = "noneedrunlink"
        UserDefaults.standard.set(valueToSave, forKey: "runlink")
        mainview.getdic = nsmutdic
        let nav = UINavigationController.init(rootViewController: mainview)
        appDelegate.SideMenu = LGSideMenuController.init(rootViewController: nav)
        appDelegate.SideMenu.setLeftViewEnabledWithWidth(280, presentationStyle: .slideAbove, alwaysVisibleOptions:[])
        appDelegate.SideMenuView = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "SidemenuViewController") as! SidemenuViewController
        appDelegate.SideMenu.leftViewStatusBarVisibleOptions = .onAll
        appDelegate.SideMenu.leftViewStatusBarStyle = .lightContent
        //        appDelegate.SideMenuView.selectedpage = 0;
        var rect = appDelegate.SideMenuView.view.frame;
        rect.size.width = 280;
        appDelegate.SideMenuView.view.frame = rect
        appDelegate.SideMenu.leftView().addSubview(appDelegate.SideMenuView.view)
        print(appDelegate.SideMenu)
        self.present(appDelegate.SideMenu, animated:true, completion: nil)
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
