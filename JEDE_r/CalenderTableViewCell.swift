//
//  CalenderTableViewCell.swift
//  JEDE_r
//
//  Created by Exarcplus on 9/25/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit

class CalenderTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var colouredView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
