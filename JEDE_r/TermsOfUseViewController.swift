//
//  TermsOfUseViewController.swift
//  JEDE_r
//
//  Created by apple on 15/06/18.
//  Copyright © 2018 Exarcplus. All rights reserved.
//

import UIKit

class TermsOfUseViewController: UIViewController {

    @IBOutlet weak var pageHeading: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var agreeText: UILabel!
    @IBOutlet weak var checkBox: UIButton!
    @IBOutlet weak var contentView: UITextView!
    @IBOutlet weak var contentTitle: UILabel!
    
    let checkedImage = "checkboxselect.png"
    let uncheckedImage = "checkboxunselect.png"
    
    var termsOfUseDescription = String()
    var termsOfUseCheckedBefore = Bool()
    
    var isChecked = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isChecked = termsOfUseCheckedBefore
        if isChecked {
            checkBox.setBackgroundImage(UIImage.init(named: checkedImage), for: .normal)
        } else {
            checkBox.setBackgroundImage(UIImage.init(named: uncheckedImage), for: .normal)
        }
        
        checkBox.setTitle("", for: .normal)
        //Localization
        pageHeading.text = "term_con_layout_con".localized
        contentTitle.text = "term_con_layout_following".localized
        continueButton.setTitle("continue_button".localized, for: .normal)
        agreeText.text = "i_agree_this_terms_and_conditions".localized
        contentView.text = ""
        contentView.text = termsOfUseDescription
        
        //Actions
        self.continueButton.addTarget(self, action: #selector(continueButtonAction), for: .touchUpInside)
        self.checkBox.addTarget(self, action: #selector(checkBoxAction), for: .touchUpInside)
    }
    
    func checkBoxAction() {
        if isChecked {
          checkBox.setBackgroundImage(UIImage.init(named: uncheckedImage), for: .normal)
        } else {
          checkBox.setBackgroundImage(UIImage.init(named: checkedImage), for: .normal)
        }
        isChecked = !isChecked
    }
    
    func continueButtonAction(){
        if !isChecked {
            let alert = UIAlertController(title: "terms_agree_button".localized, message: "term_con_layout_con".localized, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
