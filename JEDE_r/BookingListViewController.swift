//
//  BookingListViewController.swift
//  JEDE_r
//
//  Created by Exarcplus on 27/05/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import AFNetworking
import SDWebImage
import Firebase

class BookingListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate, GMSMapViewDelegate {
    
    var myclass : MyClass!
    @IBOutlet weak var tablelist : UITableView!
    var datasarr = NSMutableArray()
    @IBOutlet weak var mydate : UILabel!
    
    var datearr = NSMutableArray()
    var detailsarr = NSMutableArray()
    var Userdic = NSMutableDictionary()
    var passuserid : String!
    var passloc : String!
    var locmanager:CLLocationManager!
    var currentlocation = CLLocation()
    var movelocation = CLLocation()
    var selectdic : NSDictionary!
    var passcurrentloc : String!
    var passlooid : String!
    var bookingidstr : String!
    @IBOutlet weak var viewmap: GMSMapView!

    override func viewDidLoad() {
        super.viewDidLoad()

        myclass = MyClass()
        let view = UIView(frame:  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 20))
        view.backgroundColor = UIColor.init(red: 27.0/255.0, green: 34.0/255.0, blue: 40.0/255.0, alpha: 1.0)
        self.view.addSubview(view)
        self.navigationController?.navigationBar.isHidden = false
        
        if selectdic != nil
        {
            print(selectdic)
            passlooid = selectdic.value(forKey: "loo_id") as? String
            print(passlooid)
         bookingidstr = selectdic.value(forKey: "booking_id") as? String
//            print(bookingidstr)
            passcurrentloc = selectdic.value(forKey: "loo_location") as? String
            print(passcurrentloc)
            
            //            let imageurl = selectdic.value(forKey: "image") as? String
            //            print(imageurl!)
            //            if imageurl == "" || imageurl?.characters.count == 0 || imageurl == nil
            //            {
            //                scrollview.addTwitterCover(with: UIImage(named: "Toilets-Defualt.png"))
            //            }
            //            else
            //            {
            //                let url = URL(string: imageurl!)
            //                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            //                scrollview.addTwitterCover(with: UIImage(data: data!))
            //            }
        }
        else
        {
            //            self.scrollview.addTwitterCover(with: UIImage(named: "Toilets-Defualt.png"))
        }
        
        if UserDefaults.standard.object(forKey: "Logindetail") == nil
        {
            passuserid = ""
        }
        else
        {
            let placesData = UserDefaults.standard.object(forKey: "Logindetail") as? NSDictionary
            self.Userdic.removeAllObjects()
            if placesData != nil{
                self.Userdic.addEntries(from: (placesData as? [String : Any])!)
            }
            print(self.Userdic)
            passuserid = self.Userdic.value(forKey: "user_id") as? String
            print(passuserid)
        }
        
        locmanager = CLLocationManager()
        locmanager.delegate = self
        locmanager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locmanager.activityType = .automotiveNavigation
        locmanager.requestAlwaysAuthorization()
        viewmap.settings.myLocationButton=false
        viewmap.isMyLocationEnabled = true
        viewmap.settings.compassButton=true
        viewmap.delegate = self;
        
      //  self.loadData()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(UserDefaultManager.sharedManager.getLanguageName() == "English"){
            self.title = "BOOKING LIST"
        }
        else if(UserDefaultManager.sharedManager.getLanguageName() == "German"){
            self.title = "BUCHUNGSLISTE"
        }
        else if(UserDefaultManager.sharedManager.getLanguageName() == "French"){
            self.title = "LISTE DE RÉSERVATION"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        print("didUpdateToLocation: %@", locations.last)
        
        currentlocation = locations.last!
        viewmap.clear()
        let camera = GMSCameraPosition.init(target: currentlocation.coordinate, zoom: 11, bearing: 0, viewingAngle: 0)
        viewmap .animate(to: camera)
    }
    
    func mapView(_ mapView: GMSMapView!, idleAt position: GMSCameraPosition!)
    {
        let point = mapView.center
        let coor = mapView.projection .coordinate(for: point)
        let loc = CLLocation.init(latitude: coor.latitude, longitude: coor.longitude)
        currentlocation = loc;
        self .getAddressFromLatLon(location: currentlocation)
    }
    
    func getAddressFromLatLon(location:CLLocation)
    {
        let geoCoder = GMSGeocoder()
        self.movelocation = location
        self.loadData()
        geoCoder.reverseGeocodeCoordinate(location.coordinate, completionHandler: {(respones,err) in
            //            if err == nil
            //            {
            if respones != nil
            {
                let gmaddr = respones?.firstResult()
                print(gmaddr!)
                //                    self.currentadddress = gmaddr
                //                    var addrarray : NSMutableArray! = NSMutableArray()
                //                    let count:Int = (gmaddr?.lines.count)!
                //                    for c in 0 ..< count
                //                    {
                //                        addrarray.add(gmaddr?.lines[c] as! String)
                //                    }
                //
                //                    let formaddr = addrarray.componentsJoined(by: ", ");
                //                    NSLog("%@",formaddr)
                //                    addrarray = nil
                //                    self.locationtext.text = formaddr
                
                //                    self.dummystr = UserDefaults.standard.string(forKey: "runlink")
                //                    if self.dummystr == nil || self.dummystr == "" || self.dummystr == "needrunlink"
                //                    {
                //                        print(self.dummystr)
                //                        let timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.runlink), userInfo: nil, repeats: false)
                //                        let valueToSave = "noneedrunlink"
                //                        UserDefaults.standard.set(valueToSave, forKey: "runlink")
                //                    }
                //                    else
                //                    {
                //                        print(self.dummystr)
                //                    }
                
            }
            else
            {
                //                    self.locationtext.text = "Address not found"
            }
            //            }
            //            else
            //            {
            //                //self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("JEDE_r"), withAlert:self.myclass.StringfromKey("errorinretrive"), withIdentifier:"error")
            //            }
        })
    }
    
    func loadData()
    {
        Loader.addLoaderTo(self.tablelist)
        if AFNetworkReachabilityManager.shared().isReachable
        {
            let urlstring = String(format:"%@/booking_list.php?user_id=%@",kBaseURL,passuserid)
            print(urlstring)
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: false, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    if jsonObject.count != 0
                    {
                        self.datasarr .removeAllObjects()
                        self.datasarr.addObjects(from: jsonObject as [AnyObject])
                        print(self.datasarr)
                        self.tablelist.register(UINib.init(nibName:"BookingListTableViewCell", bundle: nil),forCellReuseIdentifier: "BookingListTableViewCell")
                        self.tablelist.estimatedRowHeight = 143;
                        
//                        let ss = self.datasarr.value(forKey: "date") as! NSArray
//                        self.datearr.addObjects(from: ss.mutableCopy() as! [AnyObject])
//                        print(self.datearr)
                        
                        for c in 0 ..< self.datasarr.count
                        {
                            let datearrs = (self.datasarr.object(at: c) as AnyObject).value(forKey: "date")
                            print(datearrs)
                           
                                let dateFormatterGet = DateFormatter()
                                //dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                                dateFormatterGet.dateFormat = "yyyy-MM-dd"
                                
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "EEEE, dd MMM yyyy"
                                
                                let date: Date? = dateFormatterGet.date(from: datearrs! as! String)
                                print(dateFormatter.string(from: date!))
                                let dddi = String(format: "%@", dateFormatter.string(from: date!))
                            self.datearr.add(dddi)
                            print(self.datearr)
                        }
                        print(self.datearr)
                        
                       
//                        for i in 0 ..< self.datasarr.count
//                        {
//                            let datearrs = (self.datasarr.object(at: i) as AnyObject).value(forKey: "details") as! NSArray
//                            print(datearrs)
//                            self.detailsarr.addObjects(from: datearrs.mutableCopy() as! [AnyObject])
//                            print(self.detailsarr)
//                        }
                        
                        for i in 0 ..< self.datasarr.count
                        {
                            
                            let datearrs = (self.datasarr.object(at: i) as AnyObject).value(forKey: "details")
                        
                            
                            print(datearrs)
                            self.detailsarr.add(datearrs)
                            print(self.detailsarr)
                        }
                        
                        print(self.datearr)
                        print(self.detailsarr)
                        
//                        let dd = self.datasarr.value(forKey: "details") as! NSArray
//                        self.detailsarr.addObjects(from: dd.mutableCopy() as! [AnyObject])
//                        print(self.detailsarr)
                        
//                        let dates = self.datasarr.value(forKey: "date") as! NSArray
//                        print(dates)
//                        
//                        let deeeta = self.datasarr.value(forKey: "details") as! NSArray
//                        print(deeeta)
//                        
//                        let mdic = NSMutableDictionary()
//                        let daar = NSMutableArray()
//                        for c in 0 ..< dates.count
//                        {
//                            let datearrs = dates.object(at: c)
//                            print(datearrs)
//                            daar.add(datearrs)
//                            print(daar)
//                        }
//                        mdic.setValue(daar, forKey: "Heading")
//                        self.datearr.add(mdic)
//                        print(self.datearr)
//                        
//                        let sdic = NSMutableDictionary()
//                        let deesr = NSMutableArray()
//                        for i in 0 ..< deeeta.count
//                        {
//                            let datearrs = deeeta.object(at: i)
//                            print(datearrs)
//                            deesr.add(datearrs)
//                            print(deesr)
//                        }
//                        sdic.setValue(deesr, forKey: "List")
//                        self.detailsarr.add(sdic)
//                        print(self.detailsarr)
//                        
//                        
//                        print(self.datearr)
//                        print(self.detailsarr)
                        
                        self.tablelist.reloadData()
                        Loader.removeLoaderFrom(self.tablelist)
                    }
                }
                else
                {
                    NSLog("jsonObject=error");
                }
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        print(self.datearr)
        print(self.datearr.count)
        return self.datearr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        let dic = self.detailsarr.object(at: section) as! NSArray;
        print(dic)
        return dic.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
       
     print(self.datearr)
        let sponser = Bundle.main.loadNibNamed("BookingheaderView", owner: self, options: nil)?[0] as! BookingheaderView
        var languageBookingList = self.datearr.object(at: section) as? String
        sponser.headerlab.text = languageBookingList?.localized
        sponser.borderview.layer.shadowColor = UIColor.black.cgColor
        sponser.borderview.layer.shadowOpacity = 0.4
        sponser.borderview.layer.shadowOffset = CGSize.zero
        sponser.borderview.layer.shadowRadius = 2
        return sponser
    }
    
    func tableView(_  tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 143
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:BookingListTableViewCell?=tableView.dequeueReusableCell(withIdentifier: "BookingListTableViewCell") as? BookingListTableViewCell
        cell?.selectionStyle = .none
        
        let dic = self.detailsarr.object(at: indexPath.section) as! NSArray;
        print(dic)
        let type = (dic.object(at: indexPath.row) as AnyObject).value(forKey: "category_name") as? String
        
        if let imageurl = (dic.object(at: indexPath.row) as AnyObject).value(forKey: "loo_image") as? String {
            if imageurl.characters.count == 0 || imageurl.isEmpty {
                if type != nil || !(type?.isEmpty)! {
                    let imageName = self.getDefaultImageForCategory(categoryName: type!)
                    cell?.looimg.image = UIImage.init(named: imageName)
                } else {
                    cell?.looimg.image = UIImage.init(named: "default_image.png")
                }
            } else {
                let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                    print(self)
                }
                let imageAbsoluteUrlList:[String] = imageurl.components(separatedBy: ",")
                let imageAbsoluteUrl = imageAbsoluteUrlList.first
                cell?.looimg.sd_setImage(with: NSURL(string:imageAbsoluteUrl!) as URL?, completed: block)
                cell?.looimg.contentMode = UIViewContentMode.scaleToFill;
            }
        } else {
            cell?.looimg.image = UIImage.init(named: "default_image.png")
        }
        let namestr = (dic.object(at: indexPath.row) as AnyObject).value(forKey: "loo_name") as? String
        if namestr == nil || namestr == ""
        {
            cell?.loonamelab.text = ""
        }
        else
        {
            cell?.loonamelab.text = namestr
        }
        
        let price = (dic.object(at: indexPath.row) as AnyObject).value(forKey: "price") as? String
        if price != "" && price != nil
        {
            cell?.pricelab.text = String(format: "CHF %@",price!)
        }
        else
        {
            cell?.pricelab.text = "FREE"
        }
        
        
        let time = (dic.object(at: indexPath.row) as AnyObject).value(forKey: "time") as? String
        if time != ""
        {
            cell?.lootimelab.text = String(format: "%@", time!)
        }
        else
        {
            cell?.lootimelab.text = String(format: " 24Hrs")
        }
        
        
        
        print(type)
        if type == ""
        {
            cell?.categoryimg.image = UIImage.init(named: "Public")
        }
        else if type == "Gas Station"
        {
            cell?.categoryimg.image = UIImage.init(named: "Gas")
        }
        else if type == "Hotel"
        {
            cell?.categoryimg.image = UIImage.init(named: "Hotels")
        }
        else if type == "Restaurant"
        {
            cell?.categoryimg.image = UIImage.init(named: "Restaurant")
        }
        else if type == "Shopping Mall"
        {
            cell?.categoryimg.image = UIImage.init(named: "shoping_marker")
        }
        else if type == "Bar"
        {
            cell?.categoryimg.image = UIImage.init(named: "bar_marker")
        }
        
        
        let stas = (dic.object(at: indexPath.row) as AnyObject).value(forKey: "status") as? String
        if stas != ""
        {
            cell?.statuslab.text = stas
        }
        else
        {
            cell?.statuslab.text = ""
        }
        
        
        let addre = (dic.object(at: indexPath.row) as AnyObject).value(forKey: "loo_address") as? String
        if addre == nil || addre == ""
        {
            cell?.looaddresslab.text = ""
        }
        else
        {
            cell?.looaddresslab.text = addre
        }
        return cell!
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let dic = self.detailsarr.object(at: indexPath.section) as! NSArray;
        print(dic)
        passloc = String(format: "%f,%f", movelocation.coordinate.latitude,movelocation.coordinate.longitude)
        print(passloc)
        //self.canclelink()
        
       let BookingId = (dic.object(at: indexPath.row) as AnyObject).value(forKey: "booking_id") as? String ?? "NA"
       let looName =  (dic.object(at: indexPath.row) as AnyObject).value(forKey: "loo_name") as? String ?? "NA"
       let BookingStatus = (dic.object(at: indexPath.row) as AnyObject).value(forKey: "status") as? String ?? "NA"
        let UserId =  UserDefaults.standard.string(forKey: "user_id") ?? "NA"
        FIRAnalytics.logEvent(withName: "booking_history_item", parameters: [
            "user_id" : UserId,
            "booking_id" : BookingId,
            "loo_name" : looName,
            "booking_status" : BookingStatus,
            "login_device" : "iOS",
            "action_name" : "booking_list_item_click"
            ])
        let next = self.storyboard?.instantiateViewController(withIdentifier: "MovetoNavigateViewController") as! MovetoNavigateViewController
        next.selectdic = dic.object(at: indexPath.row) as! NSMutableDictionary
        next.passLocationDetails = passloc
        next.bookingidstr = (dic.object(at: indexPath.row) as AnyObject).value(forKey: "booking_id") as? String
        var stas = (dic.object(at: indexPath.row) as AnyObject).value(forKey: "status") as? String
        if(stas == "on going"){
            self.navigationController?.pushViewController(next, animated: true)
        }
    }
    
    func getDefaultImageForCategory(categoryName:String) -> String {
        var imageName = "public"
        
        if categoryName == "" {
            imageName = "public"
        } else if categoryName == "Gas Station" {
            imageName = "Gas"
        } else if categoryName == "Hotel" {
            imageName = "Hotels"
        } else if categoryName == "Restaurant" {
            imageName = "Restaurant"
        } else if categoryName == "Shopping Mall" {
            imageName = "shoping_marker"
        } else if categoryName == "Bar" {
            imageName = "bar_marker"
        } else if categoryName == "Public" {
            imageName = "Public-Icon"
        } else if categoryName == "Temp" {
            imageName = "Temp-Icon"
        } else if categoryName == "Shower" {
            imageName = "Shower-Icon"
        }
        return imageName
    }
    
    
    @IBAction func OpenSideMenu(_ x:AnyObject)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.SideMenu.showLeftView(animated: true, completionHandler: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadsidemenu"), object: nil, userInfo: nil)
    }
}
