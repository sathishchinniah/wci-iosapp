//
//  Extension.swift
//  JEDE_r
//
//  Created by apple on 16/04/18.
//  Copyright © 2018 Exarcplus. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func showAlert(_ title: String?, message: String?, actionMessage: String) {
        let alertVC = UIAlertController.init(title: title,message: message, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction.init(title: actionMessage, style: .default, handler: nil))
        present(alertVC, animated: true, completion: nil)
    }
}

