//
//  MyHUDProgress.m
//  Foodboon
//
//  Created by Vignesh on 28/08/15.
//  Copyright (c) 2015 index. All rights reserved.
//

#import "MyHUDProgress.h"

@implementation MyHUDProgress
@synthesize progress;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        float y=0;
        CGSize result = [[UIScreen mainScreen] bounds].size;
        NSUserDefaults *yval=[[NSUserDefaults alloc]init];
        NSString *str=[yval valueForKey:@"yval"];
        NSString *type=[yval valueForKey:@"Type"];
        UIColor *loadcolor = [self colorFromHexString:@"#FFFFFF"];
        if ([type isEqualToString:@"Load"])
        {
            _Animationview = [[UIView alloc]initWithFrame:CGRectMake(0,0,60,60)];
            _Animationview.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2);
            _Animationview.backgroundColor = [UIColor clearColor];
          
            UILabel *bglabel = [[UILabel alloc]initWithFrame:CGRectMake(0,0,60,60)];
            bglabel.backgroundColor = loadcolor;
            bglabel.alpha = 0;
            [_Animationview addSubview:bglabel];
            
//            MDProgress *hus = [[MDProgress alloc]initWithFrame:CGRectMake(6,6,48,48)];
//            hus.progressColor = [UIColor colorWithRed:0.008 green:0.682 blue:0.937 alpha:1.00];
//            hus.trackColor = [UIColor colorWithRed:1.000 green:1.000 blue:1.000 alpha:1.00];
//            [_Animationview addSubview:hus];
            
            [self addSubview:_Animationview];
            
            _Animationview.layer.cornerRadius = 10.0;
            _Animationview.layer.masksToBounds = YES;
        }
        else
        {
            if ([str isEqualToString:@"58"])
            {
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    if(result.height <= 480)
                    {
                        y=49;
                    }
                    else if(result.height <= 568)
                    {
                        y=58;
                    }
                    else if(result.height <= 667)
                    {
                        y=68;
                    }
                    else
                    {
                        y=75;
                    }
                }
                else
                {
                    
                }
                
            }
            else if ([str isEqualToString:@"99"])
            {
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    if(result.height <= 480)
                    {
                        y=82;
                    }
                    else if(result.height <= 568)
                    {
                        y=97;
                    }
                    else if(result.height <= 667)
                    {
                        y=114;
                    }
                    else
                    {
                        y=126;
                    }
                }
                else
                {
                    
                }
                
            }
            else if ([str isEqualToString:@"95"])
            {
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    if(result.height <= 480)
                    {
                        y=78;
                    }
                    else if(result.height <= 568)
                    {
                        y=93;
                    }
                    else if(result.height <= 667)
                    {
                        y=110;
                    }
                    else
                    {
                        y=124;
                    }
                }
                else
                {
                    
                }
                
            }
            else
            {
                y=0;
            }
            progress=[[UIProgressView alloc]initWithFrame:CGRectMake(-2,y,result.width+4,2)];
            progress.progressTintColor = loadcolor;
            progress.tintColor = [self colorFromHexString:@"#FFFFFF"];
            progress.progress=0.0;
            [self addSubview:progress];
        }
    }
    return self;
}
- (id)initWithView:(UIView *)view yavl:(float)y
{
    NSLog(@"%f",y);
    progress.frame=CGRectMake(0,y,320,2);
    NSAssert(view, @"View must not be nil.");
    return [self initWithFrame:view.bounds];
}
-(void)hidehud:(float)after
{
    [self performSelector:@selector(cleanUp) withObject:nil afterDelay:after];
}
- (void)cleanUp
{
    [_activity stopAnimating];
    [self removeFromSuperview];
}

-(UIColor *)colorFromHexString:(NSString *)hexString
{
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}
-(UIImage*)changeimagecolor:(UIImage*)fromimage withcolor:(UIColor*)color
{
    UIImage *newImage = [fromimage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    UIGraphicsBeginImageContextWithOptions(fromimage.size, NO, fromimage.scale);
    [color set];
    [newImage drawInRect:CGRectMake(0, 0, fromimage.size.width, fromimage.size.height)];
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
@end
