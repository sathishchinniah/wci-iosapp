//
//  FiltersViewController.swift
//  JEDE_r
//
//  Created by Exarcplus on 16/05/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import AFNetworking


class FiltersViewController: UIViewController, TagListViewDelegate, NHRangeSliderViewDelegate, RangeSeekSliderDelegate {
    
    var myclass : MyClass!
    
    @IBOutlet weak var star1 : UIButton!
    @IBOutlet weak var star2 : UIButton!
    @IBOutlet weak var star3 : UIButton!
    @IBOutlet weak var star4 : UIButton!
    @IBOutlet weak var star5 : UIButton!
    @IBOutlet weak var rangesliderview : RangeSeekSlider!
    @IBOutlet weak var pricerangesliderview : NHRangeSliderView!
    var genderstring : String!
    @IBOutlet weak var femaleimg : UIImageView!
    @IBOutlet weak var maleimg : UIImageView!
    @IBOutlet weak var uniseximg : UIImageView!
    
    @IBOutlet weak var wheelchairbgimg : UIImageView!
    @IBOutlet weak var parkingbgimg : UIImageView!
    @IBOutlet weak var wheelchairlab : UILabel!
    @IBOutlet weak var parkinglab : UILabel!
//    @IBOutlet weak var wheelchairview : RoundView!
//    @IBOutlet weak var parkingview : RoundView!
    @IBOutlet weak var tagListView : TagListView!
    
    var star1str : String!
    var star2str : String!
    var star3str : String!
    var star4str : String!
    var star5str : String!
    var wheelchairstr : String!
    var parkingstr : String!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        myclass = MyClass()
        let view = UIView(frame:  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 20))
        view.backgroundColor = UIColor.init(red: 27.0/255.0, green: 34.0/255.0, blue: 40.0/255.0, alpha: 1.0)
        self.navigationController?.view.addSubview(view)
        
        star1.setImage(UIImage.init(named: "unselectstar.png"), for: .normal)
        star2.setImage(UIImage.init(named: "unselectstar.png"), for: .normal)
        star3.setImage(UIImage.init(named: "unselectstar.png"), for: .normal)
        star4.setImage(UIImage.init(named: "unselectstar.png"), for: .normal)
        star5.setImage(UIImage.init(named: "unselectstar.png"), for: .normal)
        
        rangesliderview.sizeToFit()
        rangesliderview.delegate = self
        rangesliderview.minValue = 0.0
        rangesliderview.maxValue = 5.0
        rangesliderview.selectedMinValue = 0.0
        rangesliderview.selectedMaxValue = 5.0
        rangesliderview.minDistance = 0.5
        rangesliderview.maxDistance = 5.0
        rangesliderview.handleDiameter = 30.0
        rangesliderview.handleColor = UIColor.lightGray
        rangesliderview.backgroundColor = UIColor.white
        rangesliderview.selectedHandleDiameterMultiplier = 1.3
        rangesliderview.numberFormatter.numberStyle = .none
        //        rangesliderview.numberFormatter.locale = Locale(identifier: "en_US")
        rangesliderview.numberFormatter.maximumFractionDigits = 1
        rangesliderview.minLabelFont = UIFont(name: "Roboto-Regular", size: 14.0)!
        rangesliderview.maxLabelFont = UIFont(name: "Roboto-Regular", size: 14.0)!
        rangesliderview.lineHeight = 10.0
        rangesliderview.minLabelColor = UIColor.black
        rangesliderview.maxLabelColor = UIColor.black
        
//        self.rangeSliderCustom.delegate = self;
//        self.rangeSliderCustom.minValue = 0;
//        self.rangeSliderCustom.maxValue = 100;
//        self.rangeSliderCustom.selectedMinimum = 40;
//        self.rangeSliderCustom.selectedMaximum = 60;
//        self.rangeSliderCustom.handleImage = [UIImage imageNamed:@"custom-handle"];
//        self.rangeSliderCustom.selectedHandleDiameterMultiplier = 1;
//        self.rangeSliderCustom.tintColorBetweenHandles = [UIColor redColor];
//        self.rangeSliderCustom.lineHeight = 10;
//        NSNumberFormatter *customFormatter = [[NSNumberFormatter alloc] init];
//        customFormatter.positiveSuffix = @"B";
//        self.rangeSliderCustom.numberFormatterOverride = customFormatter;
        
        //diff
//        self.rangesliderview.sizeToFit()
//        self.rangesliderview.delegate = self
//        self.rangesliderview.minValue = 0
//        self.rangesliderview.maxValue = 200
//        self.rangesliderview.selectedMaximum = 0
//        self.rangesliderview.selectedMaximum = 200
////        self.rangesliderview.minDistance = 0.5
////        self.rangesliderview.maxDistance = 5.0
//        self.rangesliderview.handleDiameter = 30.0
////        self.rangesliderview.tintColorBetweenHandles = UIColor.purple
//        self.rangesliderview.lineHeight = 10.0
//        
//        pricerangesliderview.sizeToFit()
//        self.pricerangesliderview.delegate = self
//        self.pricerangesliderview.minValue = 0.0
//        self.pricerangesliderview.maxValue = 5.0
//        self.pricerangesliderview.selectedMaximum = 0.0
//        self.pricerangesliderview.selectedMaximum = 5.0
//        self.pricerangesliderview.minDistance = 0.5
//        self.pricerangesliderview.maxDistance = 5.0
//        self.pricerangesliderview.handleDiameter = 30.0
//        self.pricerangesliderview.lineHeight = 10.0
        
        self.pricerangesliderview.sizeToFit()
        self.pricerangesliderview.delegate = self
        //pricerangesliderview.trackHighlightTintColor = UIColor.red
        pricerangesliderview.lowerValue = 0.0
        pricerangesliderview.minimumValue = 0.0
        pricerangesliderview.maximumValue = 5.0
        pricerangesliderview.upperValue = 5.0
        pricerangesliderview.stepValue = 1.0
        pricerangesliderview.gapBetweenThumbs = 0.5
        pricerangesliderview.lowerDisplayStringFormat = "CHF %.0f"
        pricerangesliderview.upperDisplayStringFormat = "CHF %.0f"
        
//        pricerangesliderview.sizeToFit()
//        pricerangesliderview.delegate = self
//        pricerangesliderview.minValue = 0.0
//        pricerangesliderview.maxValue = 5.0
//        pricerangesliderview.selectedMinValue = 0.0
//        pricerangesliderview.selectedMaxValue = 5.0
//        pricerangesliderview.minDistance = 0.0
//        pricerangesliderview.maxDistance = 5.0
//        pricerangesliderview.handleDiameter = 30.0
//        pricerangesliderview.selectedHandleDiameterMultiplier = 1.3
//        pricerangesliderview.numberFormatter.numberStyle = .none
//        //        rangesliderview.numberFormatter.locale = Locale(identifier: "en_US")
//        pricerangesliderview.numberFormatter.maximumFractionDigits = 1
//        pricerangesliderview.minLabelFont = UIFont(name: "Roboto-Regular", size: 14.0)!
//        pricerangesliderview.maxLabelFont = UIFont(name: "Roboto-Regular", size: 14.0)!
//        pricerangesliderview.minLabelColor = UIColor.black
//        pricerangesliderview.maxLabelColor = UIColor.black
        
        genderstring = ""
        
        star1str = "0"
        star2str = "0"
        star3str = "0"
        star4str = "0"
        star5str = "0"
        wheelchairstr = "0"
        parkingstr = "0"
        
        wheelchairbgimg.image = UIImage.init(named: "wheelchairunselected.png")
        parkingbgimg.image = UIImage.init(named: "parkingunselected.png")

        self.catlink()
        // Do any additional setup after loading the view.
        
    }
    
    
    /*
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat)
    {
            print("Standard slider updated. Min Value: \(minValue) Max Value: \(maxValue)")
        
        
    }
    
    func didStartTouches(in slider: RangeSeekSlider) {
        print("did start touches")
    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
        print("did end touches")
        pricerangesliderview.minLabelAccessibilityLabel = String(format: "%dRS",slider.minValue)
        pricerangesliderview.minLabelAccessibilityLabel = String(format: "%dRS",slider.maxValue)
    }
     */
    
    
    func rangeSlider(_ sender: TTRangeSlider!, didChangeSelectedMinimumValue selectedMinimum: Float, andMaximumValue selectedMaximum: Float)
    {
        print("Standard slider updated. Min Value: \(selectedMinimum) Max Value: \(selectedMaximum)")
    }
    
    func didStartTouches(in sender: TTRangeSlider!)
    {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func wheelchairbutton(_sender:UIButton)
    {
        if wheelchairstr == "0"
        {
            wheelchairbgimg.image = UIImage.init(named: "wheelchairselected.png")
            wheelchairstr = "1"
        }
        else
        {
            wheelchairbgimg.image = UIImage.init(named: "wheelchairunselected.png")
            wheelchairstr = "0"
        }
    }
    
    @IBAction func parkingbutton(_sender:UIButton)
    {
        if parkingstr == "0"
        {
            parkingbgimg.image = UIImage.init(named: "parkingselected.png")
            parkingstr = "1"
        }
        else
        {
            parkingbgimg.image = UIImage.init(named: "parkingunselected.png")
            parkingstr = "0"
        }
    }
    
    @IBAction func star1button(_sender:UIButton)
    {
//        star1.setImage(UIImage.init(named: "selectstar.png"), for: .normal)
//        star2.setImage(UIImage.init(named: "selectstar.png"), for: .normal)
//        star3.setImage(UIImage.init(named: "selectstar.png"), for: .normal)
//        star4.setImage(UIImage.init(named: "selectstar.png"), for: .normal)
//        star5.setImage(UIImage.init(named: "selectstar.png"), for: .normal)
        
        if star1str == "0"
        {
            star1.setImage(UIImage.init(named: "selectstar.png"), for: .normal)
            star1str = "1"
        }
        else
        {
            star1.setImage(UIImage.init(named: "unselectstar.png"), for: .normal)
            star1str = "0"
        }
        
    }
    
    @IBAction func star2button(_sender:UIButton)
    {
//        star1.setImage(UIImage.init(named: "unselectstar.png"), for: .normal)
//        star2.setImage(UIImage.init(named: "selectstar.png"), for: .normal)
//        star3.setImage(UIImage.init(named: "selectstar.png"), for: .normal)
//        star4.setImage(UIImage.init(named: "selectstar.png"), for: .normal)
//        star5.setImage(UIImage.init(named: "selectstar.png"), for: .normal)
        if star2str == "0"
        {
            star2.setImage(UIImage.init(named: "selectstar.png"), for: .normal)
            star2str = "1"
        }
        else
        {
            star2.setImage(UIImage.init(named: "unselectstar.png"), for: .normal)
            star2str = "0"
        }
    }
    
    @IBAction func star3button(_sender:UIButton)
    {
//        star1.setImage(UIImage.init(named: "unselectstar.png"), for: .normal)
//        star2.setImage(UIImage.init(named: "unselectstar.png"), for: .normal)
//        star3.setImage(UIImage.init(named: "selectstar.png"), for: .normal)
//        star4.setImage(UIImage.init(named: "selectstar.png"), for: .normal)
//        star5.setImage(UIImage.init(named: "selectstar.png"), for: .normal)
        if star3str == "0"
        {
            star3.setImage(UIImage.init(named: "selectstar.png"), for: .normal)
            star3str = "1"
        }
        else
        {
            star3.setImage(UIImage.init(named: "unselectstar.png"), for: .normal)
            star3str = "0"
        }
    }
    
    @IBAction func star4button(_sender:UIButton)
    {
//        star1.setImage(UIImage.init(named: "unselectstar.png"), for: .normal)
//        star2.setImage(UIImage.init(named: "unselectstar.png"), for: .normal)
//        star3.setImage(UIImage.init(named: "unselectstar.png"), for: .normal)
//        star4.setImage(UIImage.init(named: "selectstar.png"), for: .normal)
//        star5.setImage(UIImage.init(named: "selectstar.png"), for: .normal)
        if star4str == "0"
        {
            star4.setImage(UIImage.init(named: "selectstar.png"), for: .normal)
            star4str = "1"
        }
        else
        {
            star4.setImage(UIImage.init(named: "unselectstar.png"), for: .normal)
            star4str = "0"
        }
    }
    
    @IBAction func star5button(_sender:UIButton)
    {
//        star1.setImage(UIImage.init(named: "unselectstar.png"), for: .normal)
//        star2.setImage(UIImage.init(named: "unselectstar.png"), for: .normal)
//        star3.setImage(UIImage.init(named: "unselectstar.png"), for: .normal)
//        star4.setImage(UIImage.init(named: "unselectstar.png"), for: .normal)
//        star5.setImage(UIImage.init(named: "selectstar.png"), for: .normal)
        if star5str == "0"
        {
            star5.setImage(UIImage.init(named: "selectstar.png"), for: .normal)
            star5str = "1"
        }
        else
        {
            star5.setImage(UIImage.init(named: "unselectstar.png"), for: .normal)
            star5str = "0"
        }
    }
    
    @IBAction func femalebutton(_sender : UIButton)
    {
        if femaleimg.image == UIImage.init(named: "FemaleGrey.png")
        {
            femaleimg.image = UIImage.init(named: "Female_select.png")
            genderstring = "Female"
//            maleimg.image = UIImage.init(named: "MaleGrey.png")
//            uniseximg.image = UIImage.init(named: "UnisexGrey.png")
        }
        else
        {
            femaleimg.image = UIImage.init(named: "FemaleGrey.png")
        }
    }
    
    @IBAction func malebutton(_sender : UIButton)
    {
        if maleimg.image == UIImage.init(named: "MaleGrey.png")
        {
            maleimg.image = UIImage.init(named: "Male_select.png")
            genderstring = "Male"
//            femaleimg.image = UIImage.init(named: "FemaleGrey.png")
//            uniseximg.image = UIImage.init(named: "UnisexGrey.png")
        }
        else
        {
            maleimg.image = UIImage.init(named: "MaleGrey.png")
            genderstring = ""
        }
    }
    
    @IBAction func unisexbutton(_sender : UIButton)
    {
        if uniseximg.image == UIImage.init(named: "UnisexGrey.png")
        {
//            maleimg.image = UIImage.init(named: "MaleGrey.png")
//            femaleimg.image = UIImage.init(named: "FemaleGrey.png")
            uniseximg.image = UIImage.init(named: "Unisex_select.png")
            genderstring = "Unisex"
        }
        else
        {
            uniseximg.image = UIImage.init(named: "UnisexGrey.png")
            genderstring = ""
        }
    }
    

    
    @IBAction func closebutton(_sender:UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func catlink()
    {
        if AFNetworkReachabilityManager.shared().isReachable
        {
            let urlstring = String(format:"%@/category.php",kBaseURL)
            print(urlstring)
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: true, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    if jsonObject.count != 0
                    {
                        let copyarr = jsonObject;
                        print(copyarr)
//                        cell.tagListView.delegate = self;
//                        //    cell.tagListView.textFont = UIFont.systemFontOfSize(15)
//                        cell.tagListView.shadowRadius = 2;
//                        cell.tagListView.shadowOpacity = 0.4;
//                        cell.tagListView.shadowColor = [UIColor blackColor];
//                        cell.tagListView.shadowOffset = CGSizeMake(1, 1);
//                        cell.tagListView.delegate = self;
//                        cell.tagListView.tag = indexPath.section;
//                        for (int  i = 0; i<datarray.count; i++)
//                        {
//                            NSString *ststsr = [[datarray objectAtIndex:i]valueForKey:@"brand_name"];
//                            [cell.tagListView addTag:ststsr];
//                        }

                        self.tagListView.delegate = self
                        self.tagListView.shadowRadius = 2
                        self.tagListView.shadowOpacity = 0.4
                        self.tagListView.shadowColor = UIColor.black
                        self.tagListView.shadowOffset = CGSize(width: 1, height: 1)
//                        self.tagListView.tag = indexp
                        for c in 0 ..< copyarr.count
                        {
                            let str = (copyarr.object(at: c) as AnyObject).value(forKey: "category_name") as? String
                            print(str!)
                            self.tagListView.addTag(str!)
                        }
                    }
                }
                else
                {
                    NSLog("jsonObject=error");
                }
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
    
    
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView)
    {
        if tagView.isSelected == true
        {
            let gradient: CAGradientLayer = CAGradientLayer()
            
            gradient.colors = [UIColor.blue.cgColor, UIColor.red.cgColor]
            gradient.locations = [0.0 , 1.0]
            gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
            gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
            gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width, height: self.view.frame.size.height)
            
            self.tagListView.layer.insertSublayer(gradient, at: UInt32(sender.tag))
        }
        else
        {
            
        }
        
//        self.view.layer.insertSublayer(gradient, at: 0)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
