//
//  SidemenuViewController.swift
//  JEDE_r
//
//  Created by Exarcplus iOS 2 on 02/05/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import LGSideMenuController
import SDWebImage
import AFNetworking
import M13BadgeView
import Alamofire


class SidemenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, KNSwitcherChangeValueDelegate, UIAlertViewDelegate {
    
    var menuarra:NSMutableArray!
    var imgarr : NSMutableArray!
    @IBOutlet weak var SideList:UITableView!
    @IBOutlet weak var profilebutton : UIButton!
    @IBOutlet weak var loginlogoutbutton : UIButton!
    @IBOutlet weak var logoimg : UIImageView!
    @IBOutlet weak var namelab : UILabel!
    var Userdic = NSMutableDictionary()
    var passuserid : String!
    var responsearr = NSMutableArray()
    
    var notiCount = ""
    
    var uservendorstr : String!
    @IBOutlet var viewSwitch: UIView!
    @IBOutlet weak var userslab : UILabel!
    @IBOutlet weak var vendorslab : UILabel!
    @IBOutlet weak var waitinglab : UILabel!
    
    var switchers : KNSwitcher!
    var vendorsts : String!
    var badgeView = M13BadgeView(frame: CGRect(x: 0, y: 0, width: 24.0, height: 24.0))
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "reloadLogin"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadlogin), name: NSNotification.Name(rawValue: "reloadLogin"), object: nil)
        
        menuarra = NSMutableArray()
        imgarr = NSMutableArray()
        
        uservendorstr = "User"
        vendorsts = ""
        
        if #available(iOS 9.0, *)
        {
            switchers = KNSwitcher(frame: CGRect(x: 0, y: 0, width: viewSwitch.frame.width, height:viewSwitch.frame.height), on: false)
            switchers.delegate = self
            switchers.setImages(onImage: UIImage(named: "logo_01.png"), offImage: UIImage(named: "user-2.png"))
            viewSwitch.addSubview(switchers)
        }
        else
        {
            // Fallback on earlier versions
        }
    }
    
    @available(iOS 9.0, *)
    func switcherDidChangeValue(switcher:KNSwitcher, value: Bool)
    {
        menuarra.removeAllObjects()
        imgarr.removeAllObjects()
        
        print(vendorsts)
        
        if value == false
        {
            vendorsts = ""
            self.SideList.isHidden = false
            self.waitinglab.isHidden = true
            uservendorstr = "User"
            userslab.alpha = 1.0
            vendorslab.alpha = 0.4
            print("male")
            self.badgeView.isHidden = false
            
            self.addUserMenuTitleList()
            self.addUserMenuImageList()
            
            userslab.alpha = 1.0
            vendorslab.alpha = 0.4
            
            SideList.reloadData()
        }
        else
        {
            print("female")
            print(vendorsts)
            self.badgeView.isHidden = true
            
            if self.vendorsts == ""
            {
                
                print("Handle Ok logic here")
                
                if UserDefaults.standard.object(forKey: "Logindetail") == nil
                    
                {
                    let alertMessage = "Please_Login_into_your_Account"
                    let loginAlert = UIAlertController(title: "WCI", message: alertMessage.localized, preferredStyle: UIAlertControllerStyle.alert)
                    
                    loginAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                        print("Handle Ok logic here")
                    }))
                    
                    self.present(loginAlert, animated: true, completion: nil)
                    
                }
                else
                {
                    self.getdetails()
                }
                let alertOnce = UserDefaults.standard.bool(forKey:"onlyonceAlert")
                if UserDefaults.standard.object(forKey: "Logindetail") != nil {
                    let userDict = UserDefaults.standard.object(forKey: "Logindetail") as? NSDictionary
                    let loggedInUserEmailId = userDict?.value(forKey: "email") as? String
                    let storedUserId = UserDefaults.standard.object(forKey: "userEmail") as? String
                    let vendorStatus = userDict?.value(forKey: "vendor_status") as? String
                    print("userDict")
                    print(print("userDict"))
                    print("VENDOR \(vendorStatus)")
                    if vendorStatus != "1" && alertOnce == false && (storedUserId != loggedInUserEmailId) {
                        let ProviderAlert = "Provider_Alert"
                        let refreshAlert = UIAlertController(title: "WCI", message: ProviderAlert.localized, preferredStyle: UIAlertControllerStyle.alert)
                        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                            //web view functionality
                            
                            let userDict = UserDefaults.standard.object(forKey: "Logindetail") as? NSDictionary
                            let userEmail = userDict?.value(forKey: "email") as? String
                            UserDefaults.standard.set(true,forKey:"onlyonceAlert")
                            UserDefaults.standard.set(userEmail,forKey:"userEmail")
                            
                            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                            
                            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "WebForProviderViewController") as! WebForProviderViewController
                            self.present(nextViewController, animated: true, completion: nil)
                            //                    self.navigationController?.pushViewController(nextViewController, animated: true)
                            
                        }))
                        //self.present(refreshAlert, animated: true, completion: nil)
                        
                        // If you want to become a toilet provider please complete the request form
                        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: { (action: UIAlertAction!) in
                            print("Handle Cancel Logic here")
                            let userDict = UserDefaults.standard.object(forKey: "Logindetail") as? NSDictionary
                            let userEmail = userDict?.value(forKey: "email") as? String
                            UserDefaults.standard.set(true,forKey:"onlyonceAlert")
                            UserDefaults.standard.set(userEmail,forKey:"userEmail")
                            
                            self.switchers = KNSwitcher(frame: CGRect(x: 0, y: 0, width: self.viewSwitch.frame.width, height:self.viewSwitch.frame.height), on: false)
                            self.switchers.delegate = self
                            self.switchers.setImages(onImage: UIImage(named: "logo_01.png"), offImage: UIImage(named: "user-2.png"))
                            self.switchers.removeFromSuperview()
                            self.viewSwitch.addSubview(self.switchers)
                            
                            self.SideList.isHidden = false
                            self.waitinglab.isHidden = true
                            self.switchers.on = false
                            self.uservendorstr = "User"
                            self.userslab.alpha = 1.0
                            self.vendorslab.alpha = 0.4
                            print("male")
                            
                            self.addUserMenuTitleList()
                            self.addUserMenuImageList()
                            
                            self.userslab.alpha = 1.0
                            self.vendorslab.alpha = 0.4
                            
                            self.badgeView.isHidden = false
                            self.SideList.reloadData()
                        }))
                        present(refreshAlert, animated: true, completion: nil)
                    }
                    
                }
                    
                    
                else if self.vendorsts == "0"
                {
                    self.SideList.isHidden = true
                    //                self.SideList.isHidden = false
                    self.waitinglab.isHidden = false
                    self.userslab.alpha = 0.4
                    self.vendorslab.alpha = 1.0
                    self.waitinglab.text =  "activity_main_vender_req_message".localized
                    self.getdetails()
                }
                else if self.vendorsts == "1"
                {
                    print("sidemenu")
                    self.getdetails()
                    self.SideList.isHidden = false
                    self.waitinglab.isHidden = true
                    self.waitinglab.text = ""
                    
                    self.userslab.alpha = 0.4
                    self.vendorslab.alpha = 1.0
                    
                    self.addVendorMenuTitleList()
                    self.addVendorMenuImageList()
                    self.badgeView.isHidden = true
                    self.SideList.reloadData()
                }
                else
                {
                    print("blocked")
                    self.getdetails()
                    self.SideList.isHidden = true
                    //self.SideList.isHidden = false
                    self.waitinglab.isHidden = false
                    self.userslab.alpha = 0.4
                    self.vendorslab.alpha = 1.0
                    self.waitinglab.text = "activity_main_vender_req_message".localized
                    //"Your Vendor account is blocked. Please contact admin"
                }
            }
        }
        
    }
    
    func reloadlogin()
    {
        print("reloadlogin")
        let placesData = UserDefaults.standard.object(forKey: "Logindetail") as? NSDictionary
        if placesData != nil {
            self.Userdic.removeAllObjects()
            self.Userdic.addEntries(from: (placesData as? [String : Any])!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getdetails()
    {
        if passuserid == nil{
            passuserid = ""
        }
        let urlstring  = String(format:kBaseDomainUrl + "/json/vendor_request.php?user_id=%@",passuserid)
        
        print(urlstring)
        let encodestr = urlstring.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let manager = AFHTTPSessionManager();
        manager.responseSerializer = AFHTTPResponseSerializer()
        manager.get(encodestr!, parameters: nil, success:{(task,responseObject) -> Void in
            do {
                
                let jsonResults = try JSONSerialization.jsonObject(with: (responseObject as! NSData) as Data, options:JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                
                self.responsearr .removeAllObjects()
                self.responsearr.addObjects(from: jsonResults.value(forKey: "result") as! [AnyObject])
                print(self.responsearr)
                
                if self.responsearr.count != 0
                {
                    self.vendorsts = (self.responsearr.object(at: 0) as AnyObject).value(forKey: "vendor_status") as? String
                    print(self.vendorsts)
                    
                    if self.vendorsts == "0"
                    {
                        if #available(iOS 9.0, *)
                        {
                            let switcher = KNSwitcher(frame: CGRect(x: 0, y: 0, width: self.viewSwitch.frame.width, height:self.viewSwitch.frame.height), on: true)
                            switcher.delegate = self
                            switcher.setImages(onImage: UIImage(named: "logo_01.png"), offImage: UIImage(named: "user-2.png"))
                            self.viewSwitch.addSubview(switcher)
                        }
                        else
                        {
                            // Fallback on earlier versions
                        }
                        
                        self.SideList.isHidden = true
                        //                        self.SideList.isHidden = false
                        self.waitinglab.isHidden = false
                        self.waitinglab.text = "activity_main_vender_req_message".localized
                        self.userslab.alpha = 0.4
                        self.vendorslab.alpha = 1.0
                        
                        if self.uservendorstr == "User"
                        {
                            self.addUserMenuTitleList()
                            self.addUserMenuImageList()
                            self.userslab.alpha = 1.0
                            self.vendorslab.alpha = 0.4
                        }
                        else
                        {
                            self.userslab.alpha = 0.4
                            self.vendorslab.alpha = 1.0
                            
                            self.addVendorMenuTitleList()
                            self.addVendorMenuImageList()
                        }
                        
                        self.SideList.reloadData()
                    }
                    else if self.vendorsts == "1"
                    {
                        self.SideList.isHidden = false
                        self.waitinglab.isHidden = true
                        self.waitinglab.text = "activity_main_vender_req_message".localized
                        self.userslab.alpha = 0.4
                        self.vendorslab.alpha = 1.0
                        self.uservendorstr = "Vendor"
                        self.userslab.alpha = 0.4
                        self.vendorslab.alpha = 1.0
                        
                        self.addVendorMenuTitleList()
                        self.addVendorMenuImageList()
                        self.SideList.reloadData()
                    }
                    else
                    {
                        self.SideList.isHidden = true
                        //self.SideList.isHidden = false
                        self.waitinglab.isHidden = false
                        self.userslab.alpha = 0.4
                        self.vendorslab.alpha = 1.0
                        self.waitinglab.text = "activity_main_vender_req_message".localized
                    }
                    
                    //store login detail
                    let data = NSKeyedArchiver.archivedData(withRootObject: self.responsearr.object(at: 0) as! NSDictionary)
                    //  if((self.responsearr.object(at: 0
                    //  UserDefaults.standard.set(data, forKey:"Logindetail")
                    // UserDefaults.standard.synchronize()
                    
                    
                }
                
                
                
                
                //                self.HUD .hide(animated: true)
                // success ...
            } catch {
                // failure
                //                self.HUD .hide(animated: true)
            }
            
        }, failure:{(task,error) -> Void in
            print("Error: %@", error);
            
            
            
        })
    }
    
    
    // MARK: - Tabelview Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return menuarra.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SidemenuTableViewCell", for: indexPath) as! SidemenuTableViewCell
        cell.selectionStyle = .none
        cell.Title.text = menuarra.object(at: indexPath.row) as? String
        cell.imagee.image = imgarr.object(at: indexPath.row) as? UIImage
        print(cell.Title.text)
        print(vendorsts)
        
        if cell.Title.text == "NOTIFICATIONS" && vendorsts == ""  {
            badgeView.text = self.notiCount
            badgeView.badgeBackgroundColor = UIColor.darkGray
            cell.imagee.addSubview(badgeView as? UIView ?? UIView())
        }
        else
        {
            
        }
        
        if cell.Title.text == "LAST BOOKING"{
            
        }
        
        if uservendorstr == "User"
        {
            cell.switchButton.isHidden = true
        }
        else if vendorsts == "1"
        {
            if indexPath.row == 0
            {
                cell.switchButton.isHidden = false
            }
                
            else if indexPath.row == 1
            {
                cell.switchButton.isHidden = true
            }
                
            else if indexPath.row == 2
            {
                cell.switchButton.isHidden = true
            }
                
            else if indexPath.row == 3
            {
                cell.switchButton.isHidden = true
            }
                
            else if indexPath.row == 4
            {
                cell.switchButton.isHidden = true
            }
                
            else if indexPath.row == 5
            {
                cell.switchButton.isHidden = true
            }
                
            else if indexPath.row == 6
            {
                cell.switchButton.isHidden = true
            }
            
        }
        // cell.switchButton.isHidden = true
        return cell;
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if (indexPath.row % 2) != 0
        {
            //            cell.backgroundColor = UIColor.init(red: <#T##CGFloat#>, green: <#T##CGFloat#>, blue: <#T##CGFloat#>, alpha: <#T##CGFloat#>)
            cell.backgroundColor = UIColor .clear
        }
        else
        {
            
        }
        if indexPath.row == 0
        {
            // cell.switchButton.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.isHidden = false
        if uservendorstr == "User"
        {
            if indexPath.row == 0
            {
                UserDefaults.standard.removeObject(forKey: "Filterdetail")
                UserDefaults.standard.synchronize()
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
                self.navigationController?.navigationBar.isHidden = true
                self.navigationController?.isNavigationBarHidden = true
                
                appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
                let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
                currentview.pushViewController(mainview, animated: false)
            }
            
            if indexPath.row == 1
            {
                
                if UserDefaults.standard.object(forKey: "Logindetail") == nil
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
                    appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
                    let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    currentview.pushViewController(mainview, animated: false)
                }
                else
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
                    appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
                    currentview.isNavigationBarHidden = false
                    self.navigationController?.isNavigationBarHidden = false
                    let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "FavouritesViewController") as! FavouritesViewController
                    currentview.pushViewController(mainview, animated: false)
                }
            }
            
            if indexPath.row == 2
            {
                if UserDefaults.standard.object(forKey: "Logindetail") == nil
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
                    appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
                    let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    currentview.pushViewController(mainview, animated: false)
                }
                else
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
                    currentview.isNavigationBarHidden = false
                    appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
                    let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "BookingListViewController") as! BookingListViewController
                    currentview.pushViewController(mainview, animated: false)
                }
            }
            
            //            // started for feeebbackbutton//
            if indexPath.row == 3
            {
                if UserDefaults.standard.object(forKey: "Logindetail") == nil
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
                    appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
                    let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    currentview.pushViewController(mainview, animated: false)
                }
                else
                {
                    resetNotiCount(user_id: passuserid)
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
                    appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
                    currentview.navigationBar.isHidden = true
                    self.navigationController?.navigationBar.isHidden = true
                    self.navigationController?.isNavigationBarHidden = true
                    let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "FeedbackViewController") as! FeedbackViewController
                    currentview.pushViewController(mainview, animated: false)
                }
            }
            // end for feebbackbutton cheching //
            if indexPath.row == 4
            {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
                appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
                let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
                currentview.pushViewController(mainview, animated: false)
            }
            
            
        } else if vendorsts == "1" {
            // end for last booking vendors
            //Change language for non logged in user also
            if indexPath.row == 6
            {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
                appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
                let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
                currentview.pushViewController(mainview, animated: false)
            }
            //If user is not logged in then redirect to login screen
            if UserDefaults.standard.object(forKey: "Logindetail") == nil
            {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
                appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
                let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                currentview.pushViewController(mainview, animated: false)
                return
            }
            if indexPath.row == 0
            {
                if UserDefaults.standard.object(forKey: "Logindetail") == nil
                {
                    print("")
                    
                }
                else
                {
                    print("")
                }
            }
            //Add toilet
            if indexPath.row == 1
            {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
                appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
                let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "AddLooViewController") as! AddLooViewController
                currentview.pushViewController(mainview, animated: false)
            }
            if indexPath.row == 2 //My toilets
            {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
                appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
                let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "MyLooVC") as! MyLooVC
                currentview.pushViewController(mainview, animated: false)
            }
            if indexPath.row == 3 //Guest book
            {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
                appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
                currentview.isNavigationBarHidden = false
                let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "BookingListViewController") as! BookingListViewController
                currentview.pushViewController(mainview, animated: false)
            }
            
            if indexPath.row == 4 //notifications
            {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
                appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
                let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "vendorNotificationViewController") as! vendorNotificationViewController
                currentview.pushViewController(mainview, animated: false)
                
            }
            // end for feedback
            // for last lst bookinf for vendors
            if indexPath.row == 5
            {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
                appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
                currentview.navigationBar.isHidden = true
                self.navigationController?.navigationBar.isHidden = true
                self.navigationController?.isNavigationBarHidden = true
                let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "FeedbackViewController") as! FeedbackViewController
                currentview.pushViewController(mainview, animated: false)
            }
        }
        
    }
    
    @IBAction func profilebutton(_sender : UIButton)
    {
        if UserDefaults.standard.object(forKey: "Logindetail") == nil
        {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
            appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
            let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            currentview.pushViewController(mainview, animated: false)
        }
        else
        {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
            appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
            let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
            currentview.pushViewController(mainview, animated: false)
        }
    }
    
    @IBAction func logoutbutton(_ sender: AnyObject)
    {
        
        UserDefaults.standard.removeObject(forKey: "Filterdetail")
        UserDefaults.standard.synchronize()
        if UserDefaults.standard.object(forKey: "Logindetail") == nil
        {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
            appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
            let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            currentview.pushViewController(mainview, animated: false)
            self.loginlogoutbutton.setTitle("LOG_IN".localized,for: .normal)
            self.namelab.text = "WCI"
            self.logoimg.image = UIImage.init(named: "NewLogo")
            self.profilebutton.isHidden = true
            self.Userdic.removeAllObjects()
        }
        else
        {
            self.switcherDidChangeValue(switcher: self.switchers, value: false)
            self.loginlogoutbutton.setTitle("LOG_IN".localized,for: .normal)
            self.profilebutton.isHidden = true
            
            UserDefaults.standard.removeObject(forKey: "Logindetail")
            UserDefaults.standard.synchronize()
            FBSDKLoginManager().logOut()
            
            self.namelab.text = "WCI"
            self.logoimg.image = UIImage.init(named: "NewLogo")
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
            UIApplication.shared.unregisterForRemoteNotifications()
            UserDefaults.standard.removeObject(forKey: "Logindetail")
            UserDefaults.standard.synchronize()
            let mainView = kmainStoryboard.instantiateViewController(withIdentifier:"MainScreenViewController") as! MainScreenViewController
            let nav = UINavigationController.init(rootViewController: mainView)
            appDelegate.SideMenu.rootViewController = nav
            GIDSignIn.sharedInstance().signOut()
            self.Userdic.removeAllObjects()
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func downloadLooCount(user_id:String){
        
        let params:[String:String] = ["user_id":user_id]
        var timingsArr = [String]()
        
        print(timingsArr.count)
        print(params)
        let Url = URL(string:kBaseDomainUrl + "/json/notification_count.php")!
        Alamofire.request(Url,method:.get,parameters:params,encoding: URLEncoding()).responseJSON{ response in
            let result = response.result
            print(response)
            print(result)
            
            if let dict = result.value as? Dictionary<String,AnyObject>{
                
                if let values = dict["result"] as? [Dictionary<String,AnyObject>]
                {
                    
                    for obj in values {
                        
                        if let user_count = obj["user_count"] as? String
                        {
                            print(user_count)
                            self.notiCount = user_count
                            self.SideList.reloadData()
                            
                        }
                        
                    }
                    
                }
            }
            
            
        }
        
        
        
    }
    
    
    func resetNotiCount(user_id:String){
        
        let params:[String:String] = ["user_id":user_id,"mode":"User"]
        var timingsArr = [String]()
        
        print(timingsArr.count)
        print(params)
        let Url = URL(string:kBaseDomainUrl + "/json/notification_uncount.php?user_id=7&mode=User")!
        Alamofire.request(Url,method:.get,parameters:params,encoding: URLEncoding()).responseJSON{ response in
            let result = response.result
            print(response)
            print(result)
            
        }
        
    }
    
    func reloadSideMenu() {
        print("reloadSideMenu called!!")
        let placesData = UserDefaults.standard.object(forKey: "Logindetail") as? NSDictionary
        if placesData != nil {
            self.Userdic.removeAllObjects()
            self.Userdic.addEntries(from: (placesData as? [String : Any])!)
            print("placesData")
            print(self.Userdic)
        }
        
        self.profilebutton.setTitle("drawer_header_profile".localized, for: .normal)
        self.userslab.text = "drawer_header_users".localized
        self.vendorslab.text = "drawer_header_vendors".localized
        
        self.namelab.text = self.Userdic.value(forKey: "user_name") as? String
        
        let imageurl = Userdic.value(forKey: "image") as? String;
        print(imageurl as Any)
        
        if imageurl == "" || imageurl?.characters.count == 0
        {
            self.logoimg.image = UIImage.init(named: "NewLogo")
        }
        else
        {
            let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                print(self)
                
            }
            if imageurl != nil{
                self.logoimg.sd_setImage(with: URL(string:imageurl! ?? "NewLogo"), completed: block)
                self.logoimg.contentMode = UIViewContentMode.scaleToFill;
            }
            
            self.profilebutton.isHidden = false
            
            if UserDefaults.standard.object(forKey: "Logindetail") == nil
            {
                self.loginlogoutbutton.setTitle("LOG_IN".localized,for: .normal)
                self.profilebutton.setTitle("drawer_header_profile".localized, for: .normal)
                self.namelab.text = "app_name".localized
                self.logoimg.image = UIImage.init(named: "NewLogo")
                self.profilebutton.isHidden = false
            }
            else
            {
                
                self.loginlogoutbutton.setTitle("log_out".localized,for: .normal)
                
            }
            if self.vendorsts == ""
            {
                
                print("sidemenu")
                
                self.SideList.isHidden = false
                self.waitinglab.isHidden = true
                uservendorstr = "User"
                userslab.alpha = 1.0
                vendorslab.alpha = 0.4
                print("male")
                self.addUserMenuTitleList()
                self.addUserMenuImageList()
                userslab.alpha = 1.0
                vendorslab.alpha = 0.4
                SideList.reloadData()
            }
            else if self.vendorsts == "0"
            {
                self.SideList.isHidden = true
                self.waitinglab.isHidden = false
                self.userslab.alpha = 0.4
                self.vendorslab.alpha = 1.0
                self.waitinglab.text = "activity_main_vender_req_message".localized
            }
            else if self.vendorsts == "1"
            {
                print("sidemenu")
                self.SideList.isHidden = false
                self.waitinglab.isHidden = true
                
                self.userslab.alpha = 0.4
                self.vendorslab.alpha = 1.0
                self.addVendorMenuTitleList()
                self.addVendorMenuImageList()
                self.SideList.reloadData()
            }
            else
            {
                print("blocked")
                self.SideList.isHidden = true
                //self.SideList.isHidden = false
                self.waitinglab.isHidden = false
                self.userslab.alpha = 0.4
                self.vendorslab.alpha = 1.0
                self.waitinglab.text = "activity_main_vender_req_message".localized
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        print("sidemenu-viewwillappear")
        NotificationCenter.default.addObserver(self, selector: #selector(SidemenuViewController.reloadSideMenu), name: NSNotification.Name(rawValue: "reloadsidemenu"), object: nil)
        
        self.profilebutton.setTitle("drawer_header_profile".localized, for: .normal)
        self.userslab.text = "drawer_header_users".localized
        self.vendorslab.text = "drawer_header_vendors".localized
        
        self.namelab.text = self.Userdic.value(forKey: "user_name") as? String
        let imageurl = Userdic.value(forKey: "image") as? String;
        print(imageurl as Any)
        
        if imageurl == "" || imageurl?.characters.count == 0
        {
            self.logoimg.image = UIImage.init(named: "NewLogo")
        }
        else
        {
            let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                print(self)
                
            }
            if imageurl != nil{
                self.logoimg.sd_setImage(with: URL(string:imageurl! ?? "NewLogo"), completed: block)
                self.logoimg.contentMode = UIViewContentMode.scaleToFill;
            }
        }
        
        self.profilebutton.isHidden = false
        
        if UserDefaults.standard.object(forKey: "Logindetail") == nil
        {
            self.loginlogoutbutton.setTitle("LOG_IN".localized,for: .normal)
            self.profilebutton.setTitle("drawer_header_profile".localized, for: .normal)
            self.namelab.text = self.Userdic.value(forKey: "user_name") as? String
            //
            let imageurl = Userdic.value(forKey: "image") as? String;
            print(imageurl as Any)
            
            if imageurl == "" || imageurl?.characters.count == 0
            {
                self.logoimg.image = UIImage.init(named: "NewLogo")
            }
            else
            {
                let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                    print(self)
                    
                }
                if imageurl != nil{
                    self.logoimg.sd_setImage(with: URL(string:imageurl! ?? "new_Icon.png"), completed: block)
                    self.logoimg.contentMode = UIViewContentMode.scaleToFill;
                }
            }
        }
        else
        {
            
            let placesData = UserDefaults.standard.object(forKey: "Logindetail") as? NSDictionary
            self.loginlogoutbutton.setTitle("log_out".localized,for: .normal)
            if placesData != nil{
                self.Userdic.removeAllObjects()
                self.Userdic.addEntries(from: (placesData as? [String : Any])!)
            }
            passuserid = self.Userdic.value(forKey: "user_id") as? String
            self.namelab.text = self.Userdic.value(forKey: "user_name") as? String
            downloadLooCount(user_id: passuserid ?? "")
            
            let imageurl = Userdic.value(forKey: "image") as? String;
            print(imageurl as Any)
            
            
            if imageurl == "" || imageurl?.characters.count == 0
            {
                self.logoimg.image = UIImage.init(named: "NewLogo")
            }
            else
            {
                let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                    print(self)
                    
                }
                if imageurl != nil{
                    self.logoimg.sd_setImage(with: URL(string:imageurl!), completed: block)
                    self.logoimg.contentMode = UIViewContentMode.scaleToFill;
                }
            }
            
            self.profilebutton.isHidden = false
        }
        
        if self.vendorsts == ""
        {
            print("sidemenu")
            
            self.SideList.isHidden = false
            self.waitinglab.isHidden = true
            uservendorstr = "User"
            userslab.alpha = 1.0
            vendorslab.alpha = 0.4
            
            self.addUserMenuTitleList()
            self.addUserMenuImageList()
            
            userslab.alpha = 1.0
            vendorslab.alpha = 0.4
            SideList.reloadData()
        }
        else if self.vendorsts == "0"
        {
            self.SideList.isHidden = true
            self.waitinglab.isHidden = false
            self.userslab.alpha = 0.4
            self.vendorslab.alpha = 1.0
            self.waitinglab.text = "activity_main_vender_req_message".localized
        }
        else if self.vendorsts == "1"
        {
            self.SideList.isHidden = false
            self.waitinglab.isHidden = true
            
            self.userslab.alpha = 0.4
            self.vendorslab.alpha = 1.0
            self.addVendorMenuTitleList()
            self.addVendorMenuImageList()
            self.SideList.reloadData()
        }
        else
        {
            print("blocked")
            self.SideList.isHidden = true
            //self.SideList.isHidden = false
            self.waitinglab.isHidden = false
            self.userslab.alpha = 0.4
            self.vendorslab.alpha = 1.0
            self.waitinglab.text = "activity_main_vender_req_message".localized
            //"Your Vendor account is blocked. Please contact admin"
        }
    }
    
    func addUserMenuTitleList() {
        self.menuarra.removeAllObjects()
        self.menuarra.add("activtiy_main_home".localized);
        self.menuarra.add("activity_main_thrones".localized);
        self.menuarra.add("activity_main_booking_list".localized);
        self.menuarra.add("activity_main_feedback".localized)
        self.menuarra.add("activity_main_settings".localized);
    }
    
    func addUserMenuImageList() {
        imgarr.removeAllObjects()
        imgarr = [UIImage(named: "menu_home_unselect.png")!,
                  UIImage(named: "fAV1.png")!,
                  UIImage(named: "menu_bookinglist_unselect.png")!,
                  UIImage(named: "ContactUs.png")!,
                  UIImage(named: "menu_setting_unselect.png")!]
        
    }
    
    func addVendorMenuTitleList() {
        self.menuarra.removeAllObjects()
        self.menuarra.add("activity_main_toilets_online".localized);
        self.menuarra.add("ADD_A_TOILET".localized);
        self.menuarra.add("activity_main_my_toilets".localized);
        self.menuarra.add("activity_main_last_booking".localized);
        self.menuarra.add("activity_main_vender_notification".localized);
        self.menuarra.add("activity_main_feedback".localized);
        self.menuarra.add("activity_main_settings".localized);
    }
    
    func addVendorMenuImageList() {
        self.imgarr.removeAllObjects()
        self.imgarr = [UIImage(named: "ToiletsOnline.png")!,
                       UIImage(named: "AddaToilet.png")!,
                       UIImage(named: "menu_calender_new.png")!,
                       UIImage(named: "Calendar.png")!,
                       UIImage(named: "LastBooking.png")!,
                       UIImage(named: "Notifications.png")!,
                       UIImage(named: "ContactUs.png")!]
    }
    
    
    
    
    
}

