//
//  MyHUDProgress.h
//  Foodboon
//
//  Created by Vignesh on 28/08/15.
//  Copyright (c) 2015 index. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyHUDProgress : UIView
- (id)initWithView:(UIView *)view yavl:(float)y;
-(void)hidehud:(float)after;

@property (atomic,strong)UIProgressView *progress;
@property (atomic,strong)UIActivityIndicatorView *activity;
@property (atomic,strong)UIView *Animationview;
@property (atomic,strong)UIImageView *Bigload;
@property (atomic,strong)UIImageView *smallload;
@end
