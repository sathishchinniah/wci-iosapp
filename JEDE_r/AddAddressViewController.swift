//
//  AddAddressViewController.swift
//  JEDE_r
//
//  Created by Exarcplus on 25/09/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit

class AddAddressViewController: UIViewController,CLLocationManagerDelegate,GMSMapViewDelegate,ABCGooglePlacesSearchViewControllerDelegate,UIAlertViewDelegate {
    
     var myclass : MyClass!
    @IBOutlet weak var viewmap: GMSMapView!
    @IBOutlet weak var mycenterimg: UIImageView!
    @IBOutlet weak var mytopimg: UIImageView!

    @IBOutlet weak var searchView : UIView!
    @IBOutlet weak var locationtext : UITextField!
    
    var firstload = Bool()
    var locmanager:CLLocationManager!
    var currentlocation = CLLocation()
    var currentadddress:GMSAddress!
    var storeaddress : String!
    var isInitiatedFromLocationChange = Bool()
    var marker = GMSMarker()
    let locationPinImage:String = "LocationPin"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myclass = MyClass()
        mytopimg.isHidden = true
        mycenterimg.isHidden = true
        self.navigationController?.navigationBar.isHidden = true
        
        searchView.layer.masksToBounds =  false
        searchView.layer.shadowColor = UIColor.black.cgColor;
        searchView.layer.shadowOffset = CGSize(width: 1.5, height: 1.5)
        searchView.layer.shadowOpacity = 0.5
        searchView.isHidden = false
        
        let dispatchTime1: DispatchTime = DispatchTime.now() + Double(Int64(0.8 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime1, execute: {
            
            UIView.animate(withDuration: 0.5, delay: 0.5, options: [.repeat, .curveEaseOut, .autoreverse], animations: {
                
                let transition = CATransition()
                transition.duration = 0.5
                transition.type = kCATransitionPush
                transition.subtype = kCATransitionFromBottom
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                self.searchView.layer.add(transition, forKey: nil)
                self.searchView.isHidden = false
            }, completion: nil)
            
        })

        viewmap.clear()
        locmanager = CLLocationManager()
        locmanager.delegate = self
        locmanager.desiredAccuracy = kCLLocationAccuracyBest
        locmanager.requestAlwaysAuthorization()
        let dispatchTime2: DispatchTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime2, execute: {
            self.locmanager.startUpdatingLocation()
        })
        
        viewmap.settings.myLocationButton=true
        viewmap.isMyLocationEnabled = true
        viewmap.settings.compassButton=true
        viewmap.delegate = self;
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error:" + error.localizedDescription)
        if !CLLocationManager .locationServicesEnabled()
        {
            let alert: UIAlertView = UIAlertView(title: "Turn on Location Service!", message: "Please Turn on Location to known where you are. We promise to keep your location private.", delegate: self, cancelButtonTitle: "Settings")
            alert.delegate = self
            alert.restorationIdentifier = "userLocation"
            alert.tag = 100
            alert.show()
        }
        else
        {
            if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.denied
            {
                let alert: UIAlertView = UIAlertView(title: "Enable Location Service!", message: "Please enable Location Based Services for Loocater to known where you are. We promise to keep your location private.", delegate: self, cancelButtonTitle: "Settings")
                alert.delegate = self
                alert.restorationIdentifier = "userLocation"
                alert.tag = 200
                alert.show()
            }
            else
            {
                manager.startUpdatingLocation()
            }
        }
    }
    
    func alertView(_ View: UIAlertView, clickedButtonAt buttonIndex: Int)
    {
        switch buttonIndex{
        case 1:
            NSLog("Retry");
            break;
        case 0:
            NSLog("Dismiss");
            if View.restorationIdentifier=="userLocation"
            {
                if buttonIndex == 0
                {
                    if View.tag == 100
                    {
                        //                        UIApplication.shared.openURL(URL(string:"prefs:root=LOCATION_SERVICES")!)
                        UIApplication.shared.openURL(URL(string:UIApplicationOpenSettingsURLString)!)
                        //UIApplication .shared .openURL(NSURL (string: "prefs:root=LOCATION_SERVICES")! as URL)
                    }
                    else if View.tag == 200
                    {
                        //                        UIApplication.shared.openURL(URL(string:"prefs:root=LOCATION_SERVICES")!)
                        UIApplication.shared.openURL(URL(string:UIApplicationOpenSettingsURLString)!)
                        //UIApplication .shared .openURL(NSURL (string: "prefs:root=LOCATION_SERVICES")! as URL)
                        //                       UIApplication .shared .openURL(NSURL (string: UIApplicationOpenSettingsURLString)! as URL);
                    }
                }
            }
            break;
        default:
            NSLog("Default");
            break;
            //Some code here..
        }
    }
    
    func mapView(_ mapView: GMSMapView!, didChange position: GMSCameraPosition!)
    {
        print("MAPVIEW Did CHANGE: \(mapView.myLocation)")
        if  firstload == true
        {
            //mycenterimg.image = UIImage (named: "cross_marker.png")
            mytopimg.image = UIImage (named: "check2.png")
            mycenterimg.isHidden = true
        }
        
        mytopimg.isHidden = true
    }
    
    func mapView(_ mapView: GMSMapView!, didBeginDragging marker: GMSMarker!) {
        print("didBeginDragging")
    }
    
    func mapView(_ mapView: GMSMapView!, didEndDragging marker: GMSMarker!) {
        print("didEndDragging")
    }

    
    func mapView(_ mapView: GMSMapView!, idleAt position: GMSCameraPosition!)
    {
        print("idleAt Position \(position.target)")
        if  firstload
        {
            if !isInitiatedFromLocationChange {
                //mycenterimg.image = UIImage (named: "no cross marker.png")
                //            mycenterimg.image = UIImage (named: "check2.png")
                
                mytopimg.image = UIImage (named: "check2.png")
                mytopimg.isHidden = true
                mycenterimg.isHidden = true
                
//                let point = mapView.center
//                let coor = mapView.projection .coordinate(for: point)
                let loc = CLLocation.init(latitude: position.target.latitude, longitude: position.target.longitude)
                currentlocation = loc;
                self.addAnnotationMarker(location: CLLocationCoordinate2DMake(currentlocation.coordinate.latitude, currentlocation.coordinate.longitude))
                self.getAddressFromLatLon(location: currentlocation)
            } else {
                isInitiatedFromLocationChange = !isInitiatedFromLocationChange
            }
        }
    }
    
    func getAddressFromLatLon(location:CLLocation)
    {
        //        if AFNetworkReachabilityManager.shared().isReachable
        //        {
        let str = String(format: "%f,%f",location.coordinate.latitude,location.coordinate.longitude)
        print(str)
        let geoCoder = GMSGeocoder()
        geoCoder.reverseGeocodeCoordinate(location.coordinate, completionHandler: {(respones,err) in
            if err == nil
            {
                if respones != nil
                {
                    let gmaddr = respones?.firstResult()
                    print(gmaddr)
                    self.currentadddress = gmaddr
                    var addrarray : NSMutableArray! = NSMutableArray()
                    let count:Int = (gmaddr?.lines!.count)!
                    for c in 0 ..< count
                    {
                        addrarray.add(gmaddr?.lines![c] as! String)
                    }
                    
                    let formaddr = addrarray.componentsJoined(by: ", ");
                    NSLog("%@",formaddr)
                    self.locationtext.text = formaddr
                    addrarray = nil
                    self.storeaddress = formaddr
                    //self.navigationItem.title = formaddr
                    self.storeaddress = str
                }
                else
                {
                    self.locationtext.text = "Address not found"
                    self.storeaddress = "Address not found"
                    self.storeaddress = str
                    //self.navigationItem.title = "Address not found"
                }
            }
            else
            {
                    //self.class.ShowsinglebutAlertwithTitle(title: self.myclass.StringfromKey(Key: "Loocater"), withAlert:self.myclass.StringfromKey(Key: "errorinretrive"), withIdentifier:"error")
            }
        })
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        viewmap.settings.compassButton = true
        print("didUpdateToLocation: %@", locations.last)
        if  firstload == false
        {
            firstload = true
            isInitiatedFromLocationChange = true
            let dispatchTime2: DispatchTime = DispatchTime.now() + Double(Int64(0.5 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: dispatchTime2, execute: {
                self.locmanager.stopUpdatingLocation()
            })
            currentlocation = locations.last!
            viewmap.clear()
            CATransaction.begin()
            CATransaction.setValue(Int(1.1), forKey: kCATransactionAnimationDuration)
            // YOUR CODE IN HERE
            let camera = GMSCameraPosition.init(target: currentlocation.coordinate, zoom: 13, bearing: 0, viewingAngle: 0)
            viewmap.animate(to: camera)
            self.addAnnotationMarker(location: CLLocationCoordinate2DMake(currentlocation.coordinate.latitude, currentlocation.coordinate.longitude))
            self.getAddressFromLatLon(location: currentlocation)
            CATransaction.commit()
        }
    }
    
   
    func addAnnotationMarker(location:CLLocationCoordinate2D) {
        viewmap.clear()
        marker = GMSMarker(position: location)
        marker.icon = UIImage(named: locationPinImage)
        
        marker.map = viewmap
    }
    
    
    
    @IBAction func Donebutton(_ x:AnyObject)
    {
        
        let n: Int! = self.navigationController?.viewControllers.count
        let myUIViewController = self.navigationController?.viewControllers[n-2]
        if myUIViewController is AddLooViewController
        {
            let map = myUIViewController as! AddLooViewController
            map.relodBackground(caddress: self.storeaddress)
        }
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func Cancelbutton(_ x:AnyObject)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func searchbuttonclick(sender: UIButton)
    {
        let searchViewController = ABCGooglePlacesSearchViewController()
        searchViewController.delegate = self as ABCGooglePlacesSearchViewControllerDelegate
        let navigationController = UINavigationController(rootViewController: searchViewController)
        self.present(navigationController, animated: true, completion: { _ in })
    }
    
    public func searchViewController(_ controller: ABCGooglePlacesSearchViewController!, didReturn place: ABCGooglePlace!)
    {
        viewmap.settings.compassButton = true
        let coordinatesString = "(\(place.location.coordinate.latitude),\(place.location.coordinate.longitude))"
        //searchLocation = place.location
        print("\(coordinatesString)")
        viewmap.clear()
        let camera = GMSCameraPosition.camera(withLatitude: place.location.coordinate.latitude, longitude: place.location.coordinate.longitude, zoom: 11)
        viewmap.camera = camera
        
    }
    
}

