//
//  MyRatingViewController.swift
//  JEDE_r
//
//  Created by Exarcplus on 28/09/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import AFNetworking
import SDWebImage

class MyRatingViewController: UIViewController
    {
    
    var myclass : MyClass!
    @IBOutlet weak var star1 : UIButton!
    @IBOutlet weak var star2 : UIButton!
    @IBOutlet weak var star3 : UIButton!
    @IBOutlet weak var star4 : UIButton!
    @IBOutlet weak var star5 : UIButton!
    @IBOutlet weak var down : UIButton!
    @IBOutlet weak var confirmRate : UIButton!
    @IBOutlet weak var likeButton: SparkButton!
    @IBOutlet weak var cleanratingview : EZRatingView!
    @IBOutlet weak var accessratingview : EZRatingView!
    @IBOutlet weak var waitingtimeratingview : EZRatingView!
    @IBOutlet weak var commentratingview : EZRatingView!
    @IBOutlet weak var allratingView : UIView!
    @IBOutlet weak var ratebutton : UIButton!
    @IBOutlet weak var notNowbtn : UIButton!
    @IBOutlet weak var textview : UITextView!
    @IBOutlet weak var updownimg : UIImageView!
    @IBOutlet weak var myheight : NSLayoutConstraint!
     @IBOutlet weak var bannerimg : UIImageView!
    var checkstr : String!
    //@IBOutlet weak var scrollview : UIScrollView!
    var getdic : NSMutableDictionary!
    
    
    var Userdic = NSMutableDictionary()
    var passuserid : String!
    var passlooid : String!
    var star1str : String!
    var star2str : String!
    var star3str : String!
    var star4str : String!
    var star5str : String!
    var isLiked : Bool!
    var downstr : String!
    var passoverall : String!
    var passcleanliness : String = ""
    var passwaitingtime : String!
    var passcomment : String!
    var passaccess : String!
    var bookingidstr : String!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        myclass = MyClass()
        let view = UIView(frame:  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 20))
        view.backgroundColor = UIColor.init(red: 27.0/255.0, green: 34.0/255.0, blue: 40.0/255.0, alpha: 1.0)
        self.navigationController?.view.addSubview(view)
        
        //let borderColor = UIColor.whiteColor.Cgcolor()
        let borderColor = UIColor.black.cgColor
        
        textview.layer.borderColor = borderColor.copy(alpha: 0.1)
        textview.layer.borderWidth = 1.0;
        textview.layer.cornerRadius = 5.0;
        
        if bookingidstr != nil
        {
            print(bookingidstr)
        }
        else
        {
            bookingidstr = ""
        }

        
        if getdic != nil
        {
            print(getdic)
            //loonamelab.text = getdic.value(forKey: "loo_name") as? String
            let typestr = getdic.value(forKey: "type") as! String
            if typestr == "Public"
            {
                self.notNowbtn.isHidden = false
                self.ratebutton.isHidden = false
            }
            else
            {
                self.ratebutton.isHidden = false
                self.notNowbtn.isHidden = false
            }
            passlooid =  getdic.value(forKey: "loo_id") as! String
            let imageurl = getdic.value(forKey: "loo_image") as? String
            //print(imageurl!)
            if imageurl == "" || imageurl?.characters.count == 0 || imageurl == nil
            {
               // scrollview.addTwitterCover(with: UIImage(named: "Toilets-Defualt.png"))
                bannerimg.image = UIImage.init(named: "default_image.png")
            }
            else
            {
//                let url = URL(string: imageurl!)
//                let data = try? Data(contentsOf: url!)
                let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                    print(self)
                }
                bannerimg.sd_setImage(with: NSURL(string:imageurl! ?? "default_image.png") as URL!, completed: block)
                bannerimg.contentMode = UIViewContentMode.scaleToFill;
                //bannerimg.sd_setImage(with: URL(string: "loo_image"), placeholderImage: UIImage(named: "placeholder.png"))
                //bannerimg.image = UIImage.init(data: <#T##Data#>)
                //make                                                    sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
               // scrollview.addTwitterCover(with: UIImage(data: data!))
            }
            
            //            let favouritestr = getdic.value(forKey: "favourite_status") as? String
            //            print(favouritestr!)
            //            if favouritestr == "0"
            //            {
            //                self.isLiked = false
            //                self.likeButton.setImage(UIImage(named: "Fav.png"), for: UIControlState())
            //                self.likeButton.unLikeBounce(0.4)
            //            }
            //            else
            //            {
            //                self.isLiked = true
            //                self.likeButton.setImage(UIImage(named: "LIKE.png"), for: UIControlState())
            //                self.likeButton.likeBounce(0.6)
            //            }
            
            self.isLiked = false
            self.likeButton.setImage(UIImage(named: "Fav.png"), for: UIControlState())
            self.likeButton.unLikeBounce(0.4)
            
        }
        else
        {
            self.isLiked = false
            self.likeButton.setImage(UIImage(named: "Fav.png"), for: UIControlState())
            self.likeButton.unLikeBounce(0.4)
            passlooid = ""
          //  scrollview.addTwitterCover(with: UIImage(named: "Toilets-Defualt.png"))
            bannerimg.image = UIImage.init(named: "default_image.png")
            
        }
        
        if UserDefaults.standard.object(forKey: "Logindetail") == nil
        {
            passuserid = ""
        }
        else
        {
//            let placesData = UserDefaults.standard.object(forKey: "Logindetail") as? NSData
//            self.Userdic.removeAllObjects()
//            self.Userdic.addEntries(from: NSKeyedUnarchiver.unarchiveObject(with: placesData! as Data) as! NSDictionary as [NSObject : AnyObject])
            
        }
        
        
        updownimg.image = UIImage.init(named: "down-arrow.png")
        myheight.constant = 191
        allratingView.isHidden = false
        checkstr = "close"
        
        star1.setImage(UIImage.init(named: "starunselect1.png"), for: .normal)
        star2.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
        star3.setImage(UIImage.init(named: "starunselect3.png"), for: .normal)
        star4.setImage(UIImage.init(named: "starunselect4.png"), for: .normal)
        star5.setImage(UIImage.init(named: "starunselect5.png"), for: .normal)
        
        self.cleanratingview?.value = 0
        //self.cleanratingview?.stepInterval = 1.0
        self.cleanratingview?.isUserInteractionEnabled = true
        self.cleanratingview?.isSelected = true
        
        
        self.accessratingview?.value = 0
        //self.accessratingview?.stepInterval = 1.0
        self.accessratingview?.isUserInteractionEnabled = true
        self.accessratingview?.isSelected = true
        
        self.waitingtimeratingview?.value = 0
        //self.waitingtimeratingview?.stepInterval = 1.0
        self.waitingtimeratingview?.isUserInteractionEnabled = true
        self.waitingtimeratingview?.isSelected = true
        
        self.commentratingview?.value = 0
        //self.commentratingview?.stepInterval = 1.0
        self.commentratingview?.isUserInteractionEnabled = true
        self.commentratingview?.isSelected = true
        
        passcleanliness = ""
        passaccess = ""
        passwaitingtime = ""
        passcomment = ""
        
        star1str = "0"
        star2str = "0"
        star3str = "0"
        star4str = "0"
        star5str = "0"
        
        downstr = "0"
       

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let placesData = UserDefaults.standard.object(forKey: "Logindetail") as? NSDictionary
        self.Userdic.removeAllObjects()
        if placesData != nil{
            self.Userdic.addEntries(from: (placesData as? [String : Any])!)
        }
        print(self.Userdic)
        passuserid = self.Userdic.value(forKey: "user_id") as? String
        print(passuserid)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
        @IBAction func star1(_sender:UIButton)
    {
        if star1str == "0"
        {
            star1.setImage(UIImage.init(named: "starselect1.png"), for: .normal)
            star1str = "1"
            self.cleanratingview?.value = 1.0
            self.accessratingview?.value = 1.0
            self.waitingtimeratingview?.value = 1.0
            self.commentratingview?.value = 1.0
            print("Selected rating: \(UInt((cleanratingview?.value)!))")
            passcleanliness = String(format: "%d",(UInt((cleanratingview?.value)!)))
            print(passcleanliness)
            print("Selected rating: \(UInt((accessratingview?.value)!))")
            passaccess = String(format: "%d",(UInt((accessratingview?.value)!)))
            print(passaccess)
            print("Selected rating: \(UInt((waitingtimeratingview?.value)!))")
            passwaitingtime = String(format: "%d",(UInt((waitingtimeratingview?.value)!)))
            print(passwaitingtime)
            print("Selected rating: \(UInt((commentratingview?.value)!))")
            passcomment = String(format: "%d",(UInt((commentratingview?.value)!)))
            print(passcomment)
            
        }
        else
        {
            star1.setImage(UIImage.init(named: "starunselect1.png"), for: .normal)
            star1str = "0"
        }
         self.avglink()
        
    }
    
    @IBAction func star2(_sender:UIButton)
    {
        if star2str == "0"
        {
            star2.setImage(UIImage.init(named: "starselect2.png"), for: .normal)
            star1.setImage(UIImage.init(named: "starselect1.png"), for: .normal)
            star2str = "1"
            self.cleanratingview?.value = 2.0
            self.accessratingview?.value = 2.0
            self.waitingtimeratingview?.value = 2.0
            self.commentratingview?.value = 2.0
            
            print("Selected rating: \(UInt((cleanratingview?.value)!))")
            passcleanliness = String(format: "%d",(UInt((cleanratingview?.value)!)))
            print(passcleanliness)
            print("Selected rating: \(UInt((accessratingview?.value)!))")
            passaccess = String(format: "%d",(UInt((accessratingview?.value)!)))
            print(passaccess)
            print("Selected rating: \(UInt((waitingtimeratingview?.value)!))")
            passwaitingtime = String(format: "%d",(UInt((waitingtimeratingview?.value)!)))
            print(passwaitingtime)
            print("Selected rating: \(UInt((commentratingview?.value)!))")
            passcomment = String(format: "%d",(UInt((commentratingview?.value)!)))
            print(passcomment)
        }
        else
        {
            star2.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
            star2str = "0"
        }
         self.avglink()
    }
    
    @IBAction func star3(_sender:UIButton)
    {
        if star3str == "0"
        {
            star2.setImage(UIImage.init(named: "starselect2.png"), for: .normal)
            star1.setImage(UIImage.init(named: "starselect1.png"), for: .normal)
            star3.setImage(UIImage.init(named: "starselect3.png"), for: .normal)
            star3str = "1"
            self.cleanratingview?.value = 3.0
            self.accessratingview?.value = 3.0
            self.waitingtimeratingview?.value = 3.0
            self.commentratingview?.value = 3.0
            
            print("Selected rating: \(UInt((cleanratingview?.value)!))")
            passcleanliness = String(format: "%d",(UInt((cleanratingview?.value)!)))
            print(passcleanliness)
            print("Selected rating: \(UInt((accessratingview?.value)!))")
            passaccess = String(format: "%d",(UInt((accessratingview?.value)!)))
            print(passaccess)
            print("Selected rating: \(UInt((waitingtimeratingview?.value)!))")
            passwaitingtime = String(format: "%d",(UInt((waitingtimeratingview?.value)!)))
            print(passwaitingtime)
            print("Selected rating: \(UInt((commentratingview?.value)!))")
            passcomment = String(format: "%d",(UInt((commentratingview?.value)!)))
            print(passcomment)
        }
        else
        {
            star3.setImage(UIImage.init(named: "starunselect3.png"), for: .normal)
            star3str = "0"
        }
         self.avglink()
    }
    
    @IBAction func star4(_sender:UIButton)
    {
        if star4str == "0"
        {
            star2.setImage(UIImage.init(named: "starselect2.png"), for: .normal)
            star1.setImage(UIImage.init(named: "starselect1.png"), for: .normal)
            star3.setImage(UIImage.init(named: "starselect3.png"), for: .normal)
            star4.setImage(UIImage.init(named: "starselect4.png"), for: .normal)
            star4str = "1"
            self.cleanratingview?.value = 4.0
            self.accessratingview?.value = 4.0
            self.waitingtimeratingview?.value = 4.0
            self.commentratingview?.value = 4.0
            
            print("Selected rating: \(UInt((cleanratingview?.value)!))")
            passcleanliness = String(format: "%d",(UInt((cleanratingview?.value)!)))
            print(passcleanliness)
            print("Selected rating: \(UInt((accessratingview?.value)!))")
            passaccess = String(format: "%d",(UInt((accessratingview?.value)!)))
            print(passaccess)
            print("Selected rating: \(UInt((waitingtimeratingview?.value)!))")
            passwaitingtime = String(format: "%d",(UInt((waitingtimeratingview?.value)!)))
            print(passwaitingtime)
            print("Selected rating: \(UInt((commentratingview?.value)!))")
            passcomment = String(format: "%d",(UInt((commentratingview?.value)!)))
            print(passcomment)
        }
        else
        {
            star4.setImage(UIImage.init(named: "starunselect4.png"), for: .normal)
            star4str = "0"
        }
         self.avglink()
    }
    
    
    @IBAction func star5(_sender:UIButton)
    {
        if star5str == "0"
        {
            star2.setImage(UIImage.init(named: "starselect2.png"), for: .normal)
            star1.setImage(UIImage.init(named: "starselect1.png"), for: .normal)
            star3.setImage(UIImage.init(named: "starselect3.png"), for: .normal)
            star4.setImage(UIImage.init(named: "starselect4.png"), for: .normal)
            star5.setImage(UIImage.init(named: "starselect5.png"), for: .normal)
            star5str = "1"
            self.cleanratingview?.value = 5.0
            self.accessratingview?.value = 5.0
            self.waitingtimeratingview?.value = 5.0
            self.commentratingview?.value = 5.0
            
            print("Selected rating: \(UInt((cleanratingview?.value)!))")
            passcleanliness = String(format: "%d",(UInt((cleanratingview?.value)!)))
            print(passcleanliness)
            print("Selected rating: \(UInt((accessratingview?.value)!))")
            passaccess = String(format: "%d",(UInt((accessratingview?.value)!)))
            print(passaccess)
            print("Selected rating: \(UInt((waitingtimeratingview?.value)!))")
            passwaitingtime = String(format: "%d",(UInt((waitingtimeratingview?.value)!)))
            print(passwaitingtime)
            print("Selected rating: \(UInt((commentratingview?.value)!))")
            passcomment = String(format: "%d",(UInt((commentratingview?.value)!)))
            print(passcomment)
        }
        else
        {
            star5.setImage(UIImage.init(named: "starunselect5.png"), for: .normal)
            star5str = "0"
        }
         self.avglink()
    }
    
    @IBAction func down(_sender:UIButton)
    {
            if checkstr == "close"
            {
                myheight.constant = 191
                updownimg.image = UIImage.init(named: "up-arrow.png")
                checkstr = "open"
                allratingView.isHidden = false
            }
            else
            {
                checkstr = "close"
                updownimg.image = UIImage.init(named: "down-arrow.png")
                myheight.constant = 0
                allratingView.isHidden = true
            }
    }
    
    
    @IBAction func confirmRate(_sender:UIButton)
    {
        if cleanratingview?.value == 0
        {
            let alert = UIAlertController(title: "Sub Rating", message: "Add Sub Rating", preferredStyle: UIAlertControllerStyle.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)

        }
        else
        {
            self.avglink()
        }
        if accessratingview?.value == 0
        {
            let alert = UIAlertController(title: "Sub Rating", message: "Add Sub Rating", preferredStyle: UIAlertControllerStyle.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            self.avglink()
        }
        if waitingtimeratingview?.value == 0
        {
            let alert = UIAlertController(title: "Sub Rating", message: "Add Sub Rating", preferredStyle: UIAlertControllerStyle.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            self.avglink()
        }
        if commentratingview?.value == 0
        {
            let alert = UIAlertController(title: "Sub Rating", message: "Add Sub Rating", preferredStyle: UIAlertControllerStyle.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            self.avglink()
        }
        myheight.constant = 0
        allratingView.isHidden = true
    }
    
    func avglink()
    {
        let a = cleanratingview?.value
        let b = accessratingview?.value
        let c = waitingtimeratingview?.value
        let d = commentratingview?.value
        
        var z = a!+b!+c!+d!
        if a == 0
        {
            z = 0
        }
        else if b == 0
        {
            z = 0
        }
        else if c == 0
        {
            z = 0
        }
        else if d == 0
        {
            z = 0
        }
        print(z)
        let x = z/4
        print(x)
        print("Selected index: \(x)")
        passoverall = String(format: "%d",(UInt(x)))
        print(passoverall)
        
        if x < 0.5
        {
            star1.setImage(UIImage.init(named: "starunselect1.png"), for: .normal)
            star2.setImage(UIImage.init(named: "starunselect2.png"), for: .normal)
            star3.setImage(UIImage.init(named: "starunselect3.png"), for: .normal)
            star4.setImage(UIImage.init(named: "starunselect4.png"), for: .normal)
            star5.setImage(UIImage.init(named: "starunselect5.png"), for: .normal)
        }
        else if x < 1.5
        {
            star1.setImage(UIImage.init(named: "starselect1.png"), for: .normal)
            //passaccess = String(format: "%d",(UInt((cleanratingview?.value)!)))
        }
        else if x < 2.5
        {
            star2.setImage(UIImage.init(named: "starselect2.png"), for: .normal)
        }
        else if x < 3.5
        {
            star3.setImage(UIImage.init(named: "starselect3.png"), for: .normal)
        }
        else if x < 4.5
        {
            star4.setImage(UIImage.init(named: "starselect4.png"), for: .normal)
        }
        else if x < 5.5
        {
            star5.setImage(UIImage.init(named: "starselect5.png"), for: .normal)
        }
        myheight.constant = 0
        allratingView.isHidden = true
    }
    
    @IBAction func cleanratingview(_ sender: EZRatingView)
    {
        print("Selected rating: \(UInt(sender.value))")
        passcleanliness = String(format: "%d",(UInt(sender.value)))
        print(passcleanliness)
        
    }
    
    @IBAction func accessratingChange(_ sender: EZRatingView)
    {
        print("Selected rating: \(UInt(sender.value))")
        passaccess = String(format: "%d",(UInt(sender.value)))
        print(passaccess)
    }
    
    @IBAction func waitingtimeratingChange(_ sender: EZRatingView)
    {
        print("Selected rating: \(UInt(sender.value))")
        passwaitingtime = String(format: "%d",(UInt(sender.value)))
        print(passwaitingtime)
    }
    
    @IBAction func commentratingChange(_ sender: EZRatingView)
    {
        print("Selected rating: \(UInt(sender.value))")
        passcomment = String(format: "%d",(UInt(sender.value)))
        print(passcomment)
    }
    
    
    @IBAction func addfavouritebutton(_sender : UIButton)
    {
        if UserDefaults.standard.object(forKey: "Logindetail") == nil
        {
            let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(mainview, animated: true)
        }
        else
        {
            isLiked = !isLiked
            if isLiked == true
            {
                likeButton.setImage(UIImage(named: "LIKE.png"), for: UIControlState())
                likeButton.likeBounce(0.6)
                likeButton.animate()
                isLiked = false
            }
            else
            {
                likeButton.setImage(UIImage(named: "Fav.png"), for: UIControlState())
                likeButton.unLikeBounce(0.4)
                isLiked = true
            }
            self.favlink()
        }
    }
    
    
    
    func favlink()
    {
        if AFNetworkReachabilityManager.shared().isReachable
        {
            let urlstring : String!
            isLiked = !isLiked
            if isLiked == true
            {
                urlstring = String(format:"%@/fav.php?user_id=%@&loo_id=%@&fav_status=1",kBaseURL,passuserid,passlooid)
                print(urlstring)
            }
            else
            {
                urlstring = String(format:"%@/fav.php?user_id=%@&loo_id=%@&fav_status=0",kBaseURL,passuserid,passlooid)
                print(urlstring)
            }
            
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: false, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    if jsonObject.count != 0
                    {
                        let copyarr = jsonObject;
                        print(copyarr)
                        
                    }
                }
                else
                {
                    NSLog("jsonObject=error");
                }
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
    
    
    @IBAction func ratebutton(_sender: UIButton)
    {
        self.runlink()
    }

    func runlink()
    {
        if AFNetworkReachabilityManager.shared().isReachable
        {
            print(passuserid)
            print(passlooid)
            print(passoverall)
            print(passcleanliness)
            if passcleanliness == ""
            {
                passcleanliness = "0"
            }
            print(passaccess)
            if passaccess == ""
            {
                passaccess = "0"
            }
            print(passwaitingtime)
            if passwaitingtime == ""
            {
                passwaitingtime = "0"
            }
            print(passcomment)
            if passcomment == ""
            {
                passcomment = "0"
            }
            print(self.textview.text)
            let urlstring = String(format:"%@/add_rating.php?user_id=%@&loo_id=%@&overall=%@&cleanliness=%@&access=%@&waiting_time=%@&comment=%@&review=%@",kBaseURL ?? "",passuserid ?? "",passlooid ?? "",passoverall ?? "",passcleanliness ?? "",passaccess ?? "",passwaitingtime ?? "",passcomment ?? "",self.textview.text ?? "")
            print(urlstring)
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: true, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    if jsonObject.count != 0
                    {
                        let str = (jsonObject.object(at: 0) as AnyObject).value(forKey: "message") as? String
                        if str == "success"
                        {
                            // i am added //
                            KGModal.sharedInstance().hide(animated: true)
                             let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            //appDelegate.SideMenu.showLeftView(animated: true, completionHandler: nil)
                            let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
                            //let nav = UINavigationController.init(rootViewController: mainview)
                            self.navigationController?.pushViewController(mainview, animated: false)
                            //self.present(nav, animated: true, completion: nil)
                            //KGModal.sharedInstance().hide(animated: true)
//                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                            let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
//                            appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
//                            let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
//                            currentview.navigationController?.popToViewController(mainview, animated: true)
//                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                            let currentview = appDelegate.SideMenu.rootViewController as! UIViewController
//                            appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
//                            //(mainview[0], animated: true)
//                            let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
//                            let nav = UINavigationController.init(rootViewController: mainview)
//                            self.navigationController?.popToViewController(mainview, animated: true)//(mainview, animated: true)
                             //self.myclass.ShowsinglebutAlertwithTitle(<#String#>, withAlert:"You have Rated Successfully", withIdentifier:"")
//                            let categorystring = NSMutableArray()
//                            let nsmutdic = NSMutableDictionary()
//                            nsmutdic.setObject("", forKey:"gender" as NSCopying);
//                            nsmutdic.setObject("", forKey:"KM" as NSCopying);
//                            nsmutdic.setObject("", forKey:"wheelchair" as NSCopying);
//                            nsmutdic.setObject("", forKey:"parking" as NSCopying);
//                            nsmutdic.setObject("", forKey:"rating" as NSCopying);
//                            nsmutdic.setObject("", forKey:"distance" as NSCopying);
//                            nsmutdic.setObject("", forKey:"price" as NSCopying);
//                            nsmutdic.setObject(categorystring, forKey:"category" as NSCopying);
//                            print(nsmutdic)
                            
                        }
                        else
                        {
                            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:str!, withIdentifier:"")
                        }
                        
                    }
                }
                else
                {
                    NSLog("jsonObject=error");
                }
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
    
    
    @IBAction func notNowbtn(_sender: UIButton)
    {
        KGModal.sharedInstance().hide(animated: true)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //appDelegate.SideMenu.showLeftView(animated: true, completionHandler: nil)
        let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
       //let nav = UINavigationController.init(rootViewController: mainview)
        self.navigationController?.pushViewController(mainview, animated: false)
       // self.present(nav, animated: true, completion: nil)
//        KGModal.sharedInstance().hide(animated: true)
//        let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
//        let nav = UINavigationController.init(rootViewController: mainview)
//        self.navigationController?.popToViewController(mainview, animated: true)
//
     //  self.changelink()
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
//        appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
//        let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
//        currentview.pushViewController(mainview, animated: true)
    }
    
   
    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
