//
//  WebForProviderViewController.swift
//  JEDE_r
//
//  Created by apple on 30/05/18.
//  Copyright © 2018 Exarcplus. All rights reserved.
//

import UIKit
import LGSideMenuController

class WebForProviderViewController: UIViewController ,UIWebViewDelegate{

    @IBOutlet weak var skip: UIButton!
    @IBOutlet weak var ProviderWebView: UIWebView!
    override func viewDidLoad() {
        
        super.viewDidLoad()
       
        self.navigationController?.navigationBar.isHidden = true
        let url : NSURL! = NSURL(string: "http://wc-i.net/#kontakt")
        ProviderWebView?.loadRequest(URLRequest(url: url as URL))
        ProviderWebView?.isOpaque = false;
        ProviderWebView?.backgroundColor = UIColor.clear
        ProviderWebView?.scalesPageToFit = true;
        ProviderWebView?.delegate = self  // Add this line to set the delegate of webView
        
        self.view.addSubview(ProviderWebView!)
        
        func webViewDidFinishLoad(webView : UIWebView) {
            //Page is loaded do what you want
        }
        self.view.backgroundColor = UIColor.white
        //skip.addTarget(self, action: #selector(Skip(_:)), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }
    @IBAction func skipAction(_ sender: UIBarButtonItem) {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.SideMenu.showLeftView(animated: true, completionHandler: nil)
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadsidemenu"), object: nil, userInfo: nil)
    }
    @IBAction func Skip(_ sender: Any) {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
//        appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
//        let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
//        present(mainview, animated: true, completion: nil)
//        //currentview.pushViewController(mainview, animated: false)
//    // self.dismiss(animated: true, completion: nil)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let mainview = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
        let valueToSave = "noneedrunlink"
        let nav = UINavigationController.init(rootViewController: mainview)
        appDelegate.SideMenu = LGSideMenuController.init(rootViewController: nav)
        appDelegate.SideMenu.setLeftViewEnabledWithWidth(280, presentationStyle: .slideAbove, alwaysVisibleOptions:[])
        appDelegate.SideMenuView = UIStoryboard(name:"Main", bundle: nil).instantiateViewController(withIdentifier: "SidemenuViewController") as! SidemenuViewController
        appDelegate.SideMenu.leftViewStatusBarVisibleOptions = .onAll
        appDelegate.SideMenu.leftViewStatusBarStyle = .lightContent
       // UserDefaults.standard.set(true,forKey:"launchedBefore")
        //        appDelegate.SideMenuView.selectedpage = 0;
        var rect = appDelegate.SideMenuView.view.frame;
        rect.size.width = 280;
        appDelegate.SideMenuView.view.frame = rect
        appDelegate.SideMenu.leftView().addSubview(appDelegate.SideMenuView.view)
        //UserDefaults.standard.set(true,forKey:"launchedBefore")
        print(appDelegate.SideMenu)
        self.present(appDelegate.SideMenu, animated:true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
