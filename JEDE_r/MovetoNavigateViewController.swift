//
//  MovetoNavigateViewController.swift
//  JEDE_r
//
//  Created by Exarcplus on 22/05/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import SDWebImage
import CoreLocation
import AFNetworking
import Alamofire
import SwiftyJSON
import LGSideMenuController
import Crashlytics
import Firebase

class MovetoNavigateViewController: UIViewController,CLLocationManagerDelegate,GMSMapViewDelegate,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var termsOfUse: UIButton!
    var myclass : MyClass!
    @IBOutlet weak var viewmap: GMSMapView!
    var locmanager:CLLocationManager!
    var currentlocation = CLLocation()
    var endlocation = CLLocation()
    var firstload = Bool()
    var currentadddress:GMSAddress!
    var markers = GMSMarker()
    var bookingidstr : String!
    var passLocationDetails:String!
    var selectdic : NSMutableDictionary!
    var destlati : NSNumber!
    var destlong : NSNumber!
    var myarr = NSMutableArray()
    var selectdicthis : NSDictionary!
    
    @IBOutlet weak var reachedpopupview : ReachedViewPopup!
    @IBOutlet weak var businessfinishedpopupview : BusinessCompletePopup!
    @IBOutlet weak var bottomviewheight : NSLayoutConstraint!
    @IBOutlet weak var bgviewheight : NSLayoutConstraint!
    @IBOutlet weak var loonamelab : UILabel!
    @IBOutlet weak var looaddresslab : UILabel!
    @IBOutlet weak var looimg : UIImageView!
    @IBOutlet weak var menuview : UIView!
    @IBOutlet weak var menutopimg : UIImageView!
    @IBOutlet weak var drivingimg : UIImageView!
    @IBOutlet weak var cycleimg : UIImageView!
    @IBOutlet weak var walkimg : UIImageView!
    @IBOutlet weak var buttonImage: UIButton!
    @IBOutlet weak var AboutFinish: UILabel!
    @IBOutlet weak var About: UITextView!
    
    var Userdic = NSMutableDictionary()
    var looDetails = NSMutableArray()
 
    var menutopstr : String!
    var passgooglemapstr : String!
    var mybool : Bool!
 
    //for animate polyline
    var polyline = GMSPolyline()
    var animationPolyline = GMSPolyline()
    var path = GMSPath()
    var animationPath = GMSMutablePath()
    var i: UInt = 0
    var timer: Timer!
    var isTermsOfUseChecked = Bool()
    var passuserid:String!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        myclass = MyClass()
        isTermsOfUseChecked = false
        print("LOG:selectdic \(selectdic)")
        
        mybool = true
        
        if bookingidstr != nil
        {
            print(bookingidstr)
        }
        else
        {
            bookingidstr = ""
        }
        
        
        if UserDefaults.standard.object(forKey: "Logindetail") == nil
        {
            passuserid = ""
        }
        else
        {
            
            let placesData = UserDefaults.standard.object(forKey: "Logindetail") as? NSDictionary
            self.Userdic.removeAllObjects()
            if placesData != nil{
                self.Userdic.addEntries(from: (placesData as? [String : Any])!)
            }
            print(self.Userdic)
            passuserid = self.Userdic.value(forKey: "user_id") as? String
        }
        
        buttonImage.addTarget(self, action: #selector(navigateToMoreInfo), for: .touchUpInside)
        if selectdic != nil
        {
            // Use common key for address for address and loo_address
            // HACK: If the key address is there then adda value for a new key 'loo_address'
            if let addressValue = selectdic.value(forKey: "address") as? String {
                selectdic.setValue(addressValue, forKey: "loo_address")
            }
            if let imageValue = selectdic.value(forKey: "image") as? String {
                selectdic.setValue(imageValue, forKey: "loo_image")
            }
            //HACK: END
            print(selectdic)
            
            loonamelab.text = selectdic.value(forKey: "loo_name") as? String
            looaddresslab.text = selectdic.value(forKey: "loo_address") as? String
            let imageurl = selectdic.value(forKey: "loo_image") as? String
            if imageurl != nil {
                let imageArray:[String] = (imageurl?.components(separatedBy: ","))!
                let firstImageUrl = imageArray.first!
                if firstImageUrl.characters.count == 0 || firstImageUrl == "" || firstImageUrl == "http://stitchplus.com/JEDE_rpanel/"
                {
                    if let categoryName = selectdic.value(forKey: "category_name") as? String {
                        let categoryImage = self.getDefaultImageForCategory(categoryName: categoryName)
                        buttonImage.setBackgroundImage(UIImage.init(named:categoryImage), for: .normal)
                    } else {
                        buttonImage.setBackgroundImage(UIImage.init(named: "default_image.png"), for: .normal)
                    }
                }
                else
                {
                    self.downloadImageForButton(imageUrlString: firstImageUrl)
                }
            } else {
                buttonImage.setBackgroundImage(UIImage.init(named: "default_image.png"), for: .normal)
            }
            
            
            let dess = selectdic.value(forKey: "loo_location") as? String
            let destilatlongarr = dess?.components(separatedBy: ",") as? NSArray
            let destlatistr = destilatlongarr?.object(at: 0) as? String
            print(destlatistr)
            let destlongstr = destilatlongarr?.object(at: 1) as? String
            print(destlongstr)
            destlati = (destlatistr as! NSString).floatValue as NSNumber
            print(destlati)
            destlong = (destlongstr as! NSString).floatValue as NSNumber
            print(destlong)
            
            walkimg.image = UIImage.init(named: "walkunselect.png")
            drivingimg.image = UIImage.init(named: "carunselect.png")
            cycleimg.image = UIImage.init(named: "cycleunselect.png")
        }
        
        let view = UIView(frame:  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 20))
        view.backgroundColor = UIColor.init(red: 27.0/255.0, green: 34.0/255.0, blue: 40.0/255.0, alpha: 1.0)
        self.view.addSubview(view)
        
        self.navigationController?.navigationBar.isHidden = false
        
        bottomviewheight.constant = 227
        bgviewheight.constant = 240
        
        menutopstr = "close"
        menutopimg.image = UIImage.init(named: "filter.png")
        passgooglemapstr = "walking"
        menuview.isHidden = true
        
        viewmap.clear()
        locmanager = CLLocationManager()
        locmanager.delegate = self
        locmanager.desiredAccuracy = kCLLocationAccuracyBest
        locmanager.activityType = .automotiveNavigation
        locmanager.requestWhenInUseAuthorization()
        viewmap.settings.myLocationButton=false
        viewmap.isMyLocationEnabled = true
        viewmap.settings.compassButton=true
        viewmap.delegate = self;
        locmanager.distanceFilter = 10.0  // Movement threshold for new events
        locmanager.startUpdatingLocation()
        locmanager.startMonitoringSignificantLocationChanges()

        self.AboutFinish.text = "navigation_layout_about".localized
        self.About.text = "navigation_layout_samtext".localized
        self.termsOfUse.backgroundColor = UIColor.init(red: 204/255, green: 204/255, blue: 204/255, alpha: 1)
        self.termsOfUse.setTitle("navigation_layout_terms_and_conditions".localized, for: .normal)
        
        if isPublicToilet() {
            self.termsOfUse.isHidden = true
        } else {
            //If not public toilet then check is the terms of use description is there
            if let termsOfUse = self.selectdic.value(forKey: "terms_conditions") as? String {
                print("termsOfUse data \(termsOfUse)")
            } else {
                self.getLooDetails()
            }
        }

    }

    func  isPublicToilet() -> Bool {
        
        let toiletType = self.selectdic.value(forKey: "type") as? String
        if toiletType == "Public" {
            return true
        }
        return false
    }
    
    func downloadImageForButton(imageUrlString:String)  {
        print("LOG: Download image from server!!")
        Crashlytics.sharedInstance().setObjectValue(imageUrlString, forKey: "DEBUG_LOG:imageUrlString")
        let urlString = URL(string: imageUrlString)
        let sessionManager = URLSession(configuration: .default)
        let downloadTask = sessionManager.dataTask(with: urlString!) { (data, response, error) in
            if let err = error {
                print("LOG:Download of image failed \(err)")
            } else {
                print("LOG:IMAGE downloaded")
                if let imageData = data {
                    print("LOG:IMAGE DATA PRESENT")
                    DispatchQueue.main.async {
                        let image = UIImage(data: imageData)
                        self.buttonImage.setBackgroundImage(image, for: .normal)
                    }
                }
            }
        }
        downloadTask.resume()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func menutop(_sender : UIButton)
    {
        if menutopstr == "close"
        {
            menutopstr = "open"
            menutopimg.image = UIImage.init(named: "close.png")
            menuview.isHidden = false
        }
        else
        {
            menutopstr = "close"
            menutopimg.image = UIImage.init(named: "filter.png")
            menuview.isHidden = true
        }
    }
    
    @IBAction func navigationPOPUP(_ sender: UIButton) {
        self.showCompletionNavigationPopup()
    }
    
    func showCompletionNavigationPopup() {
        let sponser = Bundle.main.loadNibNamed("ReachedViewPopup", owner: self, options: nil)?[0] as! ReachedViewPopup

        KGModal.sharedInstance().show(withContentView: sponser, andAnimated: true)
        
        self.view.endEditing(true)
        sponser.layer.cornerRadius = 20.0
        sponser.clipsToBounds = true
        sponser.loonamelab.text =  "finished_navigation".localized
        sponser.yesbutton.setTitle("done".localized, for: UIControlState.normal)
        sponser.yesbutton.titleLabel?.numberOfLines = 0
        sponser.nobutton.setTitle("no_need_toilets".localized, for: UIControlState.normal)
        sponser.nobutton.titleLabel?.numberOfLines = 0
        
        let imageurl = self.selectdic.value(forKey: "loo_image") as? String;
        if imageurl?.characters.count == 0 || imageurl == "" || imageurl == "http://stitchplus.com/JEDE_rpanel/"
        {
            // sponser.imgview.image = UIImage.init(named: "default_image.png")
        }
        else
        {
            let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                print(self)
            }
        }
        sponser.closeButton.addTarget(self, action: #selector(MovetoNavigateViewController.cancel(sender:)), for: UIControlEvents.touchUpInside)
        sponser.nobutton.addTarget(self, action: #selector(MovetoNavigateViewController.notyet(sender:)), for: UIControlEvents.touchUpInside)
        sponser.yesbutton.addTarget(self, action: #selector(MovetoNavigateViewController.yesbutton(sender:)), for: UIControlEvents.touchUpInside)
    }
    
    @IBAction func drivingbutton(_sender: UIButton)
    {
        passgooglemapstr = "driving"
        drivingimg.image = UIImage.init(named: "carselect.png")
        walkimg.image = UIImage.init(named: "walkunselect.png")
        cycleimg.image = UIImage.init(named: "cycleunselect.png")
        menuview.isHidden = true
        viewmap.clear()
        menutopimg.image = UIImage.init(named: "filter.png")
        menutopstr = "close"
        self.drawPath()
    }
    
    @IBAction func cyclingbutton(_sender: UIButton)
    {
        passgooglemapstr = "cycling"
        drivingimg.image = UIImage.init(named: "carunselect.png")
        walkimg.image = UIImage.init(named: "walkunselect.png")
        cycleimg.image = UIImage.init(named: "cycleselect.png")
        menuview.isHidden = true
        viewmap.clear()
        menutopimg.image = UIImage.init(named: "filter.png")
        menutopstr = "close"
        self.drawPath()
    }
    
    @IBAction func walkingbutton(_sender: UIButton)
    {
        
       
        passgooglemapstr = "walking"
        drivingimg.image = UIImage.init(named: "carunselect.png")
        walkimg.image = UIImage.init(named: "walkselect.png")
        cycleimg.image = UIImage.init(named: "cycleunselect.png")
        menuview.isHidden = true
        menutopimg.image = UIImage.init(named: "filter.png")
        viewmap.clear()
        menutopstr = "close"
        self.drawPath()
    }
    
    @IBAction func backbuttonclick(_sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func uparrowbutton(_sender: UIButton)
    {
        bottomviewheight.constant = 287
        bgviewheight.constant = 302
    }
    
    @IBAction func downarrowbutton(_sender: UIButton)
    {
        bottomviewheight.constant = 110
        bgviewheight.constant = 155
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error:" + error.localizedDescription)
        if !CLLocationManager .locationServicesEnabled()
        {
            let alert: UIAlertView = UIAlertView(title: "Turn on Location Service!", message: "Please Turn on Location to known where you are. We promise to keep your location private.", delegate: self, cancelButtonTitle: "Settings")
            alert.restorationIdentifier = "userLocation"
            alert.tag = 100
            alert.show()
        }
        else
        {
            if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.denied
            {
                let alert: UIAlertView = UIAlertView(title: "Enable Location Service!", message: "Please enable Location Based Services for WCi to known where you are. We promise to keep your location private.", delegate: self, cancelButtonTitle: "Settings")
                alert.restorationIdentifier = "userLocation"
                alert.tag = 200
                alert.show()
            }
            else
            {
                manager.startUpdatingLocation()
            }
        }
    }
    
    func alertView(_ View: UIAlertView, clickedButtonAt buttonIndex: Int)
    {
        switch buttonIndex
        {
        case 1:
            NSLog("Retry");
            break;
        case 0:
            NSLog("Dismiss");
            if View.restorationIdentifier=="userLocation"
            {
                if buttonIndex == 0
                {
                    if View.tag == 100
                    {
                        UIApplication .shared .openURL(NSURL (string: UIApplicationOpenSettingsURLString)! as URL);
                       
                        //UIApplication .shared .openURL(NSURL (string: "prefs:root=LOCATION_SERVICES")! as URL)
                    }
                    else if View.tag == 200
                    {
                        UIApplication .shared .openURL(NSURL (string: UIApplicationOpenSettingsURLString)! as URL);
                    }
                }
            }
            break;
        default:
            NSLog("Default");
            break;
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        viewmap.settings.compassButton = true
        print("didUpdateToLocation: %@", locations.last!)
        currentlocation = locations.last!
        viewmap.clear()
        let location = CLLocation(latitude: currentlocation.coordinate.latitude, longitude: currentlocation.coordinate.longitude) //changed!!!
        print(location)
        self.drawPath()
    }
    
    func getAddressFromLatLon(location:CLLocation)
    {
        let geoCoder = GMSGeocoder()
        geoCoder.reverseGeocodeCoordinate(location.coordinate, completionHandler: {(respones,err) in
            if err == nil
            {
                if respones != nil
                {
                    let gmaddr = respones?.firstResult()
                    print(gmaddr!)
                    self.currentadddress = gmaddr
                    var addrarray : NSMutableArray! = NSMutableArray()
                    let count:Int = (gmaddr?.lines!.count)!
                    for c in 0 ..< count
                    {
                        addrarray.add(gmaddr?.lines![c] as! String)
                    }
                    
                    let formaddr = addrarray.componentsJoined(by: ", ");
                    NSLog("%@",formaddr)
                    addrarray = nil
                }
                else
                {
//                    self.locationtext.text = "Address not found"
                }
            }
            else
            {
                //self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("JEDE_r"), withAlert:self.myclass.StringfromKey("errorinretrive"), withIdentifier:"error")
            }
        })
    }
    
    func getDefaultImageForCategory(categoryName:String) -> String {
        var imageName = "public"
        
        if categoryName == "" {
            imageName = "public"
        } else if categoryName == "Gas Station" {
            imageName = "Gas"
        } else if categoryName == "Hotel" {
            imageName = "Hotels"
        } else if categoryName == "Restaurant" {
            imageName = "Restaurant"
        } else if categoryName == "Shopping Mall" {
            imageName = "shoping_marker"
        } else if categoryName == "Bar" {
            imageName = "bar_marker"
        } else if categoryName == "Public" {
            imageName = "Public-Icon"
        } else if categoryName == "Temp" {
            imageName = "Temp-Icon"
        } else if categoryName == "Shower" {
            imageName = "Shower-Icon"
        }
        
        return imageName
    }
    
    func drawPath()
    {
        let origin = "\(currentlocation.coordinate.latitude),\(currentlocation.coordinate.longitude)"
        print("origin Co ordinate\(origin)")
        let destination = String(format: "%@,%@", destlati ?? "",destlong ?? "")
        print("destination Co ordinate\(destination)")
        
        let url = String(format:"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&mode=%@&key=%@",String(format: "%f,%f",currentlocation.coordinate.latitude,currentlocation.coordinate.longitude),destination,passgooglemapstr,kGoogleDirectionAPIKey)
        print(url)
        
        Alamofire.request(url).responseJSON { response in
            print(response.request)  // original URL request
            print(response.response) // HTTP URL response
            print(response.data)     // server data
            print(response.result)   // result of response serialization
            print(response.timeline)
            
            
            do {
                let json = try JSON(data: response.data!)
                print("JSON RESPONSE")
                print(json)
                let routes = json["routes"].arrayValue
                print(routes)
                
                self.viewmap.selectedMarker = self.markers
                for route in routes
                {
                    DispatchQueue.main.async {
                        let routeOverviewPolyline = route["overview_polyline"].dictionary
                        let points = routeOverviewPolyline?["points"]?.stringValue
                        self.path = GMSPath.init(fromEncodedPath: points!)!
                        self.polyline = GMSPolyline.init(path: self.path)
                        self.polyline.strokeColor = UIColor.black
                        self.polyline.strokeWidth = 2.0
                        self.polyline.map = self.viewmap
                    }
                    
                    let legs = route["legs"].arrayValue
                    print(legs)
                    for leg in legs
                    {
                        let distance = leg["distance"].dictionary
                        print(distance)
                        let textkm = distance?["text"]?.stringValue
                        print(textkm)
                        let duration = leg["duration"].dictionary
                        print(duration)
                        let texttime = duration?["text"]?.stringValue
                        print(texttime)
                        
                        self.markers.title = textkm
                        self.markers.snippet = texttime
                        self.viewmap.selectedMarker = self.markers
                        
                        let arr = textkm?.components(separatedBy: " ") as! NSArray
                        print(arr)
                        if arr.count != 0
                        {
                            let calstr = arr.object(at: 0) as! String
                            print(calstr)
                            let checkkmorm = arr.object(at: 1) as! String
                            print(checkkmorm)
                            if checkkmorm == "m"
                            {
                                let myInt = (calstr as NSString).integerValue
                                print(myInt)
                                if myInt < 20
                                {
                                    DispatchQueue.main.async {
                                        self.showCompletionNavigationPopup()
                                    }
                                }
                                else
                                {
                                    
                                }
                            }
                            else
                            {
                                
                            }
                        }
                        //                    let myInt = (myString as NSString).integerValue
                        //self.carminslab.text = String(format: "%@-%@", textkm!,texttime!)
                    }
                }
            } catch {
                print("The exception in service response!")
            }
        }
        
        let dic = NSMutableDictionary()
        dic.setValue(currentlocation.coordinate.latitude, forKey: "latitude")
        dic.setValue(currentlocation.coordinate.longitude, forKey: "longitude")
        
        let dic2 = NSMutableDictionary()
        dic2.setValue(destlati, forKey: "latitude")
        dic2.setValue(destlong, forKey: "longitude")
        
        myarr.insert(dic, at: 0)
        myarr.insert(dic2, at: 1)
        print(myarr)
        
        DispatchQueue.main.async
            {
                // start
                
                let position = CLLocationCoordinate2DMake(CLLocationDegrees(self.destlati ?? 0),CLLocationDegrees(self.destlong ?? 0))
                print(position)
                self.markers = GMSMarker(position: position)
                print(self.markers)

                if(self.selectdic == nil) {
                let type = self.selectdic.value(forKey: "category_name") as? String;
                print(type)
                if type == ""
                {
                    self.markers.icon = UIImage.init(named: "Public")
                }
                else if type == "Gas Station"
                {
                    self.markers.icon = UIImage.init(named: "Gas")
                }
                else if type == "Hotel"
                {
                    self.markers.icon = UIImage.init(named: "Hotels")
                }
                else if type == "Restaurant"
                {
                    self.markers.icon = UIImage.init(named: "Restaurant")
                }
                else if type == "Shopping Mall"

                {
                    self.markers.icon = UIImage.init(named: "shoping_marker")
                }
                else if type == "Bar"
                {
                    self.markers.icon = UIImage.init(named: "bar_marker")
                }
                
                self.markers.map = self.viewmap
        }
    }
        if mybool == true
        {
            self.focusMapToShowAllMarkers()
            self.timer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(animatePolylinePath), userInfo: nil, repeats: false)
            mybool = false
        }
        else
        {
            self.polyline.strokeColor = UIColor.init(red: 0.000, green: 0.753, blue: 0.737, alpha: 1.0)
        }
        
       // self.timer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(animatePolylinePath), userInfo: nil, repeats: true)
    }
    
    func notyet(sender:UIButton!)
    {
        print("LOG: No need toilet")
        KGModal.sharedInstance().hide(animated: true)
        let loonamestr = self.selectdic.value(forKey: "loo_name") as? String ?? "NA"
        
        
         let looId = self.selectdic.value(forKey: "loo_id") as? String ?? "NA"
         let toiletId = self.selectdic.value(forKey: "loo_ids") as? String ?? "0"
         let looBookingId = self.selectdic.value(forKey: "booking_id") as? String ?? "NA"
         let looCatName = self.selectdic.value(forKey: "category_name") as? String ?? "NA"
         let looAddress = self.selectdic.value(forKey: "loo_address") as? String ?? "NA"
         let UserId =  UserDefaults.standard.string(forKey: "user_id") ?? "0"
   
        var performCancellation = true
        let bookingId = self.bookingidstr
        let refreshAlert = UIAlertController(title: loonamestr, message: "navigation_alert_cancelled_message".localized, preferredStyle: UIAlertControllerStyle.alert)
        if (bookingId?.isEmpty)! {
            performCancellation = false
        }
        let type = self.selectdic.value(forKey: "type") as? String ?? "NA"
        
       
        refreshAlert.addAction(UIAlertAction(title: "yes".localized, style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
            
            if type == "Public" {
                let loo_id = Int(toiletId)
                let user_id = Int(UserId)
                let status = 2
                self.publicApicall(loid:loo_id!,Userid:user_id!,Status: status)
            }
            
            FIRAnalytics.logEvent(withName: "Cancelled_Navigation", parameters: [
                "user_id" : UserId,
                "loo_name" : loonamestr,
                "loo_address" : looAddress,
                "loo_id" : looId,
                "booking_id" : looBookingId,
                "category_name" : looCatName,
                "login_device" : "iOS",
                "action_name" : "navigation_task_canceled"
                ])
           self.cancelWebSeviceCall(isPerformCancellation: performCancellation)
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "no".localized, style: .cancel, handler: { (action: UIAlertAction!) in
            print("Canceled")
        }))
        
        present(refreshAlert, animated: true, completion: nil)
        
    }
    
    func publicApicall(loid:Int,Userid:Int,Status:Int) {
        let urlstring = String(format:"%@/public_booking.php?loo_id=%i&user_id=%i&status=%i",kBaseURL,loid,Userid,Status)
        print(urlstring)
        self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: true, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
            if Stats == true
            {
                NSLog("jsonObject=%@", jsonObject);
    
            }
         
        })
    }
    
    func navigateToMoreInfo() {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "NewDetailsViewController") as! NewDetailsViewController
        secondViewController.selectdic = self.selectdic
        secondViewController.passcurrentloc = passLocationDetails
        KGModal.sharedInstance().hide(animated: true)
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    func presentTermsOfUse() {
        let termsOfUseController = self.storyboard?.instantiateViewController(withIdentifier: "TermsOfUseViewController") as! TermsOfUseViewController
        termsOfUseController.termsOfUseDescription = (selectdic.value(forKey: "terms_conditions") as? String)!
        termsOfUseController.termsOfUseCheckedBefore = isTermsOfUseChecked
        self.present(termsOfUseController, animated: true) {
            self.isTermsOfUseChecked = true
        }
    }
    
    func cancel(sender:UIButton!){
        
        print("Close Button Clicked")
        KGModal.sharedInstance().hide(animated: true)
    }
    
    //When done button is clicked the below action is getting called
    func yesbutton(sender:UIButton!)
    {
        print("Button Clicked hereee")
       
        KGModal.sharedInstance().hide(animated: true)
        var performCancellation = true
        let bookingId = self.bookingidstr
        if (bookingId?.isEmpty)! {
            performCancellation = false
        }
        print(selectdic as Any)
        let UserId =  UserDefaults.standard.string(forKey: "user_id") ?? "0"
        let looId = self.selectdic.value(forKey: "loo_id") as? String ?? "NA"
        let toiletId = self.selectdic.value(forKey: "loo_ids") as? String ?? "0"
        let looBookingId = self.selectdic.value(forKey: "booking_id") as? String ?? "NA"
        let looCatName = self.selectdic.value(forKey: "category_name") as? String ?? "NA"
        let looAddress = self.selectdic.value(forKey: "loo_address") as? String ?? "NA"
        let type = self.selectdic.value(forKey: "type") as? String ?? "NA"
        

     
        if type == "Public" {
            let loo_id = Int(toiletId)
            let user_id = Int(UserId)
            let status = 1
            publicApicall(loid:loo_id!,Userid:user_id!,Status: status)
            
        }
        FIRAnalytics.logEvent(withName: "infrastructure_used", parameters: [
            "user_id" : UserId,
            "loo_id" : looId,
            "loo_address" : looAddress,
            "booking_id" : looBookingId,
            "category_name" : looCatName,
            "login_device" : "iOS",
            "action_name" : "task_completed"
            ])
        locmanager.stopUpdatingLocation()

        if self.bookingidstr.isEmpty {
            let ratingView = kmainStoryboard.instantiateViewController(withIdentifier: "NewRatingViewController") as! NewRatingViewController
            ratingView.bookingidstr = self.bookingidstr
            ratingView.getdic = self.selectdic
            self.navigationController?.pushViewController(ratingView, animated: true)
        } else {
            self.completeBookingWebServiceCall()
        }
    }
    
    func simplecall()
    {
        let sponser = Bundle.main.loadNibNamed("BusinessCompletePopup", owner: self, options: nil)?[0] as! BusinessCompletePopup
        
        KGModal.sharedInstance().show(withContentView: sponser, andAnimated: true)
        KGModal.sharedInstance().tapOutsideToDismiss = false
        self.view.endEditing(true)
        sponser.layer.cornerRadius = 20.0
        sponser.clipsToBounds = true
        sponser.businesscompleteyesbutton.addTarget(self, action: #selector(MovetoNavigateViewController.businessfinishedyes(sender:)), for: UIControlEvents.touchUpInside)
    }
    
    func businessfinishedyes(sender:UIButton!)
    {
        self.businessfinishedsuccessfullylink()
    }
    
    func businessfinishedsuccessfullylink()
   {
   }

    
    func animatePolylinePath() {
        if (self.i < self.path.count()) {
            self.animationPath.add(self.path.coordinate(at: self.i))
            self.animationPolyline.path = self.animationPath
            self.animationPolyline.strokeColor = UIColor.init(red: 0.000, green: 0.753, blue: 0.737, alpha: 1.0)
            self.animationPolyline.strokeWidth = 2
            self.animationPolyline.map = self.viewmap
            self.i += 1
        }
        else {
            self.i = 0
            self.animationPath = GMSMutablePath()
            self.animationPolyline.map = nil
        }
    }
    
    func focusMapToShowAllMarkers()
    {
        var bounds = GMSCoordinateBounds()
        print("self.mapView.camera.zoom->\(bounds)")
        for c in 0 ..< self.myarr.count
        {
            //let marker = markers.object(at: c) as! GMSMarker
            let firstlati = (self.myarr.object(at: c) as AnyObject).value(forKey: "latitude") as? NSNumber
            let firstlong = (self.myarr.object(at: c) as AnyObject).value(forKey: "longitude") as? NSNumber
            bounds = bounds.includingCoordinate(CLLocationCoordinate2DMake(CLLocationDegrees(firstlati ?? 0), CLLocationDegrees(firstlong ?? 0)))
        }
//        self.viewmap.setMinZoom(10.0, maxZoom: 20)
//        self.viewmap.animate(with: GMSCameraUpdate .fit(bounds, withPadding: 15.0))
        self.viewmap.animate(with: GMSCameraUpdate .fit(bounds))
    }
    
    
    
    func getLooDetails() {
        
        if AFNetworkReachabilityManager.shared().isReachable
        {
            print("loo_details.php \(passLocationDetails)")
            if let passlooid = selectdic.value(forKey: "loo_id") as? String {
                let urlstring = String(format:"%@/loo_details.php?loo_id=%@&current_location=%@&user_id=%@",kBaseURL ?? "",passlooid ?? "",passLocationDetails ?? "",passuserid ?? "")
                print(urlstring)
                self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: true, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                    
                    if Stats == true && jsonObject.count != 0
                    {
                        NSLog("jsonObject=%@", jsonObject);
                        self.looDetails.removeAllObjects()
                        self.looDetails.addObjects(from: jsonObject as [AnyObject])
                        if jsonObject.count != 0
                        {
                            let termsAndConditions = (self.looDetails.object(at: 0) as AnyObject).value(forKey: "terms_conditions") as? String
                                self.selectdic.setValue(termsAndConditions, forKey: "terms_conditions")
                            
                        }
                    }
                })
            }
        }
    }

    
    @IBAction func navbutton(_sender: UIButton)
    {
            var dlatlon:String!
            let slatlon = String(format:"%f,%f",currentlocation.coordinate.latitude,currentlocation.coordinate.longitude)
            dlatlon = selectdic.value(forKey: "loo_location") as? String;
            print(dlatlon)
            
            if (UIApplication.shared.canOpenURL(NSURL.init(string:"comgooglemaps://")! as URL))
            {
                let urlstr = String(format:"comgooglemaps://?saddr=%@&daddr=%@",slatlon,dlatlon)
                
                let urlwithPercentEscapes = urlstr.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
                
                //            UIApplication.sharedApplication.openURL(NSURL.init(string: urlstr.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)!)
                
                UIApplication.shared.openURL(NSURL.init(string: urlwithPercentEscapes!) as! URL)
            }
            else
            {
                //            let urlstr = String(format:"https://maps.google.com/maps?saddr=%@&daddr=%@",slatlon,dlatlon)
                //            UIApplication.sharedApplication.openURL(NSURL.init(string:urlstr.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)!)
                
                let urlstr = String(format:"https://maps.google.com/maps?saddr=%@&daddr=%@",slatlon,dlatlon)
                
                let urlwithPercentEscapes = urlstr.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
                
                UIApplication.shared.openURL(NSURL.init(string: urlwithPercentEscapes!) as! URL)
                
            }
    }
    
    
    func completeBookingWebServiceCall() {
        if AFNetworkReachabilityManager.shared().isReachable
        {
            let urlstring : String!
            urlstring = String(format:"%@/complete_cancel_booking.php?booking_id=%@&status=1",kBaseURL ,self.bookingidstr ?? "")
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: false, completionHandler:
            { (jsonObject:NSArray, status:Bool) in
                if status == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                        if jsonObject.count != 0
                        {
                            let copyarr = jsonObject;
                            print(copyarr)
                            print(self.bookingidstr)
                            let str = (jsonObject.object(at: 0) as AnyObject).value(forKey: "message") as? String
                            let bookingid = (jsonObject.object(at: 0) as AnyObject).value(forKey: "booking_id") as? String
                            
                            if str == "completed"
                            {
                                KGModal.sharedInstance().hide(animated: true)
                                let ratingView = kmainStoryboard.instantiateViewController(withIdentifier: "NewRatingViewController") as! NewRatingViewController
                                
                                ratingView.bookingidstr = self.bookingidstr
                                ratingView.getdic = self.selectdic
                                self.navigationController?.pushViewController(ratingView, animated: true)
                                
                            }
                            else
                            {
                                
                                KGModal.sharedInstance().hide(animated: true)
                                self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:str!, withIdentifier:"")
                            }
                        }
                }
                else
                {
                    NSLog("jsonObject=error");
                }
            })
                
                
        }else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
    
    func cancelWebSeviceCall(isPerformCancellation:Bool)  {
        if isPerformCancellation {
            print("LOG:Booking is made, hence first the cancellation service followed by navigate to main screen")
            if AFNetworkReachabilityManager.shared().isReachable
            {
                let urlstring : String!
                urlstring = String(format:"%@/complete_cancel_booking.php?booking_id=%@&status=2",kBaseURL ,self.bookingidstr ?? "")
                print(urlstring)
                
                self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: false, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                    if Stats == true
                    {
                        NSLog("jsonObject=%@", jsonObject);
                        if jsonObject.count != 0
                        {
                            let copyarr = jsonObject;
                            print(copyarr)
                            let str = (jsonObject.object(at: 0) as AnyObject).value(forKey: "message") as? String
                            let bookingid = (jsonObject.object(at: 0) as AnyObject).value(forKey: "booking_id") as? String
                            
                            if str == "cancelled"
                            {
                                KGModal.sharedInstance().hide(animated: true)
                                let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
                                
                                mainview.bookingid = self.bookingidstr
                                
                                self.navigationController?.pushViewController(mainview, animated: true)
                                
                            }
                            else
                            {
                                
                                KGModal.sharedInstance().hide(animated: true)
                                self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:str!, withIdentifier:"")
                            }
                        }
                    }
                    else
                    {
                        NSLog("jsonObject=error");
                    }
                })
            }
            else
            {
                self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
            }
        } else {
            print("LOG:Booking not made hence navigatin to main screen directly")
            //No booking made hence directly navigating to home screen
            let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
            self.navigationController?.pushViewController(mainview, animated: true)
        }
    }
    
    @IBAction func cancelbutton(_sender: UIButton)
    {
        let loonamestr = self.selectdic.value(forKey: "loo_name") as? String
        print(loonamestr)
        let refreshAlert = UIAlertController(title: loonamestr, message: "navigation_alert_cancelled_message".localized, preferredStyle: UIAlertControllerStyle.alert)
        
        let bookingId = self.bookingidstr
        var performCancellation = true
        if (bookingId?.isEmpty)! {
            performCancellation = false
        }
        
        refreshAlert.addAction(UIAlertAction(title: "yes".localized, style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
            self.cancelWebSeviceCall(isPerformCancellation: performCancellation)
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "no".localized, style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
            let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "MainScreenViewController") as! MainScreenViewController
           self.navigationController?.pushViewController(mainview, animated: true)
        }))
        
        present(refreshAlert, animated: true, completion: nil)
    }
    

    @IBAction func termsOfUseAction(_ sender: Any) {
        self.presentTermsOfUse()
    }
    

}
