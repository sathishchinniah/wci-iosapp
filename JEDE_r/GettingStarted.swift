// To parse the JSON, add this file to your project and do:
//
//   let gettingStarted = GettingStarted.from(json: jsonString)!

import Foundation

typealias GettingStarted = OtherGettingStarted

class OtherGettingStarted {
    let result: [Result]
    
    fileprivate init?(fromAny any: Any) {
        guard let json = any as? [String: Any] else { return nil }
        guard
            let resultAny = removeNSNull(json["result"]),
            let result = convertArray(Result.init(fromAny:), resultAny)
            else { return nil }
        self.result = result
    }
}

class Result {
    let facilities: [Facility]
    let looLocation: String
    let category: [Category]
    let accessbility: [Accessbility]
    let days: String
    let looId: String
    let looAddress: String
    let looImage: String
    let price: String
    let timings: String
    let looName: String
    let termsConditions: String
    let type: String
    
    fileprivate init?(fromAny any: Any) {
        guard let json = any as? [String: Any] else { return nil }
        guard
            let facilitiesAny = removeNSNull(json["facilities"]),
            let facilities = convertArray(Facility.init(fromAny:), facilitiesAny),
            let looLocationAny = removeNSNull(json["loo_location"]),
            let looLocation = looLocationAny as? String,
            let categoryAny = removeNSNull(json["category"]),
            let category = convertArray(Category.init(fromAny:), categoryAny),
            let accessbilityAny = removeNSNull(json["accessbility"]),
            let accessbility = convertArray(Accessbility.init(fromAny:), accessbilityAny),
            let daysAny = removeNSNull(json["days"]),
            let days = daysAny as? String,
            let looIdAny = removeNSNull(json["loo_id"]),
            let looId = looIdAny as? String,
            let looAddressAny = removeNSNull(json["loo_address"]),
            let looAddress = looAddressAny as? String,
            let looImageAny = removeNSNull(json["loo_image"]),
            let looImage = looImageAny as? String,
            let priceAny = removeNSNull(json["price"]),
            let price = priceAny as? String,
            let timingsAny = removeNSNull(json["timings"]),
            let timings = timingsAny as? String,
            let looNameAny = removeNSNull(json["loo_name"]),
            let looName = looNameAny as? String,
            let termsConditionsAny = removeNSNull(json["terms_conditions"]),
            let termsConditions = termsConditionsAny as? String,
            let typeAny = removeNSNull(json["type"]),
            let type = typeAny as? String
            else { return nil }
        self.facilities = facilities
        self.looLocation = looLocation
        self.category = category
        self.accessbility = accessbility
        self.days = days
        self.looId = looId
        self.looAddress = looAddress
        self.looImage = looImage
        self.price = price
        self.timings = timings
        self.looName = looName
        self.termsConditions = termsConditions
        self.type = type
    }
}

class Facility {
    let facilityCategoryDisableImage: String
    let facilityCategoryId: String
    let facility: [OtherFacility]
    let facilityCategoryEnableImage: String
    let facilityCategoryName: String
    let status: String
    
    fileprivate init?(fromAny any: Any) {
        guard let json = any as? [String: Any] else { return nil }
        guard
            let facilityCategoryDisableImageAny = removeNSNull(json["facility_category_disable_image"]),
            let facilityCategoryDisableImage = facilityCategoryDisableImageAny as? String,
            let facilityCategoryIdAny = removeNSNull(json["facility_category_id"]),
            let facilityCategoryId = facilityCategoryIdAny as? String,
            let facilityAny = removeNSNull(json["facility"]),
            let facility = convertArray(OtherFacility.init(fromAny:), facilityAny),
            let facilityCategoryEnableImageAny = removeNSNull(json["facility_category_enable_image"]),
            let facilityCategoryEnableImage = facilityCategoryEnableImageAny as? String,
            let facilityCategoryNameAny = removeNSNull(json["facility_category_name"]),
            let facilityCategoryName = facilityCategoryNameAny as? String,
            let statusAny = removeNSNull(json["status"]),
            let status = statusAny as? String
            else { return nil }
        self.facilityCategoryDisableImage = facilityCategoryDisableImage
        self.facilityCategoryId = facilityCategoryId
        self.facility = facility
        self.facilityCategoryEnableImage = facilityCategoryEnableImage
        self.facilityCategoryName = facilityCategoryName
        self.status = status
    }
}

class OtherFacility {
    let disableImage: String
    let facilityId: String
    let count: String
    let enableImage: String
    let facilityName: String
    let status: String
    
    fileprivate init?(fromAny any: Any) {
        guard let json = any as? [String: Any] else { return nil }
        guard
            let disableImageAny = removeNSNull(json["disable_image"]),
            let disableImage = disableImageAny as? String,
            let facilityIdAny = removeNSNull(json["facility_id"]),
            let facilityId = facilityIdAny as? String,
            let countAny = removeNSNull(json["count"]),
            let count = countAny as? String,
            let enableImageAny = removeNSNull(json["enable_image"]),
            let enableImage = enableImageAny as? String,
            let facilityNameAny = removeNSNull(json["facility_name"]),
            let facilityName = facilityNameAny as? String,
            let statusAny = removeNSNull(json["status"]),
            let status = statusAny as? String
            else { return nil }
        self.disableImage = disableImage
        self.facilityId = facilityId
        self.count = count
        self.enableImage = enableImage
        self.facilityName = facilityName
        self.status = status
    }
}

class Category {
    let catEnableImage: String
    let categoryName: String
    let catDisableImage: String
    let categoryId: String
    let status: String
    
    fileprivate init?(fromAny any: Any) {
        guard let json = any as? [String: Any] else { return nil }
        guard
            let catEnableImageAny = removeNSNull(json["cat_enable_image"]),
            let catEnableImage = catEnableImageAny as? String,
            let categoryNameAny = removeNSNull(json["category_name"]),
            let categoryName = categoryNameAny as? String,
            let catDisableImageAny = removeNSNull(json["cat_disable_image"]),
            let catDisableImage = catDisableImageAny as? String,
            let categoryIdAny = removeNSNull(json["category_id"]),
            let categoryId = categoryIdAny as? String,
            let statusAny = removeNSNull(json["status"]),
            let status = statusAny as? String
            else { return nil }
        self.catEnableImage = catEnableImage
        self.categoryName = categoryName
        self.catDisableImage = catDisableImage
        self.categoryId = categoryId
        self.status = status
    }
}

class Accessbility {
    let accessbilityEnableImage: String
    let accessbilityName: String
    let accessbilityDisableImage: String
    let accessbilityId: String
    let status: String
    
    fileprivate init?(fromAny any: Any) {
        guard let json = any as? [String: Any] else { return nil }
        guard
            let accessbilityEnableImageAny = removeNSNull(json["accessbility_enable_image"]),
            let accessbilityEnableImage = accessbilityEnableImageAny as? String,
            let accessbilityNameAny = removeNSNull(json["accessbility_name"]),
            let accessbilityName = accessbilityNameAny as? String,
            let accessbilityDisableImageAny = removeNSNull(json["accessbility_disable_image"]),
            let accessbilityDisableImage = accessbilityDisableImageAny as? String,
            let accessbilityIdAny = removeNSNull(json["accessbility_id"]),
            let accessbilityId = accessbilityIdAny as? String,
            let statusAny = removeNSNull(json["status"]),
            let status = statusAny as? String
            else { return nil }
        self.accessbilityEnableImage = accessbilityEnableImage
        self.accessbilityName = accessbilityName
        self.accessbilityDisableImage = accessbilityDisableImage
        self.accessbilityId = accessbilityId
        self.status = status
    }
}

// Serialization extensions

extension OtherGettingStarted {
    static func from(json: String, using encoding: String.Encoding = .utf8) -> OtherGettingStarted? {
        guard let data = json.data(using: encoding) else { return nil }
        return OtherGettingStarted.from(data: data)
    }
    
    static func from(data: Data) -> OtherGettingStarted? {
        guard let json = try? JSONSerialization.jsonObject(with: data, options: []) else { return nil }
        return OtherGettingStarted.from(any: json)
    }
    
    static func from(any untyped: Any) -> OtherGettingStarted? {
        return OtherGettingStarted(fromAny: untyped)
    }
    
    var jsonData: Data? {
        let json = self.any
        return try? JSONSerialization.data(withJSONObject: json, options: [])
    }
    
    var jsonString: String? {
        guard let data = self.jsonData else { return nil }
        return String(data: data, encoding: .utf8)
    }
}

extension Accessbility {
    fileprivate var any: Any {
        return [
            "accessbility_enable_image": self.accessbilityEnableImage as Any,
            "accessbility_name": self.accessbilityName as Any,
            "accessbility_disable_image": self.accessbilityDisableImage as Any,
            "accessbility_id": self.accessbilityId as Any,
            "status": self.status as Any,
        ]
    }
}

extension Category {
    fileprivate var any: Any {
        return [
            "cat_enable_image": self.catEnableImage as Any,
            "category_name": self.categoryName as Any,
            "cat_disable_image": self.catDisableImage as Any,
            "category_id": self.categoryId as Any,
            "status": self.status as Any,
        ]
    }
}

extension Facility {
    fileprivate var any: Any {
        return [
            "facility_category_disable_image": self.facilityCategoryDisableImage as Any,
            "facility_category_id": self.facilityCategoryId as Any,
            "facility": self.facility.map({ v in v.any }) as Any,
            "facility_category_enable_image": self.facilityCategoryEnableImage as Any,
            "facility_category_name": self.facilityCategoryName as Any,
            "status": self.status as Any,
        ]
    }
}

extension OtherFacility {
    fileprivate var any: Any {
        return [
            "disable_image": self.disableImage as Any,
            "facility_id": self.facilityId as Any,
            "count": self.count as Any,
            "enable_image": self.enableImage as Any,
            "facility_name": self.facilityName as Any,
            "status": self.status as Any,
        ]
    }
}

extension OtherGettingStarted {
    fileprivate var any: Any {
        return [
            "result": self.result.map({ v in v.any }) as Any,
        ]
    }
}

extension Result {
    fileprivate var any: Any {
        return [
            "facilities": self.facilities.map({ v in v.any }) as Any,
            "loo_location": self.looLocation as Any,
            "category": self.category.map({ v in v.any }) as Any,
            "accessbility": self.accessbility.map({ v in v.any }) as Any,
            "days": self.days as Any,
            "loo_id": self.looId as Any,
            "loo_address": self.looAddress as Any,
            "loo_image": self.looImage as Any,
            "price": self.price as Any,
            "timings": self.timings as Any,
            "loo_name": self.looName as Any,
            "terms_conditions": self.termsConditions as Any,
            "type": self.type as Any,
        ]
    }
}

// Helpers

fileprivate func convertArray<T>(_ converter: (Any) -> T?, _ json: Any) -> [T]? {
    guard let jsonArr = json as? [Any] else { return nil }
    var arr = [T]()
    for v in jsonArr {
        if let converted = converter(v) {
            arr.append(converted)
        } else {
            return nil
        }
    }
    return arr
}

fileprivate func convertOptional<T>(_ converter: (Any) -> T?, _ json: Any?) -> T?? {
    guard let v = json else { return .some(nil) }
    return converter(v)
}

fileprivate func convertDict<T>(_ converter: (Any) -> T?, _ json: Any?) -> [String: T]? {
    guard let jsonDict = json as? [String: Any] else { return nil }
    var dict = [String: T]()
    for (k, v) in jsonDict {
        if let converted = converter(v) {
            dict[k] = converted
        } else {
            return nil
        }
    }
    return dict
}

fileprivate func convertToAny<T>(_ dictionary: [String: T], _ converter: (T) -> Any) -> Any {
    var result = [String: Any]()
    for (k, v) in dictionary {
        result[k] = converter(v)
    }
    return result
}

fileprivate func convertDouble(_ v: Any) -> Double? {
    if let w = v as? Double { return w }
    if let w = v as? Int { return Double(w) }
    return nil
}

fileprivate let falseType = NSNumber(value: false).objCType
fileprivate let trueNumber = NSNumber(value: true)
fileprivate let trueType = trueNumber.objCType

fileprivate func convertBool(_ v: Any?) -> Bool? {
    guard let number = v as? NSNumber
        else {
            if let b = v as? Bool {
                return b
            }
            return nil
    }
    
    if number.objCType != falseType && number.objCType != trueType {
        return nil
    }
    return number.isEqual(trueNumber)
}

fileprivate func removeNSNull(_ v: Any?) -> Any? {
    if let w = v {
        if w is NSNull {
            return nil
        }
        return w
    }
    return nil
}

fileprivate func checkNull(_ v: Any?) -> NSNull? {
    if v != nil { return .none }
    return .some(NSNull())
}
