//
//  NewDetailsViewController.swift
//  JEDE_r
//
//  Created by Exarcplus on 22/06/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import AFNetworking
import SDWebImage
import KKActionSheet
import SYPhotoBrowser
import SVProgressHUD
import ZKCarousel
import Firebase



var editLoo = 0

class NewDetailsViewController: UIViewController, UIScrollViewDelegate, UIGestureRecognizerDelegate, PECropViewControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate,UINavigationControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UIAlertViewDelegate {
    
    //,iCarouselDataSource, iCarouselDelegate
    
    var imgeOne:UIImageView?
    var imgTwo:UIImageView?
    var PageControl:UIPageControl?
    
    @IBOutlet weak var rateNowButton: UIButton!
    @IBOutlet var carousel: ZKCarousel! = ZKCarousel()
    // @IBOutlet weak var CarsoulImageView: iCarousel!
    @IBOutlet weak var CarsoulImage: UIImageView!
    var myclass : MyClass!
    @IBOutlet weak var scrollview : UIScrollView!
    @IBOutlet weak var upperview : UIView!
    @IBOutlet weak var gradientview : UIView!
    @IBOutlet weak var bottomview : UIView!
    @IBOutlet weak var likeButton: SparkButton!
    var isLiked:Bool!
    @IBOutlet weak var sharebut : UIButton!
    
    @IBOutlet weak var mapViewTopButtonBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapViewtopBUttonViewHeight: NSLayoutConstraint!
    @IBOutlet weak var mapViewHeight: NSLayoutConstraint!
    @IBOutlet weak var locationDescription: UIView!
    
    var getdic = NSMutableDictionary()
    var detailsarr = NSMutableArray()
    var selectdic : NSMutableDictionary!
    var passlooid : String!
    var passuserid : String!
    var passcurrentloc : String!
    let marker = GMSMarker()
    var Userdic = NSMutableDictionary()
    
    @IBOutlet weak var viewmap: GMSMapView!
    @IBOutlet weak var loonamelab : UILabel!
    @IBOutlet weak var reviewslab : UILabel!
    @IBOutlet weak var distancelab : UILabel!
    @IBOutlet weak var pricelab : UILabel!
    @IBOutlet weak var locationlab : UILabel!
    @IBOutlet weak var infolab : UILabel!
    @IBOutlet weak var crossimg : UIImageView!
    @IBOutlet weak var backview : UIView!
    @IBOutlet weak var ratingview : EZRatingView!
    
    @IBOutlet weak var imageScroll: UIScrollView!
    @IBOutlet weak var bookview : UIView!
    @IBOutlet weak var navigateview : UIView!
    
    @IBOutlet weak var facilitycollection : UICollectionView!
    @IBOutlet weak var accessibilitycollection : UICollectionView!
    @IBOutlet var collectionfacheight : NSLayoutConstraint!
    @IBOutlet var collectionaccheight : NSLayoutConstraint!
    
    @IBOutlet weak var MoreInformation: UILabel!
    @IBOutlet weak var editBut: UIButton!
    @IBOutlet weak var DiscriptionLabel: UILabel!
    @IBOutlet weak var reviewLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var facilitiesLabel: UILabel!
    @IBOutlet weak var accessbilityLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var BackButton: UIBarButtonItem!
    
    var facilitiesarr = NSMutableArray()
    var insidefacilities = NSMutableArray()
    var accessibilityarr = NSMutableArray()
    var categoryarr = NSMutableArray()
    var Scrollimages = String()
    
    let serverResponseForAlreadyBooked = "already Booked"
    let alreadyBookedAlertIdentifier = "alreadyBookedAlertIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("NewDetailsViewController")
        print("NewDetailsViewController LOCATION \(passcurrentloc)")
        myclass = MyClass()
        self.navigationController?.navigationBar.isHidden = true
        
        self.activityIndicator.hidesWhenStopped = true
        self.showOrHideActivityIndicator(toShow: true)
        
        scrollview.delegate=self
        self.bookview.isHidden = true
        self.navigateview.isHidden = true
        
        print(editButton)
        
        if editButton == 1 {
            editBut.isHidden = false
        }
        else
        {
            editBut.isHidden = true
        }
        
        if selectdic != nil
        {
            print(selectdic)
            passlooid = selectdic.value(forKey: "loo_id") as! String
            
            // Use common key for address for address and loo_address
            // HACK: If the key address is there then adda value for a new key 'loo_address'
            if let addressValue = selectdic.value(forKey: "address") as? String {
                selectdic.setValue(addressValue, forKey: "loo_address")
            }
            if let imageValue = selectdic.value(forKey: "image") as? String {
                selectdic.setValue(imageValue, forKey: "loo_image")
            }
            //HACK: END
            
            print(passlooid)
            print(passcurrentloc)
        }
        else
        {
            //            self.scrollview.addTwitterCover(with: UIImage(named: "Toilets-Defualt.png"))
        }
        
        if UserDefaults.standard.object(forKey: "Logindetail") == nil
        {
            passuserid = ""
        }
        else
        {
            
            let placesData = UserDefaults.standard.object(forKey: "Logindetail") as? NSDictionary
            self.Userdic.removeAllObjects()
            if placesData != nil{
                self.Userdic.addEntries(from: (placesData as? [String : Any])!)
            }
            print(self.Userdic)
            passuserid = self.Userdic.value(forKey: "user_id") as? String
            print(passuserid)
        }
        
        let view = UIView(frame:  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 20))
        view.backgroundColor = UIColor.init(red: 27.0/255.0, green: 34.0/255.0, blue: 40.0/255.0, alpha: 1.0)
        self.navigationController?.view.addSubview(view)
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        self.runlink()
        
        self.DiscriptionLabel.text = "More_info_text".localized
        self.MoreInformation.text = "More_Information".localized
        self.distanceLabel.text = "loo_details_distance".localized
        self.priceLabel.text = "loo_details_price".localized
        self.accessbilityLabel.text = "loo_details_accessibility".localized
        self.facilitiesLabel.text = "loo_details_facilities".localized
        self.locationLabel.text = "loo_details_location".localized
        self.reviewLabel.text = "loo_details_reviews".localized
        self.BackButton.setBackButtonBackgroundImage(UIImage(named: "Square"), for: .normal, barMetrics: UIBarMetrics.default) 
        self.rateNowButton.setTitle("rate_not_button_title".localized, for: .normal)
        // Do any additional setup after loading the view.
    }
    
    
    func imageScrollView()
    {
        func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
            
            let pageWidth:CGFloat = imageScroll.frame.width
            let currentPage:CGFloat = floor((imageScroll.contentOffset.x-pageWidth/2)/pageWidth)+1
            
            //self.pageControl.currentPage = Int(currentPage);
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        if isPublicToilet() {
            if ((self.viewmap) != nil) {
                viewmap.translatesAutoresizingMaskIntoConstraints = false
                viewmap.isHidden = true
                self.viewmap.removeConstraints(self.viewmap.constraints)
                self.viewmap.frame = CGRect.zero
                self.viewmap.clipsToBounds = true
                self.mapViewHeight.constant = 0
                self.mapViewtopBUttonViewHeight.constant = 0
                self.mapViewBottomConstraint.constant = 0
                self.mapViewTopButtonBottomConstraint.constant = 0
              //self.viewmap.removeFromSuperview()
            }
            
            if ((locationDescription) != nil) {
                locationDescription.translatesAutoresizingMaskIntoConstraints = false
                locationDescription.isHidden = true
                self.locationDescription.removeConstraints(self.locationDescription.constraints)
                self.locationDescription.frame = CGRect.zero
            }
        } else {
            viewmap.isHidden = false
            locationDescription.isHidden = false
            self.mapViewBottomConstraint.constant = 60
            self.mapViewTopButtonBottomConstraint.constant = 60
            self.mapViewHeight.constant = 148
            self.mapViewtopBUttonViewHeight.constant = 148;
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        editButton = 0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        let currentOffset: CGFloat = scrollView.contentOffset.y
        let maximumOffset: CGFloat = scrollView.contentSize.height - scrollView.frame.size.height
        // Change 10.0 to adjust the distance from bottom
        if maximumOffset - currentOffset <= 10.0 {
            UIView.animate(withDuration: 2.5, delay: 0, options: UIViewAnimationOptions(), animations: {
                self.navigationController?.setNavigationBarHidden(false, animated: true)
                print("UNHide")
                self.backview.isHidden = true
                self.navigationItem.title = self.selectdic.value(forKey: "loo_name") as? String
            }, completion: nil)
            
        }
        else {
            UIView.animate(withDuration: 2.5, delay: 0, options: UIViewAnimationOptions(), animations: {
                self.navigationController?.setNavigationBarHidden(true, animated: true)
                self.backview.isHidden = false
                print("hide")
            }, completion: nil)
        }
    }
    
    func  isPublicToilet() -> Bool {
        let toiletType = self.selectdic.value(forKey: "type") as? String
        if toiletType == "Public" {
            return true
        }
        return false
    }
    
    @IBAction func rateNowButtonAction(_ sender: Any) {
        let ratingView = kmainStoryboard.instantiateViewController(withIdentifier: "NewRatingViewController") as! NewRatingViewController
        ratingView.getdic = self.selectdic
        self.navigationController?.pushViewController(ratingView, animated: true)
    }
    
    func runlink()
    {
        if AFNetworkReachabilityManager.shared().isReachable
        {
            print("loo_details.php \(passcurrentloc)")
            let urlstring = String(format:"%@/loo_details.php?loo_id=%@&current_location=%@&user_id=%@",kBaseURL ?? "",passlooid ?? "",passcurrentloc ?? "",passuserid ?? "")
            print(urlstring)
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: true, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                DispatchQueue.main.async {
                    self.showOrHideActivityIndicator(toShow: false)
                }
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    if jsonObject.count != 0
                    {
                        if UserDefaults.standard.object(forKey: "Logindetail") == nil
                        {
                            
                        }
                        else
                        {
                            self.addrecentlyviewlink()
                        }
                        self.detailsarr .removeAllObjects()
                        self.detailsarr.addObjects(from: jsonObject as [AnyObject])
                        print(self.detailsarr)
                        self.scrollview.removeTwitterCover()
                        
                        if !self.isPublicToilet() {
                            let termsAndConditions = (self.detailsarr.object(at: 0) as AnyObject).value(forKey: "terms_conditions") as? String
                            self.DiscriptionLabel.text = termsAndConditions
                            
                        }
                        
                        let imagestr = (self.detailsarr.object(at: 0) as AnyObject).value(forKey: "loo_image") as? String
                        if (imagestr != nil) {
                            let imagearr:[String] = (imagestr?.components(separatedBy: ","))!
                            let firstImage = imagearr.first
                            if firstImage?.characters.count == 0 || firstImage == ""
                            {
                                if let categoryType = self.selectdic.value(forKey: "category_name") as? String {
                                    let categoryImage = self.getDefaultImageForCategory(categoryName: categoryType)
                                    self.scrollview.addTwitterCover(with: UIImage(named: categoryImage))
                                } else {
                                    self.scrollview.addTwitterCover(with: UIImage(named: "Toilets-Defualt.png"))
                                }
                                
                            }
                            else
                            {
                                let imageList = (imagestr?.components(separatedBy: ",") as? NSArray)
                                print("LOG: imageList \(imageList)")
                                for i in 0 ..< imagearr.count {
                                    let imageurl = imageList?.object(at: i) as? String
                                    print("LOG: arrayurl \(imageurl)")
                                    if(imageurl?.count != 0){
                                        let arrayurl = URL(string: imageurl!)
                                        print("LOG: arrayurl \(arrayurl)")
                                        let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                                            print(self)
                                        }
                                        if let data = try? Data(contentsOf: arrayurl!)
                                        {
                                            let image: UIImage = UIImage(data: data)!
                                            let slide = ZKCarouselSlide(image:image, title: "", description: "")
                                            self.carousel.slides.append(slide)
                                            self.carousel.pageControl.numberOfPages = self.carousel.slides.count
                                            print("LOG: Count \(self.carousel.pageControl.numberOfPages)")
                                        }
                                    }
                                }
                            }
                        }
                        
                        let loonamestr = (self.detailsarr.object(at: 0) as AnyObject).value(forKey: "loo_name") as? String
                        if loonamestr == nil
                        {
                            self.loonamelab.text = ""
                        }
                        else
                        {
                            self.loonamelab.text = loonamestr
                        }
                        
                        let favouritestr = (self.detailsarr.object(at: 0) as AnyObject).value(forKey: "favourite_status") as? String
                        print(favouritestr!)
                        if favouritestr == "0"
                        {
                            self.isLiked = false
                            self.likeButton.setImage(UIImage(named: "Fav.png"), for: UIControlState())
                            self.likeButton.unLikeBounce(0.4)
                        }
                        else
                        {
                            self.isLiked = true
                            self.likeButton.setImage(UIImage(named: "LIKE.png"), for: UIControlState())
                            self.likeButton.likeBounce(0.6)
                        }
                        
                        
                        let rating = (self.detailsarr.object(at: 0) as AnyObject).value(forKey: "rating") as! NSNumber
                        print(rating)
                        print(CGFloat(rating.doubleValue))
                        
                        if rating.intValue != 0
                        {
                            self.ratingview.value = CGFloat(rating.doubleValue)
                        }
                        else
                        {
                            self.ratingview.value = 0
                        }
                        
                        let reviewsstrcount = (self.detailsarr.object(at: 0) as AnyObject).value(forKey: "reviews_count") as? String
                        if reviewsstrcount == nil || reviewsstrcount == ""
                        {
                            self.reviewslab.text = "0"
                        }
                        else
                        {
                            self.reviewslab.text = reviewsstrcount
                        }
                        
                        let distancestr = (self.detailsarr.object(at: 0) as AnyObject).value(forKey: "distance") as? String
                        if distancestr == nil || distancestr == ""
                        {
                            self.distancelab.text = ""
                        }
                        else
                        {
                            self.distancelab.text = String(format: "%@ KM", distancestr!)
                        }
                        let pricestr = (self.detailsarr.object(at: 0) as AnyObject).value(forKey: "price") as? String
                        if pricestr == nil || pricestr == ""
                        {
                            self.pricelab.text = "FREE"
                        }
                        else
                        {
                            self.pricelab.text = String(format: "CHF %@", pricestr!)
                        }
                        let locationstr = (self.detailsarr.object(at: 0) as AnyObject).value(forKey: "loo_address") as? String
                        
                        if !self.isPublicToilet() {
                            if locationstr == nil {
                                self.locationlab.text = ""
                            } else {
                                self.locationlab.text = "full_map_layout_exact_location".localized
                            }
                        }
                        
                        if self.selectdic != nil
                        {
                            //map
                            let type = self.selectdic.value(forKey: "category_name") as? String
                            let location = self.selectdic.value(forKey: "loo_location") as? String
                            
                            if type == ""
                            {
                                self.marker.icon = UIImage.init(named: "public")
                                self.bookview.isHidden = true
                                self.navigateview.isHidden = false
                            }
                            else if type == "Gas Station"
                            {
                                self.marker.icon = UIImage.init(named: "Gas")
                                self.bookview.isHidden = false
                                self.navigateview.isHidden = true
                            }
                            else if type == "Hotel"
                            {
                                self.marker.icon = UIImage.init(named: "Hotels")
                                self.bookview.isHidden = false
                                self.navigateview.isHidden = true
                            }
                            else if type == "Restaurant"
                            {
                                self.marker.icon = UIImage.init(named: "Restaurant")
                                self.bookview.isHidden = false
                                self.navigateview.isHidden = true
                                //                                                    marker.accessibilityHint = "No"
                            }
                            else if type == "Shopping Mall"
                            {
                                self.marker.icon = UIImage.init(named: "shoping_marker")
                                self.bookview.isHidden = false
                                self.navigateview.isHidden = true
                                //                                                    marker.accessibilityHint = "No"
                            }
                            else if type == "Bar"
                            {
                                self.marker.icon = UIImage.init(named: "bar_marker")
                                self.bookview.isHidden = false
                                self.navigateview.isHidden = true
                                //                                                    marker.accessibilityHint = "No"
                            }
                            
                            
                            if let locationData = location {
                                let locar:NSArray = locationData.components(separatedBy: ",") as NSArray
                                if locar.count == 2 {
                                    let lat = Double(locar.object(at: 0) as! String)
                                    let lon = Double(locar.object(at: 1) as! String)
                                    if let latitude = lat {
                                        if let longitude = lon {
                                            let loosloc = CLLocation.init(latitude: latitude, longitude: longitude)
                                            self.marker.position = CLLocationCoordinate2DMake(loosloc.coordinate.latitude, loosloc.coordinate.longitude)
                                        }
                                    }
                                }
                            }
                            
                            if !self.isPublicToilet() {
                                self.marker.zIndex = 0 ;
                                self.marker.map  = self.viewmap
                            }
                            
                            self.facilitiesarr.addObjects(from: (self.detailsarr.object(at: 0) as AnyObject).value(forKey: "facilities") as! [AnyObject])
                            self.accessibilityarr.addObjects(from: (self.detailsarr.object(at: 0) as AnyObject).value(forKey: "accessbility") as! [AnyObject])
                            
                            if self.facilitiesarr.count != 0
                            {
                                let arr = NSMutableArray()
                                for c in 0 ..< self.facilitiesarr.count
                                {
                                    
                                    let datearrs = (self.facilitiesarr.object(at: c) as AnyObject).value(forKey: "facility")
                                    print(datearrs)
                                    self.insidefacilities.add(datearrs)
                                    print(self.insidefacilities)
                                    
                                }
                                arr.add(self.insidefacilities)
                                print(arr)
                            }
                            else
                            {
                                
                            }                        
                            print(self.insidefacilities)
                            self.facilitycollection.reloadData()
                            self.accessibilitycollection.reloadData()
                            print(self.selectdic)
                            
                            //draw circle
                            if location?.characters.count != 0
                            {
                                if type == ""
                                {
                                    
                                }
                                else
                                {
                                    if let locationData = location {
                                        let locar:NSArray = locationData.components(separatedBy: ",") as NSArray
                                        if locar.count == 2 {
                                            let lat = Double(locar.object(at: 0) as! String)
                                            let lon = Double(locar.object(at: 1) as! String)
                                            if let latitude = lat {
                                                if let longitude = lon {
                                                    let loosloc = CLLocation.init(latitude: latitude, longitude: longitude)
                                                    let geoFenceCircle = GMSCircle()
                                                    geoFenceCircle.radius = 50
                                                    // Meters
                                                    geoFenceCircle.position = CLLocationCoordinate2DMake(loosloc.coordinate.latitude, loosloc.coordinate.longitude)
                                                    
                                                    if type == "Gas Station" {
                                                        geoFenceCircle.fillColor = UIColor.init(red: 129/255.0, green: 130/255.0, blue: 133/255.0, alpha: 0.2)
                                                    } else if type == "Hotel" {
                                                        geoFenceCircle.fillColor = UIColor.init(red: 40/255.0, green: 56/255.0, blue: 145/255.0, alpha: 0.2)
                                                    } else if type == "Restaurant" {
                                                        geoFenceCircle.fillColor = UIColor.init(red: 1.000, green: 0.435, blue: 0.149, alpha: 0.2)
                                                    } else if type == "Shopping Mall" {
                                                        geoFenceCircle.fillColor = UIColor.init(red: 247/255.0, green: 148/255.0, blue: 30/255.0, alpha: 0.2)
                                                    } else if type == "Bar" {
                                                       geoFenceCircle.fillColor = UIColor.init(red: 146/255.0, green: 39/255.0, blue: 143/255.0, alpha: 0.2)
                                                    }
                                                    geoFenceCircle.strokeWidth = 0
                                                    //Show marker only if it is not public toilet
                                                    if !self.isPublicToilet() {
                                                        geoFenceCircle.map = self.viewmap
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } //end else
                            }
                        }
                        self.focusMapToShowAllMarkers()
                    }
                    
                    //If is free provider then unhide the navigate button
                    let isFreeProviderValue = self.selectdic.value(forKey: "free_provider") as! String
                    if(self.isFreeBooking(freeProvider: isFreeProviderValue)) {
                        self.bookview.isHidden = true
                        self.navigateview.isHidden = false
                    }
                }
                else
                {
                    NSLog("jsonObject=error");
                }
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
    
    
    func addrecentlyviewlink()
    {
        if AFNetworkReachabilityManager.shared().isReachable
        {
            let urlstring : String!
            urlstring = String(format:"%@/add_recently_view.php?user_id=%@&loo_id=%@",kBaseURL,passuserid,passlooid)
            print(urlstring)
            
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: false, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    if jsonObject.count != 0
                    {
                        let copyarr = jsonObject;
                        print(copyarr)
                        let str = (jsonObject.object(at: 0) as AnyObject).value(forKey: "message") as? String
                        if str == "success"
                        {
                            
                        }
                        else
                        {
                            
                            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:str!, withIdentifier:"")
                        }
                        
                    }
                }
                else
                {
                    NSLog("jsonObject=error");
                }
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
    
    
    func focusMapToShowAllMarkers() {
        //Show marker only if it is not public toilet
        if !isPublicToilet() {
            let myLocation: CLLocationCoordinate2D = self.marker.position
            var bounds: GMSCoordinateBounds = GMSCoordinateBounds(coordinate: myLocation, coordinate: myLocation)
            bounds = bounds.includingCoordinate(marker.position)
            self.viewmap.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 1.0))
            self.viewmap.animate(toZoom: 16.0)
        }
    }
    
    @IBAction func backbutton(_sender: UIButton)
    {
     
        print("LOG:back button called")
        let valueToSaves = "comingfromdetails"
        UserDefaults.standard.set(valueToSaves, forKey: "screen")
        
        self.scrollview.removeTwitterCover()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        let valueToSave = "noneedrunlink"
        self.navigationController?.isNavigationBarHidden = false
        UserDefaults.standard.set(valueToSave, forKey: "runlink")
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addfavouritebutton(_sender : UIButton)
    {
        
       let looId = selectdic?.value(forKey: "loo_id") as? String ?? "NA"
        let Userid =  UserDefaults.standard.string(forKey: "user_id") ?? "NA"
        
        if UserDefaults.standard.object(forKey: "Logindetail") == nil
        {
            let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(mainview, animated: true)
        }
        else
        {
            isLiked = !isLiked
            if isLiked == true
            {
                likeButton.setImage(UIImage(named: "LIKE.png"), for: UIControlState())
                likeButton.likeBounce(0.6)
                likeButton.animate()
                isLiked = false
                FIRAnalytics.logEvent(withName: "favorite_click", parameters: [
                    "user_id" : Userid,
                    "loo_id" : looId,
                    "button_name" : "fav_button_click",
                    "favorite_status" : "fav_status=1",
                    "login_device" : "iOS",
                    "action_name" : "button_action"
                    ])

            }
            else
            {
                likeButton.setImage(UIImage(named: "Fav.png"), for: UIControlState())
                likeButton.unLikeBounce(0.4)
                isLiked = true
                FIRAnalytics.logEvent(withName: "unfavorite", parameters: [
                    "user_id" : Userid,
                    "loo_id" : looId,
                    "button_name" : "unfav_button_click",
                    "favorite_status" : "fav_status=0",
                    "login_device" : "iOS",
                    "action_name" : "button_action"
                    ])
            }
            self.favlink()
        }
    }
    
    func favlink()
    {
        if AFNetworkReachabilityManager.shared().isReachable
        {
            let urlstring : String!
            isLiked = !isLiked
            if isLiked == true
            {
                urlstring = String(format:"%@/fav.php?user_id=%@&loo_id=%@&fav_status=1",kBaseURL,passuserid,passlooid)
                print(urlstring)
            }
            else
            {
                urlstring = String(format:"%@/fav.php?user_id=%@&loo_id=%@&fav_status=0",kBaseURL,passuserid,passlooid)
                print(urlstring)
            }
            
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: false, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    if jsonObject.count != 0
                    {
                        let copyarr = jsonObject;
                        
                    }
                }
                else
                {
                    NSLog("jsonObject=error");
                }
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
    
    @IBAction func movetonavigationscreen(_sender: UIButton)
    {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "NavigateScreenViewController") as! NavigateScreenViewController
        next.selectdic = self.selectdic
        let nav = UINavigationController.init(rootViewController: next)
        self.present(nav, animated: true, completion: nil)
    }
    
    @IBAction func bookbutton(_sender: UIButton)
    {
        
        
        let isFreeProvider = self.selectdic.value(forKey: "free_provider") as! String
        if (self.isLoginRequired(freeProvider: isFreeProvider)) {
            let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(mainview, animated: true)
        } else {
            if (self.isFreeBooking(freeProvider: isFreeProvider)) {
                self.bookinglink()
            } else {
                let sponser = Bundle.main.loadNibNamed("ConfirmationBookingPopup", owner: self, options: nil)?[0] as! ConfirmationBookingPopup
                
                KGModal.sharedInstance().show(withContentView: sponser, andAnimated: true)
                self.view.endEditing(true)
                sponser.layer.cornerRadius = 20.0
                sponser.clipsToBounds = true
                sponser.confirmBooking.text = "popup_confirm_booking_title".localized
                sponser.agreeMessage.text = "popup_confirm_booking_terms".localized
                sponser.termsandconditionsbutton.setTitle("popup_confirm_booking_terms_link".localized, for: .normal)
                sponser.closebutton.setTitle("popup_confirm_booking_terms_close".localized, for: .normal)
                sponser.bookAndNavigateButton.setTitle("popup_confirm_booking_terms_Book_navigate".localized, for: .normal)
                print(self.selectdic)
                let imageurl = self.selectdic.value(forKey: "image") as? String;
                if imageurl?.characters.count == 0 || imageurl == "" || imageurl == "http://stitchplus.com/JEDE_rpanel/"
                {
                    sponser.looimg.image = UIImage.init(named: "default_image.png")
                }
                else
                {
                    let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                        print(self)
                    }
                    sponser.looimg.sd_setImage(with: NSURL(string:imageurl!) as URL!, completed: block)
                    sponser.looimg.contentMode = UIViewContentMode.scaleToFill;
                }
                let looname = self.selectdic.value(forKey: "loo_name") as? String;
                if looname == ""
                {
                    sponser.loonamelab.text = looname
                }
                else
                {
                    sponser.loonamelab.text = looname
                }
                
                
                let date = Date()
                let formatter = DateFormatter()
                
                
                formatter.dateFormat = "dd-MMM-yyyy h:mm a"
                formatter.amSymbol = "AM"
                formatter.pmSymbol = "PM"
                
                let result = formatter.string(from: date)
                let dattimearr = result.components(separatedBy: " ") as NSArray
                let dispdate = dattimearr.object(at: 0) as! String
                let dipstime = dattimearr.object(at: 1) as! String
                let ampm = dattimearr.object(at: 2) as! String
                
                
                
                sponser.datelab.text = dispdate
                sponser.timelab.text = String(format: "%@ %@", dipstime,ampm)
                
                sponser.closebutton.addTarget(self, action: #selector(DetailsViewController.closepopup(sender:)), for: UIControlEvents.touchUpInside)
                sponser.termsandconditionsbutton.addTarget(self, action: #selector(self.termsandconditionbut(sender:)), for: UIControlEvents.touchUpInside)
                sponser.bookbutton.addTarget(self, action: #selector(DetailsViewController.bookandnavigatebut(sender:)), for: UIControlEvents.touchUpInside)
            }
        }
    }
    
    @IBAction func movetonavigation2screen(_sender: UIButton)
    {
        let toiletType = self.selectdic.value(forKey: "type") as! String
        let toiletId = self.selectdic.value(forKey: "loo_id") as! String
        print(toiletId)
        let keyPublic = "Public"
        if UserDefaults.standard.object(forKey: "Logindetail") == nil && toiletType != keyPublic
        {
            let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(mainview, animated: true)
        }
        else
        {
            if UserDefaults.standard.object(forKey: "Logindetail") == nil && toiletType == keyPublic {
                let next = self.storyboard?.instantiateViewController(withIdentifier: "MovetoNavigateViewController") as! MovetoNavigateViewController
                next.selectdic = self.detailsarr.object(at: 0) as! NSMutableDictionary
                if ((self.selectdic.value(forKey: "facility_category") as? String) != nil) {
                    let value = self.selectdic.value(forKey: "facility_category") as? String
                    next.selectdic.setValue(value, forKey: "facility_category")
                    next.selectdic.setValue(toiletId, forKey: "loo_ids")
                    
                }
                
                if ((self.selectdic.value(forKey: "accessbility") as? String) != nil) {
                    let value = self.selectdic.value(forKey: "accessbility") as? String
                    next.selectdic.setValue(value, forKey: "accessbility")
                }
                
                next.bookingidstr = "0"
                self.navigationController?.pushViewController(next, animated: true)
            } else {
                
                print(self.selectdic)
                let typestr = self.selectdic.value(forKey: "type") as! String
                if typestr == "Public"
                {
                    self.bookinglink()
                }
                else
                {
                    let sponser = Bundle.main.loadNibNamed("ConfirmationBookingPopup", owner: self, options: nil)?[0] as! ConfirmationBookingPopup
                    KGModal.sharedInstance().show(withContentView: sponser, andAnimated: true)
                    self.view.endEditing(true)
                    sponser.layer.cornerRadius = 20.0
                    sponser.clipsToBounds = true
                    sponser.confirmBooking.text = "popup_confirm_booking_title".localized
                    sponser.agreeMessage.text = "popup_confirm_booking_terms".localized
                    sponser.termsandconditionsbutton.setTitle("popup_confirm_booking_terms_link".localized, for: .normal)
                    sponser.closebutton.setTitle("popup_confirm_booking_terms_close".localized, for: .normal)
                    sponser.bookAndNavigateButton.setTitle("popup_confirm_booking_terms_Book_navigate".localized, for: .normal)
                    print(self.selectdic)
                    let imageurl = self.selectdic.value(forKey: "image") as? String;
                    if imageurl?.characters.count == 0 || imageurl == "" || imageurl == "http://stitchplus.com/JEDE_rpanel/"
                    {
                        sponser.looimg.image = UIImage.init(named: "default_image.png")
                    }
                    else
                    {
                        let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                            print(self)
                        }
                        sponser.looimg.sd_setImage(with: NSURL(string:imageurl!) as URL!, completed: block)
                        sponser.looimg.contentMode = UIViewContentMode.scaleToFill;
                    }
                    let looname = self.selectdic.value(forKey: "loo_name") as? String;
                    if looname == ""
                    {
                        sponser.loonamelab.text = looname
                    }
                    else
                    {
                        sponser.loonamelab.text = looname
                    }
                    
                    
                    let date = Date()
                    let formatter = DateFormatter()
                    
                    
                    formatter.dateFormat = "dd-MMM-yyyy h:mm a"
                    formatter.amSymbol = "AM"
                    formatter.pmSymbol = "PM"
                    
                    let result = formatter.string(from: date)
                    let dattimearr = result.components(separatedBy: " ") as NSArray
                    let dispdate = dattimearr.object(at: 0) as! String
                    let dipstime = dattimearr.object(at: 1) as! String
                    let ampm = dattimearr.object(at: 2) as! String
                    
                    sponser.datelab.text = dispdate
                    sponser.timelab.text = String(format: "%@ %@", dipstime,ampm)
                    
                    sponser.closebutton.addTarget(self, action: #selector(DetailsViewController.closepopup(sender:)), for: UIControlEvents.touchUpInside)
                    sponser.termsandconditionsbutton.addTarget(self, action: #selector(self.termsandconditionbut(sender:)), for: UIControlEvents.touchUpInside)
                    sponser.bookbutton.addTarget(self, action: #selector(DetailsViewController.bookandnavigatebut(sender:)), for: UIControlEvents.touchUpInside)
                }
                
            }
        }
    }
    
    func closepopup(sender:UIButton!)
    {
        print("Button Clicked")
        KGModal.sharedInstance().hide(animated: true)
    }
    
    func termsandconditionbut(sender:UIButton!)
    {
        print("New Detail: terms and condition Button Clicked")
        KGModal.sharedInstance().hide(animated: true)
        let termsOfUseController = self.storyboard?.instantiateViewController(withIdentifier: "TermsOfUseViewController") as! TermsOfUseViewController
        let valueDict = self.detailsarr.object(at: 0) as! NSMutableDictionary
        termsOfUseController.termsOfUseDescription = (valueDict.value(forKey: "terms_conditions") as? String)!
        termsOfUseController.termsOfUseCheckedBefore = false
        self.present(termsOfUseController, animated: true, completion: nil)
    }
    
    func bookandnavigatebut(sender:UIButton!)
    {
        print("Button Clicked")
        SVProgressHUD.show()
        //KGModal.sharedInstance().hide(animated: true)
        
        self.bookinglink()      
    }
    
    func bookinglink()
    {
        if AFNetworkReachabilityManager.shared().isReachable
        {
            let isFreeProvider = self.selectdic.value(forKey: "free_provider") as! String
            if (self.isFreeBooking(freeProvider: isFreeProvider)) {
                let next = self.storyboard?.instantiateViewController(withIdentifier: "MovetoNavigateViewController") as! MovetoNavigateViewController
                next.selectdic = self.detailsarr.object(at: 0) as! NSMutableDictionary
                if ((self.selectdic.value(forKey: "facility_category") as? String) != nil) {
                    let value = self.selectdic.value(forKey: "facility_category") as? String
                    next.selectdic.setValue(value, forKey: "facility_category")
                }
                
                if ((self.selectdic.value(forKey: "accessbility") as? String) != nil) {
                    let value = self.selectdic.value(forKey: "accessbility") as? String
                    next.selectdic.setValue(value, forKey: "accessbility")
                }
                next.bookingidstr = "0"
                self.navigationController?.pushViewController(next, animated: true)
            } else {
                let urlstring = String(format:"%@/add_booking.php?user_id=%@&loo_id=%@&current_location=%@",kBaseURL,passuserid!,passlooid,passcurrentloc)
                print(urlstring)
                self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: false, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                    if Stats == true
                    {
                        
                        NSLog("jsonObject=%@", jsonObject);
                        if jsonObject.count != 0
                        {
                            var str = (jsonObject.object(at: 0) as AnyObject).value(forKey: "message") as? String
                            SVProgressHUD.dismiss()
                            KGModal.sharedInstance().hide(animated: true)
                            
                            if str == "success"
                            {
                                // SVProgressHUD.dismiss()
                                
                                let next = self.storyboard?.instantiateViewController(withIdentifier: "MovetoNavigateViewController") as! MovetoNavigateViewController
                                next.selectdic = self.detailsarr.object(at: 0) as! NSMutableDictionary
                                if ((self.selectdic.value(forKey: "facility_category") as? String) != nil) {
                                    let value = self.selectdic.value(forKey: "facility_category") as? String
                                    next.selectdic.setValue(value, forKey: "facility_category")
                                }
                                
                                if ((self.selectdic.value(forKey: "accessbility") as? String) != nil) {
                                    let value = self.selectdic.value(forKey: "accessbility") as? String
                                    next.selectdic.setValue(value, forKey: "accessbility")
                                }
                                next.bookingidstr = (jsonObject.object(at: 0) as AnyObject).value(forKey: "booking_id") as? String
                                self.navigationController?.pushViewController(next, animated: true)
                                
                            }
                            else
                            {
                                if str == self.serverResponseForAlreadyBooked {
                                    str = "Ongoing_booking_message".localized
                                    self.showAlertMessage(title: "WCi", message: str!, cancelButtonString: "Ok", identifier: self.alreadyBookedAlertIdentifier, isActionNeeded: true)
                                } else {
                                    self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:str!, withIdentifier:"")
                                }
                                
                            }
                        }
                    }
                    else
                    {
                        NSLog("jsonObject=error");
                    }
                })
            }
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
            
        }
    }
    
    @IBAction func Upload(x:AnyObject)
    {
        self.view.endEditing(true)
        let actionsheet = KKActionSheet.init(title:myclass.StringfromKey("Add Images") as String, delegate:self, cancelButtonTitle:myclass.StringfromKey("Cancel") as String, destructiveButtonTitle:nil ,otherButtonTitles:myclass.StringfromKey("Camera") as String,myclass.StringfromKey("Gallery") as String)
        actionsheet.show(in: self.view)
    }
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        if buttonIndex == 1
        {
            let imagePickerController = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                imagePickerController.delegate = self
                imagePickerController.sourceType = UIImagePickerControllerSourceType.camera;
                imagePickerController.allowsEditing = false
                UIApplication.shared.statusBarStyle = UIStatusBarStyle.default;
                self.present(imagePickerController, animated: true, completion: nil)
                UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent;
            }
        }
        else if buttonIndex == 2
        {
            let imagePickerController = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum)
            {
                imagePickerController.delegate = self
                imagePickerController.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
                imagePickerController.allowsEditing = false
                UIApplication.shared.statusBarStyle = UIStatusBarStyle.default;
                self.present(imagePickerController, animated: true, completion: nil)
            }
        }
        
    }
    
    // MARK: - Image Picker Delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        picker .dismiss(animated: true, completion: nil)
        let media =  info[UIImagePickerControllerOriginalImage] as? UIImage
        
        let controller = PECropViewController();
        controller.delegate = self;
        controller.image = media;
        controller.toolbarItems=nil;
        let ratio:CGFloat = 4.0 / 4.0;
        controller.cropAspectRatio = ratio;
        
        controller.dismiss(animated: true, completion:nil)
        
        let navigationController = UINavigationController.init(rootViewController: controller);
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            navigationController.modalPresentationStyle = UIModalPresentationStyle.formSheet;
        }
        self.present(navigationController, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker .dismiss(animated: true, completion: nil)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent;
    }
    
    func cropViewController(_ controller: PECropViewController!, didFinishCroppingImage croppedImage: UIImage!, transform: CGAffineTransform, cropRect: CGRect)
    {
        let ims:UIImage=croppedImage
        controller.dismiss(animated: true, completion:nil)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent;
        self.UploadimageWithImage(image: ims, size:CGSize(width: 1000, height: 1000))
    }
    func cropViewControllerDidCancel(_ controller: PECropViewController!)
    {
        controller.dismiss(animated: true, completion:nil)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent;
    }
    
    func UploadimageWithImage(image:UIImage,size:CGSize)
    {
        UIGraphicsBeginImageContext(size)
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let newimage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        let date=NSDate() as NSDate;
        let form = DateFormatter() as DateFormatter
        form.dateFormat="yyyy-MM-dd"
        let form1 = DateFormatter() as DateFormatter
        form1.dateFormat="HH:MM:SS"
        let datesstr = form.string(from: date as Date);
        let timestr = form1.string(from: date as Date);
        let datearr = datesstr.components(separatedBy: "-") as NSArray
        let timearr = timestr.components(separatedBy: ":") as NSArray
        let imageData = UIImageJPEGRepresentation(newimage,0.7);
        let imagename = String(format:"pic_%@_%@_%@_%@_%@_%@.png",datearr.object(at: 0) as! String,datearr.object(at: 1) as! String,datearr.object(at: 2) as! String,timearr.object(at: 0) as! String,timearr.object(at: 1) as! String,timearr.object(at: 2) as! String)
        if AFNetworkReachabilityManager.shared().isReachable
        {
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            //            hud = MBProgressHUD.showAdded(to: (navigationController?.view)!, animated: true)
            //            // Set the annular determinate mode to show task progress.
            //            hud.mode = MBProgressHUDMode.annularDeterminate
            //            hud.label.text = NSLocalizedString("Loading...", comment: "HUD loading title")
            //            hud.show(animated: true)
            
            //            HUD = MBProgressHUD .showAdded(to: (self.navigationController?.view)!, animated: true)
            //            HUD.label.text = NSLocalizedString("Loading", comment: "HUD loading title")
            //            HUD.show(animated: true)
            
            
            
            
            //            let  yval = UserDefaults()
            //            yval.setValue("10", forKey: "yval")
            //            yval.setValue("#0063AD", forKey:"loadcolor")
            //            yval.setValue("photo", forKey: "Type")
            
            //            imageHud = MyHUDProgress.init(view: self.view, yavl: 10);
            //            imageHud.hidehud(10.0)
            //            //            imageHud.frame = CGRectMake(0,0,UIScreen.main.bounds.size.width,UIScreen.mainScreen.bounds.size.height);
            //            imageHud.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            //            self.view.addSubview(imageHud)
            
            let request : NSMutableURLRequest = AFHTTPRequestSerializer().multipartFormRequest(withMethod: "POST", urlString:kBaseDomainUrl + "/upload_loo_image.php", parameters: nil, constructingBodyWith: { (formData:AFMultipartFormData) -> Void in
                
                formData.appendPart(withFileData: imageData!, name:"uploaded_file", fileName:imagename, mimeType: "image/png")
                
            }, error: nil)
            
            let conf : URLSessionConfiguration = URLSessionConfiguration.default
            
            let manager : AFURLSessionManager = AFURLSessionManager(sessionConfiguration: conf)
            
            manager.responseSerializer = AFHTTPResponseSerializer()
            let uploadTask:URLSessionUploadTask = manager.uploadTask(withStreamedRequest: request as URLRequest, progress:{(uploadProgress:Progress!) -> Void in
                
                DispatchQueue.main.async {
                    //  self.imageHud.progress.setProgress(Float(uploadProgress.fractionCompleted), animated: true)
                }
                
            })
            {(response, responseObject, error) -> Void in
                MBProgressHUD.hide(for: self.view, animated: true)
                
                //                print(error)
                //                self.imageHud.removeFromSuperview();
                //                self.imageHud = nil;
                if((error == nil))
                {
                    //                    self.storarr.add(String(format:"http://reliefz.com/doctor/images/pain/%@",imagename))
                    //                    print(self.storarr)
                    //                    print(self.storarr.count)
                    //                    if self.storarr.count != 0
                    //                    {
                    //                        self.storeimgviewheight.constant = 146
                    //                    }
                    //                    else
                    //                    {
                    //                        self.storeimgviewheight.constant = 120
                    //                    }
                    //
                    //                    self.collectionimaage.reloadData()
                    if AFNetworkReachabilityManager.shared().isReachable
                    {
                        let urlstring = String(format:kBaseDomainUrl + "/json/loo_image_update.php?loo_id=%@&image="+kBaseDomainUrl + "/images/loo/%@",self.passlooid,imagename)
                        print(urlstring)
                        self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: false, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                            if Stats == true
                            {
                                NSLog("jsonObject=%@", jsonObject);
                                if jsonObject.count != 0
                                {
                                    let sts = (jsonObject.object(at: 0) as AnyObject).value(forKey: "message") as? String
                                    if sts == "success"
                                    {
                                        self.scrollview.removeTwitterCover()
                                        self.scrollview.addTwitterCover(with: image)
                                    }
                                    else
                                    {
                                        self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:"Error Occured", withIdentifier:"internet")
                                    }
                                }
                            }
                            else
                            {
                                NSLog("jsonObject=error");
                            }
                        })
                    }
                    else
                    {
                        self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
                    }
                }
                else
                {
                    self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert: self.myclass.StringfromKey("Errorwhieleupload"), withIdentifier:"error")
                }
            }
            
            uploadTask.resume()
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert: self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        if collectionView == facilitycollection
        {
            return self.facilitiesarr.count
        }
        else if collectionView == accessibilitycollection
        {
            return 1
        }
        else
        {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView == facilitycollection
        {
            let dic = self.insidefacilities.object(at: section) as! NSArray;
            print(dic)
            return dic.count
        }
        //        else if collectionView == accessibilitycollection
        //        {
        return self.accessibilityarr.count
        //        }
        //        else
        //        {
        //            return self.categoryarr.count
        //        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        //       let headerView = Bundle.main.loadNibNamed("FacilityHeaderCollectionReusableView", owner: self, options: nil)?[0] as! FacilityHeaderCollectionReusableView
        //        if collectionView == facilitycollection
        //        {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FacilityHeaderCollectionReusableView", for: indexPath) as! FacilityHeaderCollectionReusableView
        
        
        if self.facilitiesarr.count != 0
        {
            let dataarr = (self.facilitiesarr.object(at: indexPath.section) as AnyObject).value(forKey: "facility_category_name") as! String
            
            
            // let laungauedataarr = dataarr.localized
            //            laungauedataarr.append(dataarr)
            headerView.headernamelab.text = dataarr.localized
            
            let imageurl = (self.facilitiesarr.object(at: indexPath.section) as AnyObject).value(forKey: "facility_category_image") as! String
            if imageurl.characters.count == 0 || imageurl == ""
            {
                //                headerView.headerimg.image = UIImage.init(named: "no-image.png")
            }
            else
            {
                let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                    print(self)
                }
                headerView.headerimg.sd_setImage(with: NSURL(string:imageurl) as URL!, completed: block)
                headerView.headerimg.contentMode = UIViewContentMode.scaleToFill;
            }
        }
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == facilitycollection
        {
            let cell: FiltersCollectionViewCell = (collectionView.dequeueReusableCell(withReuseIdentifier: "FiltersCollectionViewCell", for: indexPath) as? FiltersCollectionViewCell)!
            
            self.collectionfacheight.constant = self.facilitycollection.contentSize.height
            
            let dic = self.insidefacilities.object(at: indexPath.section) as! NSArray;
            
            print(dic)
            
            if cell.isSelected == true
            {
                
            }
            else
            {
                
            }
            
            let namestr = (dic.object(at: indexPath.item) as AnyObject).value(forKey: "facility_name") as! String
            if namestr == ""
            {
                
            }
            else
            {
                cell.namelab.text = namestr.localized
            }
            
            let imageurl = (dic.object(at: indexPath.item) as AnyObject).value(forKey: "facility_image") as! String
            if imageurl.characters.count == 0 || imageurl == ""
            {
                cell.insideimg.image = UIImage.init(named: "default_image.png")
            }
            else
            {
                let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                    print(self)
                }
                cell.insideimg.sd_setImage(with: NSURL(string:imageurl) as URL!, completed: block)
                cell.insideimg.contentMode = UIViewContentMode.scaleToFill;
            }
            return cell
        }
            
        else
        {
            let cell: FiltersCollectionViewCell = (collectionView.dequeueReusableCell(withReuseIdentifier: "FiltersCollectionViewCell", for: indexPath) as? FiltersCollectionViewCell)!
            self.collectionaccheight.constant = self.accessibilitycollection.contentSize.height
            let imageurl = (self.accessibilityarr.object(at: indexPath.item) as AnyObject).value(forKey: "accessbility_image") as! String
            if imageurl.characters.count == 0 || imageurl == ""
            {
                cell.insideimg.image = UIImage.init(named: "default_image.png")
            }
            else
            {
                let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                    print(self)
                }
                cell.insideimg.sd_setImage(with: NSURL(string:imageurl) as URL!, completed: block)
                cell.insideimg.contentMode = UIViewContentMode.scaleToFill;
            }
            
            let namestr = (self.accessibilityarr.object(at: indexPath.item) as AnyObject).value(forKey: "accessbility_name") as! String
            if namestr == ""
            {
                
            }
            else
            {
                cell.namelab.text = namestr.localized
            }
            return cell
        }
    }
    
    @IBAction func reviewsbutton(_ sender : UIButton)
    {
        let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "ReviewsViewController") as! ReviewsViewController
        mainview.looid = self.passlooid
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.pushViewController(mainview, animated: true)
    }
    
    @IBAction func EditButtonTapped(_ sender: Any) {
        
        editLoo = 1
        let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "AddLooViewController") as! AddLooViewController
        mainview.detailsarr = self.detailsarr
        self.navigationController?.pushViewController(mainview, animated: true)
    }
    
    
    /// Method to display the booking list page
    func showBookingListPage() {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
        appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
        currentview.isNavigationBarHidden = false
        let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "BookingListViewController") as! BookingListViewController
        currentview.pushViewController(mainview, animated: false)
    }
    
    func showAlertMessage(title:String, message:String, cancelButtonString:String, identifier:String, isActionNeeded:Bool) {
        let alert = UIAlertView(title:title, message:message, delegate:self, cancelButtonTitle:cancelButtonString)
        alert.restorationIdentifier = identifier;
        alert.show()
    }
    
    func alertView(_ View: UIAlertView, clickedButtonAt buttonIndex: Int)
    {
        print("LOG:ALERT CLICKED")
        if buttonIndex == 0 && View.restorationIdentifier == alreadyBookedAlertIdentifier {
            print("LOG:ALREADY BOOKED ALERT")
            self.showBookingListPage()
            return
        }
    }
    
    func showOrHideActivityIndicator(toShow: Bool){
        if toShow {
            self.activityIndicator.show()
            self.activityIndicator.startAnimating()
        } else {
            self.activityIndicator.hide()
            self.activityIndicator.stopAnimating()
        }
    }
    
    func getDefaultImageForCategory(categoryName:String) -> String {
        var imageName = "public"
        
        if categoryName == "" {
            imageName = "public"
        } else if categoryName == "Gas Station" {
            imageName = "Gas"
        } else if categoryName == "Hotel" {
            imageName = "Hotels"
        } else if categoryName == "Restaurant" {
            imageName = "Restaurant"
        } else if categoryName == "Shopping Mall" {
            imageName = "shoping_marker"
        } else if categoryName == "Bar" {
            imageName = "bar_marker"
        } else if categoryName == "Public" {
            imageName = "Public-Icon"
        } else if categoryName == "Temp" {
            imageName = "Temp-Icon"
        } else if categoryName == "Shower" {
            imageName = "Shower-Icon"
        }
        return imageName
    }
    
    //Method to check if login is required
    func isLoginRequired(freeProvider:String) -> Bool {
        if isPublicToilet() || (freeProvider == "1")  {
            return false
        } else if (UserDefaults.standard.object(forKey: "Logindetail") != nil) {
            return false
        } else {
            return true
        }
    }
    
    //Method to check is free booking
    func isFreeBooking(freeProvider:String) -> Bool {
        if isPublicToilet() || (freeProvider == "1")  {
            return true
        } else {
            return false
        }
    }
    
}
