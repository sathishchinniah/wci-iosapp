//
//  FavouritesViewController.swift
//  JEDE_r
//
//  Created by Exarcplus on 26/05/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import AFNetworking
import WOWCardStackView
import CoreLocation
import SDWebImage

class FavouritesViewController: UIViewController, CardStackViewDataSource, CardStackViewDelegate, CLLocationManagerDelegate, GMSMapViewDelegate, UIAlertViewDelegate {
    
    var myclass : MyClass!
    var Userdic = NSMutableDictionary()
    var passuserid : String!
    var passLocation : String!
    var datasarr = NSMutableArray()
    @IBOutlet weak var viewmap: GMSMapView!
    
    @IBOutlet weak var cardStackView: CardStackView!
    var orderNo: Int = 0
    
    
    var locmanager:CLLocationManager!
    var currentlocation = CLLocation()
    var movelocation = CLLocation()
    var passloc : String!
    var passlooid : String!
    var myindex = Int()
    
    let serverResponseForAlreadyBooked = "already Booked"
    let alreadyBookedAlertIdentifier = "alreadyBookedAlertIdentifier"
    var bookedToiletIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        myclass = MyClass()
        myindex = 0
        let view = UIView(frame:  CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 20))
        view.backgroundColor = UIColor.init(red: 27.0/255.0, green: 34.0/255.0, blue: 40.0/255.0, alpha: 1.0)
        self.view.addSubview(view)
       
        if UserDefaults.standard.object(forKey: "Logindetail") == nil
        {
            passuserid = ""
        }
        else
        {
            let placesData = UserDefaults.standard.object(forKey: "Logindetail") as? NSDictionary
            self.Userdic.removeAllObjects()
            if placesData != nil{
                self.Userdic.addEntries(from: (placesData as? [String : Any])!)
                print(self.Userdic)
                passuserid = self.Userdic.value(forKey: "user_id") as? String
                print(passuserid)
            }
        }
        
        self.cardStackView.register(nib: UINib(nibName: "MyCard", bundle: nil))
        self.cardStackView.delegate = self
        
        locmanager = CLLocationManager()
        locmanager.delegate = self
        locmanager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locmanager.activityType = .automotiveNavigation
        locmanager.requestAlwaysAuthorization()
        viewmap.settings.myLocationButton = false
        viewmap.isMyLocationEnabled = true
        viewmap.settings.compassButton = true
        viewmap.delegate = self;
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        print("THRONES \(String(describing: self.navigationController?.isNavigationBarHidden))")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
        currentview.isNavigationBarHidden = false
        self.navigationController?.isNavigationBarHidden = false
        self.title = "activity_main_thrones".localized
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        print("didUpdateToLocation: %@", locations.last)
            currentlocation = locations.last!
            viewmap.clear()
            let camera = GMSCameraPosition.init(target: currentlocation.coordinate, zoom: 11, bearing: 0, viewingAngle: 0)
            viewmap .animate(to: camera)
    }
    
    func mapView(_ mapView: GMSMapView!, idleAt position: GMSCameraPosition!)
    {
            let point = mapView.center
            let coor = mapView.projection .coordinate(for: point)
            let loc = CLLocation.init(latitude: coor.latitude, longitude: coor.longitude)
            currentlocation = loc;
            self .getAddressFromLatLon(location: currentlocation)
    }
    
    func getAddressFromLatLon(location:CLLocation)
    {
        let geoCoder = GMSGeocoder()
        self.movelocation = location
        self.loadData()
        geoCoder.reverseGeocodeCoordinate(location.coordinate, completionHandler: {(respones,err) in
//            if err == nil
//            {
                if respones != nil
                {
                    let gmaddr = respones?.firstResult()
                    print(gmaddr!)
//                    self.currentadddress = gmaddr
//                    var addrarray : NSMutableArray! = NSMutableArray()
//                    let count:Int = (gmaddr?.lines.count)!
//                    for c in 0 ..< count
//                    {
//                        addrarray.add(gmaddr?.lines[c] as! String)
//                    }
//                    
//                    let formaddr = addrarray.componentsJoined(by: ", ");
//                    NSLog("%@",formaddr)
//                    addrarray = nil
//                    self.locationtext.text = formaddr
                    
//                    self.dummystr = UserDefaults.standard.string(forKey: "runlink")
//                    if self.dummystr == nil || self.dummystr == "" || self.dummystr == "needrunlink"
//                    {
//                        print(self.dummystr)
//                        let timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.runlink), userInfo: nil, repeats: false)
//                        let valueToSave = "noneedrunlink"
//                        UserDefaults.standard.set(valueToSave, forKey: "runlink")
//                    }
//                    else
//                    {
//                        print(self.dummystr)
//                    }
                    
                }
                else
                {
//                    self.locationtext.text = "Address not found"
                }
//            }
//            else
//            {
//                //self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("JEDE_r"), withAlert:self.myclass.StringfromKey("errorinretrive"), withIdentifier:"error")
//            }
        })
    }
    
    @IBAction func OpenSideMenu(_ x:AnyObject)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.SideMenu.showLeftView(animated: true, completionHandler: nil)
          NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadsidemenu"), object: nil, userInfo: nil)
    }
    
    func loadData()
    {
        if AFNetworkReachabilityManager.shared().isReachable
        {
            passloc = String(format: "%f,%f", movelocation.coordinate.latitude,movelocation.coordinate.longitude)
            print(passloc)
            let urlstring = String(format:"%@/fav_list.php?user_id=%@&current_location=%@",kBaseURL,passuserid,passloc)
            print(urlstring)
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: false, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    if jsonObject.count != 0
                    {
                        self.datasarr.removeAllObjects()
                        self.datasarr.addObjects(from: jsonObject as [AnyObject])
                        print(self.datasarr)
                        self.cardStackView.dataSource = self
                        self.cardStackView.delegate = self
                        self.orderNo = self.datasarr.count
                    }
                }
                else
                {
                    NSLog("jsonObject=error");
                }
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
    
    func nextCard(ins: CardStackView) -> CardView? {
        var card = cardStackView.dequeueCardView() as! MyCard
        if orderNo < self.datasarr.count-1
        {
            orderNo += 1
        }
        else
        {
            orderNo = 0
        }
        print("ORDER NUMBER \(orderNo)")
        card = createCard(order: orderNo)
        return card
        

    }
    
    func cardStackView(_ cardStackView: CardStackView, cardAt index: Int) -> CardView
    {
//        if self.cardStackView == nil {
//            self.cardStackView.register(nib: UINib(nibName: "MyCard", bundle: nil))
//        }
//
//        let card = cardStackView.dequeueCardView() as! MyCard
//
//        let typestr = (self.datasarr.object(at: index) as AnyObject).value(forKey: "type") as? String
//        let looId = (self.datasarr.object(at: index) as AnyObject).value(forKey: "loo_id") as? String
//        card.id = Int(looId!)!
//
//        if typestr == "Public"
//        {
//            card.bookbutton.setTitle("NAVIGATE", for: .normal)
//        }
//        else
//        {
//            card.bookbutton.setTitle("BOOK", for: .normal)
//        }
//
//        DispatchQueue.main.async {
//            let imageurlFullList = (self.datasarr.object(at: index) as AnyObject).value(forKey: "loo_image") as? String
//            let ImageListSeperated = imageurlFullList?.components(separatedBy: ",")
//            if (ImageListSeperated != nil) {
//                let imageurl = ImageListSeperated?.first
//                if imageurl?.count == 0 || imageurl == "" || imageurl == "http://stitchplus.com/JEDE_rpanel/"
//                {
//                    card.looimg.image = UIImage.init(named: "no-image.png")
//                }
//                else
//                {
//                    let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
//
//                    }
//                    card.looimg.sd_setImage(with: URL(string: imageurl!), completed: block)
//                    card.looimg.contentMode = UIViewContentMode.scaleToFill;
//                }
//            }
//        }
//
//
//        let loonamestr = (self.datasarr.object(at: index) as AnyObject).value(forKey: "loo_name") as? String
//        if loonamestr == nil
//        {
//            card.loonamelab.text = ""
//        }
//        else
//        {
//            card.loonamelab.text = loonamestr
//        }
//        let rating = (self.datasarr.object(at: index) as AnyObject).value(forKey: "rating") as! NSNumber
//        print(rating)
//        print(CGFloat(rating.doubleValue))
//        if rating.intValue != 0
//        {
//            card.ratingview.value = CGFloat(rating.doubleValue)
//        }
//        else
//        {
//            card.ratingview.value = 0
//        }
//        let distancestr = (self.datasarr.object(at: index) as AnyObject).value(forKey: "distance") as? String
//        if distancestr == nil || distancestr == ""
//        {
//            card.distancelab.text = ""
//        }
//        else
//        {
//            card.distancelab.text = String(format: "Distance %@ KM", distancestr!)
//        }
//        let locationstr = (self.datasarr.object(at: index) as AnyObject).value(forKey: "loo_address") as? String
//        if locationstr == nil
//        {
//            card.addresslab.text = ""
//        }
//        else
//        {
//            card.addresslab.text = locationstr
//        }
//
//        card.viewMoreButton.setTitle("view_more".localized, for: .normal)
//        print(index)
//        card.viewMoreButton.addTarget(self, action: #selector(FavouritesViewController.viewMoreButonAction(sender:)), for: .touchUpInside)
//        card.bookbutton.addTarget(self, action: #selector(FavouritesViewController.bookbutton(sender:)), for: UIControlEvents.touchUpInside)
        return createCard(order: index)
    }
    
    func numOfCardInStackView(_ cardStackView: CardStackView) -> Int {
        return self.datasarr.count
    }
    
    func cardStackView(_: CardStackView, didSelect card: CardView)
    {
        return
    }

    func getCardIndex() -> Int {
       let card = cardStackView.dequeueCardView() as! MyCard
        print("LOG: DATA DICT")
        print(self.datasarr)
        var arrayIndex = -1;
        for case let dataDict as NSDictionary in self.datasarr {
            arrayIndex = arrayIndex + 1
            let looId = dataDict.value(forKey: "loo_id") as? String
            print("card.id \(card.id)")
            print("card.id \(looId)")
            
            if looId == card.id {
                print("ARRAY FOUND \(arrayIndex)")
                return arrayIndex
            }
        }
        print("DEFAULT \(arrayIndex)")
        return arrayIndex
    }
    
    func createCard(order: Int) -> MyCard
    {
        let card = cardStackView.dequeueCardView() as! MyCard
        
        let typestr = (self.datasarr.object(at: order) as AnyObject).value(forKey: "type") as? String
        if let looId = (self.datasarr.object(at: order) as AnyObject).value(forKey: "loo_id") as? String {
            print("LOOID \(looId)")
            card.id = looId
            print("card.id \(card.id)")
        }
        
        if typestr == "Public"
        {
            card.bookbutton.setTitle("NAVIGATE", for: .normal)
        }
        else
        {
            card.bookbutton.setTitle("BOOK", for: .normal)
        }
        
        let loonamestr = (self.datasarr.object(at: order) as AnyObject).value(forKey: "loo_name") as? String
        if loonamestr == nil
        {
            card.loonamelab.text = ""
        }
        else
        {
            card.loonamelab.text = loonamestr
        }
        
        DispatchQueue.main.async {
            let imageurlFullList = (self.datasarr.object(at: order) as AnyObject).value(forKey: "loo_image") as? String
            let ImageListSeperated = imageurlFullList?.components(separatedBy: ",")
            if (ImageListSeperated != nil) {
                let imageurl = ImageListSeperated?.first
                if imageurl?.count == 0 || imageurl == "" || imageurl == "http://stitchplus.com/JEDE_rpanel/"
                {
                    if let categoryType = (self.datasarr.object(at: order) as AnyObject).value(forKey: "category_name") as? String {
                        let categoryImage = self.getDefaultImageForCategory(categoryName: categoryType)
                        card.looimg.image = UIImage.init(named: categoryImage)
                    } else {
                        card.looimg.image = UIImage.init(named: "no-image.png")
                    }
                }
                else
                {
                    let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
                        
                    }
                    card.looimg.sd_setImage(with: URL(string: imageurl!), completed: block)
                    card.looimg.contentMode = UIViewContentMode.scaleToFill;
                }
            }
        }
        
        let rating = (self.datasarr.object(at: order) as AnyObject).value(forKey: "rating") as! NSNumber
        print(rating)
        print(CGFloat(rating.doubleValue))
        if rating.intValue != 0
        {
            card.ratingview.value = CGFloat(rating.doubleValue)
        }
        else
        {
            card.ratingview.value = 0
        }
        
        let distancestr = (self.datasarr.object(at: order) as AnyObject).value(forKey: "distance") as? String
        if distancestr == nil || distancestr == ""
        {
            card.distancelab.text = ""
        }
        else
        {
            card.distancelab.text = String(format: "%@ KM", distancestr!)
        }
        let locationstr = (self.datasarr.object(at: order) as AnyObject).value(forKey: "loo_address") as? String
        if locationstr == nil
        {
            card.addresslab.text = ""
        }
        else
        {
            card.addresslab.text = locationstr
        }
        
        
        card.viewMoreButton.setTitle("view_more".localized, for: .normal)
        card.viewMoreButton.titleLabel?.textAlignment = NSTextAlignment.right
        card.viewMoreButton.addTarget(self, action: #selector(FavouritesViewController.viewMoreButonAction(sender:)), for: .touchUpInside)
        //Unhide while implementing the view more
        card.viewMoreButton.isHidden = true
        card.bookbutton.addTarget(self, action: #selector(FavouritesViewController.bookbutton(sender:)), for: UIControlEvents.touchUpInside)
        card.bookbutton.tag = order
        return card
    }
    
    
    func viewMoreButonAction(sender:UIButton!) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "NewDetailsViewController") as! NewDetailsViewController
        print("Selected order number \(self.orderNo)")
        if self.orderNo < self.datasarr.count {
            secondViewController.selectdic = self.datasarr.object(at: self.orderNo) as! NSMutableDictionary
            print(secondViewController.selectdic)
        }
        secondViewController.passcurrentloc = passloc
        self.navigationController?.pushViewController(secondViewController, animated: true)
    }
    
    
    func bookbutton(sender:UIButton!)
    {
        print("Button Clicked")
        print("ODER NUMBER \(self.orderNo)")
        
        let index = sender.tag
        bookedToiletIndex = index
        let dic = self.datasarr.object(at: index) as! NSMutableDictionary
        print(":1\(dic)")
        
        let typestr = dic.value(forKey: "type") as? String
        if typestr == "Public"
        {
            let next = self.storyboard?.instantiateViewController(withIdentifier: "MovetoNavigateViewController") as! MovetoNavigateViewController
            next.selectdic = dic
            next.passLocationDetails = self.passloc
            self.navigationController?.pushViewController(next, animated: true)
        }
        else
        {
            print(dic)
            passlooid = dic.value(forKey: "loo_id") as? String
            self.bookinglink()
        }
    }

    
    func getDefaultImageForCategory(categoryName:String) -> String {
        var imageName = "public"
        
        if categoryName == "" {
            imageName = "public"
        } else if categoryName == "Gas Station" {
            imageName = "Gas"
        } else if categoryName == "Hotel" {
            imageName = "Hotels"
        } else if categoryName == "Restaurant" {
            imageName = "Restaurant"
        } else if categoryName == "Shopping Mall" {
            imageName = "shoping_marker"
        } else if categoryName == "Bar" {
            imageName = "bar_marker"
        } else if categoryName == "Public" {
            imageName = "Public-Icon"
        } else if categoryName == "Temp" {
            imageName = "Temp-Icon"
        } else if categoryName == "Shower" {
            imageName = "Shower-Icon"
        }
        
        return imageName
    }
    
    
    func closepopup(sender:UIButton!)
    {
        print("Button Clicked")
        KGModal.sharedInstance().hide(animated: true)
    }
    
    func termsandconditionbut(sender:UIButton!)
    {
        print("terms and condition Button Clicked")
        KGModal.sharedInstance().hide(animated: true)
    }
    
    func bookandnavigatebut(sender:UIButton!)
    {
        print("Button Clicked")
        KGModal.sharedInstance().hide(animated: true)
        
        if AFNetworkReachabilityManager.shared().isReachable
        {
            let urlstring = String(format:"%@/add_booking.php?user_id=%@&loo_id=%@&current_location=%@",kBaseURL,passuserid!,passlooid!,self.passloc)
            print(urlstring)
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: false, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    if jsonObject.count != 0
                    {
                        var str = (jsonObject.object(at: 0) as AnyObject).value(forKey: "message") as? String
                        if str == "success"
                        {
                            let next = self.storyboard?.instantiateViewController(withIdentifier: "MovetoNavigateViewController") as! MovetoNavigateViewController
                            next.selectdic = self.datasarr.object(at: sender.tag) as! NSMutableDictionary
                            next.passLocationDetails = self.passloc
                            next.bookingidstr = (jsonObject.object(at: 0) as AnyObject).value(forKey: "booking_id") as? String
                            self.navigationController?.pushViewController(next, animated: true)
                        }
                        else
                        {
                            if str == self.serverResponseForAlreadyBooked {
                                str = "Ongoing_booking_message".localized
                                self.showAlertMessage(title: "WCi", message: str!, cancelButtonString: "Ok", identifier: self.alreadyBookedAlertIdentifier, isActionNeeded: true)
                            } else {
                                self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:str!, withIdentifier:"")
                            }
                            
                        }
                    }
                }
                else
                {
                    NSLog("jsonObject=error");
                }
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
        
    }
    
    
    func bookinglink()
    {
        if AFNetworkReachabilityManager.shared().isReachable
        {
            let urlstring = String(format:"%@/add_booking.php?user_id=%@&loo_id=%@&current_location=%@",kBaseURL,passuserid!,passlooid!,self.passloc)
            print(urlstring)
            self.myclass.GetArraryfromURL(urlstring, inviewController: self, Withhud: false, completionHandler:{(jsonObject:NSArray,Stats:Bool) in
                if Stats == true
                {
                    NSLog("jsonObject=%@", jsonObject);
                    if jsonObject.count != 0
                    {
                        var str = (jsonObject.object(at: 0) as AnyObject).value(forKey: "message") as? String
                        if str == "success"
                        {
                            let next = self.storyboard?.instantiateViewController(withIdentifier: "MovetoNavigateViewController") as! MovetoNavigateViewController
                            next.selectdic = self.datasarr.object(at: self.bookedToiletIndex) as! NSMutableDictionary
                            print("LOG: selectdic \(next.selectdic)")
                            next.bookingidstr = (jsonObject.object(at: 0) as AnyObject).value(forKey: "booking_id") as? String
                            next.passLocationDetails = self.passloc
                            self.navigationController?.pushViewController(next, animated: true)
                        }
                        else
                        {
                            if str == self.serverResponseForAlreadyBooked {
                                str = "Ongoing_booking_message".localized
                            }
                            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:str!, withIdentifier:"")
                        }
                    }
                }
                else
                {
                    NSLog("jsonObject=error");
                }
            })
        }
        else
        {
            self.myclass.ShowsinglebutAlertwithTitle(self.myclass.StringfromKey("WCi"), withAlert:self.myclass.StringfromKey("Check your connection"), withIdentifier:"internet")
        }
    }
    
    
    
    /// Method to display the booking list page
    func showBookingListPage() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let currentview = appDelegate.SideMenu.rootViewController as! UINavigationController
        appDelegate.SideMenu.hideLeftView(animated: true, completionHandler: nil)
        let mainview = kmainStoryboard.instantiateViewController(withIdentifier: "BookingListViewController") as! BookingListViewController
        currentview.isNavigationBarHidden = false
        currentview.pushViewController(mainview, animated: false)
    }
    
    func showAlertMessage(title:String, message:String, cancelButtonString:String, identifier:String, isActionNeeded:Bool) {
        let alert = UIAlertView(title:title, message:message, delegate:self, cancelButtonTitle:cancelButtonString)
        alert.restorationIdentifier = identifier;
        alert.show()
    }
    
    func alertView(_ View: UIAlertView, clickedButtonAt buttonIndex: Int)
    {
        print("LOG:ALERT CLICKED")
        if buttonIndex == 0 && View.restorationIdentifier == alreadyBookedAlertIdentifier {
            print("LOG:ALREADY BOOKED ALERT")
            self.showBookingListPage()
            return
        }
    }

}
