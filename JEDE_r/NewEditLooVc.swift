//
//  NewEditLooVc.swift
//  JEDE_r
//
//  Created by Exarcplus on 12/4/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit
import ImageSlideshow
import Alamofire

class NewEditLooVc: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var imageSlide: ImageSlideshow!
    @IBOutlet weak var looNameTextField: SkyFloatingLabelTextField!
    
    @IBOutlet weak var PriceTextField: SkyFloatingLabelTextField!
    
    
    @IBOutlet weak var offerersCollectionView: UICollectionView!
    
    @IBOutlet weak var facilitiesTableView: UITableView!
    
    @IBOutlet weak var addressTextField: UITextView!
    @IBOutlet weak var AccessabilityCollectionView: UICollectionView!
    
    @IBOutlet weak var infoTextView: UITextView!
    
    @IBOutlet weak var termsandcTextView: UITextView!

    @IBOutlet weak var dateAndTimeTable: UITableView!
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        facilitiesTableView.register(UINib.init(nibName:"AddFacilityTableViewCell", bundle: nil),forCellReuseIdentifier: "AddFacilityTableViewCell")
        facilitiesTableView.estimatedRowHeight = 48;
        facilitiesTableView.rowHeight = 48
        facilitiesTableView.tableFooterView?.isHidden = true
//        
//        facilitiesTableView.delegate = self
//        facilitiesTableView.dataSource = self
//        
//        dateAndTimeTable.delegate = self
//        dateAndTimeTable.dataSource = self
        
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    func getDatastructure(){
        
        let params:[String:String] = ["loo_id":"147"]
        
        Alamofire.request(kBaseURL + "/ios_vender_loo_details.php", method: .get,parameters: params).responseJSON { response in
            
            print(params)
            print(response)
            print(response.result.value)
            
            if response.result.isSuccess {
                //                let js = JSON(response.result.value)
                //                //let responseJSON = response.result.value as! [String:AnyObject]
                //
                //                let jsonString = try? JSONSerialization.jsonObject(with: responseJSON, options: [])
                
                //let loo = Loo.from(data: response.result.value)
                
                let loo = GettingStarted.from(data: response.data! )
                
                print(loo?.result)
                print(loo?.result[0].looId)
                print(loo?.result[0].category[0].catDisableImage)
                print(loo?.result[0].facilities[0].facility[0].facilityName)
                
                
                
                
                
            }
            else {
                print("Error \(String(describing: response.result.error))")
                
            }
        }
    }
    
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
      
        if collectionView == offerersCollectionView
        {
            return 1
        }
        else
        {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
      
//        if collectionView == offerersCollectionView
//        {
//
//            return self.accessibilityarr.count
//        }
//        else
//        {
//
//            return self.categoryarr.count
//        }
        return 2
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cellId: NSString = "Cell"
        let cell: UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId as String, for: indexPath)
        //dequeueReusableCellWithIdentifier(cellId) as UITableViewCell
//        if collectionView == accessibilitycollection
//        {
//            let cell: FiltersCollectionViewCell = (collectionView.dequeueReusableCell(withReuseIdentifier: "FiltersCollectionViewCell", for: indexPath) as? FiltersCollectionViewCell)!
//            self.collectionaccheight.constant = self.accessibilitycollection.contentSize.height
//
//            let apps = (self.accessibilityarr.object(at: indexPath.item) as AnyObject).value(forKey: "accessbility_id") as! String
//            print(apps)
//            print(ds)
//            print(indexPath.item)
//
//            //print(ds[0]["accessbility"]?["\(indexPath.item)"]?!["status"]?++ )
//
//            print(accessibilityidstore)
//            if accessibilityidstore.contains(apps)
//            {
//                let imageurl = (self.accessibilityarr.object(at: indexPath.item) as AnyObject).value(forKey: "accessbility_enable_image") as! String
//                if imageurl.characters.count == 0 || imageurl == ""
//                {
//                    cell.insideimg.image = UIImage.init(named: "default_image.png")
//                }
//                else
//                {
//                    let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
//                        print(self)
//                    }
//                    cell.insideimg.sd_setImage(with: NSURL(string:imageurl) as URL!, completed: block)
//                    cell.insideimg.contentMode = UIViewContentMode.scaleToFill;
//                }
//
//            }
//            else
//            {
//                let imageurl = (self.accessibilityarr.object(at: indexPath.item) as AnyObject).value(forKey: "accessbility_disable_image") as! String
//                if imageurl.characters.count == 0 || imageurl == ""
//                {
//                    cell.insideimg.image = UIImage.init(named: "default_image.png")
//                }
//                else
//                {
//                    let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
//                        print(self)
//                    }
//                    cell.insideimg.sd_setImage(with: NSURL(string:imageurl) as URL!, completed: block)
//                    cell.insideimg.contentMode = UIViewContentMode.scaleToFill;
//                }
//            }
//
//            let namestr = (self.accessibilityarr.object(at: indexPath.item) as AnyObject).value(forKey: "accessbility_name") as! String
//            if namestr == ""
//            {
//
//            }
//            else
//            {
//                cell.namelab.text = namestr
//            }
//            return cell
//        }
//
//        else
//        {
//            let cell: FiltersCollectionViewCell = (collectionView.dequeueReusableCell(withReuseIdentifier: "FiltersCollectionViewCell", for: indexPath) as? FiltersCollectionViewCell)!
//            self.collectioncatheight.constant = self.categorycollection.contentSize.height
//
//            let apps = (self.categoryarr.object(at: indexPath.item) as AnyObject).value(forKey: "category_id") as! String
//            print(apps)
//            print(categoryidstore)
//            if categoryidstore.contains(apps)
//            {
//                let imageurl = (self.categoryarr.object(at: indexPath.item) as AnyObject).value(forKey: "category_enable_image") as! String
//                if imageurl.characters.count == 0 || imageurl == ""
//                {
//                    cell.insideimg.image = UIImage.init(named: "default_image.png")
//                }
//                else
//                {
//                    let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
//                        print(self)
//                    }
//                    cell.insideimg.sd_setImage(with: NSURL(string:imageurl) as URL!, completed: block)
//                    cell.insideimg.contentMode = UIViewContentMode.scaleToFill;
//                }
//            }
//            else
//            {
//                let imageurl = (self.categoryarr.object(at: indexPath.item) as AnyObject).value(forKey: "category_disable_image") as! String
//                if imageurl.characters.count == 0 || imageurl == ""
//                {
//                    cell.insideimg.image = UIImage.init(named: "default_image.png")
//                }
//                else
//                {
//
//                    let block: SDWebImageCompletionBlock! = {(image, error, cacheType, imageURL) -> Void in
//                        print(self)
//                    }
//                    cell.insideimg.sd_setImage(with: NSURL(string:imageurl) as URL!, completed: block)
//                    cell.insideimg.contentMode = UIViewContentMode.scaleToFill;
//                }
//            }
//
//            let namestr = (self.categoryarr.object(at: indexPath.item) as AnyObject).value(forKey: "category_name") as! String
//            if namestr == ""
//            {
//
//            }
//            else
//            {
//                cell.namelab.text = namestr
//            }
//
//
        
            return cell
    
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        //        if collectionView == facilitycollection
        //        {
        //            dic = self.insidefacilities.object(at: indexPath.section) as! NSArray;
        //            print(dic)
        //            let ids = (dic.object(at: indexPath.item) as AnyObject).value(forKey: "facility_id") as! String
        //            print(ids)
        //            //        let ids = dic.value(forKey: "facility_id") as? String;
        //            if facilityidstore.contains(ids)
        //            {
        //                self.facilityidstore.remove(ids)
        //            }
        //            else
        //            {
        //                self.facilityidstore.add(ids)
        //            }
        //            let indexPath = IndexPath(row: indexPath.item, section: indexPath.section)
        //            self.facilitycollection.reloadItems(at: [indexPath])
        //            let indexSet = IndexSet(integer: indexPath.section)
        //            self.facilitycollection.reloadSections(indexSet)
        //        }
//        if collectionView == accessibilitycollection
//        {
//            let ids = (self.accessibilityarr.object(at: indexPath.item) as AnyObject).value(forKey: "accessbility_id") as! String
//
//            print(ids)
//
//            if accessibility_id.contains(ids){
//
//            }
//            else
//            {
//                accessibility_id.append(ids)
//            }
//
//            //accessibility_id.append(ids)
//            //        let ids = dic.value(forKey: "facility_id") as? String;
//            if accessibilityidstore.contains(ids)
//            {
//                self.accessibilityidstore.remove(ids)
//                //print(accessibility_id)
//
//            }
//            else
//            {
//                self.accessibilityidstore.add(ids)
//
//                //print(accessibility_id)
//            }
//            let indexPath = IndexPath(row: indexPath.item, section: 0)
//            self.accessibilitycollection.reloadItems(at: [indexPath])
//        }
//        else
//        {
//            let ids = (self.categoryarr.object(at: indexPath.item) as AnyObject).value(forKey: "category_id") as! String
//
//            print(ids)
//
//            offerer_id.removeAll()
//            offerer_id.append(ids)
//
//
//            //        let ids = dic.value(forKey: "facility_id") as? String;
//            if categoryidstore.contains(ids)
//            {
//
//                self.categoryidstore.removeAllObjects()
//                self.categoryidstore.remove(ids)
//
//
//            }
//            else
//            {
//                self.categoryidstore.removeAllObjects()
//                self.categoryidstore.add(ids)
//
//
//            }
//            //            let indexPath = IndexPath(row: indexPath.item, section: 0)
//            //            self.categorycollection.reloadItems(at: [indexPath])
//            self.categorycollection.reloadData()
        
            
            
            
        }
    

    

}
