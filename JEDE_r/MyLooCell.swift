//
//  MyLooCell.swift
//  JEDE_r
//
//  Created by Exarcplus on 9/21/17.
//  Copyright © 2017 Exarcplus. All rights reserved.
//

import UIKit

class MyLooCell: UITableViewCell {
    
    
    @IBOutlet weak var looImage: UIImageView!
    
    @IBOutlet weak var looName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        looImage.layer.cornerRadius = 7.0
        looImage.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
